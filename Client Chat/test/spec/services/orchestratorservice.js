'use strict';

describe('Service: orchestratorService', function () {

  // load the service's module
  beforeEach(module('clientChatCorretorApp'));

  // instantiate service
  var orchestratorService;
  beforeEach(inject(function (_orchestratorService_) {
    orchestratorService = _orchestratorService_;
  }));

  it('should do something', function () {
    expect(!!orchestratorService).toBe(true);
  });

});
