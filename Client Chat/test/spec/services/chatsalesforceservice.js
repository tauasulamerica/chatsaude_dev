'use strict';

describe('Service: chatSalesforceService', function () {

  // load the service's module
  beforeEach(module('clientChatCorretorApp'));

  // instantiate service
  var chatSalesforceService;
  beforeEach(inject(function (_chatSalesforceService_) {
    chatSalesforceService = _chatSalesforceService_;
  }));

  it('should do something', function () {
    expect(!!chatSalesforceService).toBe(true);
  });

});
