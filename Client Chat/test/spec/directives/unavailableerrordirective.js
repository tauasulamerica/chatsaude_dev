'use strict';

describe('Directive: unavailableerrordirective', function () {

  // load the directive's module
  beforeEach(module('clientChatCorretorApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<unavailableerrordirective></unavailableerrordirective>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the unavailableerrordirective directive');
  }));
});
