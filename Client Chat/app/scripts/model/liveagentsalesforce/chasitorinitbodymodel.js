'use strict';

/**
 * @ngdoc service
 * @name clientChatCorretorApp.ChasitorInitBodyModel
 * @description
 * # ChasitorInitBodyModel
 * Factory in the clientChatCorretorApp.
 */
angular.module('clientChatCorretorApp')
    .factory('ChasitorInitBodyModel', function () {

        /**
   * Constructor, with class name
   */
        function ChasitorInitBodyModel() { }

        /**
         * Static method, assigned to class
         * Instance ('this') is not available in static context
         */
        ChasitorInitBodyModel.build = function (data) {

            var model = {
                "sessionId": data.id,
                "organizationId": data.organizationId,
                "deploymentId": data.deploymentId,
                "buttonId": data.buttonId,
                "userAgent": data.userAgent,
                "language": "pt-BR",
                "screenResolution": "1900x1080",
                "visitorName": data.visitorName,
                "prechatDetails": [
                    {
                        "label": "Nome",
                        "value": data.visitorName,
                        "entityMaps": [{
                            "entityName": "Contact",
                            "fieldName": "Name",
                            "isFastFillable": false,
                            "isAutoQueryable": false,
                            "isExactMatchable": false
                        }],
                        "transcriptFields": ["NOME__c"],
                        "displayToAgent": true
                    },
                    {
                        "label": "Area",
                        "value": data.area,
                        "entityMaps": [
                            {
                                "entityName": "Case",
                                "fieldName": "IDC_AREA__c",
                                "isFastFillable": false,
                                "isAutoQueryable": false,
                                "isExactMatchable": false
                            }
                        ],
                        "transcriptFields": ["IDC_AREA__c"],
                        "displayToAgent": true
                    },
                    {
                        "label": "CodigodoProdutor",
                        "value": data.codProdutor,
                        "entityMaps": [
                            {
                                "entityName": "LiveChatTranscript",
                                "fieldName": "CodigodoPlano",
                                "isFastFillable": false,
                                "isAutoQueryable": false,
                                "isExactMatchable": false
                            }
                        ],
                        "transcriptFields": ["Codigo_do_Produtor__c"],
                        "displayToAgent": true
                    },
                    {
                        "label": "CodigoNAC",
                        "value": data.codNac,
                        "entityMaps": [
                            {
                                "entityName": "LiveChatTranscript",
                                "fieldName": "CodigodoCliente",
                                "isFastFillable": false,
                                "isAutoQueryable": false,
                                "isExactMatchable": false
                            }
                        ],
                        "transcriptFields": ["Codigo_NAC__c"],
                        "displayToAgent": true
                    },
                    {
                        "label": "Origem",
                        "value": data.origin,
                        "entityMaps": [
                            {
                                "entityName": "Case",
                                "fieldName": "Origin",
                                "isFastFillable": false,
                                "isAutoQueryable": false,
                                "isExactMatchable": false
                            }
                        ],
                        "transcriptFields": ["Origin"],
                        "displayToAgent": true
                    },
                    {
                        "label": "NomeDoProdutor",
                        "value": data.razaoSocial,
                        "entityMaps": [
                            {
                                "entityName": "Contact",
                                "fieldName": "Name",
                                "isFastFillable": false,
                                "isAutoQueryable": false,
                                "isExactMatchable": false
                            }
                        ],
                        "transcriptFields": ["Name"],
                        "displayToAgent": true
                    },
                    {
                        "label": "Retencao Parcial",
                        "value": data.contadorRetencaoParcial,
                        "entityMaps": [
                            {
                                "entityName": "Case",
                                "fieldName": "Retencao_Parcial__c",
                                "isFastFillable": false,
                                "isAutoQueryable": false,
                                "isExactMatchable": false
                            }
                        ],
                        "transcriptFields": [""],
                        "displayToAgent": true
                    }
                    ,
                    {
                        "label": "Intencao",
                        "value": data.intent,
                        "entityMaps": [
                            {
                                "entityName": "Case",
                                "fieldName": "Intencao__c",
                                "isFastFillable": false,
                                "isAutoQueryable": false,
                                "isExactMatchable": false
                            }
                        ],
                        "transcriptFields": [""],
                        "displayToAgent": true
                    },
                    {
                        "label": "Action",
                        "value": data.action,
                        "entityMaps": [
                            {
                                "entityName": "Case",
                                "fieldName": "Action__c",
                                "isFastFillable": false,
                                "isAutoQueryable": false,
                                "isExactMatchable": false
                            }
                        ],
                        "transcriptFields": [""],
                        "displayToAgent": true
                    },
                    {
                        "label": "Retorno do Servico",
                        "value": data.retornoServico,
                        "entityMaps": [
                            {
                                "entityName": "Case",
                                "fieldName": "Retorno_do_Servico__c",
                                "isFastFillable": false,
                                "isAutoQueryable": false,
                                "isExactMatchable": false
                            }
                        ],
                        "transcriptFields": [""],
                        "displayToAgent": true
                    },
                    {
                        "label": "Contador Dialogo",
                        "value": data.contadorDialogo,
                        "entityMaps": [
                            {
                                "entityName": "Case",
                                "fieldName": "Contador_dialogo__c",
                                "isFastFillable": false,
                                "isAutoQueryable": false,
                                "isExactMatchable": false
                            }
                        ],
                        "transcriptFields": [""],
                        "displayToAgent": true
                    },
                    {
                        "label": "Telefone da Pessoa",
                        "value": data.telefone,
                        "entityMaps": [
                            {
                                "entityName": "Case",
                                "fieldName": "TELEFONE_PESSOA__c",
                                "isFastFillable": false,
                                "isAutoQueryable": false,
                                "isExactMatchable": false
                            }
                        ],
                        "transcriptFields": [""],
                        "displayToAgent": true
                    },
                    {
                        "label": "E-mail da Pessoa de Contato",
                        "value": data.email,
                        "entityMaps": [
                            {
                                "entityName": "Case",
                                "fieldName": "NME_PESSOA_CONTATO_EMAIL__c",
                                "isFastFillable": false,
                                "isAutoQueryable": false,
                                "isExactMatchable": false
                            }
                        ],
                        "transcriptFields": [""],
                        "displayToAgent": true
                    },
                    {
                        "label": "Id Session",
                        "value": data.idSessao,
                        "entityMaps": [
                            {
                                "entityName": "Case",
                                "fieldName": "Id_Session__c",
                                "isFastFillable": false,
                                "isAutoQueryable": false,
                                "isExactMatchable": false
                            }
                        ],
                        "transcriptFields": [""],
                        "displayToAgent": true
                    }
                ],
                "prechatEntities": [
                    {
                        "entityName": "Case",
                        "showOnCreate": true,
                        "saveToTranscript": "Case",
                        "entityFieldsMaps": [
                            {
                                "fieldName": "NOME_DA_PESSOA__c",
                                "label": "Nome",
                                "doFind": false,
                                "isExactMatch": false,
                                "doCreate": true
                            },
                            {
                                "fieldName": "Origin",
                                "label": "Origem",
                                "doFind": false,
                                "isExactMatch": true,
                                "doCreate": true
                            },
                            {
                                "fieldName": "Retencao_Parcial__c",
                                "label": "Retencao Parcial",
                                "doFind": false,
                                "isExactMatch": false,
                                "doCreate": true
                            },
                            {
                                "fieldName": "Intencao__c",
                                "label": "Intencao",
                                "doFind": false,
                                "isExactMatch": true,
                                "doCreate": true
                            },
                            {
                                "fieldName": "Action__c",
                                "label": "Action",
                                "doFind": false,
                                "isExactMatch": false,
                                "doCreate": true
                            },
                            {
                                "fieldName": "Retorno_do_Servico__c",
                                "label": "Retorno do Servico",
                                "doFind": false,
                                "isExactMatch": true,
                                "doCreate": true
                            },
                            {
                                "fieldName": "Contador_dialogo__c",
                                "label": "Contador Dialogo",
                                "doFind": false,
                                "isExactMatch": true,
                                "doCreate": true
                            },
                            {
                                "fieldName": "TELEFONE_PESSOA__c",
                                "label": "Telefone da Pessoa",
                                "doFind": false,
                                "isExactMatch": true,
                                "doCreate": true
                            },
                            {
                                "fieldName": "NME_PESSOA_CONTATO_EMAIL__c",
                                "label": "E-mail da Pessoa de Contato",
                                "doFind": false,
                                "isExactMatch": true,
                                "doCreate": true
                            },
                            {
                                "fieldName": "Id_Session__c",
                                "label": "Id Session",
                                "doFind": false,
                                "isExactMatch": true,
                                "doCreate": true
                            }
                        ]
                    }
                ],
                "receiveQueueUpdates": true,
                "isPost": true
            };

            return model;
        };

        /**
         * Return the constructor function
         */
        return ChasitorInitBodyModel;
    });
