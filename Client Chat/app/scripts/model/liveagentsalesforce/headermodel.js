'use strict';

/**
 * @ngdoc service
 * @name clientChatCorretorApp.LiveAgentHeaderModel
 * @description
 * # LiveAgentHeaderModel
 * Factory in the clientChatCorretorApp.
 */
angular.module('clientChatCorretorApp')
  .factory('LiveAgentHeaderModel', function (ChatConfig) {

    /**
   * Constructor, with class name
   */
    function LiveAgentHeaderModel() {}

    /**
     * Static method, assigned to class
     * Instance ('this') is not available in static context
     */
    LiveAgentHeaderModel.build = function (data) {

      //if undefined, is necessary set to null
      if(!data.affinity) {
        data.affinity = null;
      }

      var model = {
        "X-LIVEAGENT-API-VERSION": data.apiVersion,
        "X-LIVEAGENT-AFFINITY": data.affinity,
        "X-LIVEAGENT-SESSION-KEY": data.sessionKey,
        "X-LIVEAGENT-SEQUENCE": data.sequence,
        "Authorization": ChatConfig.sensediaAuthorizationToken(),
        "client_id": ChatConfig.sensediaClientId(),
        "access_token": data.accessToken,
        "x-gateway-process-id": data.idSessao,
        "x-gateway-request-id": data.requestId
      }

      return model;
    };

    /**
     * Return the constructor function
     */
    return LiveAgentHeaderModel;
  });
