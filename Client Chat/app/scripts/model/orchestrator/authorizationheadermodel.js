'use strict';

/**
 * @ngdoc service
 * @name clientChatCorretorApp.AuthorizationHeaderModel
 * @description
 * # AuthorizationHeaderModel
 * Factory in the clientChatCorretorApp.
 */
angular.module('clientChatCorretorApp')
  .factory('AuthorizationHeaderModel', function (
    ChatConfig,
    $location) {

    /**
   * Constructor, with class name
   */
    function AuthorizationHeaderModel() { }

    /**
   * Static method, assigned to class
   * Instance ('this') is not available in static context
   */
    AuthorizationHeaderModel.build = function (data) {
      var model = {
        "Authorization": ChatConfig.sensediaAuthorizationToken(),
        "client_id": ChatConfig.sensediaClientId()
      }

      if (data) {
        model = {
          "Authorization": "",
          "client_id": ChatConfig.sensediaClientId(),
          "access_token": data.accessToken,
          "x-gateway-process-id": data.idSessao,
          "x-gateway-request-id": data.requestId
        };

        if(data.chatIdle) {
          model.chatIdle = data.chatIdle
        }

        if (typeof (data.Authorization) !== "undefined" &&
          typeof ($location.search().env) !== "undefined" &&
          $location.search().env === "local") {
          model.Authorization = data.Authorization;
        }
      }

      return model;
    };

    /**
     * Return the constructor function
     */
    return AuthorizationHeaderModel;
  });
