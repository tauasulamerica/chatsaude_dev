'use strict';

/**
 * @ngdoc service
 * @name clientChatCorretorApp.MessageModel
 * @description
 * # MessageModel
 * Factory in the clientChatCorretorApp.
 */
angular.module('clientChatCorretorApp')
  .factory('WatsonSendMsgBodyModel', function () {

    /**
   * Constructor, with class name
   */
    function WatsonSendMsgBodyModel() {}

    /**
   * Static method, assigned to class
   * Instance ('this') is not available in static context
   */
    WatsonSendMsgBodyModel.build = function (data) {
      if(data.text) {
        // Watson responde com erro quando a mensagem possui algum destes caracteres.
        data.text = data.text.replace(/[\x00-\x19]+/g, '');
      }

      var model = {
        "context": data.context,
        "idSessao": data.watsonSessionId,
        "texto": data.text,
        "requestId": data.requestId,
        "respostaAnterior": data.respostaAnterior
      }

      return model;
    };

    /**
     * Return the constructor function
     */
    return WatsonSendMsgBodyModel;
  });
