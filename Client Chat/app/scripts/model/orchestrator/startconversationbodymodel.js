'use strict';

/**
 * @ngdoc service
 * @name clientChatCorretorApp.StartConversationBodyModel
 * @description
 * # StartConversationBodyModel
 * Factory in the clientChatCorretorApp.
 */
angular.module('clientChatCorretorApp')
  .factory('WatsonStartConversationBodyModel', function () {

    /**
   * Constructor, with class name
   */
    function WatsonStartConversationBodyModel() {}

    /**
     * Static method, assigned to class
     * Instance ('this') is not available in static context
     */
    WatsonStartConversationBodyModel.build = function (data) {

      var model = {
        "params": data.params,
        "nome": data.nome,
        "email": data.email,
        "telefone": data.telefone,
        "carteirinha": data.carteirinha,
        "tipoproduto": data.tipoproduto, 
        "codempresa": data.codempresa,
        "requestId": data.requestId
      }

      return model;
    };

    /**
     * Return the constructor function
     */
    return WatsonStartConversationBodyModel;
  });
