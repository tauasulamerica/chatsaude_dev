'use strict';

/**
 * @ngdoc service
 * @name clientChatCorretorApp.MessageModel
 * @description
 * # MessageModel
 * Factory in the clientChatCorretorApp.
 */
angular.module('clientChatCorretorApp')
  .factory('MessageModel', function () {

    /**
   * Constructor, with class name
   */
    function MessageModel(type, output, dateHour, atendente, corretor, requestId) {
      this.type = type;
      this.output = output;
      this.dateHour = dateHour;
      this.atendente = atendente;
      this.corretor = corretor;
      this.requestId = requestId;
    }

    /**
   * Static method, assigned to class
   * Instance ('this') is not available in static context
   */
    MessageModel.build = function (data) {
      return new MessageModel(
        data.type,
        data.output,
        data.dateHour,
        data.atendente,
        data.corretor,
        data.requestId
      );
    };

    /**
     * Return the constructor function
     */
    return MessageModel;
  });
