'use strict';

/**
 * @ngdoc overview
 * @name clientChatCorretorApp
 * @description
 * # clientChatCorretorApp
 *
 * Main module of the application.
 */
angular
  .module('clientChatCorretorApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'luegg.directives',
    'angularFileUpload',
    'ngDialog',
    'angularUUID2',
    'ngMask'
    ])
  .config(function ($routeProvider, $httpProvider) {

    $httpProvider.defaults.timeout = 6000;

    $routeProvider
      .when('/chat', {
        templateUrl: 'views/chat.html',
        controller: 'ChatCtrl',
        controllerAs: 'chat'
      })
      .otherwise({
        redirectTo: '/chat'
      });
  });
