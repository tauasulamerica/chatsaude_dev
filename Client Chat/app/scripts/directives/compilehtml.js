'use strict';

/**
 * @ngdoc directive
 * @name clientChatCorretorApp.directive:compileHtml
 * @description
 * # compileHtml
 */
angular.module('clientChatCorretorApp')
  .directive('compileHtml', function ($compile) {
  return function (scope, element, attrs) {
    scope.$watch(
      function(scope) {
        return scope.$eval(attrs.compileHtml);
      },
      function(value) {
        element.html(value);
        $compile(element.contents())(scope);
      }
    );
  };
});
