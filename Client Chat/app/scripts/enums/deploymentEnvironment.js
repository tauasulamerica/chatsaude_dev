'use strict';

angular.module('clientChatCorretorApp')
    //Arquivo de configurações padrões do Projeto
    .factory('DeploymentEnvironment', function () {

        var deploymentEnvironment = {
            "development": 0,
            "stage": 1,
            "production": 2,
            "local": 3
        };

        return {
            deploymentEnvironment: function () {
                return deploymentEnvironment;
            }
        };
    });
