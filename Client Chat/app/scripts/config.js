'use strict';

angular.module('clientChatCorretorApp')

    //Arquivo de configurações padrões do Projeto
    .factory('ChatConfig', ['$location', 'DeploymentEnvironment', function ($location, DeploymentEnvironment) {

        var liveAgentBaseUrl,
            orchestratorBaseUrl,
            orchestratorBaseAppEngineUrl,
            sensediaBaseURL,
            sensediaGenericBaseURL,
            organizationId,
            deploymentId,
            buttonId,
            environment,
            watsonAuthorization = "",
            sensediaAuthorization = "Basic MGQ3MWE2NjItZTU0NC0zZjc1LWI1MDgtMzkzMzAyMjEzODk5OmU1ZTgyNGM3LTkwYzUtMzg1Ny1iMDg5LTQwM2MyNzllOGM1Ng==",
            clientId = "0d71a662-e544-3f75-b508-393302213899",
            tokenLocal = "5d1e56af0c5f48d9970ebd062a86f94c",
            timeoutMessageLiveAgent = 42000,
            oficinaSensediaBaseURL;

        var params = $location.search();

        if (typeof (params.env) !== "undefined") {
            if (params.env === "development") {
                environment = DeploymentEnvironment.deploymentEnvironment().development;
            } else if (params.env === "stage") {
                environment = DeploymentEnvironment.deploymentEnvironment().stage;
            } else if (params.env === "production") {
                environment = DeploymentEnvironment.deploymentEnvironment().production;
            } else if (params.env === "local") {
                environment = DeploymentEnvironment.deploymentEnvironment().local;
            }
        } else {
            environment = DeploymentEnvironment.deploymentEnvironment().production;
        }

        if (environment === DeploymentEnvironment.deploymentEnvironment().development) {
            liveAgentBaseUrl = 'https://apisulamerica.sensedia.com/dev/saude/segurado/chat/bot/salesforce/api/v1';
            orchestratorBaseUrl = "https://apisulamerica.sensedia.com/dev/saude/segurado/chat/bot/api/v1";
            orchestratorBaseAppEngineUrl = "http://localhost:8080/_ah/api/chatcorretor/v1";
            sensediaBaseURL = "https://apisulamerica.sensedia.com/dev/saude/segurado/chat/bot/api/v1";
            sensediaGenericBaseURL = "http://apisulamerica.sensedia.com";
            organizationId = "00D0x0000008oWa";
            deploymentId = "57231000000NXL9";
            buttonId = "57331000000NZyh";
            oficinaSensediaBaseURL = "https://apisulamerica.sensedia.com/dev/saude/segurado/chat/bot/watson/api/v1";
        } else if (environment === DeploymentEnvironment.deploymentEnvironment().stage) {
            liveAgentBaseUrl = 'https://apisulamerica.sensedia.com/dev/saude/segurado/chat/bot/salesforce/api/v1';
            orchestratorBaseUrl = "https://apisulamerica.sensedia.com/dev/saude/segurado/chat/bot/api/v1";
            orchestratorBaseAppEngineUrl = "https://chatsaudesegurado-h.appspot.com/_ah/api/chatcorretor/v1";
            sensediaBaseURL = "https://apisulamerica.sensedia.com/dev/saude/segurado/chat/bot/api/v1";
            sensediaGenericBaseURL = "http://apisulamerica.sensedia.com";
            organizationId = "00D0x0000008oWa";
            deploymentId = "57231000000NXL9";
            buttonId = "57331000000NZyh";
            oficinaSensediaBaseURL = "https://apisulamerica.sensedia.com/dev/saude/segurado/chat/bot/watson/api/v1";
        } else if (environment === DeploymentEnvironment.deploymentEnvironment().production) {
            liveAgentBaseUrl = 'https://apisulamerica.sensedia.com/dev/saude/segurado/chat/bot/salesforce/api/v1';
            orchestratorBaseUrl = "https://apisulamerica.sensedia.com/dev/saude/segurado/chat/bot/api/v1";
            orchestratorBaseAppEngineUrl = "https://chatsaudesegurado-h.appspot.com/_ah/api/chatcorretor/v1";
            sensediaBaseURL = "https://apisulamerica.sensedia.com/dev/saude/segurado/chat/bot/api/v1";
            sensediaGenericBaseURL = "http://apisulamerica.sensedia.com";
            organizationId = "00D0x0000008oWa";
            deploymentId = "57231000000NXL9";
            buttonId = "57331000000NZyh";
            oficinaSensediaBaseURL = "https://apisulamerica.sensedia.com/dev/saude/segurado/chat/bot/watson/api/v1";
        } else if (environment === DeploymentEnvironment.deploymentEnvironment().local) {
            liveAgentBaseUrl = 'https://apisulamerica.sensedia.com/dev/saude/segurado/chat/bot/salesforce/api/v1';
            orchestratorBaseUrl = "http://localhost:8080/_ah/api/chatcorretor/v1";
            orchestratorBaseAppEngineUrl = "https://chatsaudesegurado-h.appspot.com/_ah/api/chatcorretor/v1";
            sensediaBaseURL = "https://apisulamerica.sensedia.com/dev/saude/segurado/chat/bot/api/v1";
            sensediaGenericBaseURL = "http://apisulamerica.sensedia.com";
            organizationId = "00D0x0000008oWa";
            deploymentId = "57231000000NXL9";
            buttonId = "57331000000NZyh";
            oficinaSensediaBaseURL = "https://apisulamerica.sensedia.com/dev/saude/segurado/chat/bot/watson/api/v1";
        }

        return {
            watsonAuthorizationToken: function () {
                return watsonAuthorization;
            },
            sensediaAuthorizationToken: function () {
                return sensediaAuthorization;
            },
            sensediaClientId: function () {
                return clientId;
            },
            sensediaLiveAgentAcessTokenUrl: function () {
                return liveAgentBaseUrl + "/oauth/access-token";
            },
            liveAgentOrganizationId: function () {
                return organizationId;
            },
            liveAgentDeploymentId: function () {
                return deploymentId;
            },
            liveAgentButtonId: function () {
                return buttonId;
            },
            liveAgentSessionUrl: function () {
                return liveAgentBaseUrl + "/sessao";
            },
            liveAgentChasitorInitUrl: function () {
                return liveAgentBaseUrl + "/conversas"
            },
            liveAgentGetMessagesUrl: function () {
                return orchestratorBaseAppEngineUrl + "/mensagens";
            },
            liveAgentSendMessageUrl: function () {
                return liveAgentBaseUrl + "/conversas";
            },
            liveAgentChatEndUrl: function () {
                return liveAgentBaseUrl + "/conversas";
            },
            liveAgentAvailabilityUrl: function (parameters) {
                return liveAgentBaseUrl + "/Visitor/Availability?Availability.prefix=Visitor&Availability.ids=" + parameters.btnId + "&deployment_id=" + parameters.deploymentId + "&org_id=" + parameters.orgId;
            },
            liveAgentDeleteSessionUrl: function (sessionKey) {
                return liveAgentBaseUrl + "/sessao/" + sessionKey;
            },
            sensediaOrchestratorAcessTokenUrl: function () {
                return sensediaBaseURL + "/oauth/access-token";
            },
            sensediaOrchestratorGetBoleto: function () {
                return sensediaGenericBaseURL + "/pagamentos/api/v1/boletos/auto/segundaVia";
            },
            orchestratorGetChatLog: function (parameters) {
                return orchestratorBaseUrl + "/conversas/" + parameters["sessionId"];
            },
            orchestratorStartConversation: function () {
                return orchestratorBaseUrl + "/conversas";
            },
            orchestratorConversation: function () {
                return orchestratorBaseUrl + "/conversas";
            },
            orchestratorEndChat: function (parameters) {
                return orchestratorBaseUrl + "/conversas/" + parameters.headers["x-gateway-process-id"];
            },
            tokenLocal: function() {
                return tokenLocal;
            },
            parametros: function () {
                return orchestratorBaseUrl + "/parametros";
            },
            salesForceUploadArquivosURL: function() {
                return liveAgentBaseUrl + "/manifestacoes/arquivos";
            },
            timeoutMessageLiveAgent: function() {
                return timeoutMessageLiveAgent;
            },
            liveAgentResyncSessionUrl: function () {
                return liveAgentBaseUrl + "/sessao/resync";
            },
            oficinaLogConversation: function () {
                return oficinaSensediaBaseURL + "/conversation/historicos";
            }
        };
    }]);
