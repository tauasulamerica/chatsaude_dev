/**
*
*  Base64 encode / decode
*  http://www.webtoolkit.info/
*
**/


var QueryStringUtil = {

    montarUrlQueryString: function (baseUrl, parameters) {
        var url = baseUrl;
        var cont = 0;
        for (var key in parameters) {
            if(cont==0){
                url+="?";
            } else {
                url+="&";
            }
            url+=key+"="+parameters[key];
            cont++;
        }
        return url;
    }
}