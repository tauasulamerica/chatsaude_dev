window.ValidacaoUtil = (function (ValidacaoUtil) {

    var verificaCpfCnpj = function(valor) {
        valor = valor.toString();
        valor = valor.replace(/[^0-9]/g, '');

        if (valor.length === 11) {
            return 'CPF';
        } else if (valor.length === 14) {
            return 'CNPJ';
        } else {
            return false;
        }
    };

    var calcDigitosPosicoes = function(digitos, posicoes, soma_digitos) {
        digitos = digitos.toString();

        for (var i = 0; i < digitos.length; i++) {
            soma_digitos = soma_digitos + (digitos[i] * posicoes);
            posicoes--;
            if (posicoes < 2) {
                posicoes = 9;
            }
        }

        soma_digitos = soma_digitos % 11;

        if (soma_digitos < 2) {
            soma_digitos = 0;
        } else {
            soma_digitos = 11 - soma_digitos;
        }

        var cpf = digitos + soma_digitos;

        return cpf;
    };

    var validaCpf = function(valor) {
        valor = valor.toString();
        valor = valor.replace(/[^0-9]/g, '');

        var digitos = valor.substr(0, 9);
        var novo_cpf = calcDigitosPosicoes(digitos, 10, 0);
        var novo_cpf = calcDigitosPosicoes(novo_cpf, 11, 0);

        if (novo_cpf === valor) {
            return true;
        } else {
            return false;
        }
    };

    var validaCnpj = function(valor) {
        valor = valor.toString();
        valor = valor.replace(/[^0-9]/g, '');

        var cnpj_original = valor;
        var primeiros_numeros_cnpj = valor.substr(0, 12);
        var primeiro_calculo = calcDigitosPosicoes(primeiros_numeros_cnpj, 5, 0);
        var segundo_calculo = calcDigitosPosicoes(primeiro_calculo, 6, 0);
        var cnpj = segundo_calculo;

        if (cnpj === cnpj_original) {
            return true;
        }

        return false;
    };

    var validaCpfCnpj = function(valor) {
        var valida = verificaCpfCnpj(valor);
        valor = valor.toString();
        valor = valor.replace(/[^0-9]/g, '');

        if (valida === 'CPF') {
            return validaCpf(valor);
        } else if (valida === 'CNPJ') {
            return validaCnpj(valor);
        } else {
            return false;
        }
    };

    var validaData = function (data) {
        var regexData = /^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/;

        if (!regexData.test(data)) {
            return false;
        }

        var dia = data.substring(0, 2)
        var mes = data.substring(3, 5)
        var ano = data.substring(6, 10)

        if (dia > 0 && dia <= 31 && mes > 0 && mes <= 12) {
            if ((mes == 4 || mes == 6 || mes == 9 || mes == 11) && dia > 30) {
                return false;
            }
            if (mes == 2) {
                if (ano % 4 == 0 && (ano % 100 != 0 || ano % 400 == 0)) {
                    if (dia > 29)
                        return false;
                } else if (dia > 28) {
                    return false;
                }
            }
        } else {
            return false;
        }

        return true;
    }

    var htmlToText = function (source) {

        var result;

        result = source.replace('\r', ' ');
        result = result.replace('\n', ' ');
        result = result.replace('\t', '');
        result = result.replace('( )+', ' ');

        result = result.replace('<( )*head([^>])*>','<head>');
        result = result.replace('(<( )*(/)( )*head( )*>)','</head>');
        result = result.replace('(<head>).*(</head>)','');

        result = result.replace('<( )*script([^>])*>','<script>');
        result = result.replace('(<( )*(/)( )*script( )*>)','</script>');
        result = result.replace('(<script>).*(</script>)','');

        result = result.replace('<( )*style([^>])*>','<style>');
        result = result.replace('(<( )*(/)( )*style( )*>)','</style>');
        result = result.replace('(<style>).*(</style>)','');

        result = result.replace('<( )*td([^>])*>','\t');

        result = result.replace('<( )*br( )*>','\r');
        result = result.replace('<( )*li( )*>','\r');

        result = result.replace('<( )*div([^>])*>','\r');
        result = result.replace('<( )*tr([^>])*>','\r');
        result = result.replace('<( )*p([^>])*>','\r\r');

        result = result.replace('<[^>]*>',' ');

        result = result.replace('&bull;',' * ');
        result = result.replace('&lsaquo;','<');
        result = result.replace('&rsaquo;','>');
        result = result.replace('&trade;','(tm)');
        result = result.replace('&frasl;','/');
        result = result.replace('&lt;','<');
        result = result.replace('&gt;','>');
        result = result.replace('&copy;','(c)');
        result = result.replace('&reg;','(r)');

        result = result.replace('&(.{2,6});', '');

        result = result.replace('\n', '\r');

        result = result.replace('(\r)( )+(\r)','\r\r');
        result = result.replace('(\t)( )+(\t)','\t\t');
        result = result.replace('(\t)( )+(\r)','\t\r');
        result = result.replace('(\r)( )+(\t)','\r\t');
        result = result.replace('(\r)(\t)+(\r)','\r\r');
        result = result.replace('(\r)(\t)+','\r\t');
        var breaks = '\r\r\r';
        var tabs = '\t\t\t\t\t';

        for (var index=0; index<result.length; index++)
        {
            result = result.replace(breaks, '\r\r');
            result = result.replace(tabs, '\t\t\t\t');
            breaks = breaks + '\r';
            tabs = tabs + '\t';
        }

        return result;
    }

    ValidacaoUtil.validaCpfCnpj = validaCpfCnpj;
    ValidacaoUtil.verificaCpfCnpj = verificaCpfCnpj;
    ValidacaoUtil.validaData = validaData;
    ValidacaoUtil.htmlToText = htmlToText;

    return ValidacaoUtil;
}({}));