'use strict';

angular.module('clientChatCorretorApp')
    //Arquivo de configurações padrões do Projeto
    .factory('Debug', function () {
        return {
            print: function (flagDebug, functionName, object) {
                if(flagDebug) {
                    console.log(functionName);
                    console.log(object);
                }
            }
        };
    });
