'use strict';

/**
 * @ngdoc function
 * @name clientChatCorretorApp.controller:ChatCtrl
 * @description
 * # ChatCtrl
 * Controller of the clientChatCorretorApp
 */
angular.module('clientChatCorretorApp')
  .controller('ChatCtrl', function (
    $scope,
    $location,
    $interval,
    $window,
    LiveAgentService,
    OrchestratorService,
    AuthorizationHeaderModel,
    MessageModel,
    LiveAgentHeaderModel,
    ChasitorInitBodyModel,
    WatsonStartConversationBodyModel,
    WatsonSendMsgBodyModel,
    Debug,
    ChatConfig,
    FileUploader,
    ngDialog,
    uuid2,
    PdfService,
    OficinaService) {

    var apiVersion = 37,
      sequence = 1,
      sessionKey,
      affinityToken,
      accessToken,
      context,
      enterKeyCode = 13,
      userName,
      debug = false,
      liveAgentName,
      messagesInterval = 7000,
      messageTimeout,
      codNac,
      codProdutor,
      razaoSocial,
      watsonSessionId,
      watsonTranscript,
      watsonStartConversationBody = {},
      watsonAgentName = "Atendimento Segurado Saúde",
      watsonAcessToken,
      watsonUltimoOutput,
      sendTranscriptRetry = 0,
      idleTimeOut,
      presenceConfirmInterval,
      idleLimit = 180000,
      idleTimeoutResetCount = 0,
      endChatRetry = 0,
      endChatMaxRetry = 3,
      getMessagesRetry = 0,
      boletoLocked = false,
      oficinaTypeWatson = 1,
      oficinaTypeCorretor = 2,
      oficinaTypeAtendente = 3,
      mensagemErroIndisponibilidadeOrquestrador = "Infelizmente nosso sistema está indisponível neste momento. Aguarde estou pedindo ajuda a um colega que irá atendê-lo.",
      mensagemErroHistoricoNaoEncontrado = "ERRO: boleto do corretor encontrado, mas não foi possível baixar e recuperar histórico da conversa.",
      mensagemNotificacaoTitulo = "Notificação de nova mensagem",
      mensagemNotificacaoCorpo = "Você possui uma nova mensagem, por favor verificar",
      mensagemNotificacaoImagem = "../images/logo_sulamerica.png";

    $scope.message = { text: "" };
    $scope.name = "";
    $scope.formattedAgentName = "Assistente";
    $scope.conversation = [];
    $scope.glued = true;
    $scope.error = { unavailableError: false, errorMessage: "No momento nenhum agente está disponível para atendê-lo no chat." };
    $scope.readOnly = true;
    $scope.agentTyping = false;
    $scope.chatEnd = { wasEnded: false, formattedMsg: ", saiu da conversa." };
    $scope.forwardedToLiveAgent = { wasForwarded: false, formattedMsg: "", showMessage: false };
    $scope.waitingLoader = true;
    $scope.showInputName = false;
    $scope.workingHours = { };
    $scope.exibeFileUpload = false;
    $scope.enableUploadButton = false;
    $scope.requestId = "";
    $scope.telefoneAtendimento = "";
    $scope.horarioAtendimentoTelefone = "";
    $scope.presencePopupVisible = false;
    $scope.exibirBotoesSimOuNao = false;
    $scope.mascaraCpfCnpj = "999.999.999-99";

    // Não deve ser considerado actions de finalização e redirecionamento na abertura de caso (FIM_ATENDIMENTO_WATSON, REDIRECIONAR_ATENDENTE)
    $scope.invalidActions = [8000, 9000];

    var uploader = $scope.uploader = new FileUploader();
    $scope.uploaderOptions = { url: "", headers: { access_token: "", client_id: "", organizationId: "", sessionId: "", fileToken: "", encoding: "UTF-8", baseURL: "" }, autoUpload: false, removeAfterUpload: true };

    if (typeof ($location.search().debug) !== 'undefined' && $location.search().debug != null) {
      debug = $location.search().debug;
    }

    /**
     * onPageExit
     * call live agent chatEnd endpoint before closing the page
     *
     *
     */
    window.onbeforeunload = function (event) {
      event.preventDefault();

      if ($scope.forwardedToLiveAgent.wasForwarded) {
        endLiveAgentChat();
        deleteLiveAgentSession();
      }
    };

    /**
     * onKeyUp
     * trigger event when some key is press
     * @param {Object} $event
     * @param {Object} message
     */
    $scope.onKeyUp = function ($event, message) {
      if ($event.keyCode === enterKeyCode) {
        //if popup exists, enter will remove the element
        if (angular.element(document.querySelector('#pop')).length > 0 && message) {
          $scope.closePopUp(message);

        } else if (message.text) {
          $scope.sendInputedText(message);
        }
      }
    };

    /**
     * closePopUp
     * close pop up and start conversation with Watson
     * @param {String} name
     */
    $scope.closePopUp = function (name) {
      Debug.print(debug, "closePopUp, Name: ", name);

      if ($scope.formInfo.$valid) {

        userName = $scope.chat.nome;
        
        if(window && window.localStorage){
          if($scope.chat.nome){
            window.localStorage.setItem("nome", $scope.chat.nome);
          }

          if($scope.chat.email){
            window.localStorage.setItem("email", $scope.chat.email);
          }

          if($scope.chat.telefone){
            window.localStorage.setItem("telefone", $scope.chat.telefone);
          }

          if($scope.chat.carteirinha){
            window.localStorage.setItem("carteirinha", $scope.chat.carteirinha);
          }

          if($scope.chat.tipoproduto){
            window.localStorage.setItem("tipoproduto", $scope.chat.tipoproduto);
          }

          if($scope.chat.codempresa){
            window.localStorage.setItem("codempresa", $scope.chat.codempresa);
          }          


        }

        $scope.showInputName = false;
        angular.element(document.querySelector('#mascara')).remove();
        angular.element(document.querySelector('#pop')).remove();

        setWatsonConversationStartParams();
      }
    };

    var regexValidacaoFormulario = {
      apolice: /^([0-9]{6,8})$/,
      endosso: /^([0-9]{6,8})$/,
      proposta: /^4([0-9]{8})$/,
      pedidoEndosso: /^6([0-9]{6,8})$/,
    };

    var validarRegex = function(nomeCampo){
      var regex = regexValidacaoFormulario[nomeCampo],
          campo = $scope.formulario.campos[nomeCampo];

      if(!campo[campo.model]){
        return false;
      } else {
        return regex.test(campo[campo.model]);
      }
    };

    var validarData = function(nomeCampo){
      var campo = $scope.formulario.campos[nomeCampo];

      if(!campo[campo.model]){
        return false;
      } else {
        return ValidacaoUtil.validaData(campo[campo.model]);
      }
    };
    
    var validarCpfCnpj = function(nomeCampo){
      var campo = $scope.formulario.campos[nomeCampo];

      if(!campo[campo.model]){
        return false;
      } else {
        var cpfCnpj = $scope.formulario.campos.cpfCnpj; 
        //cpfCnpj.visivel = ValidacaoUtil.verificaCpfCnpj(campo.valor) === 'CPF';
        
        $scope.mascaraCpfCnpj = campo[campo.model].replace( /\D/g, '').length <= 11 ? "999.999.999-999" : "99.999.999/9999-99";

        // SE FOR CNPJ E TIVER DATA DE NASCIMENTO, OCULTA O CAMPO DATA DE NASCIMENTO
        var indexCampo = 0;
        var camposNecessarios = $scope.formulario.camposNecessarios;
        for(indexCampo in camposNecessarios){     
          var campoAtual = camposNecessarios[indexCampo];
          if (campoAtual.campoFormulario == "dataNascimento") {
            var temDataNascimento = true;
          }
        }

        if (temDataNascimento) {
          $scope.formulario.campos.dataNascimento.visivel = (campo[campo.model].length > 14 ? false : true);
        }

        return ValidacaoUtil.validaCpfCnpj(campo[campo.model]);
      }
    };

    var validarSucursal = function(nomeCampo){
      var campo = $scope.formulario.campos[nomeCampo];

      if(!campo[campo.model]){
        return false;
      } else {
        return campo[campo.model] > 0;
      }
    };

    var esconderCamposFormulario = function(){
      var nomeCampo = "";
      for(nomeCampo in $scope.formulario.campos){
        var campo = $scope.formulario.campos[nomeCampo];
        campo.visivel = false;
        campo[campo.model] = null;
      }

      $scope.formulario.visivel = false;
    }

    var definirCamposVisiveis = function(){
      esconderCamposFormulario();

      var indexCampo = 0, 
      campos = $scope.formulario.camposNecessarios;

      for(indexCampo in campos){
        var campo = campos[indexCampo];

        if(!campo.campoFormulario){
          continue;
        }

        $scope.formulario.campos[campo.campoFormulario].visivel = true;

        if(context){
          var valor = context[campo.variavelOrigem],
          campo = $scope.formulario.campos[campo.campoFormulario];

          if(campo.inputType === "number" && valor != "" && valor != null){
            campo[campo.model] = parseInt(valor.replace( /\D/g, ''));
          } else {
            campo[campo.model] = valor;
          }
        }
      }

      $scope.validarFormulario();
      context.fg_formulario_exibido = "S";
      $scope.formulario.visivel = true;
      $scope.formulario.titulo = context["titulo_form"];
    }

    var atualizarVariaveisContexto = function(){
      if(!context){
        return; 

      }

      var indexCampo = 0, 
      campos = $scope.formulario.camposNecessarios;

      for(indexCampo in campos){
       var campo = campos[indexCampo];      
       var campoArray = $scope.formulario.campos[campo.campoFormulario];

        if(campo.campoFormulario){
          if (campo.campoFormulario === 'cpfCnpj')
          {
            var valorCnpj = campoArray[campoArray.model];
            context[campo.variavelOrigem] = valorCnpj.replace( /\D/g, '') + "";
          }
          else
          {
            context[campo.variavelOrigem] = campoArray[campoArray.model] + "";
          }
        }
      }
    }

    $scope.formulario = {
      campos: {
        apolice: { model: "apolice", visivel: true, valido: false, nomeExibicao: "Apólice", placeholder: "Digite o número", inputType: "number", metodoValidar: function() { return validarRegex('apolice'); }, mask: " " },
        endosso: { model: "endosso", visivel: true, valido: false, nomeExibicao: "Endosso", placeholder: "Digite o número", inputType: "number", metodoValidar: function() { return validarRegex('endosso'); }, mask: " " },
        pedidoEndosso: { model: "pedidoEndosso", visivel: true, valido: false, nomeExibicao: "Pedido de Endosso", placeholder: "Digite o número", inputType: "number", metodoValidar: function() { return validarRegex('pedidoEndosso'); }, mask: " " },
        proposta: { model: "proposta", visivel: true, valido: false, nomeExibicao: "Proposta", placeholder: "Digite o número", inputType: "number", metodoValidar: function() { return validarRegex('proposta'); }, mask: " " },
        sucursal: { model: "sucursal", visivel: true, valido: false, nomeExibicao: "Filial", placeholder: "Selecione", inputType: "text", metodoValidar: function() { return validarSucursal('sucursal'); }, mask: " " },
        cpfCnpj: { model: "cpfCnpj", visivel: true, valido: false, nomeExibicao: "CPF ou CNPJ", placeholder: "Digite o número", inputType: "text", metodoValidar: function() { return validarCpfCnpj('cpfCnpj'); }, mask: "(campo.cnfCnpj.lenght > 11 ? 999.999.999-99 : 99.999.999/9999-99)" },
        dataNascimento: { model: "dataNascimento", visivel: true, valido: false, nomeExibicao: "Data de nascimento", placeholder: "DD/MM/AAAA", inputType: "text", metodoValidar: function() { return validarData('dataNascimento'); }, mask: "39/19/2099" },
        carteirinha: { model: "carteirinha", visivel: true, valido: false, nomeExibicao: "Número da Carteirinha", placeholder: "Produto + Código de Identificação", inputType: "text", metodoValidar: function() { return true; }, mask: "999 99999 9999 9999 9999" },
      },
    };

    $scope.confirmaFormulario = function(){
      if($scope.validarFormulario()){
        atualizarVariaveisContexto();

        $scope.sendInputedText({text: $scope.formulario.texto});
        $scope.formulario.visivel = false;
        $scope.exibirBotoesSimOuNao = false;
      }
    };

    $scope.cancelaFormulario = function(){
       $scope.formulario.visivel = false;
       $scope.exibirBotoesSimOuNao = false;
       
       watsonUltimoOutput = null;
       context.fg_formulario_exibido = null;
       context.exibirBotaoSimNao = false;
       $scope.exibirBotoesSimOuNao = false;
       $scope.sendInputedText({text: "Cancelar."});
    };       
    
    $scope.validarFormulario = function(){
      var valido = true, campoIndex = "", textoFormulario = "";

      for(campoIndex in $scope.formulario.campos){
        var campo = $scope.formulario.campos[campoIndex];

        if(campo.visivel){
          campo.valido = campo.metodoValidar();
          valido = valido && campo.valido;
          textoFormulario += campo.nomeExibicao + ": " + campo[campo.model] + "\n<br/>";
        }
      }
      
      $scope.formulario.texto = textoFormulario;

      return valido;
    };

    /**
     * setGridHeight
     * set the correct height for the conversation grid element
     */
    $scope.setGridHeight = function () {
      var headerHeight = angular.element(document.querySelector('#topo')).prop('offsetHeight'),
        footerHeight = angular.element(document.querySelector('#footer')).prop('offsetHeight'),
        windowHeight = window.innerHeight - headerHeight - footerHeight - 45;

      angular.element(document.querySelector("#grid")).css('height', windowHeight + 'px');
      
      return "{'height': '" + windowHeight + "px'}";
    };

    /**
     * sendInputedText
     * create message following the model
     * @param {Object} message
     */
    $scope.sendInputedText = function (message) {
      Debug.print(debug, "sendInputedText, Message: ", message);

      if (!$scope.readOnly && message.text) {
        var msgInstance = MessageModel.build({
          type: "corretor",
          output: { text: [message.text] },
          dateHour: new Date(),
          atendente: { nome: watsonAgentName },
          corretor: { nome: userName },
          requestId: uuid2.newuuid()
        });

        Debug.print(debug, "sendInputedText, Msg: ", msgInstance);

        $scope.conversation.push(msgInstance);
        message.text = "";

        sendMessage(msgInstance);
      }
    };

    $scope.enviarTexto = function(texto){
      $scope.exibirBotoesSimOuNao = false;
      $scope.sendInputedText({text: texto});
    };

    $scope.openBoletoPdf = function (urlBoleto, exibirPopupDebitoConta) {
      Debug.print(debug, "getBoletoPdf, url: ", urlBoleto);

      if(exibirPopupDebitoConta){
        $scope.exibirPopupBoletoDebitoConta = true;
        $scope.urlBoletoDebitoConta = urlBoleto;
        
        return;
      }

      if(!boletoLocked) {
        boletoLocked = true;

        getBoletoPdf(urlBoleto, function (response) {
          var blob = new Blob([response.data], { type: "application/pdf" });
          Debug.print(debug, "getBoletoPdf, blob: ", blob);
          var fileURL = URL.createObjectURL(blob);

          window.open(fileURL);
          boletoLocked = false;
        });
      }
    };

    $scope.respostaBoletoDebitoConta = function(resposta){

      $scope.exibirPopupBoletoDebitoConta = false;

      if(resposta){
        $scope.openBoletoPdf($scope.urlBoletoDebitoConta, false);
      }
    };

    /**
     * handleGeneralError
     * log the error message and status, also remove the 'waiting' element, show the error element
     * and set the input as readOnly
     * @param {Object} response
     */
    var handleGeneralError = function (response) {
      Debug.print(debug, "handleGeneralError", response);

      removeWaitingAgentElement();

      if (!$scope.chatEnd.wasEnded) {
        removeChatGridElement();

        if (!$scope.forwardedToLiveAgent.wasForwarded) {

          if (response && response.data && response.data.error) {
            $scope.error.errorMessage = response.data.error.message;
          }
        }

        $scope.error.unavailableError = true;
      }

      $scope.readOnly = true;
      $scope.exibeFileUpload = false;
      $scope.enableUploadButton = false;
      $scope.exibirBotoesSimOuNao = false;
    };

    /**
     * removeWaitingAgentElement
     * remove the 'waiting' element
     */
    var removeWaitingAgentElement = function () {
      // angular.element(document.querySelector('.waiting-agent')).remove();
      $scope.waitingLoader = false;
    };

    /**
     * removeChatGridElement
     * remove the conversation grid element
     */
    var removeChatGridElement = function () {
      angular.element(document.querySelector('.conversation-grid')).remove();
    };

    /**
     * setWatsonConversationStartParams
     * verify the initial required params and start Watson conversation
     * @param {String} name
     */
    var setWatsonConversationStartParams = function () {
      var queryString = $location.search();

      Debug.print(debug, "setWatsonConversationStartParams: Location:", queryString);

      //if ($scope.chat && $scope.chat.nome && queryString && queryString.params) {
        if (1==1) {
        var params = queryString.params;

        watsonStartConversationBody = WatsonStartConversationBodyModel.build({
          "params": params,
          "nome": $scope.chat.nome,
          "email": $scope.chat.email,
          "telefone": $scope.chat.telefone,
          "carteirinha": $scope.chat.carteirinha,
          "tipoproduto": $scope.chat.tipoproduto,
          "codempresa": $scope.chat.codempresa,
          "requestId": uuid2.newuuid()
        });

        Debug.print(debug, "setWatsonConversationStartParams: watsonStartConversationBody: ", watsonStartConversationBody);

        getSensediaAuthorizationOrchestrator(startWatsonConversation);

      } else {
        var error = {
          data: {
            error: {
              message: "Parâmetros para inicialização do chat inválidos."
            }
          }
        };
        handleGeneralError(error);
      }
    };

    /**
     * sendMessage
     * send message to Watson or Live Agent Salesforce
     * @param {Object} message
     */
    var sendMessage = function (message) {
      Debug.print(debug, "sendMessage", message);

      if ($scope.forwardedToLiveAgent.wasForwarded) {

        var parameters = {
          headers: LiveAgentHeaderModel.build({
            "apiVersion": apiVersion,
            "affinity": affinityToken,
            "sessionKey": sessionKey,
            "accessToken": accessToken,
            "idSessao": watsonSessionId,
            "requestId": message.requestId
          }),

          body: {
            "text": message.output.text[0]
          }
        };

        Debug.print(debug, "sendMessage, Params: ", parameters);

        //call Live Agent Service
        LiveAgentService.sendMessage(successSendLiveAgentMessage, sendMessageError, parameters);

        //Send CORRETOR message to oficina
        sendOficinaLog(context.workspace_id, context.conversation_id, message.output.text[0], oficinaTypeCorretor, codNac, "[]", "[]");
      } else {

        var parameters = {
          headers: AuthorizationHeaderModel.build({
            "accessToken": watsonAcessToken,
            "Authorization": ChatConfig.tokenLocal(), //Usado somente quando em Debug local
            "idSessao": watsonSessionId,
            "requestId": message.requestId
          }),

          body: WatsonSendMsgBodyModel.build({
            "context": context,
            "watsonSessionId": watsonSessionId,
            "text": message.output.text[0],
            "requestId": message.requestId,
            "respostaAnterior": watsonUltimoOutput,
          })
        };

        $scope.readOnly = true;
        endIdleTimeout();

        Debug.print(debug, "sendMessage, Params:", parameters);

        //call Watson Orchestrator service
        OrchestratorService.sendWatsonMessage(successSendWatsonMsg, sendMessageError, parameters);

        //Send CORRETOR message to oficina
        sendOficinaLog(context.workspace_id, context.conversation_id, message.output.text[0], oficinaTypeCorretor, codNac, "[]", "[]");
      }
    }

    /**
     * successSendLiveAgentMessage
     * just log the returned message
     * @param {Object} response
     */
    var successSendLiveAgentMessage = function (response) {
      Debug.print(debug, "successSendLiveAgentMessage", response);
    };

    /**
     * errorSendOficinaLog
     * log the returned error from sendOficinaLog
     * @param {Object} response
     */
    var errorSendOficinaLog = function (response) {
      Debug.print(debug, "errorSendOficinaLog", response);
    };

    /**
     * sendMessageError
     * log the returned error from send message
     * @param {Object} response
     */
    var sendMessageError = function (response) {
      Debug.print(debug, "sendMessageError", response);

      sendOficinaLog(context.workspace_id, context.conversation_id, output.text, oficinaTypeWatson, codNac, "[]", "[]");

      if (!$scope.forwardedToLiveAgent.wasForwarded) {
        //failed to send to watson, forward to live agent
        getSensediaAuthorizationLiveAgent();
      } else {
        //failed to send to live agent
        handleGeneralError();
      }
    };

    /********************************** Live Agent Salesforce methods **********************************/

    /**
     * getSensediaAuthorizationLiveAgent
     * get live agent session data
     */
    var getSensediaAuthorizationLiveAgent = function () {
      //$scope.forwardedToLiveAgent.wasForwarded = true;

      if (!$scope.waitingLoader) {
        $scope.forwardedToLiveAgent.showMessage = true;
        $scope.forwardedToLiveAgent.formattedMsg = "Transferindo... ";
        $scope.exibirBotoesSimOuNao = false;
        
        if($scope.mensagemPreChat){
          $scope.forwardedToLiveAgent.formattedMsg += $scope.mensagemPreChat;
        }
      }

      var params = {
        headers: LiveAgentHeaderModel.build({})
      }

      Debug.print(debug, "getSensediaAuthorizationLiveAgent, Params: ", params);
      $scope.readOnly = true;

      LiveAgentService.getSensediaAcessToken(getChatSession, handleGeneralError, params);
    };

    /**
     * getChatSession
     * get live agent session data
     * @param {Object} response
     */
    var getChatSession = function (response) {
      Debug.print(debug, "getChatSession, Response: ", response);

      accessToken = response.data.access_token;

      var parameters = {
        headers: LiveAgentHeaderModel.build({
          "apiVersion": apiVersion,
          "accessToken": accessToken,
          "idSessao": watsonSessionId,
          "requestId": uuid2.newuuid()
        })
      };

      Debug.print(debug, "getChatSession, Params: ", parameters);

      LiveAgentService.getSession(initChatAfterSession, handleGeneralError, parameters);
    };

    /**
     * initChatAfterSession
     * stablish connection with live agent API passing some initial parameters
     * @param {Object} response
     */
    var initChatAfterSession = function (response) {
      Debug.print(debug, "initChatAfterSession, Response: ", response);

      sessionKey = response.data.key;
      affinityToken = response.data.affinityToken;

      var parameters = {
        headers: LiveAgentHeaderModel.build({
          "apiVersion": apiVersion,
          "affinity": affinityToken,
          "sessionKey": sessionKey,
          "sequence": sequence,
          "accessToken": accessToken,
          "idSessao": watsonSessionId,
          "requestId": uuid2.newuuid()
        }),

        body: ChasitorInitBodyModel.build({
          "id": response.data.id,
          "organizationId": LiveAgentService.getOrganizationId(),
          "deploymentId": LiveAgentService.getDeploymentId(),
          "buttonId": LiveAgentService.getButtonId(),
          "userAgent": navigator.userAgent,
          "visitorName": userName,
          "codProdutor": codProdutor,
          "codNac": codNac,
          "origin": "Chat",
          "area": "Auto Emissão Individual",
          "razaoSocial": razaoSocial,
          "contadorRetencaoParcial": $scope.contadorRetencaoParcial,
          "intent": $scope.intent,
          "action": $scope.action,
          "retornoServico": $scope.retornoServico,
          "contadorDialogo": $scope.contadorDialogo,
          "email": $scope.chat.email,
          "telefone": $scope.chat.telefone,
          "carteirinha": $scope.chat.carteirinha,
          "tipoproduto": $scope.chat.tipoproduto,
          "codempresa": $scope.chat.codempresa,
          "idSessao": watsonSessionId
        })
      };

      Debug.print(debug, "initChatAfterSession, Params: ", parameters);

      LiveAgentService.chasitorInit(chasitorInitSuccess, handleGeneralError, parameters);
    };

    /**
     * chasitorInitSuccess
     * after success connecting to live agent, start to pulling messages from the endpoint
     * @param {Object} response
     */
    var chasitorInitSuccess = function (response) {
      Debug.print(debug, "chasitorInitSuccess, Response: ", response);

      $scope.forwardedToLiveAgent.wasForwarded = true;

      sendMessageRequest();
    };

    var sendMessageRequest = function(){
      var parameters = getParametersLiveAgent();

      Debug.print(debug, "chasitorInitSuccess, Params: ", parameters);

      LiveAgentService.getMessages(getMessagesSuccess, getMessagesError, parameters);
    };

    var getParametersLiveAgent = function(){
      return {
        headers: LiveAgentHeaderModel.build({
          "apiVersion": apiVersion,
          "affinity": affinityToken,
          "sessionKey": sessionKey,
          "accessToken": accessToken,
          "idSessao": watsonSessionId,
          "requestId": uuid2.newuuid()
        })
      };
    };

    var getMessagesError = function (response) {
      Debug.print(debug, "getMessagesError, Response: ", response);

      // Devemos ignorar erros de conflito ou timeout e tratar erros 503 para fazer o resync da sessão
      if (response.status === 409 || response.xhrStatus === "abort") {
        sendMessageRequest();      
      } else if (response.status === 503) {
        LiveAgentService.resyncSession(resyncSessionSuccess, handleGeneralError, getParametersLiveAgent());
      } else if (getMessagesRetry < 3) {
        getMessagesRetry++;
        sendMessageRequest();
      } else {
        handleGeneralError();
      }
    };

    var resyncSessionSuccess = function (response){
      if(response && response.data && response.data.isValid){
        affinityToken = response.data.affinityToken;
        sendMessageRequest();
      } else {
        handleGeneralError(response);
      }
    };

    /**
     * getMessagesSuccess
     * handle all messages/actions returned by the API
     * @param {Object} response
     */
    var getMessagesSuccess = function (response) {
      Debug.print(debug, "getMessagesSuccess, Reponse: ", response);
      response = response.data;

      if (response && response.messages && sequence != response.sequence) {
        // to avoid duplicated messages
        sequence = response.sequence;

        angular.forEach(response.messages, function (item, i) {
          if (item.type == 'ChatRequestFail') {
            handleGeneralError(response);
          } else if (item.type == 'ChatEnded') {
            $scope.chatEnd.wasEnded = true;
            $scope.chatEnd.formattedMsg = liveAgentName + $scope.chatEnd.formattedMsg;
            $scope.readOnly = true;
            $scope.exibeFileUpload = false;
            $scope.enableUploadButton = false;

            //Send log to Oficina
            sendOficinaLog(context.workspace_id, context.conversation_id, $scope.chatEnd.formattedMsg, oficinaTypeAtendente, codNac, "[]", "[]");
          } else if (item.type == 'ChatEstablished') {
            if (watsonTranscript) {
              sendLiveAgentFirstMessage();
            }

            //add analytics event
            ga('send', 'event', 'Chat Corretor', 'salesforce-atendeu', 'Salesforce - Atendimento realizado');

          } else if (item.type == 'QueueUpdate') {
            var queuePosition;

            if (typeof (item.message.position) !== "undefined") {
              queuePosition = item.message.position;

              if (queuePosition == 0) {
                $scope.readOnly = false;
              }
            }
          } else if (item.type == 'ChatMessage') {
            $scope.readOnly = false;
            $scope.agentTyping = false;
            liveAgentName = item.message.name;

            removeWaitingAgentElement();
            $scope.forwardedToLiveAgent.showMessage = false;
            $scope.formattedAgentName = "Chat Emissão Auto";

            var msgInstance = MessageModel.build({
              type: "atendente",
              output: { "text": [item.message.text] },
              dateHour: new Date(),
              atendente: { nome: liveAgentName },
              corretor: { nome: userName }
            });

            $scope.conversation.push(msgInstance);

            notificarNovaMensagemRecebida();

            //Send log to Oficina
            sendOficinaLog(context.workspace_id, context.conversation_id, item.message.text, oficinaTypeAtendente, codNac, "[]", "[]");

          } else if (item.type == 'AgentTyping') {
            $scope.agentTyping = true;
          } else if (item.type == 'AgentNotTyping') {
            $scope.agentTyping = false;
          } else if (item.type == 'FileTransfer') {
            //Upload de arquivos
            Debug.print(debug, "FileTransfer, Item: ", item);
            if (item.message.type == 'Requested') {
              Debug.print(debug, "FileTransfer, Requested: ", item.message);
              if (item.message.uploadServletUrl
                && item.message.fileToken
                && item.message.cdmServletUrl) {

                var chatKey = sessionKey.split('!')[0];

                $scope.uploaderOptions.url = ChatConfig.salesForceUploadArquivosURL();
                $scope.uploaderOptions.headers.access_token = accessToken;
                $scope.uploaderOptions.headers.client_id = ChatConfig.sensediaClientId();
                $scope.uploaderOptions.headers.organizationId = LiveAgentService.getOrganizationId();
                $scope.uploaderOptions.headers.fileToken = item.message.fileToken;
                $scope.uploaderOptions.headers.sessionId = chatKey;
                $scope.uploaderOptions.headers.baseURL = item.message.uploadServletUrl;

                Debug.print(debug, "FileTransfer, Fileupload Data: ", $scope.uploaderOptions);
                uploader = $scope.uploader = new FileUploader($scope.uploaderOptions);

                uploader.filters.push({
                  name: 'customFilter',
                  fn: function (item /*{File|FileLikeObject}*/, options) {
                    return this.queue.length = 1;
                  }
                });

                $scope.enableUploadButton = true;
                $scope.setGridHeight();
              } else {
                $scope.enableUploadButton = false;
                $scope.exibeFileUpload = false;
              }

              //Send log to Oficina
            sendOficinaLog(context.workspace_id, context.conversation_id, "FileTransfer, Requested", oficinaTypeAtendente, codNac, "[]", "[]");
            } else if (item.message.type == 'Canceled') {
              Debug.print(debug, "FileTransfer, Canceled: ", item.message);
              $scope.enableUploadButton = false;
              $scope.exibeFileUpload = false;

              //Send log to Oficina
              sendOficinaLog(context.workspace_id, context.conversation_id, "FileTransfer, Canceled", oficinaTypeAtendente, codNac, "[]", "[]");
            } else if (item.message.type == 'Success') {
              Debug.print(debug, "FileTransfer, Success: ", item.message);
              $scope.enableUploadButton = false;
              $scope.exibeFileUpload = false;

              //Send log to Oficina
              sendOficinaLog(context.workspace_id, context.conversation_id, "FileTransfer, Success", oficinaTypeAtendente, codNac, "[]", "[]");
            }
          }
        });
      }

      if(!$scope.chatEnd.wasEnded){
        sendMessageRequest();
      }
    };

    /**
     * sendLiveAgentTranscript
     * send watson transcript to live agent
     */
    var sendLiveAgentTranscript = function () {
      if (sendTranscriptRetry < 3) {
        var parameters = {
          headers: LiveAgentHeaderModel.build({
            "apiVersion": apiVersion,
            "affinity": affinityToken,
            "sessionKey": sessionKey,
            "accessToken": accessToken,
            "idSessao": watsonSessionId,
            "requestId": uuid2.newuuid()
          }),

          body: {
            "text": watsonTranscript
          }
        };

        Debug.print(debug, "sendLiveAgentTranscript, Params: ", parameters);

        //call Live Agent Service
        LiveAgentService.sendMessage(null, sendLiveAgentTranscriptError, parameters);
      }
    };

    /**
     * sendLiveAgentTranscriptError
     * if an error is returned, try again
     * @param {Object} response
     */
    var sendLiveAgentTranscriptError = function (response) {
      Debug.print(debug, "sendLiveAgentTranscriptError", response);

      sendTranscriptRetry++;
      sendLiveAgentTranscript();
    };

    var sendLiveAgentFirstMessage = function(){
      var parameters = getParametersLiveAgent();
      parameters.body = {
        "text": "Transferência do chat inteligente"
      };

      LiveAgentService.sendMessage(sendLiveAgentTranscript, sendLiveAgentTranscript, parameters);      
    };

    /**
     * endLiveAgentChat
     * call chatEnd endpoint and delete session after success as callback
     */
    var endLiveAgentChat = function () {
      var parameters = {
        headers: LiveAgentHeaderModel.build({
          "apiVersion": apiVersion,
          "affinity": affinityToken,
          "sessionKey": sessionKey,
          "sequence": sequence,
          "accessToken": accessToken,
          "idSessao": watsonSessionId,
          "requestId": uuid2.newuuid()
        }),

        body: {
          "reason": "client"
        }
      };

      Debug.print(debug, "endLiveAgentChat, Params: ", parameters);

      LiveAgentService.chatEnd(null, null, parameters);
    };

    /**
     * deleteLiveAgentSession
     * dele the live agent session to complete the chatEnd
     */
    var deleteLiveAgentSession = function () {
      var parameters = {
        headers: LiveAgentHeaderModel.build({
          "apiVersion": apiVersion,
          "affinity": affinityToken,
          "sessionKey": sessionKey,
          "sequence": sequence,
          "accessToken": accessToken,
          "idSessao": watsonSessionId,
          "requestId": uuid2.newuuid()
        })
      };

      Debug.print(debug, "deleteLiveAgentSession, Params: ", parameters);

      LiveAgentService.deleteSession(null, null, parameters);
    };

    /********************************** Orchestrator Watson methods **********************************/

    /**
     * getSensediaAuthorizationOrchestrator
     * Get token authorization to use the Orchestrator application
     */
    var getSensediaAuthorizationOrchestrator = function (successCallback) {
      Debug.print(debug, "getSensediaAuthorizationOrchestrator", successCallback);

      var parameters = {
        headers: AuthorizationHeaderModel.build()
      };

      Debug.print(debug, "getSensediaAuthorizationOrchestrator, Params: ", parameters);

      OrchestratorService.getSensediaAcessToken(successCallback, handleGeneralError, parameters);
    };

    /**
     * getBoletoPdf
     */
    var getBoletoPdf = function (urlBoleto, successCallback) {
      Debug.print(debug, "getBoletoPdf", urlBoleto);

      getSensediaAuthorizationOrchestrator( function(response) {
        var parameters = {

          headers: {
            "Accept": "application/pdf",
            "Content-Type": "application/pdf",
            "client_id": "0d71a662-e544-3f75-b508-393302213899",
            "access_token": response.data["access_token"]
          },
          url: urlBoleto
        };

        Debug.print(debug, "getBoletoPdf, Params: ", parameters);

        OrchestratorService.getBoletoPdf(successCallback, errorGetBoleto, parameters);
      });
    };

    var errorGetBoleto = function (error) {
      Debug.print(debug, "errorGetBoleto", error);

      if (!$scope.chatEnd.wasEnded) {
        var output = {};
        output = {
                    "text": [mensagemErroIndisponibilidadeOrquestrador]
                 };

         var msgInstance = MessageModel.build({
                type: "atendente",
                output: output,
                dateHour: new Date(),
                atendente: { nome: watsonAgentName },
                corretor: { nome: userName }
              });

          sendOficinaLog(context.workspace_id, context.conversation_id, "<< ERRO AO OBTER BOLETO >>", oficinaTypeWatson, codNac, "[]", "[]");

          $scope.conversation.push(msgInstance);
          
          sendOficinaLog(context.workspace_id, context.conversation_id, output.text, oficinaTypeWatson, codNac, "[]", "[]");

          getSensediaAuthorizationOrchestrator(function (response) {
            var parameters = {

              headers: AuthorizationHeaderModel.build ({
                "accessToken": response.data["access_token"],
                "Authorization": ChatConfig.tokenLocal(), //Usado somente quando em Debug local
                "idSessao": watsonSessionId,
                "requestId": uuid2.newuuid()
              }),

              sessionId: watsonSessionId
            }

            OrchestratorService.getChatLog(successGetLog, errorGetChatLog, parameters);
        });
      }
    }

    var errorGetChatLog = function (error) {
      Debug.print(debug, "errorGetChatLog", error);
      handleEndChatWatson({
        enviarSalesforce: true,
        logConversa: { value: mensagemErroHistoricoNaoEncontrado}
      });
    }

    var successGetLog = function (response) {
      Debug.print(debug, "successGetLog", response);
      handleEndChatWatson(response.data);
    }

    /**
     * startWatsonConversation
     * make the initial request to start the conversation with Watson
     */
    var startWatsonConversation = function (response) {
      Debug.print(debug, "startWatsonConversation", response);

      if (response.data && response.data["access_token"]) {
        watsonAcessToken = response.data["access_token"];
      }

      var parameters = {
        headers: AuthorizationHeaderModel.build({
          accessToken: watsonAcessToken,
          //used only on local debug
          Authorization: ChatConfig.tokenLocal()
        }),
        body: watsonStartConversationBody
      };

      Debug.print(debug, "startWatsonConversation, Params: ", parameters);

      OrchestratorService.startWatsonConversation(firstWatsonMessage, handleGeneralError, parameters);
    };

    /**
     * firstWatsonMessage
     * handle the first returned message from Watson
     * @param {Object} response
     */
    var firstWatsonMessage = function (response) {
      Debug.print(debug, "firstWatsonMessage", response);

      // Verifica se retornou os parâmetros e grava os dados
      if (response.data.parametros) {
        Debug.print(debug, "firstWatsonMessage, Parâmetros da URL: ", response.data.parametros);

        //codNac = response.data.parametros.codigoNAC;
        //@@saude@@
        codNac = $scope.chat.carteirinha;
        codProdutor = response.data.parametros.codigoProdutor;
        razaoSocial = response.data.parametros.razaoSocial;
      } else if (!response.data.enviarSalesforce) {
          var error = {
            data: {
              error: {
                message: "Parâmetros para inicialização do chat inválidos."
              }
            }
          };
          handleGeneralError(error);
      }

      if (response.data.context) {
        context = response.data.context;
        watsonSessionId = response.data.idSessao;
      }

      if (response.data.enviarSalesforce) {
        handleEndChatWatson(response.data);
      } else {
        if (!response.data.output ||
          (response.data.output.text && (response.data.output.text.length === 0) ||
            (response.data.output.text && (!response.data.output.text[0] || !response.data.output.text[0].trim())))) {

          Debug.print(debug, "firstWatsonMessage, Não veio Output ou Output.Text vazio", response);
          getSensediaAuthorizationOrchestrator(startWatsonConversation);

        } else {
          removeWaitingAgentElement();

          var msgInstance = MessageModel.build({
            type: "atendente",
            output: response.data.output,
            dateHour: new Date(),
            atendente: { nome: watsonAgentName },
            corretor: { nome: userName }
          });

          Debug.print(debug, "firstWatsonMessage, Msg: ", msgInstance);

          $scope.conversation.push(msgInstance);
          $scope.readOnly = false;

          document.getElementById("inputMessage").disabled = false;
          document.getElementById("inputMessage").focus();

          //WATSON FIRST MESSAGE
          var jsonIntents = response.data.jsonIntents?response.data.jsonIntents:"[]";
          var jsonEntities = response.data.jsonEntities?response.data.jsonEntities:"[]";

          sendOficinaLog(context.workspace_id, context.conversation_id, response.data.output.text.join("</br>"), oficinaTypeWatson, codNac, jsonIntents, jsonEntities);


          if(context && context.mensagem_pre_chat){
            $scope.mensagemPreChat = context.mensagem_pre_chat;
          }

          notificarNovaMensagemRecebida();
          handleEndChatWatson(response.data);

          resetIdleTimeout();
        }
      }
    };

    var successSendOficinaLog = function (response) {
      Debug.print(debug, "successSendOficinaLog", response);
    }

    /**
     * successSendWatsonMsg
     * handle the response from Watson, adding the element in the page
     * @param {Object} response
     */
    var successSendWatsonMsg = function (response) {
      Debug.print(debug, "successSendWatsonMsg", response);

      if (response.data.context) {
        context = response.data.context;
      }
      
      if(response.data.camposFormulario){
        $scope.formulario.camposNecessarios = response.data.camposFormulario;
        $scope.exibirBotoesSimOuNao = false;
        definirCamposVisiveis();
        watsonUltimoOutput = response.data;
        
        var jsonIntents = response.data.jsonIntents?response.data.jsonIntents:"[]";
        var jsonEntities = response.data.jsonEntities?response.data.jsonEntities:"[]";
        sendOficinaLog(context.workspace_id, context.conversation_id, "<< FORMULARIO >> ", oficinaTypeWatson, codNac, jsonIntents, jsonEntities);

      } else {
        var msgInstance = MessageModel.build({
          type: "atendente",
          output: response.data.output,
          dateHour: new Date(),
          atendente: { nome: watsonAgentName },
          corretor: { nome: userName }
        });

        watsonUltimoOutput = null;

        Debug.print(debug, "successSendWatsonMsg, Msg: ", msgInstance);

        if (response.data.context) {
          context = response.data.context;
          if(context.exibirBotaoSimNao == "true"){
            $scope.exibirBotoesSimOuNao = true;
            context.exibirBotaoSimNao = "false";
          }
        }

        if (response.data.retornoServico === "erro") {
            sendOficinaLog(context.workspace_id, context.conversation_id, "<< ERRO SERVIÇO >>", oficinaTypeWatson, codNac, "[]", "[]");
        }

        //Send WATSON message to oficina
        var jsonIntents = response.data.jsonIntents?response.data.jsonIntents:"[]";
        var jsonEntities = response.data.jsonEntities?response.data.jsonEntities:"[]";
        sendOficinaLog(context.workspace_id, context.conversation_id, response.data.output.text.join("</br>"), oficinaTypeWatson, codNac, jsonIntents, jsonEntities);

        $scope.conversation.push(msgInstance);
      }

      $scope.readOnly = false;

      document.getElementById("inputMessage").disabled = false;
      document.getElementById("inputMessage").focus();

      resetIdleTimeout();
      notificarNovaMensagemRecebida();
      handleEndChatWatson(response.data);
    };

    /**
     * 
     * @param {*} workspaceId 
     * @param {*} conversationId 
     * @param {*} mensagem 
     * @param {*} tipo 
     * @param {*} codNac 
     * @param {*} jsonIntents 
     * @param {*} jsonEntities 
     */
    var sendOficinaLog = function(workspaceId, conversationId, mensagem, tipo, codNac, jsonIntents, jsonEntities){
        var parameters = {
          headers: AuthorizationHeaderModel.build({"accessToken": watsonAcessToken}),
          body: {
              "workspace_id": workspaceId,
              "conversation_id": conversationId,
              "mensagem": mensagem,
              "tipo": tipo,
              "user": codNac,
              "intents": JSON.parse(jsonIntents),
              "entities": JSON.parse(jsonEntities)
          }
        };

        OficinaService.logConversation(successSendOficinaLog, errorSendOficinaLog, parameters);
    };

    /**
     * resetIdleTimeout
     * reset idle timeout that ends the session and open a case
     */
    var resetIdleTimeout = function () {
      endIdleTimeout();
      idleTimeOut = window.setTimeout(function () {
        checkIdleTimeoutCount();
      }, idleLimit);
    };

    /**
     * endIdleTimeout
     * clear and stop the idle timeout
     */
    var endIdleTimeout = function () {
      clearTimeout(idleTimeOut);
    };

    var checkIdleTimeoutCount = function(){
      endIdleTimeout();
      
      if(idleTimeoutResetCount > 1){
        endWatsonChatSession();

      } else {
        //idleLimit = 180000;
        showPresencePopup();
      }
    };

    var showPresencePopup = function(){
      idleTimeoutResetCount++;

      var timerMs = idleLimit, interval = 1000;
      
      $scope.$apply(function(){
        $scope.presencePopupVisible = true;
        $scope.presenceTimer = millisToMinutesAndSeconds(timerMs);
      });
      
      presenceConfirmInterval = window.setInterval(function () {
        timerMs -= interval;

        $scope.$apply(function(){
          $scope.presenceTimer = millisToMinutesAndSeconds(timerMs);
        });

        if(timerMs <= 0){
          $scope.$apply(function(){
            $scope.presencePopupVisible = false;
          });

          clearInterval(presenceConfirmInterval);
          endWatsonChatSession();
        }
      }, interval);
    };

    var millisToMinutesAndSeconds = function (millis) {
      var minutes = Math.floor(millis / 60000);
      var seconds = ((millis % 60000) / 1000).toFixed(0);

      return (minutes > 0 ? minutes + ":" + (seconds < 10 ? '0' : '') + seconds : seconds + " segundos");
    }

    $scope.resetIdleTimeout = function(){
      $scope.presencePopupVisible = false;

      clearInterval(presenceConfirmInterval);      
      resetIdleTimeout();
    };

    /**
     * endWatsonChatSession
     * call endSession endpoint, terminating the Watson conversation
     */
    var endWatsonChatSession = function () {

      $scope.formulario.visivel = false;
      $scope.exibirBotoesSimOuNao = false;

      if (endChatRetry < endChatMaxRetry) {

        var parameters = {
          headers: AuthorizationHeaderModel.build({
            "accessToken": watsonAcessToken,
            "Authorization": ChatConfig.tokenLocal(), //Usado somente quando em Debug local
            "idSessao": watsonSessionId,
            "chatIdle": true
          })
        };

        Debug.print(debug, "endWatsonChatSession - parameters", parameters);

        OrchestratorService.endChatSession(endWatsonChatSucess, endWatsonChatError, parameters);
      } else {

        $scope.chatEnd.wasEnded = true;
        $scope.chatEnd.formattedMsg = watsonAgentName + $scope.chatEnd.formattedMsg;
        $scope.readOnly = true;
      }
    };

    var endWatsonChatSucess = function (response) {
      Debug.print(debug, "endWatsonChatSucess", response);
      $scope.chatEnd.wasEnded = true;
      $scope.chatEnd.formattedMsg = watsonAgentName + $scope.chatEnd.formattedMsg;
      $scope.readOnly = true;

      //add analytics event
      ga('send', 'event', 'Chat Corretor', 'watson-atendeu', 'Watson - Atendimento finalizado por inatividade');
    };

    var endWatsonChatError = function (response) {
      endChatRetry++;
      endWatsonChatSession();
    };

    /**
     * handleEndChatWatson
     * handle the end of the chat or forward to live agent
     * @param {Object} data
     */
    var handleEndChatWatson = function (data) {
      Debug.print(debug, "handleEndChatWatson, Data: ", data);

      fillOutputInformation(data);

      if (data.encerradoWatson) {
        endIdleTimeout();
        $scope.chatEnd.wasEnded = true;
        $scope.chatEnd.formattedMsg = watsonAgentName + $scope.chatEnd.formattedMsg;
        $scope.readOnly = true;

        //add analytics event
        ga('send', 'event', 'Chat Corretor', 'watson-atendeu', 'Watson - Atendimento finalizado');

      } else if (data.enviarSalesforce) {
        endIdleTimeout();
        if ($scope.conversation.length > 0) {
          //transferred to live agent after conversation with Watson
          ga('send', 'event', 'Chat Corretor', 'watson-atendeu', 'Watson - Atendimento transferido');
        }

        if (data.logConversa) {
          //watsonTranscript = data.logConversa.value;
          watsonTranscript = ValidacaoUtil.htmlToText(data.logConversa.value);
        }
        //init chat with Live Agent
        getSensediaAuthorizationLiveAgent();
      }
    };

    var fillOutputInformation = function(data){
      if(data.context){
        $scope.contadorRetencaoParcial = data.context.ret_parcial_count;
        
        if(data.context.intent){
          $scope.intent = data.context.intent;
        }

        if(data.context.system){
          $scope.contadorDialogo = data.context.system.dialog_request_counter;
        }
      }

      if(data.output 
          && data.output.action 
          && $scope.invalidActions.indexOf(data.output.action) === -1){
        $scope.action = data.output.action;
      }

      if(data.retornoServico){
        $scope.retornoServico = data.retornoServico;
      }
    }

    /**
     * notificarNovaMensagemRecebida
     * Notifica o usuário sobre a nova mensagem recebida com som e mensagem de notificação
     */
    var notificarNovaMensagemRecebida = function () {
      var audio = new Audio('../resources/Mensagem_recebida.wav');
      audio.play();

      // Devemos exibir a notificação apenas se a janela não estiver ativa
      if (document.hidden && "Notification" in window) {
        var opcoes = {
          body: mensagemNotificacaoCorpo,
          icon: mensagemNotificacaoImagem
        };

        var notificacao = new Notification(mensagemNotificacaoTitulo, opcoes);
      }
    };

    /**
     * obterParametrosChat
     * Obtem os parâmetros para iniciar o chat
     */
    var obterParametrosChat = function (response) {
      Debug.print(debug, "obterParametrosChat", response);

      if (response.data && response.data["access_token"]) {
        watsonAcessToken = response.data["access_token"];
      }

      var parameters = {
        headers: AuthorizationHeaderModel.build({
          accessToken: watsonAcessToken,
          //used only on local debug
          Authorization: ChatConfig.tokenLocal()
        }),
        params: {
          organizationId: LiveAgentService.getOrganizationId(),
          deploymentId: LiveAgentService.getDeploymentId(),
          buttonId: LiveAgentService.getButtonId()
        }
      };

      Debug.print(debug, "obterParametrosChat, Params: ", parameters);

      OrchestratorService.obterParametrosChat(obterParametrosChatSuccess, handleGeneralError, parameters);
    };

    /**
     * obterParametrosChatSuccess
     * Success callback from obterParametrosChat
     */
    var obterParametrosChatSuccess = function (response) {
      Debug.print(debug, "obterParametrosChatSuccess", response);

      if (response.data) {
        $scope.telefoneAtendimento = response.data["telefoneAtendimento"];
        $scope.horarioAtendimentoTelefone = response.data["horarioAtendimentoTelefone"];
      }

      if (response.data && response.data["horarioDentroAtendimento"] && response.data["agenteDisponivel"] &&
        response.data["horaInicial"] && response.data["horaFinal"]) {

        $scope.workingHours.start = formatWorkingHour(response.data["horaInicial"]);
        $scope.workingHours.end = formatWorkingHour(response.data["horaFinal"]);
        $scope.showInputName = true;
        
        $scope.emailTelefoneObrigatorio = response.data["emailTelefoneObrigatorio"];

        $scope.chat = {};

        if(window && window.localStorage){
          if(window.localStorage.getItem("nome")){
            $scope.chat.nome = window.localStorage.getItem("nome");
          }

          if(window.localStorage.getItem("email")){
            $scope.chat.email = window.localStorage.getItem("email");
          }

          if(window.localStorage.getItem("telefone")){
            $scope.chat.telefone = window.localStorage.getItem("telefone");
          }

          if(window.localStorage.getItem("carteirinha")){
            $scope.chat.carteirinha = window.localStorage.getItem("carteirinha");
          }          

          if(window.localStorage.getItem("tipoproduto")){
            $scope.chat.tipoproduto = window.localStorage.getItem("tipoproduto");
          }

          if(window.localStorage.getItem("codempresa")){
            $scope.chat.codempresa = window.localStorage.getItem("codempresa");
          }
        }

      } else {
        Debug.print(debug, "obterParametrosChatError");
        removeWaitingAgentElement();
        $scope.error.unavailableError = true;
      }

    };

    /**
     * formatWorkingHour
     * it formats the argument hour
     * @param {String} hour
     */
    var formatWorkingHour = function (hour) {
      var formattedHour = hour.substring(0, 2) + "h",
        minutes = hour.substring(3, 5);

      if (minutes > 0) {
        formattedHour = formattedHour + minutes;
      }

      return formattedHour;
    };

    /*
    * Exibir a caixa de upload de arquivos do Chat
    */
    $scope.troggleFileUpload = function () {
      if ($scope.enableUploadButton) {
        $scope.exibeFileUpload = !$scope.exibeFileUpload;
      }
    };

    /*
    * Envia o arquivo para o servlet do SalesForce
    */
    $scope.enviarArquivo = function () {
      if ($scope.enableUploadButton && uploader.queue && uploader.queue.length > 0) {
        $scope.enableUploadButton = false;
        uploader.uploadAll();
      }
    };

    $scope.downloadPDF = function () {
      PdfService.downloadPDF($scope.conversation);
    };

    $scope.limparFila = function () {
      if (uploader != null) {
        uploader.clearQueue();
      }
    };

    uploader.onAfterAddingFile = function (fileItem) {
      Debug.print(debug, "uploader.onAfterAddingFile", fileItem);
    };

    /*
    * Função de Callback para upload de arquivos
    */
    uploader.onSuccessItem = function (fileItem, response, status, headers) {
      Debug.print(debug, "uploader.onSuccessItem", response);
      Debug.print(debug, "uploader.onSuccessItem", status);

      $scope.exibeFileUpload = false;
      $scope.enableUploadButton = false;
    };

    /*
    * Função de callback para erro em upload de arquivos
    */
    uploader.onErrorItem = function (fileItem, response, status, headers) {
      Debug.print(debug, "uploader.onErrorItem", response);
      Debug.print(debug, "uploader.onErrorItem", status);

      ngDialog.open({ template: '<p>Desculpe, mas não foi possível enviar seu arquivo. Por favor, tente novamente.</p>', plain: true, className: 'ngdialog-theme-default' });
    };

    /**
     * Obtem os parâmetros para iniciar a sessão
     */
    getSensediaAuthorizationOrchestrator(obterParametrosChat);

    if ("Notification" in window && Notification.permission !== "denied") {
      Notification.requestPermission();
    }

    window.onresize = function(event) {
      $scope.setGridHeight();
    };
  });
