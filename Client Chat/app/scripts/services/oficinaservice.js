'use strict';

/**
 * @ngdoc service
 * @name clientChatCorretorApp.oficinaService
 * @description
 * # OficinaService
 * Service in the clientChatCorretorApp.
 */
angular.module('clientChatCorretorApp')
  .service('OficinaService', function ($http, ChatConfig) {

     return {
      logConversation: function (sucessCallback, errorCallback, parameters) {
        $http({
          method: "POST",
          url: ChatConfig.oficinaLogConversation(),
          data: parameters.body,
          headers: parameters.headers
        }).then(sucessCallback, errorCallback);
      }
    }

  });
