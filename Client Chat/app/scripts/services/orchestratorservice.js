'use strict';

/**
 * @ngdoc service
 * @name clientChatCorretorApp.orchestratorService
 * @description
 * # orchestratorService
 * Service in the clientChatCorretorApp.
 */
angular.module('clientChatCorretorApp')
  .service('OrchestratorService', function ($http, ChatConfig) {

    var formatWatsonHeaders = function (headers) {
      if (!headers) {
        headers = {};
      }

      headers.Authorization = ChatConfig.watsonAuthorizationToken();
      return headers;
    };

    return {

      getSensediaAcessToken: function (sucessCallback, errorCallback, parameters) {
        $http({
          method: "POST",
          url: ChatConfig.sensediaOrchestratorAcessTokenUrl(),
          headers: parameters.headers
        }).then(sucessCallback, errorCallback);
      },

      getBoletoPdf: function (sucessCallback, errorCallback, parameters) {
        $http({
          method: "GET",
          responseType:'arraybuffer',
          url: parameters.url,
          headers: parameters.headers
        }).then(sucessCallback, errorCallback);
      },

      getChatLog: function (sucessCallback, errorCallback, parameters) {
        $http({
          method: "GET",
          url: ChatConfig.orchestratorGetChatLog(parameters),
          headers: parameters.headers
        }).then(sucessCallback, errorCallback);
      },

      startWatsonConversation: function (sucessCallback, errorCallback, parameters) {
        $http({
          method: "POST",
          url: ChatConfig.orchestratorStartConversation(),
          data: parameters.body,
          headers: parameters.headers
        }).then(sucessCallback, errorCallback);
      },

      sendWatsonMessage: function (sucessCallback, errorCallback, parameters) {
        $http({
          method: "PUT",
          url: ChatConfig.orchestratorConversation(),
          data: parameters.body,
          headers: parameters.headers
        }).then(sucessCallback, errorCallback);
      },

      obterParametrosChat: function (sucessCallback, errorCallback, parameters) {
        $http({
          method: "GET",
          url: ChatConfig.parametros() + "?buttonId=" + parameters.params.buttonId + "&deploymentId=" + parameters.params.deploymentId + "&organizationId=" + parameters.params.organizationId,
          headers: parameters.headers
        }).then(sucessCallback, errorCallback);
      },

      endChatSession: function (sucessCallback, errorCallback, parameters) {
        $http({
          method: "DELETE",
          url: ChatConfig.orchestratorEndChat(parameters),
          headers: parameters.headers
        }).then(sucessCallback, errorCallback);
      }
    }

  });
