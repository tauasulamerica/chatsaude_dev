'use strict';

/**
 * @ngdoc service
 * @name clientChatCorretorApp.chatSalesforceService
 * @description
 * # chatSalesforceService
 * Service in the clientChatCorretorApp.
 */
angular.module('clientChatCorretorApp')
  .service('LiveAgentService', function ($http, ChatConfig) {

    return {

      getOrganizationId: function() {
        return ChatConfig.liveAgentOrganizationId();
      },

      getDeploymentId: function() {
        return ChatConfig.liveAgentDeploymentId();
      },

      getButtonId: function() {
        return ChatConfig.liveAgentButtonId();
      },

      getSensediaAcessToken: function(sucessCallback, errorCallback, parameters) {
        $http({
          method: "POST",
          url: ChatConfig.sensediaLiveAgentAcessTokenUrl(),
          headers: parameters.headers
        }).then(sucessCallback, errorCallback);
      },

      getSession: function (sucessCallback, errorCallback, parameters) {
        $http({
          method: "GET",
          url: ChatConfig.liveAgentSessionUrl(),
          headers: parameters.headers
        }).then(sucessCallback, errorCallback);
      },

      chasitorInit: function(sucessCallback, errorCallback, parameters) {
        $http({
          method: "POST",
          url: ChatConfig.liveAgentChasitorInitUrl(),
          headers: parameters.headers,
          data: parameters.body
        }).then(sucessCallback, errorCallback);
      },

      getMessages: function(sucessCallback, errorCallback, parameters) {
        $http({
          method: "GET",
          url: ChatConfig.liveAgentGetMessagesUrl(),
          headers: parameters.headers,
          timeout: ChatConfig.timeoutMessageLiveAgent()
        }).then(sucessCallback, errorCallback);
      },

      sendMessage: function(sucessCallback, errorCallback, parameters) {
        $http({
          method: "PUT",
          url: ChatConfig.liveAgentSendMessageUrl(),
          headers: parameters.headers,
          data: parameters.body
        }).then(sucessCallback, errorCallback);
      },

      chatEnd: function(sucessCallback, errorCallback, parameters) {
        $http({
          method: "DELETE",
          url: ChatConfig.liveAgentChatEndUrl(),
          headers: parameters.headers,
          data: parameters.body
        }).then(sucessCallback, errorCallback);
      },

      deleteSession: function(sucessCallback, errorCallback, parameters) {
        $http({
          method: "DELETE",
          url: ChatConfig.liveAgentDeleteSessionUrl(parameters.headers['X-LIVEAGENT-SESSION-KEY']),
          headers: parameters.headers,
          data: parameters.body
        }).then(sucessCallback, errorCallback);
      },

      getAgentAvailability: function(sucessCallback, errorCallback, parameters) {
        $http({
          method: "GET",
          url: ChatConfig.liveAgentAvailabilityUrl(parameters.urlConfig),
          headers: parameters.headers
        }).then(sucessCallback, errorCallback);
      },

      resyncSession: function(sucessCallback, errorCallback, parameters) {
        $http({
          method: "GET",
          url: ChatConfig.liveAgentResyncSessionUrl(),
          headers: parameters.headers,
          data: parameters.body
        }).then(sucessCallback, errorCallback);
      }
      
    }
  });
