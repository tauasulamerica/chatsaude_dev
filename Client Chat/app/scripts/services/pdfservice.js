'use strict';

angular.module('clientChatCorretorApp').service('PdfService', function ($http, ChatConfig) {

    return {
        downloadPDF: function(conversation) {

            var addPageHeader = function (doc){
                var imgData = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMMAAADDCAYAAAA/f6WqAAAgAElEQVR4nO2deXwcxZn3v91zSqPLuixLsi0fsg2+L4wNxgbMbUggnCFASEjySXbfhGSXJCy7b052c7ybNwkJARJyAIZwGjC3Mdhg7PjA933KRpJlWfetObreP7p6pnumZyQZZzfZt76fjzwz3VVPPVVdv6qnqnvGmhBCoFAo0P+7HVAo/lZQYlAoJEoMCoVEiUGhkCgxKBQSJQaFQqLEoFBIlBgUCokSg0IhUWJQKCRKDAqFRIlBoZAoMSgUEiUGhUKixKBQSJQYFAqJEoNCIVFiUCgkSgwKhUSJQaGQKDEoFBIlBoVCosSgUEiUGBQKiRKDQiFRYlAoJEoMCoVEiUGhkCgxKBQSJQaFQqLEoFBIlBgUCokSg0IhUWJQKCRKDAqFRIlBoZAoMSgUEiUGhUKixKBQSJQYFAqJEoNCIVFiUCgkSgwKhUSJQaGQKDEoFBIlBoVCosSgUEiUGBQKiRKDQiFRYlAoJEoMCoVEiUGhkCgxKBQSJQaFQqLEoFBIlBgUCokSg0IhUWJQKCRKDAqFRIlBoZAoMSgUEiUGhUKixKBQSJQYFAqJEoNCIVFiUCgkSgwKhUSJQaGQKDEoFBIlBoVCosSgUEi8mU4KIdA0LeV1sOkHe14IARA/Zj/vltbtsxACIr0YbfXQcgyj5Rii9SNEVxMi3AN9naB7wB9Cy8qF/Ao8xWNh+EQ8xWPAl5WwI0n21c3PTJ8z+exWhlu7DeRPpnZya1NHe7n4mtKmtrLd0v1P8k0T9lyDZCBRDCa/5eTp2zAg2o9oriFWux2jbifGqcPQ1YSIhUnqUkCasnQPWt4I9Kq56Gdfhmf4BNA8H8u3M0U6cQwkvsHaGmyaQfkhBGY729D0/3bf0vpLaruliGGgUep0OkmmfEOZfYQRxWg5jlG7A+PYJoy6ndDTCvb09n6fRgNpy9A09PKpeOfegqdqHni8A+b7OB0o3Sw5lI6RaVQdaPQcapkAsY5moi0niLbUE22qJ9bRTKy7DaO3ExGN2HJo6KE89GAITygfT84wPPnFePJK8OQW4MkrRs/OO6O+fdx20wzDEJahwUzXmaazwYRB6Tq/2/QHmKFP4yFiRz7AOLoB0V4Pkb7kWoJlwywofswUhJCvWkIg1nk39ege9MoZeM7/Ap6ySYA2pGk+XWOnaz876cKEwXb2dKFbsj27zfT1MjB6uug/voe+fRvoPbyVaHM9Rl83xKKJfAg0NNureVQTIDTNbG6sa6Oj+QLogWz0nGH4y8bgGz4af/l4/CPGoucUogeC4PEN2OZnut1SxJDO8GBGQLepyO1CpHM6XnakD6N+F7FDazGObUK0fmQlSurTmtmnsTq2ZUz+YyXEvBiOMi2RWDakLgQioRFfEG3aNfjPvR09K39A/906+GAu6EAju5vQ0pVh9yW53Exl298bvV30HNhM37ZV9B7aQqyjWTaprdx429p90RKnrLQaIDRbOk0eIy4S65gG6KF8fMWVeIsrCVROwFc2Bl9JJd7CcvB4XQeb5LY43XZzXTOcboiUaYTMVIYQBqKnDXFyH7GD72Ec24zoagYRI2O8T+J03F68ZyeV7fGbr7EIzgvobt9mBm3YKHwX/iPa6Lnouidjuww1hh1MmOh24QdqX+tccnq3cwDEovTVHqR3y5t0bVlJrLMVhGFrDKtNksoSIqnDawirv6fzTQ5qVr6UJLaBCk1D8wbQAln4Skfir5iAf3gVvhFj8RaV48nKRQtmkzx7n067acIkY8J0YVFqHTPnszskhIFoOY5x/EOiNZuhYS+ipyWRJzGkJ2wkhm/ZfzW0QA5a3nC03FK07EK0UBFaMBey8sHrRwvmm4vkQMg0F+4BYWD0tqP1dSJ6WhEdDRht9YiWY4ju5vgFtqZ+AM0bwDP9k+jn3oknkD3ojptpcMjUXunCqOT2H8rIZ89jYcSi9O5YTdf6l+k9tAURi8THikT31hBWqGkalpGne5inBUN4C0rx5BbiCRWAz48npwA03ZHO6O3CCPdBLIrR00GspxOjp51Ydzuir9vmpzUqCYdvmj+Id9hwPPmlZqhVVoW3uBJvUQXevCK0YChDuxnE2k4RbjhC/9FdeAtKzTAptvMVjNpt6JUz0MunoBWUg8c/4EyQ3MDpO4gBvR0YnY2Ik/sx6nYQ+2grxEd/50jsrIEHsvLQsvLRCirQC0ejDRuJVjgKvaACgrlmI2sa9tFhoBE7tRMJMGKI7maM2u3ybxui7QTCiMV900vHoS/+Gr6RM1zrPdRZ0c2XgcKoTDbTiTA5bayrlZ4tK+lY8wyRpjqsXmbm1xKhp0isx8zjJNZcXj/eYcPxl48jOGYavvJx+IaPwZtXKK+JHr+oA4Yv1jUQwhysIhGMrlZinS3EulqJtZ8yX9tOEetsIdrRhNHXjYj0I6JhRLjfvH6RMOh6fG3iCeWhh/LRrM0QIRCRMEIYeHIKCIw6m+xpiwiMOkuuGYRhjtBbnsOo24mWNxx9+CT08snoJePQCkeBL4t4YBfHFgta8WOkD9F1yhxtW44jWo5hNB2FzkZEVzPCiJKMJjsymoaWU4xWPA69eAxayTi0YaPQcwohmI/m9Q9q5E3HQKNtyrlIL0bjQaL7VmHsfwf6Osyy/Nl4z70N76wb4uFXpvh1KL5lWuydTr2Sz8d6Ouj64AU6PlhOrPVkUpRj+yDDFE2GLHEp6B6Co2UHqp6Dv3QUWiD7jPiWKQ2khn1GuA8R7pNi6EMYMUTY3FyJdbaCEXPa8fnRQ/nowRB6dj56dq7zvH3NIISA7hZih9cSO7YJceoIousURPshkIOWlS9FAZovAP6Qudff3w3RPjPk6O92btaQFJVrmtmxswvQQsXoRVVQNBq9qAqtcDR6dkHGhhkKg1nnDNpWuJvIrteJbX0e0VZnTtNlZ+G78KvoZWeh6UO7mf9xfRvKGg4g2n6Kzr+8QueaPxPrarMSmK+Z7AhA1/CPGEdoxkXknHMVnmHDM5Y9VN9OZ4060Ex5Or7F1wyuuw/hbkRvB6K9HtFWj+g8iehsRPS2mzNAXzsYRqrRYK4ZZuUUQTAPLVRojvihYjO2D4RMIfmCrgucTLHvYBej9nzpQphk+5liditNrKcdY9+bRP/yOPS2gy8bz9Sr8M27DbLyM9oaqm9uF9ttFM10PtbdTte65XSsXU6s7STOoUluhlozQvysDG0CWWRPOpec+dcQHDsDLZDlKPPj+DaYDYDBnrf783F8SxGDm8ODZaAF4FAqNZS4OV0HS36fzt9MvqSrW6z9BNH3HyK2/11zJgwV4z3nVnxTrkDzZw/Y+QcqP936I1N7ONo73Efnhldof+cJoi0NKWsytzWaJYTAyAlkz7iYnFmX4Ckckbas0/ZNMpSZYTDt+HF9c9xn+LgMVkjp1DzYMoYyfbo1arpRP52dtGmiYfq3v4RY/wfo7wI0tMJKtOmfwj/pQsgqyCiodL4NVNdMvolohO5da2l/43eETxzG3B61bkgm1gKmDSkKXceTV0z2lIXkzLkM/8hJaL7AGfctHZmu0VDK/Li+uW6tZjL412SwZX4c3850XiEEkfrdiNU/x2g4ED+u5RSjVy/CU70IT/lk8PhO27eBBG297z+2m/Y3HqVnz3rMcd4KiZyhkfXek1tI1qR5ZE9fTHD8LPSs3NOOBgbybaDQNp2tTDbOtG/xmWEgx+1p3CqTnMZtWkzHQOndKpAund3fdPlO115Gwt2E//IYse0voUV64/k0jxctpwRt5Cy8I6cjSifhyc6HQGjAnSiLdCGG1fbRtkY6Vv6Jjr+8jBEJ49zhw9ylA/TsXLxFFWRVzybr7AXmdqJtJyjdjPlxfBts+w8mpPpr+3ZaT63+PTKUkWWwI3TKecPAOLmXyLsPIE7scU+ke8zwKXuYee8kVATBXHOnzuM3F+HegLkJoXvM42jmq23HSkT6EZ2N9B7ZTeu7z5jrAuSYL9948orxV4zHP3ISwaqp+MvGoBeOQNf1tKFIcl0HCi0ytclgwubBzhr/Fb5phmEIN5UNZrrKNK0N9DlTnDeYUSNT3nR+upVhLydTmYNZd1jHjWgE4/D7RDcuQ5wy43YzZrcP1lrK8z7mMRHfzZFHrZPYicY89IQDRIxs9FAB3sIyvIUj8BZX4CsZhW94FZ6cfOx3fd2uhVtbJqcfzIjs1naDTf+34pvr1momBurEA8WI6fIlO5fufbp0p+P7mc6TUsdwL9FjmzF2vYpxfDNEw26Z0m7zJN7KRa/mwVM+GW3qUrTiiWj+bPSsHDT5eEg6n2Dw68HBxukDXYvBtNvfmm+OmeF0Ch6IgUb8M8Fgw5pM6SwyTbun44v1OdbRgFGzCePoekTTUUT7CYQw5LifeMpHk0/ECUDTdLScIvNm5Og5aGPmm9/KY+htmGl2HEx4kS42zzRr/r35lnbN8NcQgJtDmcKa/2oyhUVWHQYrvExl0N+F0d0C7fXmDc3uVrAeU9F0cz2RW4IoGIknR64pdM+AYelQjrn6lSZNpvAyXf3/Hn07owvooUxdg807WJEMdlo+nfx/TdtDSXMm8vw1bPy17P5X+5b2+wydPf0cqj3FnpoGmtt76OztIzvop7Qgl0mjSxlbXsSw3OyMChzo+N8CpxPrZsr/t8SZEvHHCRv/nnzzJo+8zR3dPPTiOpav2c6R+mYS923kak5+Li/O55K5E/mH6xZSPbIkxVHLQbvWBjNz2CvmVmFDCD7c9xGb932EkF8+GZYT5JZL56aUnVyu9dre1cvrf9lLY2snTe3d8XTF+SE+f9U8QtnBtD4OdVE2UBw8mNDMrS3sbZVpcrfOhaMx/rzyQw7XN8fPTR5Txo0XzUy7ZhrIZjrfNuyp4d0PD9EbNr8TPXXsCK6/cEZGO2427a9nyrdMtrxWpphh8Nr6PXzrwZc50dyRWop1oeRLfVM7f3p9I8+t3sY/XreQu29aTMDnTbmog1Wv67SVZAdgzZaDfOYHT9DTZ+3MCCZVDIuLYaCR5GRzO7fd/xSb9tRA/ItCZh6/z8vZo0tZcs5Zaf1M52+6ct3qkM43t3zWuYwLv0GsvR55cS3f/cNbGPLBykmjh3PHFfPSijtTaJrJtxfe3cpXfvY8/ZHEo/rXLZrGpxZPj6fNVJ7buTPl20DtplsZ//DqBr70k6dtQpCq0Wwf7UoyN8np7g3z0yff4b6HVhCJxga9OEpWvhDCdTRIzv/y2l309PUn/MvwlVB7+ZaN51Zvk0KQea3ZDghHory6bg+GzRc3fzIx2LRuvlltkM6G26jpljbZ9701J/jFc2vjQqgaUcQT//t2xlUUxW0MNIANxre+/gi/XfEX+sPWr2QM7JvbdU8u80z4ZrefLq0uhODdLQf59m9W0NsfkR1eWN6a7+PHNGcd5ashDH7/2kZ+/fx7KQUlO+AWNg2U3joWixnUN3eCkB1Y2J3BYcOt4v39YV5cuzdRN5FUEQGvrt9Da3uXqz/p/EpXN/vrQL65pUuXx62+bnkAevrC3PvQqzS3dQLmjPD8/Z+LCyGTnaH61hcO09rVKwcY2V9c+mdy6DNQXc6Eb4NpN29vf4Qf/PFNYtb3EjQNq+OfVTWcz199LnMmjsLn89DV08+Ow/W8um43728/YuYRmJXW4MHlH/CJhVMZW1GSUtHk6SrTuQGxXEx3Os1MdLCuiV2Hra84aphPcMZTgAZNHb2s3X6QTyyaleKbm4+ZpmA3X4a6wZAp/2B8O1TXxOiyQj5/9XyyAj7uunoBo8uGOfKns23ZSOd38jGf18uSORM4u6rMcXzupFEp6d38TjcDnAnfBtNu3o17j7PjUH2yKS6aVc2f/u0z5GQFHGfOOXs0n196Lk+u3MI3H3zJEbs3tXfz5Fsfct9nL3N1eKhrhvQIHA/huKUQqXHnS+/voi8cwRJvXFWOdAbL39/D0vOn4/F4HL4Nxcf+SJSTLZ0YhoGmaRTlh+Jt6Tb1A3R099HWZT7kV5CTRV7IfSHvVsd0cXRVWSFfv2kxIPB6PJQV5abYGcpaJhOhrAA//OLStHkHstcXjtDQ3CnbQJAd9FNRnE8oqQ8O1reYYdDS0UN3bz9grglLCnLwenTXdvNuO1iLIZ95l8WQFfBz3x2XZrx4tyyZyf7jJ3ng2TWyM5lT4lub9nHPrRcT8PvYcqCWJ9/a7HBQ1zX+4drzGD2iOKVyFjsOn+DF93bQ2WN+n3XS6OHcedU8l+pa07HLmaSG6u7t540N++J1tH/P1+kIrNtVQ21ja4qPA5XT3dvPBzuP8vSqrWzed5z27j6z7YDsoJ+p48q56eKZXD7vLLKD5ve5u3vDvLR2J0+t3MLhulP0hc2FZ8DvZWRpAZfMncTNS2YxsrTAdbRz6xQnmjp4fvU2Xt+wl8O1TfSFI+a3N3WNorwQF86q5vbLz+HsquF4PKlfV80ksO6+MPWn2tl2sJZjDS2cbO1iRnUltyyZia7r9EciPPfONrYfrsd6IHpGdQW3XjrHdQCIGYKtB46z7K3NrN56iLbO3ngb+LweCnKCnD99HF+4egHTxpfjlf6mE79hGOypaeDpVVtZtfkAp9q6CEfN70J7dJ2CnCAzJ4zkEwuncOGsanKyAvGB2nuqzfxSSnyUFVCYm82Y8qKMnUDXde6+cRGHapvYtPcYhhD09Uc48NEpunrDBPw+jtQ18egr663qAwKPrnPDomnxjmZVxqrQ9oO1XPPtR+no7o2vCZacM4nPXnlO4gG2eEfGMaq7NY71edfhOvYdb0yIR84MY8uLOHaylVgshjVjNLd3887m/dx5dapgk6dvi3W7arjv4RVsP1RPIoZLtGt7dx8nmjtYuXEfi2ZN4Nff+BTN7V3c/cuX2LL/eIpfACdbOti87zgPLl/L125YxJevPZ+g3/mTl/ZOEYnG+NPrG/nxslU0t3e7+tHW2cvhuiYef2MTt10+l3+5/RKG5SYe4063CK071c7jb2xi+XvbOVTXFF+MIzSuv2gmtyyZCcAvn17Nvz+xyrIGaFy3aBqfvmR2ynU51dbJ9/7wFs+s2kI0Zvv6sLzu/ZEoXb39/PntD3lh9XZuXjKb7991RcqMaV2X1o4efrzsbf742kb6IxGzn4AjHG7t7OHoiRZeWLONiaOG8/27ruSSuRNNMWQH/LYGAzSNlo4e9tY0MH/KmIxT3bDcbB77t1vp6TN3D2KGgWEICnKzbBk02+Cb2kkdOwLAinV7TCGQmseJu1/p4tLla/cQjUYd4vH7vHzvziV86+E3qW9qix83hOD5Ndu546r58cedHSXbRmTDMHjyrU18++HXzOlYkCTQJD+B1VsOsPSbj9DZ3Wfe53DkESl52zt7+MEf32R3zUl+efe1ZPl9KSNjfyTKP//qZf789ubE+s+yK0TKoNEfjvC7FevZdrCOR791IyPLilzbLxozWPbmJu5/bKXjnoxzm9H8t7u3n9c37LcKJhl7yFx3qo0771/G5v21KenioWvcb41wNMZjb2zkYO0pHv32zZQV5TkGptaOHm753p/YsPuYzT/LDrZBJuHb/uON3P7DJ/jVN67nU4umo1eU5Nt2Vsy/3nCEr/zns7y6bjfRWOLnNtw6hdfjIS8UJC8UpCAni6L8EB7HL0Uk70gl8ibbEoaBOVPZ07qNVCLhc4ZdAqux2jt7eHvzAWceIaiuLOaScyZzwYyxKTa3HT7JkbpTDv+SO6AQgm0HPuI7f3hLCiHRhg57ye0gBEfrmmhq60qci5edmhZAGAbPv7OF/3h8lZzFEkRjMX6y7B2eXLmJWMza1IDEzVJS6m6Vu3nvMe5+ILH2s++MGYbg3x9byT0Pvmz6KtLVzSQSjdLTH8axDe9yfXr7zf5lCkGk9S2e3/a3fucR7n341fjMpGka0ZjBPb9+UQohyY69fKt9bef6w1H+9eFXqT3Vjvf8aWPJDQXp7Ol3OHGsoYU7friMsRVF3HrJHJbMncC4imKy4jOJZc99NyCOdWwo62NseUSajLZwIrns5MX7B7uO8lFja9IoAddeMI1AwM/1i6fz9KqtWHe0QaO7t59X1u3h7puGx+0mzz6xmMHPnl5DizVias6Rp2RYDrnZAXr6wpxs7UoMJtbiX9hGL8yYtmRYDgGvh8a2LrnVTSKNEPxuxToWTKniinPPitf1vW2H+c2La+XXnRM+DC/M5ZrzpzB74kh0TWPrwTqWv7eDhuYO2THMWXv11kM8/OJavn7zRY7RdtmbG/nV8++bIUzytbWKcRusHGlTB8CHlr/P+9uPJAzJ4GHquHI+ecFUpowdAcDempM8++5Wdh1psNnSeHXdLt75cBZLZHizee8xVnyw22EPASOK87h20TRmVlei6xp7jjbw3OrtHGtocfh2srWTl97fgXdseTHXnD+VZfGFbryWGIbBodpTfO8Pb/CjJ95mfGUxC6eP4/J5Z3HO2aMJ+r0pHS+54omRydlI6WLTuJPC6Yt7MvdZwR7GxGIxXl67m2g05ggXcrL8XDKnGiEEM8aPoGpEIUfrmxIXWMBr6/fyhavnE8oOxv21+113son3dx2zlw5CI5Tt55ufvpgbLpxBXihIV1+Ytzbs4zuPvk5rhy3UsMWyU8aO4Pt3XcnkMWUEfF5qG9v48bJVvLJuN8IQ8Xbs6w/zq2dXs2RONV6Ph2g0xi+fe5++vrAjHJ09sZKHv3kLY8sL4+1xw0Uz+eInFvDVnz3L+zuOxkdKgeDR1zZz++VzKCoww4/Glg5++tTqRGQgEoJEM38f1ef1MLK0gAkjS1LHOiu9Zj8kaGju4PevbXRcC69H52s3LuLuGxcTCvrj/i6ZM5HPXDaHr/yfZ1i56QCCxKL7qbe3cOHsajy6zrtbDhKNGY4BsbQwhxf/4y6qR5bE7V17wTQ+f/W5XH/f79lTc9Ixc249UItX1zV++IWr2Hf8JB/u/SgxGssNIov+SJTdRxvYfbSBh5Z/wLC8bC6cNZ5PLJzGopnjyA9lOSqduOD20TgxaqbbDbAupts6A8chzf2UbZYSQtDc1sma7UcTvkhmTxzJxNFlaJpGQV4Ol8yZyCMvNzts7qo5yZ6aE8w9O3XtBLCjpomOzm5btUyf7rv9Er587cJ4ulBWgNsun0so6OMLP3kmsfiUjCor5Mnv3MbI4YXxY/k5WTx8zw3cfn+Etzftd6T/8OAJjtU1Mn50OXuPNbBp7zFHO1WWDuORe25kbEViE8Tyv6qskD/e92ku/cYjHLaFgfWNLbyz5Qg3XmwuhF9bt4vaU4l1lNV2WUE/1y2ezp1XzmPymDKCfpcfOnAMfM4Z++W1O6g71e4Q1g0XzeLezyyJb2Xb/S3MC/HovZ/myIlmx7gY8HvR5Sx9+5XzWHr+VEd3GJaXTWVJQYq9EUX5zJ440hSDLUdnT7/5OEZ+TpAnv3MHt142m4DPi2vMbo/DNGjt7OaFNdu58/5lXPb13/DGhr3EDCN1xBdCjoC2+CR+Kt2onxR729xwpLGtQ9LdaVy7s4aTzW0pMe7V500m4PchhLnDdfV5k/H7vI6YvbfPvh3r9FsIwa5DH6X4lJ8b4ur5Zzlib8unhVNHU1yQm2gXyazxZVSWDkspIysY4PZLZyVCH1mHcCTC0cbO+JqlLxy1hV6C6xdPo0re+Ez2QQjBsLwcbrxwms0Pc3Rcv+cYhryGq7YcTInjdU3jZ//rOn7x1euYPXFk/Fm0lHbXkq6drU6vrtudCEc18Ho9/MO1C/B4PA5bdp9DWQGmji1n6jjzb8rYEVRXJkb8iuJ8ptnOTR1XTkVxvsNeNGbQ1dPPk29t5uW1uxK+xQdp2//pVjoshwe+fj2fXzqfR19Zz1sb95PYdiU1ZpTHDSHMVfkPnuBbt17M3TctRrenPa0bOva5NfWQY00RLybVVjQa44X3djn9EILcnGwunTvBEVLNGD+C8ZWl7Dlaj30HbMUHu/n6TYsJZQXj5cRnnfZO7DE/aORk+SmUHT75PkB+Xg7F+SEaW9od7VJRUuC6ZSuEoCg3iMfjIRZNdFqEoK2zB4BDH52UM01iDdIXjvLUyg/TtK1JY1sXuq45fhBxz9EG+vvDGAKON3bgmH2FYMH0cVx/4TR0faAvYznXZhb94QhbD9Zj39U5a3QZY0YUxdvL3m7JbWFPY39vF471uamti5qGVo7UN7HjcD27jpxg15ETtHT0JM1cVrvJR7jtxmdOqOSXd19Pa1cP63fWsHrbQdZsOUR9c4e54+CopIh/jkRi/GTZKsqL87h5yezU1hfCVRjpb6nbFpiuuKrEYfejxlY2WA/l2ep53pRRVA4viqfTNI2cUDaXnzOBPUfqsf9PP8dOtrF5Xy2LZ1U77JvrEcMZAgrn+eR6aZqGpmvOtY6mkZMVTMln+Wb7YAstEi3QHdFsbWuee+jFtanNY63Bkn5aPjHbazS2dhKORIgamA9Dajiu25QxZXh0PaWzug9qqTP68YYWwlHDVqagsiSPQFKo5XY/x/7ZrTxN02jt7GH5mh08+85WDnx0is7efiJR25rHqrPdN/m5tz9iPsJtKcsqRNc1CnOzWXreZJaeN5loLMah2iY27jnGqg8PsGbbIdq7+nCMHJr5zPxPlq3imvOnpt5Cd9lKTd+Q0qBLPkclXEYfu713thyU25eJzuLxePjkwmmu5V593mQeenk9PT398cVtOBJlxQe7uGDGuJR9eIefadYw7lVLXkelS+Yyw9qOGYZBS3uXzZ49vd20bSASSX5aa0RpTwhBT1+Yjq6+lHJ93tSYPu0Mb9m1nWpp70zsTEnfggGfbdLOfK8oXV/RNI0dh+r40k+fYd+xk65pHCKw+ybbraO7zzkzJBdsvXp0nYmjSpk4qpTPXDaHpvZufvHsGn63Yj3hiPzfdeTFPd7YxobdR7lw9kTLKrZSHWVZFUldO8i0aTtLqsqTR5NYLMYLa3baspijpmEY/OiJVfzs6dUpVpKaBVwAAAyoSURBVKMxg3A4QvIDfG9t2s+/dPZQmBdK76tVRlL9LFxnPpf2yDziuq277OcyEN/dSboe1hrJNXtihkyuW9oZzO5Ukvs+r7X7mEhv+w27tPbStY3lx6HaU9z6vceoPdVOPKJwzGqCUFaAorwQ86dUoWsaT729JVFHibe9q5f+iPMmTtDvJT8nK+1UXzosl+9//kpCwQA/fXKVo9aGIdhb08BFcyZZOWSZzlHC7QaW7aytJVLaKmm2cPpovR470cSm+N3NRCcQAmri+8zpcNqvb+pg/c4jXHXe1AwzmdOvjOkcaQeaISXJIz/mIzFFBTm2NjLP//CLS6kozstsz4WsgJ/s7Cw6e+XDl9ZjL0llu90wTTrg9FtSVlyAz6sTi0lBCI2GZnO28OvuD8+52k/iP596RwoB7O059+zRXDB9HLMmVjK2vJiKknxysgL86rk1Sb7JNcN9j7zKn1dtcRifPq6cN//vV+JP97ktlnRd47NXniPFYNkzK9jVH0UIgc9r3Yl2rheitlVbckXjMZ5tdtA1DccPa8XjZucoY/kH8OLavUTDYVt4YKt3mvWLcxZL2DdiBs++s5UrF0xxv/CDCHlSi3L64HbhU3donDOsBpTkeGx2zPR52T4+sXCqw17yoJMu9HAOStaMkXn0dg9hrOuXSFdeUkBelo++/sR12X74BLUnWxhbWTpE30xaOrrljp+tXTS4/4tX8aVPLHBseKSgOa+3PmXsCAxDOP721DSwdvvhRJ40xhpaOp2LQdl5QwHz2ZnSghx0XY87CWAI2H/8VMp2H5i36XcfbUjYkq/52T50PSkeTrNNq2kavf1h3ti4N6nimt1kGuwJbB1Pgw376qhtbMuczcVw8k5H2pJd1yPpfEvkmVpdhTc+6Jh+v7R2N/39Yde43t45ojGDcCQa/4smPeZhtpn8S9No6Tubu7/nTRvnONbX18dza3amrBeS2yIWM4hEY44/IQRH6prMR0BsNocX5vGpRdPQdedgbtlMREJOH72LZ1UTCvrp7uuPH+yPxPjGAy/yzA8+S/XI0pTGBGjv7uNHj68k3mlkGKRpGuMrzKc9RxTnMyw3Sz5BaSKEwVNvb+HGi2eRHUz88G7MMPjTG5vYcajOSiknFI2ZE0Y6Y02BTdWpjbfzUL28he8cSYN+r7yPkplozKDb+p6GLKuprYvVWw5w+5XnOhPbd3IydBiXo4486XZQUnHmmTVhBKXDcqmPP24ieG/bYZ5ZvYPbLpvj6KzWazgS5ZfPvc/jb2yMPy4N8Nkr5nLv7ZemKc99JnBb9JoDpIbbI/JXzT+Ll9fuNp+vknkeXL6WhdPHcu7kKtcQ6UhdE19/YDl7axKLY4+u8fQPPmtuK9vXTQJ8Pg8ej57im/V5T01Dol62Gdo7obKEC2aM4/W/OEfSmhPNLP3mI3z6ktlcNHsClSUF5GQFqG1qY9Pe4zz2+kb2HG1IGaWL87M5Z3IVAOXFeUwcNZx1Ow47QoKNe49z5/3LuOvq+VSUFtDU1sWTK7ew/L3tNg/M9IGAjwVTqpzXJ8MIKoRgxbrd5lRssxP0e/nDvTczrXpk2rwWDc0d3PLdx2hs7YxXLWYYvLR2F5++dC5e265KcuyfzOBGexyDTbobiG7llBUXsmTWOB5788O4P5FojHsfWkFLRw93XDGX/JD5PyQZhqCpvYsfPf42f3p9I4bNvu7xsGROtYu/7vVz62iOPGlm70UzJ1A1opDDdU3x4+1dvXzu35/kP758DRfPriY76EfDfBJ3877j/NMDyzlY2+SwNaO6knHlxRytb5LRRyxeXm1jGy+s2cHnlp6Lxz4QAO9vO8SqzQcSvtnq6/V4dL79mUtYs+2w8z6CptHY2sXPn17Nz59ZQ8rIFx8wRDw9wG2XzYk/H+/zevn0JbNZt+MIzjgc3tq0n7fijxm4x+poGrMnVDB1fGWaXZyEPavCPT29vLHpoDyVSDO+ophFM6sJBtL/DLx1vCQ/m3mTq1ixdqej3hv31XG0rpHxo8oyjNo4/El+nyjI6X/adK5lOEX05U8u4LUN+xPbyJj3Cb73+9d55KUPOLuqjIDfSzgaY9uBWuej2HJkXDr/bGZMGOVSXLr1ldNn5zawez5NM7/x9+VPnsc9D75str+czRpaOrnz/mVUlhQwvrIYr0en7lQ7+483Jh5fkfY8us5Xb7iA7KCf6lHDKSvMoaahNd4+QsB9j7zKh/s/4tJzJlFSkENDcwfv7zjCC6u3m08YO9ZZcmYAmDpuBD/84lXc+5sV5i8bxK+ThmPki09/0kjSLsOsCRV85boLHJW/duEUHn9zExt21SRHBrYWTRKCZh4LZQf4p5svlMonkUYkpSUxsm7YV0tN/JmbREFLz5tMVjCQdvvYNGva9fl8fPL8yaYYbPvRXT29vLlpP+NHlSXdECPuczLpO7j9YrhjdrKk5rK1n+X/hKpy/vWOy7jn1y8SiUTjHQwNTjS1m794ktw5bTehRpcVcd/tFzvuI8TTODYInL7ZfXDfWEjdJNA0jVsvm8O7Ww/y6rrUn+2vPdVGbWNrwka8rRLtdP2FM1i6YDIAfq+HL1yzgH/77WvyG5smkWiMp1dt5elVW/HoGjH7Fq7DNy1uWrcu6h1XnMNP/uEa8nKykkSQVEHN/l6LN+rUsSP47bdvoTDP+d+gZgX9/PJr1zG+ssQW6yfbFbYyzfMej853P3cZF8ysdklv/TlH4lgsxooPdssvtwjpmyAr6OfyeZMcaZN3bZJ3Ry6YVkVpYT7x52zkaPfKuj22EIzEzGj5BQ47bvbtvg0UfgzLyyEr4Evkc7kuGnDrpbP45q1L8Pt9iXT2zhjXnkic1zTKCvN48J9vYMKoshS78ethDX7xw4nBI2NI59KFhBAEfF5+8bVPsWTuxIy+xY9piTTXLJzKj79yjePrn3dcOY+l5002E9nzSZ8TX3ayC8JWpnyvWw2vaxq3XTaXlT//Ctctmk5utv0OsktF5aFhedl88ZoFvPijuxhbXuxoLIvqkSW89OO7uP6iGWQHAy72EiLQdI2Jo0r53b0387ml85O+KGSVHVdDwoKm0dzezbtbDyXVVmNWdQUTRw13pLW/t/9ZFBbks3h6VVI5GruOnmRPTcOA4UyyXWd6+2ybmg8SnauqopRFM8bJeqcvy6PrfOOmxTz5nduYMrbc+XyYvUw5GmYHzO3X1//zS8yfPDr9DCaS8uPs+K71s8/cSX5af4V52Tz2r7fy3c9dzoiifIdvzk5r2hhdVsT9X1zKb795U3wNZJUZCvr53bdv4Z5bLqK4IOS0Za+DHCCCfq/taduEj16rcpbxCSNLeeRbN3G4rolVmw+wYc8x9hxtoKPbvD2v6xolBTlMGFnC7EmjuGzeJKrKCh3Tpr3RrM/lxfk8fM9N7D3WELdb29hGQ0sHoWCAEcV5TB5TxsLp47hgxnjyXX4ZQtPgotnVDB+W4zheNsycjTp6+lk0fRxi2lhbJrhs7sT4WsHum+WvW0fQNPjc0nMJBAKOi6Np0NVrfs113pSxxHDuThXkZeP1JIUbEl3TuHrB2cwaX+44Pq26wtUvgIDPywN3X8f508ayp6YBYQg0DcaUp/7uka5rXDR7AvPOrmLNtkO8vHYn2w7W0y5/cSOU5WfiqFKmj6/gyvmTOatqOB499bqFsgJcf9EMOrv7HfZnTxyZktaO3+flmvOn0HDWaMfxWRMrQUt9tijo9/HVGxZxw0UzWbX5ACs37edgbSOtHb14PBoVJQWcVVXGBdPHsnhmNYV52a7XSwiB16Nz7+2XcNsVc3l78wHWbj9MzYmW+HerPR6dssJcpo2v4NK5E6lvaufPq7YSlr/8N2Fk6f8//42VQjEQQ/tv7RWK/8EoMSgUEiUGhUKixKBQSJQYFAqJEoNCIVFiUCgkSgwKhUSJQaGQKDEoFBIlBoVCosSgUEiUGBQKiRKDQiFRYlAoJEoMCoVEiUGhkCgxKBQSJQaFQqLEoFBIlBgUCokSg0IhUWJQKCRKDAqFRIlBoZAoMSgUEiUGhUKixKBQSJQYFAqJEoNCIVFiUCgkSgwKhUSJQaGQKDEoFBIlBoVCosSgUEiUGBQKiRKDQiFRYlAoJEoMCoVEiUGhkCgxKBQSJQaFQqLEoFBIlBgUCokSg0IhUWJQKCRKDAqFRIlBoZAoMSgUEiUGhUKixKBQSJQYFAqJEoNCIVFiUCgkSgwKhUSJQaGQKDEoFBIlBoVCosSgUEiUGBQKiRKDQiFRYlAoJEoMCoVEiUGhkCgxKBQSJQaFQqLEoFBIlBgUCokSg0IhUWJQKCRKDAqF5P8BM1rXZRD9NVIAAAAASUVORK5CYII='
                doc.addImage(imgData, 'JPEG', 150, 10, 30, 30);
            
                doc.setTextColor(0);          
                doc.setFontSize(12);
                doc.setFontType("normal");
                doc.text(['Chat Emissão Auto SulAmérica'], 20, 25);
            
                var date = new Date();
                var formatedDate = withZero(date.getDate()) + '/' + withZero(date.getMonth() + 1) + '/' + withZero(date.getFullYear());
      
                doc.setFontSize(11);
                doc.text(formatedDate, 20, 35);
                doc.addImage(imgData, 'JPEG', 150, 10, 30, 30);
            };
            
            var withZero = function(number){
                return number < 10 ? '0' + number : number.toString();
            };

            var stripHtml = function(html)
            {
                if(html.indexOf("mensagens pagamento") >= 0){
                    return tratarHtmlPagamentos(html);
                }

               var tmp = document.createElement("DIV");
               tmp.innerHTML = html;
               return tmp.textContent || tmp.innerText || "";
            };

            var tratarHtmlPagamentos = function(html){
                var texto = "";

                var tmp = document.createElement("DIV");
                tmp.innerHTML = html;

                var parcelas = angular.element(tmp).children();
                var parcelaIndex = 0;

                var obterInformacoes = function(elementos){
                    var textoInformacao = "";

                    var informacaoIndex = 0;
                    for(informacaoIndex in elementos){
                        var informacao = angular.element(elementos[informacaoIndex]).children();
                        
                        if(informacao.length > 0){
                            var label = informacao[0].childNodes[0].textContent;
                            var conteudo = "";

                            if(informacao[0].childNodes.length > 1){
                                conteudo = informacao[0].childNodes[1].innerText;
                            }

                            textoInformacao += label + ': ' + conteudo + '\t';
                        }
                    }

                    return textoInformacao;
                };

                for(parcelaIndex in parcelas){
                    var parcela = angular.element(parcelas[parcelaIndex]).children();
                    var primeiraLinha = angular.element(parcela[0]).children();
                    var segundaLinha = angular.element(parcela[1]).children();
                    
                    texto += obterInformacoes(primeiraLinha);
                    texto += '\n'                    
                    texto += obterInformacoes(segundaLinha);
                    
                    texto += '\n\n'
                }

                texto += '\n';

                return texto;
            };
            
            var doc = new jsPDF(),
              initialLinePosition = 55,
              linePosition = initialLinePosition,
              maxPageLinePosition = 260,
              leftMargin = 20,
              lineSpacing = 10,
              textSpacing = 5,
              maxTextLength = 170,
              fontSize = 12;
      
            addPageHeader(doc);
            
            for (var idx in conversation){
                if(linePosition > maxPageLinePosition) {
                    doc.addPage();
                    linePosition = initialLinePosition;
                    addPageHeader(doc);
                }

                doc.setTextColor(0);
                doc.setFontSize(fontSize);
                doc.setFontType("bold");
                
                for (var cv in conversation[idx].output.text){
                    var text = stripHtml(conversation[idx].output.text[cv]);
                    var strArr = doc.splitTextToSize(text, maxTextLength);
                    
                    doc.text(strArr, leftMargin, linePosition);
                    linePosition += textSpacing * strArr.length;
                }
                
                var date = new Date(conversation[idx].dateHour);
                var formatedTime = withZero(date.getHours()) + ':' + withZero(date.getMinutes()) + ':' + withZero(date.getSeconds());
                
                doc.setTextColor(150);
                doc.setFontSize(fontSize);
                doc.setFontType("bold");
      
                var textName;
                if(conversation[idx].type == 'corretor'){
                  textName = conversation[idx].corretor.nome;
                } else {
                  textName = conversation[idx].atendente.nome;
                }
      
                doc.text([textName + ', ' + formatedTime], leftMargin, linePosition);
                linePosition += lineSpacing;
            }
      
            doc.save('chat.pdf');

            //add analytics event
            ga('send', 'event', 'Chat Corretor', 'salvar-pdf', 'Salvar PDF');
        }
    };
});