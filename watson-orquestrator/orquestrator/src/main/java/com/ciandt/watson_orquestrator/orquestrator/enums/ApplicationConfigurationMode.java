package com.ciandt.watson_orquestrator.orquestrator.enums;

/**
 * Created by rodrigosclosa on 19/12/16.
 */

public enum ApplicationConfigurationMode {
    DEVELOPMENT, HOMOLOG, PRODUCTION, LOCAL
}
