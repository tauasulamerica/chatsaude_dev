package com.ciandt.watson_orquestrator.orquestrator.resolver.memcache;

import com.ciandt.watson_orquestrator.orquestrator.constants.Constants;
import com.ciandt.watson_orquestrator.orquestrator.factory.memcache.MemcacheFactory;
import com.ciandt.watson_orquestrator.orquestrator.model.memcache.SFCorretorModel;

import java.util.logging.Logger;

/**
 * Created by rodrigogs on 10/03/17.
 */

public class SFCorretorCacheResolver {
    private static final String CLASS_NAME = SFCorretorCacheResolver.class.getName();
    private static Logger log = Logger.getLogger(SFCorretorCacheResolver.class.getName());
    private MemcacheFactory<String, SFCorretorModel> memcacheFactory = null;

    public SFCorretorCacheResolver() {
        log.entering(CLASS_NAME, "<init>");
        memcacheFactory = new MemcacheFactory<String, SFCorretorModel>(Constants.Memcache.EXPIRATION_CORRETOR);
        log.exiting(CLASS_NAME, "<init>");
    }

    public SFCorretorModel getCorretor(String key) {
        log.entering(CLASS_NAME, "getCorretor");
        SFCorretorModel retorno = null;

        if(memcacheFactory != null && key != null) {
            retorno = memcacheFactory.getFromCache(key);
        }

        log.exiting(CLASS_NAME, "getCorretor", retorno);
        return retorno;
    }

    public void saveCorretor(String key, SFCorretorModel sfCorretorModel) {
        log.entering(CLASS_NAME, "saveCorretor");
        if(memcacheFactory != null && key != null) {
            if(!memcacheFactory.exists(key)) {
                memcacheFactory.saveToCache(key, sfCorretorModel);
            }
        }
        log.exiting(CLASS_NAME, "saveCorretor");
    }
}
