package com.ciandt.watson_orquestrator.orquestrator.resolver.config;

/**
 * Created by rodrigosclosa on 19/12/16.
 */
public class SensediaConfig {
    private String baseURLSensedia;
    private String clientId;
    private String clientSecret;
    private String grantType;
    private Integer timeout;
    private Integer tentativas;
    private Integer intervaloTentativas;
    private Integer tempoMinimoTentativa;

    public SensediaConfig() {
    }

    public String getBaseURLSensedia() {
        return baseURLSensedia;
    }

    public void setBaseURLSensedia(String baseURLSensedia) {
        this.baseURLSensedia = baseURLSensedia;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getGrantType() {
        return grantType;
    }

    public void setGrantType(String grantType) {
        this.grantType = grantType;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public Integer getTimeout() {
        return timeout;
    }

    public void setTimeout(Integer timeout) {
        this.timeout = timeout;
    }

    public Integer getTentativas() {
        return tentativas;
    }

    public void setTentativas(Integer tentativas) {
        this.tentativas = tentativas;
    }

    public Integer getIntervaloTentativas() {
        return intervaloTentativas;
    }

    public void setIntervaloTentativas(Integer intervaloTentativas) {
        this.intervaloTentativas = intervaloTentativas;
    }

	public Integer getTempoMinimoTentativa() {
		return tempoMinimoTentativa;
	}

	public void setTempoMinimoTentativa(Integer tempoMinimoTentativa) {
		this.tempoMinimoTentativa = tempoMinimoTentativa;
	}
}
