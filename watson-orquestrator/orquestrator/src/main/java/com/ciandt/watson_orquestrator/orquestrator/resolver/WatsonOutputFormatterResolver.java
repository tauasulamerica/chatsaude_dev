package com.ciandt.watson_orquestrator.orquestrator.resolver;

import com.ciandt.watson_orquestrator.orquestrator.entity.MapeamentoCampos;
import com.ciandt.watson_orquestrator.orquestrator.entity.MapeamentoServicos;
import com.ciandt.watson_orquestrator.orquestrator.exceptions.EmptyErrorServiceException;
import com.ciandt.watson_orquestrator.orquestrator.exceptions.GeneralErrorServiceException;
import com.ciandt.watson_orquestrator.orquestrator.exceptions.NotFoundServiceException;
import com.ciandt.watson_orquestrator.orquestrator.exceptions.SaSServicesException;
import com.ciandt.watson_orquestrator.orquestrator.model.sulamerica.MensagemAlternativaBaseOutput;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import io.swagger.client.sensedia.watson.StringUtil;
import io.swagger.client.sensedia.watson.model.OutputData;

/**
 * Created by rodrigogs on 09/01/17.
 */

public class WatsonOutputFormatterResolver {
    private final String fieldNameCritica = "critica";

    public WatsonOutputFormatterResolver() {
    }

    public OutputData onSuccess(MapeamentoServicos servico, Object outputValues, OutputData originalOutput, List<MapeamentoCampos> camposSaida) throws IllegalAccessException {
        OutputData retorno = originalOutput;
        List<String> textos = new ArrayList<String>();

        //Pega sempre a posição 0 que é a mensagem de sucesso
        String texto = retorno.getText().get(0);
        String critica = "";
        
        if(originalOutput.getMensagensAlternativas() != null 
        			&& originalOutput.getMensagensAlternativas().size() > 0
        			&& outputValues instanceof MensagemAlternativaBaseOutput) {
        		
        		MensagemAlternativaBaseOutput mensagemAlternativa = (MensagemAlternativaBaseOutput)outputValues;
        		if(mensagemAlternativa.getIdentificadorMensagem() != null 
        				&& !mensagemAlternativa.getIdentificadorMensagem().isEmpty()
        				&& originalOutput.getMensagensAlternativas().get(mensagemAlternativa.getIdentificadorMensagem()) != null){
        			texto = originalOutput.getMensagensAlternativas().get(mensagemAlternativa.getIdentificadorMensagem());
        		}
        }

        for (MapeamentoCampos campo : camposSaida) {
            for (Field field : outputValues.getClass().getDeclaredFields()) {
                if(field.getName().equals(campo.getVariavelOrigem())) {
                    // make private fields accessible
                    field.setAccessible(true);

                    Object paramValue = field.get(outputValues);

                    if(paramValue != null) {

                        //Regras de formatação de retorno
                        if(field.getName().equals(fieldNameCritica)) {
                            critica = ((String)paramValue);
                        } else {
                            //@@saude]
                            if (texto.indexOf(campo.getCampoValorDestino()) >= 0) {
                                texto = texto.replace(campo.getCampoValorDestino(), (String)paramValue);
                            }
                            else
                            {
                                texto = texto + (String)paramValue;
                            }

                        }
                    }
                }
            }
        }

        textos.add(texto);

        if(critica != null && critica != "") {
            textos.add(critica);
        }

        //Pega o último texto do array para adicionar a frase de continuidade
        if(originalOutput.getText().get(retorno.getText().size()-1) != null) {
            textos.add(originalOutput.getText().get(originalOutput.getText().size() - 1));
        }

        if(textos != null && textos.size() > 0) {
            retorno.setText(textos);
        }

        return retorno;
    }

    public OutputData onError(SaSServicesException tipoException, OutputData originalOutput) {
        final Integer posicaoNotFound = 1;
        final Integer posicaoGeneralError = 2;
        final Integer posicaoEmptyError = 3;

        OutputData retorno = originalOutput;

        List<String> textos = new ArrayList<String>();

        if (tipoException != null) {
            if(tipoException instanceof NotFoundServiceException) {
                if((retorno.getText().size()-1) > posicaoNotFound) {
                    textos.add(retorno.getText().get(posicaoNotFound));
                }
                else
                {
                    textos.add("Desculpe, houve um erro na consulta (NOT FOUND)");
                }
                retorno.setText(textos);
            } else if (tipoException instanceof GeneralErrorServiceException) {
                if((retorno.getText().size()-1) > posicaoGeneralError) {
                    textos.add(retorno.getText().get(posicaoGeneralError));
                }
                else
                {
                    textos.add("Desculpe, houve um erro na consulta (SERVICE ERROR)");
                }
                retorno.setText(textos);
            } else if (tipoException instanceof EmptyErrorServiceException) {
                if((retorno.getText().size()-1) > posicaoEmptyError) {
                    textos.add(retorno.getText().get(posicaoEmptyError));
                }
                else
                {
                    textos.add("Desculpe, houve um erro na consulta (EMPTY ERROR)");
                }
                retorno.setText(textos);
            }
        }

        return retorno;
    }

    public String htmlToText(String source) {

        String result;

        // Remove HTML Development formatting
        // Replace line breaks with space
        // because browsers inserts space
        result = source.replace("\r", " ");
        // Replace line breaks with space
        // because browsers inserts space
        result = result.replace("\n", " ");
        // Remove step-formatting
        result = result.replace("\t", "");
        // Remove repeating spaces because browsers ignore them
        result = result.replaceAll("( )+", " ");

        // Remove the header (prepare first by clearing attributes)
        result = result.replaceAll("<( )*head([^>])*>","<head>");
        result = result.replaceAll("(<( )*(/)( )*head( )*>)","</head>");
        result = result.replaceAll("(<head>).*(</head>)","");

        // remove all scripts (prepare first by clearing attributes)
        result = result.replaceAll("<( )*script([^>])*>","<script>");
        result = result.replaceAll("(<( )*(/)( )*script( )*>)","</script>");
        result = result.replaceAll("(<script>).*(</script>)","");

        // remove all styles (prepare first by clearing attributes)
        result = result.replaceAll("<( )*style([^>])*>","<style>");
        result = result.replaceAll("(<( )*(/)( )*style( )*>)","</style>");
        result = result.replaceAll("(<style>).*(</style>)","");

        // insert tabs in spaces of <td> tags
        result = result.replaceAll("<( )*td([^>])*>","\t");

        // insert line breaks in places of <BR> and <LI> tags
        result = result.replaceAll("<( )*br( )*>","\r");
        result = result.replaceAll("<( )*li( )*>","\r");

        // insert line paragraphs (double line breaks) in place
        // if <P>, <DIV> and <TR> tags
        result = result.replaceAll("<( )*div([^>])*>","\r");
        result = result.replaceAll("<( )*tr([^>])*>","\r");
        result = result.replaceAll("<( )*p([^>])*>","\r\r");

        // Remove remaining tags like <a>, links, images,
        // comments etc - anything that's enclosed inside < >
        result = result.replaceAll("<[^>]*>"," ");

        result = result.replaceAll("&bull;"," * ");
        result = result.replaceAll("&lsaquo;","<");
        result = result.replaceAll("&rsaquo;",">");
        result = result.replaceAll("&trade;","(tm)");
        result = result.replaceAll("&frasl;","/");
        result = result.replaceAll("&lt;","<");
        result = result.replaceAll("&gt;",">");
        result = result.replaceAll("&copy;","(c)");
        result = result.replaceAll("&reg;","(r)");
        // Remove all others. More can be added, see
        // http://hotwired.lycos.com/webmonkey/reference/special_characters/
        result = result.replaceAll("&(.{2,6});", "");

        // for testing
        //result.replaceAll(
        //       this.txtRegex.Text,string.Empty,
        //       System.Text.RegularExpressions.RegexOptions.IgnoreCase);

        // make line breaking consistent
        result = result.replaceAll("\n", "\r");

        // Remove extra line breaks and tabs:
        // replace over 2 breaks with 2 and over 4 tabs with 4.
        // Prepare first to remove any whitespaces in between
        // the escaped characters and remove redundant tabs in between line breaks
        result = result.replaceAll(
                "(\r)( )+(\r)","\r\r");
        result = result.replaceAll(
                "(\t)( )+(\t)","\t\t");
        result = result.replaceAll(
                "(\t)( )+(\r)","\t\r");
        result = result.replaceAll(
                "(\r)( )+(\t)","\r\t");
        // Remove redundant tabs
        result = result.replaceAll(
                "(\r)(\t)+(\r)","\r\r");
        // Remove multiple tabs following a line break with just one tab
        result = result.replaceAll(
                "(\r)(\t)+","\r\t");
        // Initial replacement target string for line breaks
        String breaks = "\r\r\r";
        // Initial replacement target string for tabs
        String tabs = "\t\t\t\t\t";
        for (int index=0; index<result.length(); index++)
        {
            result = result.replaceAll(breaks, "\r\r");
            result = result.replaceAll(tabs, "\t\t\t\t");
            breaks = breaks + "\r";
            tabs = tabs + "\t";
        }

        return result;
    }
}
