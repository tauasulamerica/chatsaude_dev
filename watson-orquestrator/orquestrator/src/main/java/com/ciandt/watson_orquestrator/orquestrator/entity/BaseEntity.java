package com.ciandt.watson_orquestrator.orquestrator.entity;

import com.googlecode.objectify.annotation.Id;

import java.io.Serializable;

/**
 * Created by rodrigosclosa on 21/12/16.
 */

public class BaseEntity implements Serializable {
    @Id
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
