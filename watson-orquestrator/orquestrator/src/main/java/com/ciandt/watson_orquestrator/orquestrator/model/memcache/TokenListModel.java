package com.ciandt.watson_orquestrator.orquestrator.model.memcache;

import com.ciandt.watson_orquestrator.orquestrator.enums.TokenTypesEnum;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rodrigogs on 01/03/17.
 */
public class TokenListModel implements Serializable {
    private List<TokenDataModel> tokens;

    public TokenListModel() {
        this.tokens = new ArrayList<TokenDataModel>();
    }

    public TokenListModel(List<TokenDataModel> tokens) {
        this.tokens = tokens;
    }

    public List<TokenDataModel> getTokens() {
        return tokens;
    }

    public void setTokens(List<TokenDataModel> tokens) {
        this.tokens = tokens;
    }

    public void addToken(TokenDataModel tokenDataModel) {
        this.tokens.add(tokenDataModel);
    }
}
