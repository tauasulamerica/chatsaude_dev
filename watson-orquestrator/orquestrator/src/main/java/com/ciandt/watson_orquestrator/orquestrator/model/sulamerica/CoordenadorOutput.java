package com.ciandt.watson_orquestrator.orquestrator.model.sulamerica;

import java.io.Serializable;

/**
 * Created by rodrigogs on 03/01/17.
 */

public class CoordenadorOutput implements Serializable {
    private String status;
    private String critica;

    public CoordenadorOutput() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCritica() {
        return critica;
    }

    public void setCritica(String critica) {
        this.critica = critica;
    }
}
