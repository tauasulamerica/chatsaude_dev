package com.ciandt.watson_orquestrator.orquestrator.business;

import com.ciandt.watson_orquestrator.orquestrator.resolver.ConfigResolver;

import org.glassfish.jersey.internal.util.ExceptionUtils;

import java.lang.reflect.ParameterizedType;
import java.util.logging.Logger;

/**
 * Created by rodrigogs on 24/04/17.
 */

public abstract class BaseBusiness<T> {
    private static Logger log = null;
    protected Class<T> clazz;
    public ConfigResolver<T> configResolver = null;
    public T config = null;

    public BaseBusiness() {
        log = Logger.getLogger(getClass().getName());
        clazz = (Class<T>) ((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        Object instance = instanceNew(clazz);
        configResolver = new ConfigResolver<T>(instance.getClass());

        try {
            config = configResolver.getConfig();
        } catch (Exception e) {
            log.severe("Erro ao recuperar as configurações do BO: " + ExceptionUtils.exceptionStackTraceAsString(e));
        }
    }

    public Object instanceNew(Class<?> clazz) {
        Object o = null;

        try {
            o = Class.forName(clazz.getName()).newInstance();
        } catch (Exception e) {
            log.severe("Erro ao instanciar um novo objeto. " + ExceptionUtils.exceptionStackTraceAsString(e));
        }

        return o;
    }
}
