package com.ciandt.watson_orquestrator.orquestrator.service.sulamerica;

import com.ciandt.watson_orquestrator.orquestrator.constants.Constants;
import com.ciandt.watson_orquestrator.orquestrator.dao.MapeamentoServicosDao;
import com.ciandt.watson_orquestrator.orquestrator.dao.SessaoDao;
import com.ciandt.watson_orquestrator.orquestrator.entity.MapeamentoServicos;
import com.ciandt.watson_orquestrator.orquestrator.entity.Sessao;
import com.ciandt.watson_orquestrator.orquestrator.enums.ActionWatsonEnum;
import com.ciandt.watson_orquestrator.orquestrator.enums.SituacaoDocumentoEnum;
import com.ciandt.watson_orquestrator.orquestrator.enums.TokenTypesEnum;
import com.ciandt.watson_orquestrator.orquestrator.exceptions.AccessTokenNullException;
import com.ciandt.watson_orquestrator.orquestrator.exceptions.GeneralErrorServiceException;
import com.ciandt.watson_orquestrator.orquestrator.exceptions.NotFoundServiceException;
import com.ciandt.watson_orquestrator.orquestrator.exceptions.SaSServicesException;
import com.ciandt.watson_orquestrator.orquestrator.helper.LoggerHelper;
import com.ciandt.watson_orquestrator.orquestrator.model.sulamerica.CoordenadorInput;
import com.ciandt.watson_orquestrator.orquestrator.model.sulamerica.CoordenadorOutput;
import com.ciandt.watson_orquestrator.orquestrator.resolver.config.SaSServicesConfig;
import com.ciandt.watson_orquestrator.orquestrator.service.BaseServices;
import com.ciandt.watson_orquestrator.orquestrator.service.sensedia.SensediaSevice;
import com.google.api.server.spi.response.UnauthorizedException;

import org.glassfish.jersey.internal.util.ExceptionUtils;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import io.swagger.client.sulamerica.auto.ApiClient;
import io.swagger.client.sulamerica.auto.ApiException;
import io.swagger.client.sulamerica.auto.api.EndossosApi;
import io.swagger.client.sulamerica.auto.api.PropostasApi;
import io.swagger.client.sulamerica.auto.model.Criticas;
import io.swagger.client.sulamerica.auto.model.Documento;

/**
 * Created by rodrigogs on 03/01/17.
 */

public class Coordenador extends BaseServices<SaSServicesConfig> implements SaSService<CoordenadorInput, CoordenadorOutput> {
    private static Logger log = null;
    private String accessToken = "";
    private SensediaSevice sensediaSevice = null;
    private EndossosApi endossosApi = null;
    private PropostasApi propostasApi = null;
    private ApiClient clientConfig = null;
    private MapeamentoServicosDao mapeamentoServicosDao = null;
    private SessaoDao sessaoDao = null;
    private final String codigoCriticaEmProcessamento = "235";
    int numRetries = 0;

    public Coordenador() {
        super();
        log = Logger.getLogger(getClass().getName());
        sensediaSevice = new SensediaSevice(config.getBaseURLServicosSaS());
        mapeamentoServicosDao = new MapeamentoServicosDao();
        sessaoDao = new SessaoDao();

        propostasApi = new PropostasApi();
        endossosApi = new EndossosApi();

        clientConfig = new ApiClient();
        clientConfig.setConnectTimeout(config.getTimeout());
        clientConfig.setBasePath(config.getBaseURLServicosSaS());

        propostasApi.setApiClient(clientConfig);
        endossosApi.setApiClient(clientConfig);
    }

    @Override
    public void requestAccessToken(Long idSessao) throws AccessTokenNullException, GeneralErrorServiceException {
        try {
            accessToken = sensediaSevice.getAccessToken(idSessao, TokenTypesEnum.Documentos);

            if(accessToken == null) {
                throw new AccessTokenNullException();
            }
        } catch (UnauthorizedException e) {
            log.severe("Coordenador - requestAccessToken: Não foi possível gerar um token de acesso para Sensedia. Stack trace: " + ExceptionUtils.exceptionStackTraceAsString(e));
            throw new GeneralErrorServiceException();
        }
    }

    @Override
    public CoordenadorOutput integrar(CoordenadorInput objetoEntrada, Long idSessao, String requestId) throws SaSServicesException {
        CoordenadorOutput retorno = new CoordenadorOutput();

        while (numRetries <= config.getTentativas()) {
            try {
                requestAccessToken(idSessao);

                Documento response = null;
                Sessao sessao = sessaoDao.getByKey(idSessao);

                LoggerHelper.getInstance(log).log("Coordenador - Objeto entrada", objetoEntrada);

                if (sessao == null) {
                    log.severe("Coordenador - integrar: Sessão não encontrada para o ID: " + idSessao);
                    throw new NotFoundServiceException();
                }

                MapeamentoServicos servico = mapeamentoServicosDao.getByProperty("codigoIntencao", objetoEntrada.getAction());

                if (servico == null) {
                    log.severe("Coordenador - integrar: Não foi possível recuperar os dados do mapeamento do serviço " + objetoEntrada.getAction());
                    throw new GeneralErrorServiceException();
                }

                if (servico.getCodigoIntencao().equals(ActionWatsonEnum.PROPOSTA_CONSULTA_STATUS.getCodigo()) ||
                        servico.getCodigoIntencao().equals(ActionWatsonEnum.PROPOSTA_CONSULTA_CRITICA.getCodigo())) {
                    response = propostasApi.propostasCodigoNacNumeroDocumentoGet(config.getClientId(), accessToken, String.valueOf(idSessao), requestId, sessao.getCodigoNAC(), objetoEntrada.getNumDocumento());
                } else if (servico.getCodigoIntencao().equals(ActionWatsonEnum.PEDIDO_DE_ENDOSSO_CONSULTA_STATUS.getCodigo()) ||
                        servico.getCodigoIntencao().equals(ActionWatsonEnum.PEDIDO_DE_ENDOSSO_CONSULTA_CRITICA.getCodigo())) {
                    response = endossosApi.endossosCodigoNacNumeroDocumentoGet(config.getClientId(), accessToken, String.valueOf(idSessao), requestId, sessao.getCodigoNAC(), objetoEntrada.getNumDocumento());
                } else {
                    log.info("Coordenador - integrar: Action retornada do Watson diferente de PROPOSTA_CONSULTA_STATUS, PROPOSTA_CONSULTA_CRITICA, PEDIDO_DE_ENDOSSO_CONSULTA_STATUS ou PEDIDO_DE_ENDOSSO_CONSULTA_CRITICA.");
                    throw new NotFoundServiceException();
                }

                if (response == null) {
                    log.severe("Coordenador - integrar: Retorno da execução do serviço vazio ou nulo.");
                    throw new NotFoundServiceException();
                }

                LoggerHelper.getInstance(log).log("Coordenador - response", response);

                SituacaoDocumentoEnum situacao = SituacaoDocumentoEnum.get(response.getSiglaSituacao());

                if (situacao == null) {
                    log.severe("Coordenador - integrar: Situação não econtrada ou inválida. Sigla: " + response.getSiglaSituacao());
                    throw new NotFoundServiceException();
                }

                if (servico.getCodigoIntencao().equals(ActionWatsonEnum.PROPOSTA_CONSULTA_STATUS.getCodigo()) ||
                        servico.getCodigoIntencao().equals(ActionWatsonEnum.PEDIDO_DE_ENDOSSO_CONSULTA_STATUS.getCodigo())) {
                    //Retorna o status do documento
                    retorno.setStatus(situacao.getDescricao());

                    if (servico.getRetornaCritica() && SituacaoDocumentoEnum.Recusado.equals(situacao)) {
                        retorno.setCritica("Motivo da recusa: " + response.getMotivoRecusa().getDescricaoRecusa());
                    }
                } else if (servico.getCodigoIntencao().equals(ActionWatsonEnum.PROPOSTA_CONSULTA_CRITICA.getCodigo()) ||
                        servico.getCodigoIntencao().equals(ActionWatsonEnum.PEDIDO_DE_ENDOSSO_CONSULTA_CRITICA.getCodigo())) {

                    if (situacao.getSigla().equals(SituacaoDocumentoEnum.Pendente.getSigla())) {
                        //Se for pendente, envia direto para o atendente
                        log.info("Coordenador - integrar: Sigla: " + response.getSiglaSituacao());
                        throw new NotFoundServiceException();
                    }

                    //Retorna as críticas
                    List<Criticas> criticas = response.getCriticas();

                    LoggerHelper.getInstance(log).log("Coordenador - Críticas", response.getCriticas());

                    if (criticas == null || criticas.size() == 0) {
                        log.info("Coordenador - integrar: Críticas não econtradas.");
                        throw new NotFoundServiceException();
                    }

                    retorno.setStatus(criticas.get(0).getDescricaoCritica());
                } else {
                    log.info("Coordenador - integrar: Action retornada do Watson diferente de PROPOSTA_CONSULTA_STATUS, PEDIDO_DE_ENDOSSO_CONSULTA_STATUS, PROPOSTA_CONSULTA_CRITICA ou PEDIDO_DE_ENDOSSO_CONSULTA_CRITICA.");
                    throw new GeneralErrorServiceException();
                }
            } catch (AccessTokenNullException an) {
                log.severe("Coordenador: Não foi possível gerar um token de acesso para Sensedia. Stack trace: " + ExceptionUtils.exceptionStackTraceAsString(an));
                throw new GeneralErrorServiceException();
            } catch (ApiException e) {
                if (e.getCode() == Constants.ApiErrorCodes.FORBIDDEN) {
                    requestAccessToken(idSessao);
                    numRetries++;
                    sleep(config.getIntervaloTentativas());

                    integrar(objetoEntrada, idSessao, requestId);
                } else if (e.getCode() == Constants.ApiErrorCodes.UNAUTHORIZED) {
                    log.info("Coordenador - integrar: UNAUTHORIZED. " + ExceptionUtils.exceptionStackTraceAsString(e));
                    numRetries++;
                    sleep(config.getIntervaloTentativas());

                    integrar(objetoEntrada, idSessao, requestId);
                } else {
                    LoggerHelper.getInstance(log).log(Level.SEVERE, "Coordenador - Erro", e.getResponseBody());

                    if (e.getResponseBody().contains(Constants.ApiErrorCodes.ERROR)) {
                        log.info("Coordenador - integrar: Dados não encontrados. " + ExceptionUtils.exceptionStackTraceAsString(e));
                        throw new NotFoundServiceException();
                    } else {
                        log.info("Coordenador - integrar: Problema ao executar a chamada a API de Documentos. " + ExceptionUtils.exceptionStackTraceAsString(e));
                        throw new GeneralErrorServiceException();
                    }
                }
            } catch (SaSServicesException se) {
                log.info("Coordenador - integrar: Retorno do serviço: " + ExceptionUtils.exceptionStackTraceAsString(se));
                throw se;
            } catch (Exception e) {
                log.severe("Coordenador - integrar: Erro geral: " + ExceptionUtils.exceptionStackTraceAsString(e));
                throw new GeneralErrorServiceException();
            }

            return retorno;
        }

        log.severe("Coordenador - integrar: Número de tentativas excedidas. Tentativas: " + numRetries);
        return null;
    }

    @Override
    public Class<CoordenadorInput> getTypeParameterInputClass() {
        return CoordenadorInput.class;
    }

    @Override
    public Class<CoordenadorOutput> getTypeParameterOutputClass() {
        return CoordenadorOutput.class;
    }
}
