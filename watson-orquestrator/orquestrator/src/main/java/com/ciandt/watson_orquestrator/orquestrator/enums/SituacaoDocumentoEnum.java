package com.ciandt.watson_orquestrator.orquestrator.enums;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by rodrigogs on 12/01/17.
 */

public enum SituacaoDocumentoEnum {

    Recusado("RC", "Recusado"),
    Emitido("EM", "Emitido"),
    Pendente("PE", "Pendente"),
    Cancelado("CN", "Cancelado"),
    Criticado("CR", "Criticado"),
    Pagamento_Pendente("PP", "Pagamento Pendente"),
    A_Renovar("AR", "A Renovar"),
    Reabilitada("RB", "Reabilitada");

    private static final Map<String, SituacaoDocumentoEnum> lookup = new HashMap<String, SituacaoDocumentoEnum>();

    static {
        for(SituacaoDocumentoEnum s : EnumSet.allOf(SituacaoDocumentoEnum.class))
            lookup.put(s.getSigla(), s);
    }

    private String sigla;
    private String descricao;

    private SituacaoDocumentoEnum(String sigla, String descricao) {
        this.sigla = sigla;
        this.descricao = descricao;
    }

    public String getSigla() { return sigla; }

    public String getDescricao() { return descricao; }

    public static SituacaoDocumentoEnum get(String sigla) {
        return lookup.get(sigla);
    }
}
