package com.ciandt.watson_orquestrator.orquestrator.model.sulamerica;

import java.io.Serializable;

public class MensagemAlternativaBaseOutput implements Serializable {
    private String identificadorMensagem;

	public String getIdentificadorMensagem() {
		return identificadorMensagem;
	}

	public void setIdentificadorMensagem(String identificadorMensagem) {
		this.identificadorMensagem = identificadorMensagem;
	}
}
