package com.ciandt.watson_orquestrator.orquestrator.model.salesforce;

import java.io.Serializable;
import java.util.List;

public class LiveAgentMessageList implements Serializable {
	private List<LiveAgentMessage> messages = null;
	private Integer sequence;
	private Integer offset;

	public List<LiveAgentMessage> getMessages() {
		return messages;
	}

	public void setMessages(List<LiveAgentMessage> messages) {
		this.messages = messages;
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	public Integer getOffset() {
		return offset;
	}

	public void setOffset(Integer offset) {
		this.offset = offset;
	}
}
