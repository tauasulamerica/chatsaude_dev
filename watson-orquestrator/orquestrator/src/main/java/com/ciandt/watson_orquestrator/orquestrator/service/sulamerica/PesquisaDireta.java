package com.ciandt.watson_orquestrator.orquestrator.service.sulamerica;

import com.ciandt.watson_orquestrator.orquestrator.constants.Constants;
import com.ciandt.watson_orquestrator.orquestrator.dao.MapeamentoServicosDao;
import com.ciandt.watson_orquestrator.orquestrator.dao.SessaoDao;
import com.ciandt.watson_orquestrator.orquestrator.entity.MapeamentoServicos;
import com.ciandt.watson_orquestrator.orquestrator.entity.Sessao;
import com.ciandt.watson_orquestrator.orquestrator.enums.ActionWatsonEnum;
import com.ciandt.watson_orquestrator.orquestrator.enums.SituacaoDocumentoEnum;
import com.ciandt.watson_orquestrator.orquestrator.enums.TokenTypesEnum;
import com.ciandt.watson_orquestrator.orquestrator.exceptions.AccessTokenNullException;
import com.ciandt.watson_orquestrator.orquestrator.exceptions.GeneralErrorServiceException;
import com.ciandt.watson_orquestrator.orquestrator.exceptions.NotFoundServiceException;
import com.ciandt.watson_orquestrator.orquestrator.exceptions.SaSServicesException;
import com.ciandt.watson_orquestrator.orquestrator.helper.LoggerHelper;
import com.ciandt.watson_orquestrator.orquestrator.model.sulamerica.PesquisaDiretaInput;
import com.ciandt.watson_orquestrator.orquestrator.model.sulamerica.PesquisaDiretaOutput;
import com.ciandt.watson_orquestrator.orquestrator.resolver.config.SaSServicesConfig;
import com.ciandt.watson_orquestrator.orquestrator.service.BaseServices;
import com.ciandt.watson_orquestrator.orquestrator.service.sensedia.SensediaSevice;
import com.google.api.server.spi.response.UnauthorizedException;

import org.glassfish.jersey.internal.util.ExceptionUtils;

import java.util.logging.Level;
import java.util.logging.Logger;

import io.swagger.client.sulamerica.auto.ApiClient;
import io.swagger.client.sulamerica.auto.ApiException;
import io.swagger.client.sulamerica.auto.api.DocumentosApi;
import io.swagger.client.sulamerica.auto.model.DocumentoBasico;

/**
 * Created by rodrigogs on 03/01/17.
 */

public class PesquisaDireta extends BaseServices<SaSServicesConfig> implements SaSService<PesquisaDiretaInput, PesquisaDiretaOutput> {
    private final String tipoDocumentoApolice = "APO";
    private final String tipoDocumentoEndosso = "END";
    private final String tipoDocumentoProposta = "PRO";

    private static Logger log = null;
    private String accessToken = "";
    private SensediaSevice sensediaSevice = null;
    private DocumentosApi api = null;
    private ApiClient clientConfig = null;
    private MapeamentoServicosDao mapeamentoServicosDao = null;
    private SessaoDao sessaoDao = null;
    int numRetries = 0;

    public PesquisaDireta() {
        super();
        log = Logger.getLogger(getClass().getName());
        sensediaSevice = new SensediaSevice(config.getBaseURLServicosSaS());
        api = new DocumentosApi();
        mapeamentoServicosDao = new MapeamentoServicosDao();
        sessaoDao = new SessaoDao();

        clientConfig = new ApiClient();
        clientConfig.setConnectTimeout(config.getTimeout());
        clientConfig.setBasePath(config.getBaseURLServicosSaS());

        api.setApiClient(clientConfig);
    }

    @Override
    public void requestAccessToken(Long idSessao) throws AccessTokenNullException, GeneralErrorServiceException {
        try {
            accessToken = sensediaSevice.getAccessToken(idSessao, TokenTypesEnum.Documentos);

            if(accessToken == null) {
                throw new AccessTokenNullException();
            }
        } catch (UnauthorizedException e) {
            log.severe("PesquisaDireta - requestAccessToken: Não foi possível gerar um token de acesso para Sensedia. Stack trace: " + ExceptionUtils.exceptionStackTraceAsString(e));
            throw new GeneralErrorServiceException();
        }
    }

    @Override
    public PesquisaDiretaOutput integrar(PesquisaDiretaInput objetoEntrada, Long idSessao, String requestId) throws SaSServicesException {
        PesquisaDiretaOutput retorno = new PesquisaDiretaOutput();

        while (numRetries <= config.getTentativas()) {
            try {
                requestAccessToken(idSessao);

                String tipoDocumento = "";
                String numeroApolice = null;
                String numeroEndosso = null;

                Sessao sessao = sessaoDao.getByKey(idSessao);

                LoggerHelper.getInstance(log).log("PesquisaDireta - Objeto entrada", objetoEntrada);

                if (sessao == null) {
                    log.severe("PesquisaDireta - integrar: Sessão não encontrada para o ID: " + idSessao);
                    throw new NotFoundServiceException();
                }

                MapeamentoServicos servico = mapeamentoServicosDao.getByProperty("codigoIntencao", objetoEntrada.getAction());

                if (servico == null) {
                    log.severe("PesquisaDireta - integrar: Não foi possível recuperar os dados do mapeamento do serviço " + objetoEntrada.getAction());
                    throw new GeneralErrorServiceException();
                }

                if (servico.getCodigoIntencao().equals(ActionWatsonEnum.APOLICE_CONSULTA_STATUS.getCodigo())) {
                    tipoDocumento = tipoDocumentoApolice;
                    numeroApolice = objetoEntrada.getCodigoApolice();
                } else if (servico.getCodigoIntencao().equals(ActionWatsonEnum.ENDOSSO_CONSULTA_STATUS.getCodigo())) {
                    tipoDocumento = tipoDocumentoEndosso;
                    numeroEndosso = objetoEntrada.getCodigoEndosso();
                } else {
                    log.info("PesquisaDireta - integrar: Action retornada do Watson diferente de APOLICE_CONSULTA_STATUS ou ENDOSSO_CONSULTA_STATUS.");
                    throw new NotFoundServiceException();
                }

                DocumentoBasico response = api.documentosGet(config.getClientId(), accessToken, String.valueOf(idSessao), requestId, null, tipoDocumento, null, objetoEntrada.getCodigoSucursal(), numeroApolice, numeroEndosso, null, null, null, sessao.getCodigoCorretor());

                if (response == null) {
                    log.severe("PesquisaDireta - integrar: Retorno da execução do serviço documentosGet vazio ou nulo.");
                    throw new NotFoundServiceException();
                }

                LoggerHelper.getInstance(log).log("PesquisaDireta - response", response);

                SituacaoDocumentoEnum situacao = SituacaoDocumentoEnum.get(response.getSiglaSituacao());

                if (situacao == null) {
                    log.severe("PesquisaDireta - integrar: Situação não econtrada ou inválida. Sigla: " + response.getSiglaSituacao());
                    throw new NotFoundServiceException();
                }

                retorno.setStatus(situacao.getDescricao());

                if (servico.getRetornaCritica() && SituacaoDocumentoEnum.Recusado.equals(situacao)) {
                    retorno.setCritica("Motivo da recusa: " + response.getMotivoRecusa());
                }
            } catch (AccessTokenNullException an) {
                log.severe("PesquisaDireta: Não foi possível gerar um token de acesso para Sensedia. Stack trace: " + ExceptionUtils.exceptionStackTraceAsString(an));
                throw new GeneralErrorServiceException();
            } catch (ApiException e) {
                if (e.getCode() == Constants.ApiErrorCodes.FORBIDDEN) {
                    requestAccessToken(idSessao);
                    numRetries++;
                    sleep(config.getIntervaloTentativas());

                    integrar(objetoEntrada, idSessao, requestId);
                } else if (e.getCode() == Constants.ApiErrorCodes.UNAUTHORIZED) {
                    log.info("PesquisaDireta - integrar: UNAUTHORIZED. " + ExceptionUtils.exceptionStackTraceAsString(e));
                    numRetries++;
                    sleep(config.getIntervaloTentativas());

                    integrar(objetoEntrada, idSessao, requestId);
                } else {
                    LoggerHelper.getInstance(log).log(Level.SEVERE, "PesquisaDireta - Erro", e.getResponseBody());

                    if (e.getResponseBody().contains(Constants.ApiErrorCodes.ERROR)) {
                        log.info("PesquisaDireta - integrar: Documento não encontrado. " + ExceptionUtils.exceptionStackTraceAsString(e));
                        throw new NotFoundServiceException();
                    } else {
                        log.info("PesquisaDireta - integrar: Problema ao executar a chamada a API de Documentos. " + ExceptionUtils.exceptionStackTraceAsString(e));
                        throw new GeneralErrorServiceException();
                    }
                }
            } catch (SaSServicesException se) {
                log.info("PesquisaDireta - integrar: Retorno do serviço: " + ExceptionUtils.exceptionStackTraceAsString(se));
                throw se;
            } catch (Exception e) {
                log.severe("PesquisaDireta - integrar: Erro geral: " + ExceptionUtils.exceptionStackTraceAsString(e));
                throw new GeneralErrorServiceException();
            }

            return retorno;
        }

        log.severe("PesquisaDireta - integrar: Número de tentativas excedidas. Tentativas: " + numRetries);
        return null;
    }

    @Override
    public Class<PesquisaDiretaInput> getTypeParameterInputClass() {
        return PesquisaDiretaInput.class;
    }

    @Override
    public Class<PesquisaDiretaOutput> getTypeParameterOutputClass() {
        return PesquisaDiretaOutput.class;
    }
}
