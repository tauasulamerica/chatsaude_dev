package com.ciandt.watson_orquestrator.orquestrator.resolver.config;

/**
 * Created by rodrigosclosa on 19/12/16.
 */

public class SalesforceConfig {
    private Integer timeout;
    private String urlLiveAgent;
    private String urlManifestacao;
    private String clientId;
    private Integer tentativas;
    private Integer intervaloTentativas;
    private Integer apiVersion;
    private Integer getMessagesTimeout;
    private Integer tempoMinimoTentativa;

	public SalesforceConfig() {
    }

    public Integer getTimeout() {
        return timeout;
    }

    public void setTimeout(Integer timeout) {
        this.timeout = timeout;
    }

    public String getUrlLiveAgent() {
        return urlLiveAgent;
    }

    public void setUrlLiveAgent(String urlLiveAgent) {
        this.urlLiveAgent = urlLiveAgent;
    }

    public String getUrlManifestacao() {
        return urlManifestacao;
    }

    public void setUrlManifestacao(String urlManifestacao) {
        this.urlManifestacao = urlManifestacao;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public Integer getTentativas() {
        return tentativas;
    }

    public void setTentativas(Integer tentativas) {
        this.tentativas = tentativas;
    }

    public Integer getIntervaloTentativas() {
        return intervaloTentativas;
    }

    public void setIntervaloTentativas(Integer intervaloTentativas) {
        this.intervaloTentativas = intervaloTentativas;
    }

    public Integer getApiVersion() {
        return apiVersion;
    }

    public void setApiVersion(Integer apiVersion) {
        this.apiVersion = apiVersion;
    }
    
    public Integer getGetMessagesTimeout() {
		return getMessagesTimeout;
	}

	public void setGetMessagesTimeout(Integer getMessagesTimeout) {
		this.getMessagesTimeout = getMessagesTimeout;
	}

	public Integer getTempoMinimoTentativa() {
		return tempoMinimoTentativa;
	}

	public void setTempoMinimoTentativa(Integer tempoMinimoTentativa) {
		this.tempoMinimoTentativa = tempoMinimoTentativa;
	}
}
