package com.ciandt.watson_orquestrator.orquestrator.business;

import com.ciandt.watson_orquestrator.orquestrator.dao.AtendentesDao;
import com.ciandt.watson_orquestrator.orquestrator.entity.Atendentes;
import com.ciandt.watson_orquestrator.orquestrator.model.AtendenteConversaOutput;

import java.util.List;
import java.util.Random;

/**
 * Created by rodrigogs on 11/01/17.
 */

public class AtendentesBO {
    private AtendentesDao atendentesDao = null;

    public AtendentesBO() {
        atendentesDao = new AtendentesDao();
    }

    public AtendenteConversaOutput getAtendente() {
        AtendenteConversaOutput retorno = new AtendenteConversaOutput();

        List<Atendentes> atendentes = atendentesDao.listAll();

        if(atendentes != null && atendentes.size() > 0) {
            Random r = new Random();
            Atendentes atendente = atendentes.get(r.nextInt(atendentes.size()));

            if(atendente != null){
                retorno.setId(atendente.getId());
                retorno.setNome(atendente.getNome());
            }
        }

        return retorno;
    }

    public AtendenteConversaOutput pesquisarAtendente(Long id) {
        AtendenteConversaOutput retorno = new AtendenteConversaOutput();

        Atendentes atendente = atendentesDao.getByKey(id);

        if(atendente != null){
            retorno.setId(atendente.getId());
            retorno.setNome(atendente.getNome());
        }

        return retorno;
    }
}
