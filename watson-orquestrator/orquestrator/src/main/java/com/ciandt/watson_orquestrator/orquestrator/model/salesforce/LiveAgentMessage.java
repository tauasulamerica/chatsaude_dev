package com.ciandt.watson_orquestrator.orquestrator.model.salesforce;

import java.io.Serializable;

public class LiveAgentMessage implements Serializable {

	private String type;
	private LiveAgentMessageBody message;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public LiveAgentMessageBody getMessage() {
		return message;
	}

	public void setMessage(LiveAgentMessageBody message) {
		this.message = message;
	}

}
