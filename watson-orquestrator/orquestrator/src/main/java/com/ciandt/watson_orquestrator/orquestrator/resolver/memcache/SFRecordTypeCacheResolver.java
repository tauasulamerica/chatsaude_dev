package com.ciandt.watson_orquestrator.orquestrator.resolver.memcache;

import com.ciandt.watson_orquestrator.orquestrator.constants.Constants;
import com.ciandt.watson_orquestrator.orquestrator.factory.memcache.MemcacheFactory;
import com.ciandt.watson_orquestrator.orquestrator.model.memcache.SFCorretorModel;
import com.ciandt.watson_orquestrator.orquestrator.model.memcache.SFRecordTypeModel;

import java.util.logging.Logger;

/**
 * Created by rodrigogs on 10/03/17.
 */

public class SFRecordTypeCacheResolver {
    private static final String CLASS_NAME = SFRecordTypeCacheResolver.class.getName();
    private static Logger log = Logger.getLogger(SFRecordTypeCacheResolver.class.getName());
    private MemcacheFactory<String, SFRecordTypeModel> memcacheFactory = null;

    public SFRecordTypeCacheResolver() {
        log.entering(CLASS_NAME, "<init>");
        memcacheFactory = new MemcacheFactory<String, SFRecordTypeModel>(Constants.Memcache.EXPIRATION_RECORDTYPE);
        log.exiting(CLASS_NAME, "<init>");
    }

    public SFRecordTypeModel getRecordType(String key) {
        log.entering(CLASS_NAME, "getRecordType");
        SFRecordTypeModel retorno = null;

        if(memcacheFactory != null && key != null) {
            retorno = memcacheFactory.getFromCache(key);
        }

        log.exiting(CLASS_NAME, "getRecordType", retorno);
        return retorno;
    }

    public void saveRecordType(String key, SFRecordTypeModel sfRecordTypeModel) {
        log.entering(CLASS_NAME, "saveRecordType");
        if(memcacheFactory != null && key != null) {
            if(!memcacheFactory.exists(key)) {
                memcacheFactory.saveToCache(key, sfRecordTypeModel);
            }
        }
        log.exiting(CLASS_NAME, "saveRecordType");
    }
}
