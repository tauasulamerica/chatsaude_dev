package com.ciandt.watson_orquestrator.orquestrator.model.salesforce;

import java.io.Serializable;
import java.util.List;

/**
 * Created by rodrigogs on 19/04/17.
 */

public class LiveAgentAvailabilityMessage implements Serializable {
    private List<LiveAgentAvailabilityResults> results;

    public LiveAgentAvailabilityMessage() {
    }

    public List<LiveAgentAvailabilityResults> getResults() {
        return results;
    }

    public void setResults(List<LiveAgentAvailabilityResults> results) {
        this.results = results;
    }
}
