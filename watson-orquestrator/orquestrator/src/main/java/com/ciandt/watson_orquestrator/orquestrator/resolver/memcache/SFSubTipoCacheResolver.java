package com.ciandt.watson_orquestrator.orquestrator.resolver.memcache;

import com.ciandt.watson_orquestrator.orquestrator.constants.Constants;
import com.ciandt.watson_orquestrator.orquestrator.factory.memcache.MemcacheFactory;
import com.ciandt.watson_orquestrator.orquestrator.model.memcache.SFCorretorModel;
import com.ciandt.watson_orquestrator.orquestrator.model.memcache.SFSubTipoModel;

import java.util.logging.Logger;

/**
 * Created by rodrigogs on 10/03/17.
 */

public class SFSubTipoCacheResolver {
    private static final String CLASS_NAME = SFSubTipoCacheResolver.class.getName();
    private static Logger log = Logger.getLogger(SFSubTipoCacheResolver.class.getName());
    private MemcacheFactory<String, SFSubTipoModel> memcacheFactory = null;

    public SFSubTipoCacheResolver() {
        log.entering(CLASS_NAME, "<init>");
        memcacheFactory = new MemcacheFactory<String, SFSubTipoModel>(Constants.Memcache.EXPIRATION_SUBTIPO);
        log.exiting(CLASS_NAME, "<init>");
    }

    public SFSubTipoModel getSubTipo(String key) {
        log.entering(CLASS_NAME, "getSubTipo");
        SFSubTipoModel retorno = null;

        if(memcacheFactory != null && key != null) {
            retorno = memcacheFactory.getFromCache(key);
        }

        log.exiting(CLASS_NAME, "getSubTipo", retorno);
        return retorno;
    }

    public void saveSubTipo(String key, SFSubTipoModel sfSubTipoModel) {
        log.entering(CLASS_NAME, "saveSubTipo");
        if(memcacheFactory != null && key != null) {
            if(!memcacheFactory.exists(key)) {
                memcacheFactory.saveToCache(key, sfSubTipoModel);
            }
        }
        log.exiting(CLASS_NAME, "saveSubTipo");
    }
}
