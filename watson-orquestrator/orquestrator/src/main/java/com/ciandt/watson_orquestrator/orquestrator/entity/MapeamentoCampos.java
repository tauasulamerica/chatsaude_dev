package com.ciandt.watson_orquestrator.orquestrator.entity;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;

import java.io.Serializable;

/**
 * Created by rodrigosclosa on 21/12/16.
 */
@Entity
public class MapeamentoCampos extends BaseEntity implements Serializable {
    @Index
    private Integer codigoIntencao;

    @Index
    private Integer ordem;

    @Index
    private String variavelOrigem;
    @Index
    private String campoValorDestino;
    @Index
    private String tipo;
    private String campoFormulario;

    public MapeamentoCampos() {
    }

    public MapeamentoCampos(Integer codigoIntencao, Integer ordem, String variavelOrigem, String campoValorDestino, String tipo, String campoFormulario) {
        this(codigoIntencao, ordem, variavelOrigem, campoValorDestino, tipo);
        this.campoFormulario = campoFormulario;
    }

    public MapeamentoCampos(Integer codigoIntencao, Integer ordem, String variavelOrigem, String campoValorDestino, String tipo) {
        this.codigoIntencao = codigoIntencao;
        this.ordem = ordem;
        this.variavelOrigem = variavelOrigem;
        this.campoValorDestino = campoValorDestino;
        this.tipo = tipo;
    }

    public Integer getCodigoIntencao() {
        return codigoIntencao;
    }

    public void setCodigoIntencao(Integer codigoIntencao) {
        this.codigoIntencao = codigoIntencao;
    }

    public String getVariavelOrigem() {
        return variavelOrigem;
    }

    public void setVariavelOrigem(String variavelOrigem) {
        this.variavelOrigem = variavelOrigem;
    }

    public String getCampoValorDestino() {
        return campoValorDestino;
    }

    public void setCampoValorDestino(String campoValorDestino) {
        this.campoValorDestino = campoValorDestino;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Integer getOrdem() {
        return ordem;
    }

    public void setOrdem(Integer ordem) {
        this.ordem = ordem;
    }

    public String getCampoFormulario() {
        return campoFormulario;
    }

    public void setCampoFormulario(String campoFormulario) {
        this.campoFormulario = campoFormulario;
    }
}
