package com.ciandt.watson_orquestrator.orquestrator.model.memcache;

import com.ciandt.watson_orquestrator.orquestrator.enums.TokenTypesEnum;

import java.io.Serializable;

/**
 * Created by rodrigogs on 01/03/17.
 */

public class TokenDataModel implements Serializable {
    private TokenTypesEnum tipo;
    private String token;

    public TokenDataModel(TokenTypesEnum tipo, String token) {
        this.tipo = tipo;
        this.token = token;
    }

    public TokenTypesEnum getTipo() {
        return tipo;
    }

    public void setTipo(TokenTypesEnum tipo) {
        this.tipo = tipo;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
