package com.ciandt.watson_orquestrator.orquestrator.entity;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;

import java.io.Serializable;

/**
 * Created by rodrigosclosa on 21/12/16.
 */
@Entity
public class MapeamentoServicos extends BaseEntity implements Serializable {
    @Index
    private Integer codigoIntencao;
    @Index
    private String codigoServico;
    private String servico;
    private String metodo;
    private Boolean retornaCritica;

    public MapeamentoServicos() {
    }

    public MapeamentoServicos(Integer codigoIntencao, String codigoServico, String servico, String metodo, Boolean retornaCritica) {
        this.codigoIntencao = codigoIntencao;
        this.codigoServico = codigoServico;
        this.servico = servico;
        this.metodo = metodo;
        this.retornaCritica = retornaCritica;
    }

    public Integer getCodigoIntencao() {
        return codigoIntencao;
    }

    public void setCodigoIntencao(Integer codigoIntencao) {
        this.codigoIntencao = codigoIntencao;
    }

    public String getCodigoServico() {
        return codigoServico;
    }

    public void setCodigoServico(String codigoServico) {
        this.codigoServico = codigoServico;
    }

    public String getServico() {
        return servico;
    }

    public void setServico(String servico) {
        this.servico = servico;
    }

    public String getMetodo() {
        return metodo;
    }

    public void setMetodo(String metodo) {
        this.metodo = metodo;
    }

    public Boolean getRetornaCritica() {
        return retornaCritica;
    }

    public void setRetornaCritica(Boolean retornaCritica) {
        this.retornaCritica = retornaCritica;
    }
}
