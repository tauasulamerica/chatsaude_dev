package com.ciandt.watson_orquestrator.orquestrator.model.salesforce;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rodrigogs on 19/04/17.
 */

public class LiveAgentAvailabilityMessages implements Serializable {
    private String type;
    private LiveAgentAvailabilityMessage message;

    public LiveAgentAvailabilityMessages() {
        message = new LiveAgentAvailabilityMessage();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public LiveAgentAvailabilityMessage getMessage() {
        return message;
    }

    public void setMessage(LiveAgentAvailabilityMessage message) {
        this.message = message;
    }
}