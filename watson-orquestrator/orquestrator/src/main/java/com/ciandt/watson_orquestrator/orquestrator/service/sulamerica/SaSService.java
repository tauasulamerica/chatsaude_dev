package com.ciandt.watson_orquestrator.orquestrator.service.sulamerica;

import com.ciandt.watson_orquestrator.orquestrator.exceptions.AccessTokenNullException;
import com.ciandt.watson_orquestrator.orquestrator.exceptions.GeneralErrorServiceException;
import com.ciandt.watson_orquestrator.orquestrator.exceptions.SaSServicesException;
import com.ciandt.watson_orquestrator.orquestrator.model.sulamerica.ServiceInputBase;

/**
 * Created by rodrigogs on 03/01/17.
 */

public interface SaSService<E extends ServiceInputBase, S> {
    S integrar(E objetoEntrada, Long idSessao, String requestId) throws SaSServicesException;
    Class<E> getTypeParameterInputClass();
    Class<S> getTypeParameterOutputClass();
    void requestAccessToken(Long idSessao) throws AccessTokenNullException, GeneralErrorServiceException;
}
