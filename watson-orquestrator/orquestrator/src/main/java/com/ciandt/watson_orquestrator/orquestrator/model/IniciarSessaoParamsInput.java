package com.ciandt.watson_orquestrator.orquestrator.model;

import java.io.Serializable;

/**
 * Created by rodrigogs on 03/01/17.
 */

public class IniciarSessaoParamsInput implements Serializable {
//@@saude@@
//    public String codigoNAC;
//    public String codigoProdutor;
//    public String razaoSocial;
    public String dataInicio;
    public Integer duracao;

    public String origem;
    public String carteirinha;
    public String tipoProduto;

    public IniciarSessaoParamsInput() {
    }


//    public String getCodigoNAC() {

    public String getDataInicio() {
        return dataInicio;
    }
    //    }

    public void setDataInicio(String dataInicio) {
        this.dataInicio = dataInicio;
    }

    //        this.razaoSocial = razaoSocial;

    public Integer getDuracao() {
        return duracao;
    }

    //    public void setRazaoSocial(String razaoSocial) {
    public void setDuracao(Integer duracao) {
        this.duracao = duracao;
    }

    //        return codigoNAC;
    //
    //    }
    //        return razaoSocial;
    //    public String getRazaoSocial() {
    //
    //    }
    //        this.codigoProdutor = codigoProdutor;
    //    public void setCodigoProdutor(String codigoProdutor) {
    //
    //    }
    //        return codigoProdutor;
    //    public String getCodigoProdutor() {
    //
    //    }
    //        this.codigoNAC = codigoNAC;
    //    public void setCodigoNAC(String codigoNAC) {
    //
    //    }
    public String getOrigem() {
        return origem;
    }

    public void setOrigem(String origem) {
        this.origem = origem;
    }

    public String getCarteirinha() {
        return carteirinha;
    }

    public void setCarteirinha(String carteirinha) {
        this.carteirinha = carteirinha;
    }

    public String getTipoProduto() {
        return tipoProduto;
    }

    public void setTipoProduto(String tipoProduto) {
        this.tipoProduto = tipoProduto;
    }
}
