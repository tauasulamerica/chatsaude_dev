package com.ciandt.watson_orquestrator.orquestrator.model.watson;

public class DadosCorretorWatsonBean {

    private String nome;
    private String email;
    private String cod_cia;
    private String cod_nac;
    private String cod_produtor;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCod_cia() {
        return cod_cia;
    }

    public void setCod_cia(String cod_cia) {
        this.cod_cia = cod_cia;
    }

    public String getCod_nac() {
        return cod_nac;
    }

    public void setCod_nac(String cod_nac) {
        this.cod_nac = cod_nac;
    }

    public String getCod_produtor() {
        return cod_produtor;
    }

    public void setCod_produtor(String cod_produtor) {
        this.cod_produtor = cod_produtor;
    }
}
