package com.ciandt.watson_orquestrator.orquestrator.model.salesforce;

import java.io.Serializable;

/**
 * Created by rodrigogs on 10/01/17.
 */

public class LiveAgentPostBodyModel implements Serializable {
    private String body;

    public LiveAgentPostBodyModel() {
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

}
