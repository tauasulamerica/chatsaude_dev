package com.ciandt.watson_orquestrator.orquestrator.servlet;

import com.ciandt.watson_orquestrator.orquestrator.business.ManifestacoesBO;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by rodrigogs on 06/03/17.
 */

public class ManifestacoesServlet extends HttpServlet {
    private ManifestacoesBO manifestacoesBO;

    public ManifestacoesServlet() {
        manifestacoesBO = new ManifestacoesBO();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        //Header setado quando vem de uma chamada Cron
        if(req.getHeader("X-Appengine-Cron") == null || req.getHeader("X-Appengine-Cron").equals(false)) {
            throw new ServletException("Chamada inválida para este serviço.");
        }

        if(!manifestacoesBO.integrarManifestacoes()) {
            throw new ServletException("Integração falhou!");
        } else {
            resp.setStatus(HttpServletResponse.SC_OK);
        }

    }
}
