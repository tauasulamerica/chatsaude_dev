package com.ciandt.watson_orquestrator.orquestrator.util;

import com.ciandt.watson_orquestrator.orquestrator.entity.Atendentes;
import com.ciandt.watson_orquestrator.orquestrator.entity.Log;
import com.ciandt.watson_orquestrator.orquestrator.entity.MapeamentoCampos;
import com.ciandt.watson_orquestrator.orquestrator.entity.MapeamentoPayloadManifestacao;
import com.ciandt.watson_orquestrator.orquestrator.entity.MapeamentoServicos;
import com.ciandt.watson_orquestrator.orquestrator.entity.ParametroChat;
import com.ciandt.watson_orquestrator.orquestrator.entity.Sessao;
import com.ciandt.watson_orquestrator.orquestrator.entity.Token;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.ObjectifyService;

/**
 * Created by rodrigosclosa on 29/08/16.
 */
public class OfyService {

    static {
        ObjectifyService.register(Token.class);
        ObjectifyService.register(Log.class);
        ObjectifyService.register(MapeamentoServicos.class);
        ObjectifyService.register(MapeamentoCampos.class);
        ObjectifyService.register(Sessao.class);
        ObjectifyService.register(Atendentes.class);
        ObjectifyService.register(ParametroChat.class);
        ObjectifyService.register(MapeamentoPayloadManifestacao.class);
    }

    public static Objectify ofy() {
        return ObjectifyService.ofy();
    }

    public static ObjectifyFactory factory() {
        return ObjectifyService.factory();
    }

}