package com.ciandt.watson_orquestrator.orquestrator.factory.sulamerica;

import com.ciandt.watson_orquestrator.orquestrator.service.sulamerica.SaSService;

/**
 * Created by rodrigogs on 05/01/17.
 */
public class ServicesFactory {
    private final String packageName = "com.ciandt.watson_orquestrator.orquestrator.service.sulamerica.";

    private static ServicesFactory ourInstance = new ServicesFactory();

    public static ServicesFactory getInstance() {
        return ourInstance;
    }

    private ServicesFactory() {
    }

    public SaSService getService(String serviceName) throws Exception {
        Class<?> serviceClass = Class.forName(packageName + serviceName);
        return getServiceWithClass(serviceClass);
    }

    private SaSService getServiceWithClass(Class serviceClass) throws Exception {
        return (SaSService)serviceClass.newInstance();
    }
}
