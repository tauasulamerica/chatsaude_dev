package com.ciandt.watson_orquestrator.orquestrator.util;

import com.ciandt.watson_orquestrator.orquestrator.enums.ApplicationConfigurationMode;

import org.glassfish.jersey.internal.util.ExceptionUtils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * Created by rodrigosclosa on 19/12/16.
 */
public class ConfigReader {
    private static String filename = null;
    private static final Logger log = Logger.getLogger(ConfigReader.class.getName());

    private static ConfigReader ourInstance;
    private static Properties properties;

    public static ConfigReader getInstance() {
        if(ourInstance == null) {
            ourInstance = new ConfigReader();
        }

        return ourInstance;
    }

    private ConfigReader() {
        //Verifica o modo de config do App para carregar o arquivo de config correto
        String configMode = System.getProperty("ApplicationConfigurationMode");

        if(configMode.equals(ApplicationConfigurationMode.PRODUCTION.name())) {
            filename = "config-PROD.properties";
        } else if (configMode.equals(ApplicationConfigurationMode.HOMOLOG.name())) {
            filename = "config-HOM.properties";
        } else if (configMode.equals(ApplicationConfigurationMode.DEVELOPMENT.name())) {
            filename = "config-DEV.properties";
        } else if (configMode.equals(ApplicationConfigurationMode.LOCAL.name())) {
            filename = "config-LOCAL.properties";
        }

        properties = new Properties();
        InputStream input = null;

        try {
            input = getClass().getClassLoader().getResourceAsStream(filename);

            if (input != null) {
                properties.load(input);
            } else {
                log.info("Não foi possível abrir o arquivo " + filename);
                throw new FileNotFoundException("Não foi possível abrir o arquivo '" + filename + "'");
            }

        } catch (IOException e) {
            log.severe("Instance ConfigReader; IOException: " + ExceptionUtils.exceptionStackTraceAsString(e));
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    log.severe("Instance ConfigReader; IOException: " + ExceptionUtils.exceptionStackTraceAsString(e));
                }
            }
        }
    }

    public Object getParam(String paramName) {
        Object valor = null;

        if(properties != null) {
            valor = tryParseInt(properties.getProperty(paramName));

            if(valor == null) {
                valor = properties.getProperty(paramName);
            }
        }

        return valor;
    }

    public Integer tryParseInt(Object obj) {
        Integer retVal;
        try {
            retVal = Integer.parseInt((String) obj);
        } catch (NumberFormatException nfe) {
            retVal = null; // or null if that is your preference
        }
        return retVal;
    }
}
