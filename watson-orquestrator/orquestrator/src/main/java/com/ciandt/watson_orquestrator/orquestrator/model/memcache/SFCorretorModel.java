package com.ciandt.watson_orquestrator.orquestrator.model.memcache;

import java.io.Serializable;

/**
 * Created by rodrigogs on 10/03/17.
 */

public class SFCorretorModel implements Serializable {
    private String identificador;
    private String nome;

    public SFCorretorModel() {
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
