package com.ciandt.watson_orquestrator.orquestrator.model.sulamerica;

import java.io.Serializable;

/**
 * Created by gsanchez on 13/01/2017.
 */

public class ConsultarDadosApoliceOutput implements Serializable {
    private String link;
    private String linkApolice;

    public ConsultarDadosApoliceOutput() {
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getLinkApolice() {
        return linkApolice;
    }

    public void setLinkApolice(String linkApolice) {
        this.linkApolice = linkApolice;
    }
}
