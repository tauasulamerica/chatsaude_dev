package com.ciandt.watson_orquestrator.orquestrator.constants;

/**
 * Created by rodrigogs on 19/01/17.
 */

public interface Constants {

    interface ApiErrorCodes {
        String ERROR = "ERROR";

        int FORBIDDEN = 403;

        int UNAUTHORIZED = 401;
    }

    interface ManifestacoesApi {
        String NAO_RETORNADO = "NAO_RETORNADO";
        String INTENCAO = "intencao";
        String ACTION = "action";
        int MINUTOS_INATIVIDADE = 60;
        String RETORNO_SERVICO_SUCESSO = "sucesso";
        String RETORNO_SERVICO_INSUCESSO = "insucesso";
        String RETORNO_SERVICO_ERRO = "erro";
        String RETORNO_SERVICO_FORMULARIO = "formulario";
        String RESPOSTA_ESTATICA = "informacao";
        String SUBTIPO_INATIVIDADE_SEM_SERVICO = "Inatividade sem servico";
        String SUBTIPO_INATIVIDADE_COM_SERVICO = "Inatividade com servico";
    }

    interface Status {
        String PENDENTE = "Pendente";
        String PAGO = "Pago";
        String PAGAMENTO_A_MENOR = "Pagamento a menor";
        String CANCELAMENTO_PEDIDO = "Cancelamento a Pedido";
        String PAGO_INDENIZACAO_INTEGRAL = "Pago por Indenização Integral";
        String CANCELADO_FALTA_PAGAMENTO = "Cancelado por Falta de Pagamen";
        String CANCELADO = "Cancelado";
    }

    interface TipoTransacao {
        String A_VENCER = "4";
        String VENCIDO = "5";
    }

    interface TipoPagamento {
        String DEBITO_EM_CONTA = "Débito em Conta";
        String CARTAO_DE_CREDITO = "Cartão de Crédito";
    }

    interface Memcache {
        int EXPIRATION_DELTA = 3600; //1 hora
        int EXPIRATION_TOKEN = 3600;
        int EXPIRATION_CORRETOR = 86400; // 1 dia
        int EXPIRATION_SUBTIPO = 86400;
        int EXPIRATION_RECORDTYPE = 86400;
    }

    interface ApiInfo {
        String ORIGEM = "CH";
        String SISTEMA_ORIGEM = "APA";

        int LIMITE_PARCELAS_VENCIDAS = 1;
        int TAMANHO_CNPJ = 14;
    }

    interface Crypto {
        String AES_KEY = "dIXoN30PyRolYSe46yAMAHa8bURnISh14bucKeT95BelABoREd63bRutAL67unjustLy25sALLE46RevoLvING";
    }
    
    interface Parametros {
    		String CHAVE = "chave";
    		String HORA_INICIO = "horaInicio";
    		String HORA_FIM = "horaFim";
    		String CHECK_ATENDENTES = "checkAtendentes";
    		String EMAIL_TELEFONE_OBRIGATORIO = "emailTelefoneObrigatorio";
    		String TELEFONE_ATENDIMENTO = "telefoneAtendimento";
    		String HORARIO_ATENDIMENTO_TELEFONE = "horarioAtendimentoTelefone";
    }

    interface Watson {
    		String MENSAGEM_PRE_CHAT = "mensagem_pre_chat";
    }
    
    interface MensagensAlternativas {
		String PRIMEIRA_PARCELA_DCC = "PrimeiraParcelaDCC";
		String PRIMEIRA_PARCELA_CARTAO_CREDITO = "PrimeiraParcelaCartaoDeCredito";
		String DEMAIS_PARCELAS_CARTAO_CREDITO = "DemaisParcelasCartaoDeCredito";
    }

    interface CamposFormulario {
        String APOLICE = "apolice";
        String ENDOSSO = "endosso";
        String PEDIDO_ENDOSSO = "pedidoEndosso";
        String PROPOSTA = "proposta";
        String SUCURSAL = "sucursal";
        String CPF_CNPJ = "cpfCnpj";
        String DATA_NASCIMENTO = "dataNascimento";

        //@@saude@@
        String CARTEIRINHA = "carteirinha";
    }

    interface Templates {
        String APOLICE_CONSULTA_PAGAMENTO = "templateApoliceConsultaPagamento.html";
        String APOLICE_CONSULTA_PAGAMENTO_PENDENTE = "templateApoliceConsultaPagamentoPendente.html";

        String CONSULTA_PAGAMENTO_INDIVIDUAL = "templateConsultaPagamentoIndividual.html";
        String CONSULTA_PAGAMENTO_INDIVIDUAL_PENDENTE = "templateConsultaPagamentoIndividualPendente.html";
        String CONSULTA_PAGAMENTO_PME = "templateConsultaPagamentoIndividual.html";
        String CONSULTA_PAGAMENTO_PME_PENDENTE = "templateConsultaPagamentoIndividualPendente.html";
    }
}
