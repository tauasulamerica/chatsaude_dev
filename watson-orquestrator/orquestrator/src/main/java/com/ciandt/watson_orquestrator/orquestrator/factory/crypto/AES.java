package com.ciandt.watson_orquestrator.orquestrator.factory.crypto;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.logging.Logger;

import com.ciandt.watson_orquestrator.orquestrator.helper.LoggerHelper;
import org.apache.commons.codec.binary.Base64;

import org.glassfish.jersey.internal.util.ExceptionUtils;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by rodrigogs on 02/03/17.
 */
public class AES {
    private static Logger log = null;
    private static SecretKeySpec secretKey;
    private static byte[] key;

    public AES(String myKey) {
        log = Logger.getLogger(getClass().getName());
        MessageDigest sha = null;

        try {
            key = myKey.getBytes("UTF-8");
            sha = MessageDigest.getInstance("SHA-1");
            key = sha.digest(key);
            key = Arrays.copyOf(key, 16);
            secretKey = new SecretKeySpec(key, "AES");
        } catch (NoSuchAlgorithmException e) {
            LoggerHelper.getInstance(log).log("AES - Constructor error: ", ExceptionUtils.exceptionStackTraceAsString(e));
        } catch (UnsupportedEncodingException e) {
            LoggerHelper.getInstance(log).log("AES - Constructor error: ", ExceptionUtils.exceptionStackTraceAsString(e));
        }
    }

    public String encrypt(String strToEncrypt)
    {
        try
        {
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            return new String(Base64.encodeBase64(cipher.doFinal(strToEncrypt.getBytes("UTF-8"))));
        } catch (Exception e) {
            LoggerHelper.getInstance(log).log("AES - encrypt error: ", ExceptionUtils.exceptionStackTraceAsString(e));
        }

        return null;
    }

    public String decrypt(String strToDecrypt)
    {
        try
        {
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            return new String(cipher.doFinal(Base64.decodeBase64(strToDecrypt.getBytes("UTF-8"))));
        } catch (Exception e) {
            LoggerHelper.getInstance(log).log("AES - decrypt error: ", ExceptionUtils.exceptionStackTraceAsString(e));
        }

        return null;
    }
}
