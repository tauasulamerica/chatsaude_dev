package com.ciandt.watson_orquestrator.orquestrator.service.sulamerica;

import com.ciandt.watson_orquestrator.orquestrator.constants.Constants;
import com.ciandt.watson_orquestrator.orquestrator.dao.MapeamentoServicosDao;
import com.ciandt.watson_orquestrator.orquestrator.dao.SessaoDao;
import com.ciandt.watson_orquestrator.orquestrator.entity.Sessao;
import com.ciandt.watson_orquestrator.orquestrator.enums.ActionWatsonEnum;
import com.ciandt.watson_orquestrator.orquestrator.enums.TokenTypesEnum;
import com.ciandt.watson_orquestrator.orquestrator.exceptions.AccessTokenNullException;
import com.ciandt.watson_orquestrator.orquestrator.exceptions.GeneralErrorServiceException;
import com.ciandt.watson_orquestrator.orquestrator.exceptions.NotFoundServiceException;
import com.ciandt.watson_orquestrator.orquestrator.exceptions.SaSServicesException;
import com.ciandt.watson_orquestrator.orquestrator.helper.LoggerHelper;
import com.ciandt.watson_orquestrator.orquestrator.model.sulamerica.ConsultarDadosApoliceInput;
import com.ciandt.watson_orquestrator.orquestrator.model.sulamerica.ConsultarDadosApoliceOutput;
import com.ciandt.watson_orquestrator.orquestrator.resolver.config.SaSServicesConfig;
import com.ciandt.watson_orquestrator.orquestrator.service.BaseServices;
import com.ciandt.watson_orquestrator.orquestrator.service.sensedia.SensediaSevice;
import com.google.api.server.spi.response.UnauthorizedException;

import org.glassfish.jersey.internal.util.ExceptionUtils;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import io.swagger.client.sulamerica.auto.ApiClient;
import io.swagger.client.sulamerica.auto.ApiException;
import io.swagger.client.sulamerica.auto.api.ApolicesApi;
import io.swagger.client.sulamerica.auto.model.Apolice;

/**
 * Created by gsanchez on 13/01/2017.
 */

public class ConsultarDadosApolice  extends BaseServices<SaSServicesConfig> implements SaSService<ConsultarDadosApoliceInput, ConsultarDadosApoliceOutput>  {
    private static Logger log = null;
    private String accessToken = "";
    private SensediaSevice sensediaSevice = null;
    private ApolicesApi api = null;
    private ApiClient clientConfig = null;
    private MapeamentoServicosDao mapeamentoServicosDao = null;
    private SessaoDao sessaoDao = null;
    int numRetries = 0;

    public ConsultarDadosApolice() {
        super();
        log = Logger.getLogger(getClass().getName());
        sensediaSevice = new SensediaSevice(config.getBaseURLServicosSaS());
        api = new ApolicesApi();
        mapeamentoServicosDao = new MapeamentoServicosDao();
        sessaoDao = new SessaoDao();

        clientConfig = new ApiClient();
        clientConfig.setConnectTimeout(config.getTimeout());
        clientConfig.setBasePath(config.getBaseURLServicosSaS());

        api.setApiClient(clientConfig);
    }

    @Override
    public void requestAccessToken(Long idSessao) throws AccessTokenNullException, GeneralErrorServiceException {
        try {
            accessToken = sensediaSevice.getAccessToken(idSessao, TokenTypesEnum.Documentos);

            if(accessToken == null) {
                throw new AccessTokenNullException();
            }
        } catch (UnauthorizedException e) {
            log.severe("ConsultarDadosApolice - requestAccessToken: Não foi possível gerar um token de acesso para Sensedia. Stack trace: " + ExceptionUtils.exceptionStackTraceAsString(e));
            throw new GeneralErrorServiceException();
        }
    }

    @Override
    public ConsultarDadosApoliceOutput integrar(ConsultarDadosApoliceInput objetoEntrada, Long idSessao, String requestId) throws SaSServicesException {
        ConsultarDadosApoliceOutput retorno = new ConsultarDadosApoliceOutput();

        while (numRetries <= config.getTentativas()) {
            try {
                requestAccessToken(idSessao);

                Sessao sessao = sessaoDao.getByKey(idSessao);

                LoggerHelper.getInstance(log).log("ConsultarDadosApolice - Objeto entrada", objetoEntrada);

                if (sessao == null) {
                    log.severe("ConsultarDadosApolice - integrar: Sessão não encontrada para o ID: " + idSessao);
                    throw new NotFoundServiceException();
                }

                String numeroApolice = null;
                String numeroEndosso = null;

                if(ActionWatsonEnum.ENDOSSO_DOCUMENTO_EMISSAO2VIA.getCodigo().equals(objetoEntrada.getAction())) {
                    numeroEndosso = objetoEntrada.getDocumentoApolice();
                } else if (ActionWatsonEnum.APOLICE_DOCUMENTO_EMISSAO2VIA.getCodigo().equals(objetoEntrada.getAction()) ||
                        ActionWatsonEnum.APOLICE_CARTEIRINHA_EMISSAO2VIA.getCodigo().equals(objetoEntrada.getAction())) {
                    numeroApolice = objetoEntrada.getDocumentoApolice();
                }

                List<Apolice> response = api.apolicesGet(config.getClientId(), accessToken, String.valueOf(idSessao), requestId, objetoEntrada.getNumCnpj(), null, numeroApolice, numeroEndosso, null, null, null, null, null);

                if (response == null || response.size() == 0) {
                    log.severe("ConsultarDadosApolice - integrar: Retorno da execução do serviço apolicesGet vazio ou nulo.");
                    throw new NotFoundServiceException();
                }

                LoggerHelper.getInstance(log).log("ConsultarDadosApolice - response", response);

                for (Apolice apolice : response) {
                    LoggerHelper.getInstance(log).log("ConsultarDadosApolice - Apolice", apolice);

                    if(ActionWatsonEnum.ENDOSSO_DOCUMENTO_EMISSAO2VIA.getCodigo().equals(objetoEntrada.getAction())) {
                        if(objetoEntrada.getNumCnpj().length() == Constants.ApiInfo.TAMANHO_CNPJ) {
                            //Validar somente número do endosso para CNPJ
                            if (!apolice.getNumeroEndosso().equals(Integer.valueOf(objetoEntrada.getDocumentoApolice()))) {
                                log.severe("ConsultarDadosApolice - integrar: Número de Endosso não é igual ao do documento pesquisado.");
                                throw new NotFoundServiceException();
                            }
                        } else {
                            //Validar data de nascimento e número da apolice
                            if(apolice.getDataNacimento().trim() == "" || apolice.getDataNacimento() == null) {
                                log.severe("ConsultarDadosApolice - integrar: Data de Nascimento vazia ou nula.");
                                throw new NotFoundServiceException();
                            }

                            if (!apolice.getNumeroEndosso().equals(Integer.valueOf(objetoEntrada.getDocumentoApolice())) ||
                                    !apolice.getDataNacimento().equals(objetoEntrada.getDataNascimento())) {
                                log.severe("ConsultarDadosApolice - integrar: Número de Apólice e Data de Nascimento não são iguais ao do documento pesquisado.");
                                throw new NotFoundServiceException();
                            }
                        }

                        retorno.setLink(String.format("<a href=\"%1$s\" target=\"_blank\">%1$s</a>", apolice.getApolice()));
                    } else if (ActionWatsonEnum.APOLICE_DOCUMENTO_EMISSAO2VIA.getCodigo().equals(objetoEntrada.getAction()) ||
                            ActionWatsonEnum.APOLICE_CARTEIRINHA_EMISSAO2VIA.getCodigo().equals(objetoEntrada.getAction()) ||
                            ActionWatsonEnum.APOLICE_KIT_SEGURADO_EMISSAO2VIA.getCodigo().equals(objetoEntrada.getAction())) {
                        if(objetoEntrada.getNumCnpj().length() == 14) {
                            //Validar somente número da apolice para CNPJ
                            if (!apolice.getNumeroApolice().equals(Integer.valueOf(objetoEntrada.getDocumentoApolice()))) {
                                log.severe("ConsultarDadosApolice - integrar: Número de Apólice não é igual ao do documento pesquisado.");
                                throw new NotFoundServiceException();
                            }
                        } else {
                            //Validar data de nascimento e número da apolice
                            if(apolice.getDataNacimento().trim() == "" || apolice.getDataNacimento() == null) {
                                log.severe("ConsultarDadosApolice - integrar: Data de Nascimento vazia ou nula.");
                                throw new NotFoundServiceException();
                            }

                            if (!apolice.getNumeroApolice().equals(Integer.valueOf(objetoEntrada.getDocumentoApolice())) ||
                                    !apolice.getDataNacimento().equals(objetoEntrada.getDataNascimento())) {
                                log.severe("ConsultarDadosApolice - integrar: Número de Apólice e Data de Nascimento não são iguais ao do documento pesquisado.");
                                throw new NotFoundServiceException();
                            }
                        }

                        if (ActionWatsonEnum.APOLICE_CARTEIRINHA_EMISSAO2VIA.getCodigo().equals(objetoEntrada.getAction())) {
                            retorno.setLink(String.format("<a href=\"%1$s\" target=\"_blank\">%1$s</a>", apolice.getCarterinha()));
                        } else if (ActionWatsonEnum.APOLICE_DOCUMENTO_EMISSAO2VIA.getCodigo().equals(objetoEntrada.getAction())) {
                            retorno.setLink(String.format("<a href=\"%1$s\" target=\"_blank\">%1$s</a>", apolice.getApolice()));
                        } else if(ActionWatsonEnum.APOLICE_KIT_SEGURADO_EMISSAO2VIA.getCodigo().equals(objetoEntrada.getAction())) {
                            retorno.setLink(String.format("<a href=\"%1$s\" target=\"_blank\">%1$s</a>", apolice.getCarterinha()));
                            retorno.setLinkApolice(String.format("<a href=\"%1$s\" target=\"_blank\">%1$s</a>", apolice.getApolice()));
                        }
                    } else {
                        log.severe("ConsultarDadosApolice - integrar: Action retornada do Watson diferente de APOLICE_CARTEIRINHA_EMISSAO2VIA, ENDOSSO_DOCUMENTO_EMISSAO2VIA ou APOLICE_DOCUMENTO_EMISSAO2VIA.");
                        throw new NotFoundServiceException();
                    }
                }
            } catch (AccessTokenNullException an) {
                log.severe("ConsultarDadosApolice: Não foi possível gerar um token de acesso para Sensedia. Stack trace: " + ExceptionUtils.exceptionStackTraceAsString(an));
                throw new GeneralErrorServiceException();
            } catch (ApiException e) {
                if (e.getCode() == Constants.ApiErrorCodes.FORBIDDEN) {
                    requestAccessToken(idSessao);
                    numRetries++;
                    sleep(config.getIntervaloTentativas());

                    integrar(objetoEntrada, idSessao, requestId);
                } else if (e.getCode() == Constants.ApiErrorCodes.UNAUTHORIZED) {
                    log.info("ConsultarDadosApolice - integrar: UNAUTHORIZED. " + ExceptionUtils.exceptionStackTraceAsString(e));

                    numRetries++;
                    sleep(config.getIntervaloTentativas());

                    integrar(objetoEntrada, idSessao, requestId);
                } else {
                    LoggerHelper.getInstance(log).log(Level.SEVERE, "ConsultarDadosApolice - Erro", e.getResponseBody());

                    if (e.getResponseBody().contains(Constants.ApiErrorCodes.ERROR)) {
                        log.info("ConsultarDadosApolice - integrar: Apolice não encontrada. " + ExceptionUtils.exceptionStackTraceAsString(e));
                        throw new NotFoundServiceException();
                    } else {
                        log.info("ConsultarDadosApolice - integrar: Problema ao executar a chamada a API de Apolice. " + ExceptionUtils.exceptionStackTraceAsString(e));
                        throw new GeneralErrorServiceException();
                    }
                }
            } catch (SaSServicesException se) {
                log.info("ConsultarDadosApolice - integrar: Retorno do serviço: " + ExceptionUtils.exceptionStackTraceAsString(se));
                throw se;
            } catch (Exception e) {
                log.severe("ConsultarDadosApolice - integrar: Erro geral: " + ExceptionUtils.exceptionStackTraceAsString(e));
                throw new GeneralErrorServiceException();
            }

            return retorno;
        }

        log.severe("ConsultarDadosApolice - integrar: Número de tentativas excedidas. Tentativas: " + numRetries);
        return null;
    }

    @Override
    public Class<ConsultarDadosApoliceInput> getTypeParameterInputClass() {
        return ConsultarDadosApoliceInput.class;
    }

    @Override
    public Class<ConsultarDadosApoliceOutput> getTypeParameterOutputClass() {
        return ConsultarDadosApoliceOutput.class;
    }
}
