package com.ciandt.watson_orquestrator.orquestrator.model.sulamerica;

import java.io.Serializable;

/**
 * Created by rodrigogs on 03/01/17.
 */

public class VerificaCorretorInput extends ServiceInputBase implements Serializable {
    private String codigoNac;

    public VerificaCorretorInput() {
    }

    public String getCodigoNac() {
        return codigoNac;
    }

    public void setCodigoNac(String codigoNac) {
        this.codigoNac = codigoNac;
    }
}
