package com.ciandt.watson_orquestrator.orquestrator.model.sulamerica;

import java.io.Serializable;

public class ConsultarDadosBoletoSaudeOutput  extends MensagemAlternativaBaseOutput implements Serializable {
    private String link;
    private String numParcela;
    private String dataVencimento;
    private String tabelaParcelas;

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }


    public String getDataVencimento() {
        return dataVencimento;
    }

    public void setDataVencimento(String dataVencimento) {
        this.dataVencimento = dataVencimento;
    }

    public String getNumParcela() {
        return numParcela;
    }

    public void setNumParcela(String numParcela) {
        this.numParcela = numParcela;
    }

    public String getTabelaParcelas() {
        return tabelaParcelas;
    }

    public void setTabelaParcelas(String tabelaParcelas) {
        this.tabelaParcelas = tabelaParcelas;
    }
}
