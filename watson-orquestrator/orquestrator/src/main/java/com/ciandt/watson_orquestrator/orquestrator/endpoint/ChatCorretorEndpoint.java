package com.ciandt.watson_orquestrator.orquestrator.endpoint;

import com.ciandt.watson_orquestrator.orquestrator.business.ChatCorretorBO;
import com.ciandt.watson_orquestrator.orquestrator.constants.Constants;
import com.ciandt.watson_orquestrator.orquestrator.exceptions.SaSServicesException;
import com.ciandt.watson_orquestrator.orquestrator.factory.crypto.AES;
import com.ciandt.watson_orquestrator.orquestrator.model.ParametrosChatOutput;
import com.ciandt.watson_orquestrator.orquestrator.model.ConversaInput;
import com.ciandt.watson_orquestrator.orquestrator.model.ConversaOutput;
import com.ciandt.watson_orquestrator.orquestrator.model.EncryptInput;
import com.ciandt.watson_orquestrator.orquestrator.model.EncryptOutput;
import com.ciandt.watson_orquestrator.orquestrator.model.FeedbacksInput;
import com.ciandt.watson_orquestrator.orquestrator.model.IniciarSessaoInput;
import com.ciandt.watson_orquestrator.orquestrator.model.salesforce.LiveAgentMessageList;
import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.response.ConflictException;
import com.google.api.server.spi.response.InternalServerErrorException;
import com.google.api.server.spi.response.NotFoundException;
import com.google.api.server.spi.response.UnauthorizedException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.logging.Logger;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by rodrigosclosa on 13/12/16.
 */

@Api(
        name = "chatcorretor",
        version = "v1",
        namespace = @ApiNamespace(
                ownerDomain = "endpoint.orquestrator.watson_orquestrator.ciandt.com",
                ownerName = "endpoint.orquestrator.watson_orquestrator.ciandt.com",
                packagePath = ""
        )
)
public class ChatCorretorEndpoint {
    private static final String CLASS_NAME = ChatCorretorBO.class.getName();
    private ChatCorretorBO chatCorretorBO;
    private static final Logger log = Logger.getLogger(ChatCorretorEndpoint.class.getName());

    public ChatCorretorEndpoint() {
        chatCorretorBO = new ChatCorretorBO();
    }

    @ApiMethod(name = "iniciarSessao", path = "conversas", httpMethod = ApiMethod.HttpMethod.POST)
    public ConversaOutput iniciarSessao(HttpServletRequest req, IniciarSessaoInput iniciarSessaoInput) throws ConflictException, NotFoundException, UnauthorizedException, SaSServicesException {
        log.entering(CLASS_NAME, "iniciarSessao");
        return chatCorretorBO.iniciarSessao(req, iniciarSessaoInput);
    }

    @ApiMethod(name = "conversa", path = "conversas", httpMethod = ApiMethod.HttpMethod.PUT)
    public ConversaOutput conversa(HttpServletRequest req, ConversaInput conversa) throws ConflictException, UnauthorizedException, SaSServicesException {
        log.entering(CLASS_NAME, "conversa");
        return chatCorretorBO.conversa(req, conversa);
    }

    @ApiMethod(name = "sendFeedback", path = "feedbacks", httpMethod = ApiMethod.HttpMethod.POST)
    public void sendFeedback(HttpServletRequest req, FeedbacksInput input) throws ConflictException, UnauthorizedException {
        log.entering(CLASS_NAME, "sendFeedback");
        chatCorretorBO.sendFeedback(req, input.getIdentificadorSessao(), input.getSatisfacaoAtentimento());
    }

    @ApiMethod(name = "finalizarChat", path = "conversas/{idSessao}", httpMethod = ApiMethod.HttpMethod.DELETE)
    public void finalizarChat(HttpServletRequest req, @Named("idSessao") Long idSessao) throws ConflictException, UnauthorizedException {
        log.entering(CLASS_NAME, "finalizarChat");
        chatCorretorBO.encerrarChat(req, idSessao);
    }

    @ApiMethod(name = "logConversa", path = "conversas/{idSessao}", httpMethod = ApiMethod.HttpMethod.GET)
    public ConversaOutput buscarHistorico(HttpServletRequest req, @Named("idSessao") Long idSessao) throws ConflictException, UnauthorizedException {
        log.entering(CLASS_NAME, "logConversa");
        return chatCorretorBO.getLogConversa(req, idSessao);
    }

    @ApiMethod(name = "obterParametrosChat", path = "parametros", httpMethod = ApiMethod.HttpMethod.GET)
    public ParametrosChatOutput obterParametrosChat(@Named("organizationId") String organizationId, @Named("deploymentId") String deploymentId, @Named("buttonId") String buttonId) throws ConflictException {
        log.entering(CLASS_NAME, "parametros");
        return chatCorretorBO.obterParametrosChat(organizationId, deploymentId, buttonId);
    }

    @ApiMethod(name = "encryptString", path = "encrypt", httpMethod = ApiMethod.HttpMethod.POST)
    public EncryptOutput encryptString(EncryptInput encryptInput) throws ConflictException, UnsupportedEncodingException {

        EncryptOutput output = new EncryptOutput();
        AES aes = new AES(Constants.Crypto.AES_KEY);

        output.setCryptografado(aes.encrypt(encryptInput.getTexto()));

        return output;
    }
    
    @ApiMethod(name = "mensagens", path = "mensagens", httpMethod = ApiMethod.HttpMethod.GET)
    public LiveAgentMessageList mensagens(HttpServletRequest request) throws ConflictException, IOException, InternalServerErrorException {
        return chatCorretorBO.getMessages(request);
    }
}
