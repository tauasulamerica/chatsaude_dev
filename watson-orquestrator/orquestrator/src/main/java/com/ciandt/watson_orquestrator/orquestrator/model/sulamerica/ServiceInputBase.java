package com.ciandt.watson_orquestrator.orquestrator.model.sulamerica;

public class ServiceInputBase {
    private Integer action;

    public Integer getAction() {
        return action;
    }

    public void setAction(Integer action) {
        this.action = action;
    }
}
