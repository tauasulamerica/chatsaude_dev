package com.ciandt.watson_orquestrator.orquestrator.business;

import com.ciandt.watson_orquestrator.orquestrator.constants.Constants;
import com.ciandt.watson_orquestrator.orquestrator.dao.AtendentesDao;
import com.ciandt.watson_orquestrator.orquestrator.dao.MapeamentoCamposDao;
import com.ciandt.watson_orquestrator.orquestrator.dao.MapeamentoPayloadManifestacaoDao;
import com.ciandt.watson_orquestrator.orquestrator.dao.MapeamentoServicosDao;
import com.ciandt.watson_orquestrator.orquestrator.dao.ParametroChatDao;
import com.ciandt.watson_orquestrator.orquestrator.dao.TokenDao;
import com.ciandt.watson_orquestrator.orquestrator.entity.Atendentes;
import com.ciandt.watson_orquestrator.orquestrator.entity.MapeamentoCampos;
import com.ciandt.watson_orquestrator.orquestrator.entity.MapeamentoPayloadManifestacao;
import com.ciandt.watson_orquestrator.orquestrator.entity.MapeamentoServicos;
import com.ciandt.watson_orquestrator.orquestrator.entity.ParametroChat;
import com.ciandt.watson_orquestrator.orquestrator.entity.Token;
import com.ciandt.watson_orquestrator.orquestrator.helper.TokenHelper;
import com.ciandt.watson_orquestrator.orquestrator.enums.ActionWatsonEnum;
import com.google.api.server.spi.response.UnauthorizedException;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.utils.SystemProperty;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by rodrigosclosa on 28/12/16.
 */

public class SeedDataBO {

    private MapeamentoServicosDao mapeamentoServicosDao = null;
    private MapeamentoCamposDao mapeamentoCamposDao = null;
    private AtendentesDao atendentesDao = null;
    private ParametroChatDao parametroChatDao = null;
    private MapeamentoPayloadManifestacaoDao mapeamentoPayloadManifestacaoDao = null;
    private TokenDao tokensDao = null;

    public SeedDataBO() {
        mapeamentoServicosDao = new MapeamentoServicosDao();
        mapeamentoCamposDao = new MapeamentoCamposDao();
        atendentesDao = new AtendentesDao();
        parametroChatDao = new ParametroChatDao();
        mapeamentoPayloadManifestacaoDao = new MapeamentoPayloadManifestacaoDao();
        tokensDao = new TokenDao();
    }

    public void seedServicosWatson(HttpServletRequest request) throws UnauthorizedException {

        //Tokens de Acesso
        List<Token> tokens = new ArrayList<Token>();
        Token token = tokensDao.getByProperty("codigo", "CHAT-CORRETOR");

        if (token == null) {
            token = new Token();
            token.setDescricao("Chat Corretor");
            token.setCodigo("CHAT-CORRETOR");
            token.setToken("5d1e56af0c5f48d9970ebd062a86f94c");
            token.setAtivo(true);

            tokensDao.insert(token);
            tokens.add(token);
        }

        token = tokensDao.getByProperty("codigo", "POSTMAN");

        if (token == null) {
            token = new Token();
            token.setDescricao("Postman Testes");
            token.setCodigo("POSTMAN");
            token.setToken("3a00f83be3924a9fb1299a878de1cbb7");
            token.setAtivo(true);

            tokensDao.insert(token);
            tokens.add(token);
        }

        if(TokenHelper.getInstance().tokenValido(request)) {
            ArrayList<MapeamentoServicos> servicos = new ArrayList<MapeamentoServicos>();

//            servicos.add(new MapeamentoServicos(ActionWatsonEnum.APOLICE_CONSULTA_STATUS.getCodigo(), "1", "PesquisaDireta", "integrar", false));
//            servicos.add(new MapeamentoServicos(ActionWatsonEnum.ENDOSSO_CONSULTA_STATUS.getCodigo(), "1", "PesquisaDireta", "integrar", false));
//            servicos.add(new MapeamentoServicos(ActionWatsonEnum.PROPOSTA_CONSULTA_STATUS.getCodigo(), "2", "Coordenador", "integrar", true));
//            servicos.add(new MapeamentoServicos(ActionWatsonEnum.PEDIDO_DE_ENDOSSO_CONSULTA_STATUS.getCodigo(), "2", "Coordenador", "integrar", true));
//            servicos.add(new MapeamentoServicos(ActionWatsonEnum.PROPOSTA_CONSULTA_CRITICA.getCodigo(), "2", "Coordenador", "integrar", false));
//            servicos.add(new MapeamentoServicos(ActionWatsonEnum.PEDIDO_DE_ENDOSSO_CONSULTA_CRITICA.getCodigo(), "2", "Coordenador", "integrar", false));
//            servicos.add(new MapeamentoServicos(ActionWatsonEnum.APOLICE_BOLETO_EMISSAO2VIA.getCodigo(), "3", "ConsultarDadosBoleto", "integrar", false));
//            servicos.add(new MapeamentoServicos(ActionWatsonEnum.ENDOSSO_BOLETO_EMISSAO2VIA.getCodigo(), "3", "ConsultarDadosBoleto", "integrar", false));
//            servicos.add(new MapeamentoServicos(ActionWatsonEnum.APOLICE_DOCUMENTO_EMISSAO2VIA.getCodigo(), "3", "ConsultarDadosApolice", "integrar", false));
//            servicos.add(new MapeamentoServicos(ActionWatsonEnum.ENDOSSO_DOCUMENTO_EMISSAO2VIA.getCodigo(), "3", "ConsultarDadosApolice", "integrar", false));
//            servicos.add(new MapeamentoServicos(ActionWatsonEnum.APOLICE_CARTEIRINHA_EMISSAO2VIA.getCodigo(), "3", "ConsultarDadosApolice", "integrar", false));
//            servicos.add(new MapeamentoServicos(ActionWatsonEnum.APOLICE_KIT_SEGURADO_EMISSAO2VIA.getCodigo(), "3", "ConsultarDadosApolice", "integrar", false));
//            servicos.add(new MapeamentoServicos(ActionWatsonEnum.VERIFICAR_CORRETOR.getCodigo(), "4", "VerificaCorretor", "integrar", false));
//            servicos.add(new MapeamentoServicos(ActionWatsonEnum.FIM_ATENDIMENTO_WATSON.getCodigo(), "0", "", "", false));
//            servicos.add(new MapeamentoServicos(ActionWatsonEnum.REDIRECIONAR_ATENDENTE.getCodigo(), "0", "", "", false));
//            servicos.add(new MapeamentoServicos(ActionWatsonEnum.APOLICE_CONSULTA_PAGAMENTO.getCodigo(), "3", "ConsultarDadosBoleto", "integrar", false));
            //NOVO SERVICO CONSULTA PAGAMENTO ENDOSSO
            //servicos.add(new MapeamentoServicos(ActionWatsonEnum.ENDOSSO_CONSULTA_PAGAMENTO.getCodigo(), "3", "ConsultarDadosBoleto", "integrar", false));

            //@@saude@@
            servicos.add(new MapeamentoServicos(ActionWatsonEnum.STATUS_PAGAMENTO_INDIVIDUAL.getCodigo(), "3", "ConsultarDadosBoletoSaude", "integrar", false));
            servicos.add(new MapeamentoServicos(ActionWatsonEnum.STATUS_PAGAMENTO_PME.getCodigo(), "3", "ConsultarDadosBoletoSaude", "integrar", false));
            servicos.add(new MapeamentoServicos(ActionWatsonEnum.STATUS_PAGAMENTO_DEMAPOSENTADO.getCodigo(), "3", "ConsultarDadosBoletoSaude", "integrar", false));



            for (MapeamentoServicos servico : servicos) {
                if (mapeamentoServicosDao.getByProperty("codigoIntencao", servico.getCodigoIntencao()) == null) {
                    mapeamentoServicosDao.insert(servico);
                }
            }

            //Mapeamento de Campos dos Serviços
            ArrayList<MapeamentoCampos> campos = new ArrayList<MapeamentoCampos>();

//            //Parametros de Entrada
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.APOLICE_CONSULTA_STATUS.getCodigo(), 1, "num_documento", "codigoApolice", "E", Constants.CamposFormulario.APOLICE));
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.APOLICE_CONSULTA_STATUS.getCodigo(), 2, "num_sucursal", "codigoSucursal", "E", Constants.CamposFormulario.SUCURSAL));
//
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.ENDOSSO_CONSULTA_STATUS.getCodigo(), 1, "num_documento", "codigoEndosso", "E", Constants.CamposFormulario.ENDOSSO));
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.ENDOSSO_CONSULTA_STATUS.getCodigo(), 2, "num_sucursal", "codigoSucursal", "E", Constants.CamposFormulario.SUCURSAL));
//
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.PROPOSTA_CONSULTA_STATUS.getCodigo(), 1, "num_documento", "numDocumento", "E", Constants.CamposFormulario.PROPOSTA));
//
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.PEDIDO_DE_ENDOSSO_CONSULTA_STATUS.getCodigo(), 1, "num_documento", "numDocumento", "E", Constants.CamposFormulario.PEDIDO_ENDOSSO));
//
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.PROPOSTA_CONSULTA_CRITICA.getCodigo(), 1, "num_documento", "numDocumento", "E", Constants.CamposFormulario.PROPOSTA));
//
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.PEDIDO_DE_ENDOSSO_CONSULTA_CRITICA.getCodigo(), 1, "num_documento", "numDocumento", "E", Constants.CamposFormulario.PEDIDO_ENDOSSO));
//
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.APOLICE_BOLETO_EMISSAO2VIA.getCodigo(), 1, "num_documento", "numDocumento", "E", Constants.CamposFormulario.APOLICE));
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.APOLICE_BOLETO_EMISSAO2VIA.getCodigo(), 2, "num_sucursal", "numSucursal", "E", Constants.CamposFormulario.SUCURSAL));
//            //campos.add(new MapeamentoCampos(ActionWatsonEnum.APOLICE_BOLETO_EMISSAO2VIA.getCodigo(), 3, "cpf_cnpj", "cpf", "E", ""));
//
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.ENDOSSO_BOLETO_EMISSAO2VIA.getCodigo(), 1, "num_documento", "numDocumento", "E", Constants.CamposFormulario.APOLICE));
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.ENDOSSO_BOLETO_EMISSAO2VIA.getCodigo(), 2, "num_sucursal", "numSucursal", "E", Constants.CamposFormulario.SUCURSAL));
//            //campos.add(new MapeamentoCampos(ActionWatsonEnum.ENDOSSO_BOLETO_EMISSAO2VIA.getCodigo(), 3, "cpf_cnpj", "cpf", "E", ""));
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.ENDOSSO_BOLETO_EMISSAO2VIA.getCodigo(), 5, "num_endosso", "numEndosso", "E", Constants.CamposFormulario.ENDOSSO));
//
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.APOLICE_DOCUMENTO_EMISSAO2VIA.getCodigo(), 1, "num_documento", "documentoApolice", "E", Constants.CamposFormulario.APOLICE));
//            //campos.add(new MapeamentoCampos(ActionWatsonEnum.APOLICE_DOCUMENTO_EMISSAO2VIA.getCodigo(), 2, "num_sucursal", "codigoSucursal", "E", ""));
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.APOLICE_DOCUMENTO_EMISSAO2VIA.getCodigo(), 3, "cpf_cnpj", "numCnpj", "E", Constants.CamposFormulario.CPF_CNPJ));
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.APOLICE_DOCUMENTO_EMISSAO2VIA.getCodigo(), 4, "data_nascimento", "dataNascimento", "E", Constants.CamposFormulario.DATA_NASCIMENTO));
//
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.ENDOSSO_DOCUMENTO_EMISSAO2VIA.getCodigo(), 1, "num_documento", "documentoApolice", "E", Constants.CamposFormulario.ENDOSSO));
//            //campos.add(new MapeamentoCampos(ActionWatsonEnum.ENDOSSO_DOCUMENTO_EMISSAO2VIA.getCodigo(), 2, "num_sucursal", "codigoSucursal", "E", ""));
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.ENDOSSO_DOCUMENTO_EMISSAO2VIA.getCodigo(), 3, "cpf_cnpj", "numCnpj", "E", Constants.CamposFormulario.CPF_CNPJ));
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.ENDOSSO_DOCUMENTO_EMISSAO2VIA.getCodigo(), 4, "data_nascimento", "dataNascimento", "E", Constants.CamposFormulario.DATA_NASCIMENTO));
//
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.APOLICE_CARTEIRINHA_EMISSAO2VIA.getCodigo(), 1, "num_documento", "documentoApolice", "E", Constants.CamposFormulario.APOLICE));
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.APOLICE_CARTEIRINHA_EMISSAO2VIA.getCodigo(), 2, "num_sucursal", "codigoSucursal", "E", Constants.CamposFormulario.SUCURSAL));
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.APOLICE_CARTEIRINHA_EMISSAO2VIA.getCodigo(), 3, "cpf_cnpj", "numCnpj", "E", Constants.CamposFormulario.CPF_CNPJ));
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.APOLICE_CARTEIRINHA_EMISSAO2VIA.getCodigo(), 4, "data_nascimento", "dataNascimento", "E", Constants.CamposFormulario.DATA_NASCIMENTO));
//
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.APOLICE_KIT_SEGURADO_EMISSAO2VIA.getCodigo(), 1, "num_documento", "documentoApolice", "E", Constants.CamposFormulario.APOLICE));
//            //campos.add(new MapeamentoCampos(ActionWatsonEnum.APOLICE_KIT_SEGURADO_EMISSAO2VIA.getCodigo(), 2, "num_sucursal", "codigoSucursal", "E", ""));
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.APOLICE_KIT_SEGURADO_EMISSAO2VIA.getCodigo(), 3, "cpf_cnpj", "numCnpj", "E", Constants.CamposFormulario.CPF_CNPJ));
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.APOLICE_KIT_SEGURADO_EMISSAO2VIA.getCodigo(), 4, "data_nascimento", "dataNascimento", "E", Constants.CamposFormulario.DATA_NASCIMENTO));
//
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.APOLICE_CONSULTA_PAGAMENTO.getCodigo(), 1, "num_documento", "numDocumento", "E", Constants.CamposFormulario.APOLICE));
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.APOLICE_CONSULTA_PAGAMENTO.getCodigo(), 2, "num_sucursal", "numSucursal", "E", Constants.CamposFormulario.SUCURSAL));
//            //campos.add(new MapeamentoCampos(ActionWatsonEnum.APOLICE_CONSULTA_PAGAMENTO.getCodigo(), 3, "cpf_cnpj", "cpf", "E", Constants.CamposFormulario.CPF_CNPJ));
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.APOLICE_CONSULTA_PAGAMENTO.getCodigo(), 1, "tabelaParcelas", "%%token_tabela_parcelas%%", "S"));
//
//            //NOVO SERVICO CONSULTA PAGAMENTO ENDOSSO
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.ENDOSSO_CONSULTA_PAGAMENTO.getCodigo(), 1, "num_documento", "numDocumento", "E", Constants.CamposFormulario.APOLICE));
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.ENDOSSO_CONSULTA_PAGAMENTO.getCodigo(), 2, "num_sucursal", "numSucursal", "E", Constants.CamposFormulario.SUCURSAL));
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.ENDOSSO_CONSULTA_PAGAMENTO.getCodigo(), 5, "num_endosso", "numEndosso", "E", Constants.CamposFormulario.ENDOSSO));
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.ENDOSSO_CONSULTA_PAGAMENTO.getCodigo(), 1, "tabelaParcelas", "%%token_tabela_parcelas%%", "S"));
//
//            //Parametros de Saida
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.APOLICE_CONSULTA_STATUS.getCodigo(), 1, "status", "%%token_pesq_direta%%", "S"));
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.ENDOSSO_CONSULTA_STATUS.getCodigo(), 1, "status", "%%token_pesq_direta%%", "S"));
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.PROPOSTA_CONSULTA_STATUS.getCodigo(), 1, "status", "%%token_coord%%", "S"));
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.PROPOSTA_CONSULTA_STATUS.getCodigo(), 2, "critica", "critica", "S"));
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.PEDIDO_DE_ENDOSSO_CONSULTA_STATUS.getCodigo(), 1, "status", "%%token_coord%%", "S"));
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.PEDIDO_DE_ENDOSSO_CONSULTA_STATUS.getCodigo(), 2, "critica", "critica", "S"));
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.PROPOSTA_CONSULTA_CRITICA.getCodigo(), 1, "status", "%%token_coord%%", "S"));
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.PEDIDO_DE_ENDOSSO_CONSULTA_CRITICA.getCodigo(), 1, "status", "%%token_coord%%", "S"));
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.APOLICE_DOCUMENTO_EMISSAO2VIA.getCodigo(), 1, "link", "%%token_dados_apolice%%", "S"));
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.ENDOSSO_DOCUMENTO_EMISSAO2VIA.getCodigo(), 1, "link", "%%token_dados_apolice%%", "S"));
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.APOLICE_CARTEIRINHA_EMISSAO2VIA.getCodigo(), 1, "link", "%%token_dados_apolice%%", "S"));
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.APOLICE_KIT_SEGURADO_EMISSAO2VIA.getCodigo(), 1, "link", "%%token_dados_carteirinha%%", "S"));
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.APOLICE_KIT_SEGURADO_EMISSAO2VIA.getCodigo(), 1, "linkApolice", "%%token_dados_apolice%%", "S"));
//
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.APOLICE_BOLETO_EMISSAO2VIA.getCodigo(), 1, "link", "%%link%%", "S"));
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.APOLICE_BOLETO_EMISSAO2VIA.getCodigo(), 2, "numParcela", "%%token_n_parcela%%", "S"));
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.APOLICE_BOLETO_EMISSAO2VIA.getCodigo(), 3, "dataVencimento", "%%token_data_vencimento%%", "S"));
//
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.ENDOSSO_BOLETO_EMISSAO2VIA.getCodigo(), 1, "link", "%%link%%", "S"));
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.ENDOSSO_BOLETO_EMISSAO2VIA.getCodigo(), 2, "numParcela", "%%token_n_parcela%%", "S"));
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.ENDOSSO_BOLETO_EMISSAO2VIA.getCodigo(), 3, "dataVencimento", "%%token_data_vencimento%%", "S"));
//
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.PROPOSTA_BOLETO_EMISSAO2VIA.getCodigo(), 1, "link", "%%link%%", "S"));
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.PROPOSTA_BOLETO_EMISSAO2VIA.getCodigo(), 2, "numParcela", "%%token_n_parcela%%", "S"));
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.PROPOSTA_BOLETO_EMISSAO2VIA.getCodigo(), 3, "dataVencimento", "%%token_data_vencimento%%", "S"));
//
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.PEDIDO_DE_ENDOSSO_BOLETO_EMISSAO2VIA.getCodigo(), 1, "link", "%%link%%", "S"));
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.PEDIDO_DE_ENDOSSO_BOLETO_EMISSAO2VIA.getCodigo(), 2, "numParcela", "%%token_n_parcela%%", "S"));
//            campos.add(new MapeamentoCampos(ActionWatsonEnum.PEDIDO_DE_ENDOSSO_BOLETO_EMISSAO2VIA.getCodigo(), 3, "dataVencimento", "%%token_data_vencimento%%", "S"));

//@@saude@@
            //campos.add(new MapeamentoCampos(ActionWatsonEnum.STATUS_PAGAMENTO.getCodigo(), 1, "carteirinha", "codigoBeneficiario", "E", Constants.CamposFormulario.CARTEIRINHA));
            campos.add(new MapeamentoCampos(ActionWatsonEnum.STATUS_PAGAMENTO_INDIVIDUAL.getCodigo(), 3, "tabelaParcelas", "%%token_boleto_individual%%", "S"));
            campos.add(new MapeamentoCampos(ActionWatsonEnum.STATUS_PAGAMENTO_PME.getCodigo(), 3, "tabelaParcelas", "%%token_boleto_pme%%", "S"));
            campos.add(new MapeamentoCampos(ActionWatsonEnum.STATUS_PAGAMENTO_DEMAPOSENTADO.getCodigo(), 3, "tabelaParcelas", "%%token_boleto_demaposentado%%", "S"));
            

            for (MapeamentoCampos campo : campos) {
                Query.Filter f1 = new Query.FilterPredicate("codigoIntencao", Query.FilterOperator.EQUAL, campo.getCodigoIntencao());
                Query.Filter f2 = new Query.FilterPredicate("variavelOrigem", Query.FilterOperator.EQUAL, campo.getVariavelOrigem());
                Query.Filter f3 = new Query.FilterPredicate("tipo", Query.FilterOperator.EQUAL, campo.getTipo());

                Query.Filter filter = Query.CompositeFilterOperator.and(f1, f2, f3);

                if (mapeamentoCamposDao.getByFilter(filter) == null) {
                    mapeamentoCamposDao.insert(campo);
                }
            }

            //Nomes dos atendentes
            ArrayList<Atendentes> atendentes = new ArrayList<Atendentes>();

            atendentes.add(new Atendentes("Luana"));
            atendentes.add(new Atendentes("Guilherme"));
            atendentes.add(new Atendentes("Lara"));
            atendentes.add(new Atendentes("Henrique"));
            atendentes.add(new Atendentes("Nicole"));
            atendentes.add(new Atendentes("Leonardo"));
            atendentes.add(new Atendentes("Ana Cecília"));
            atendentes.add(new Atendentes("Gabriela"));
            atendentes.add(new Atendentes("Camila"));
            atendentes.add(new Atendentes("Letícia"));
            atendentes.add(new Atendentes("Manuela"));

            for (Atendentes atd : atendentes) {
                if (atendentesDao.getByProperty("nome", atd.getNome()) == null) {
                    atendentesDao.insert(atd);
                }
            }

            ArrayList<MapeamentoPayloadManifestacao> payloads = new ArrayList<MapeamentoPayloadManifestacao>();

//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.APOLICE_CONSULTA_STATUS.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Apólice/Certificado/Endosso","Consulta/Informações do Documento","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.PRAZO_EMISSAO.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Apólice/Certificado/Endosso","Consulta/Informações do Documento","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.PRAZO_VISTORIA_PREVIA.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Apólice/Certificado/Endosso","Consulta/Informações do Documento","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.APOLICE_ALTERACAO_FORMA_PAGTO_OU_DADOS_BANCARIOS.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Apólice/Certificado/Endosso","Consulta/Informações do Documento","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.APOLICE_RECUSA_A_PEDIDO.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Apólice/Certificado/Endosso","Consulta/Informações do Documento","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.APOLICE_ALTERACAO_QUANTIDADE_DE_PARCELAS‬‬.getCodigo(),"Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Apólice/Certificado/Endosso","Consulta/Informações do Documento","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.TRANSMITIR_ENDOSSO.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Apólice/Certificado/Endosso","Consulta/Informações do Documento","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.APOLICE_DEVOLUCAO.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Apólice/Certificado/Endosso","Consulta/Informações do Documento","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.APOLICE_ALTERACAO_MELHOR_DIA_DE_VENCIMENTO.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Apólice/Certificado/Endosso","Consulta/Informações do Documento","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.APOLICE_ALTERACAO_MELHOR_DIA_DE_PAGAMENTO.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Apólice/Certificado/Endosso","Consulta/Informações do Documento","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.APOLICE_ALTERACAO_CARTAO_DE_CREDITO.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Apólice/Certificado/Endosso","Consulta/Informações do Documento","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.APOLICE_ALTERACAO_CARTAO_DE_CREDITO_PARA_BOLETO.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Apólice/Certificado/Endosso","Consulta/Informações do Documento","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.PROPOSTA_CONSULTA_STATUS.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Proposta","Informação","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.PEDIDO_DE_ENDOSSO_CONSULTA_STATUS.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Pedido de endosso","Informação","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.APOLICE_BOLETO_EMISSAO2VIA_SEM_JUROS.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Apólice/Certificado/Endosso","2ª via de Boleto DCC","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.APOLICE_PRAZO_MAXIMO_DE_REPIQUE.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Apólice/Certificado/Endosso","Consulta de Pagamento","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.APOLICE_CONSULTA_RETORNO_BANCARIO.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Apólice/Certificado/Endosso","Consulta de Pagamento","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.APOLICE_CONSULTA_REPIQUE.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Apólice/Certificado/Endosso","Consulta de Pagamento","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.APOLICE_PAGAMENTO_EM_DUPLICIDADE.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Apólice/Certificado/Endosso","Consulta - Devolução de Valores","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.ENDOSSO_DEVOLUÇÃO.getCodigo(),"Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Apólice/Certificado/Endosso","Consulta - Devolução de Valores","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.RASTREADOR.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Apólice/Certificado/Endosso","Informação Rastreador","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.DOCUMENTACAO_BLINDAGEM.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Proposta","Regularização de Crítica","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.PROPOSTA_CONSULTA_CRITICA.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Proposta","Consulta - crítica","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.PEDIDO_DE_ENDOSSO_CONSULTA_CRITICA.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Pedido de endosso","Consulta - crítica","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.PEDIDO_DE_ENDOSSO_BOLETO_EMISSAO2VIA.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Pedido de endosso","2ª via da FCA","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.PEDIDO_DE_ENDOSSO_DOCUMENTO_EMISSAO2VIA.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Pedido de endosso","2º via de documento","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.APOLICE_BOLETO_EMISSAO2VIA.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Apólice/Certificado/Endosso","2ª via de boleto carnê","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.APOLICE_EMISSAO2VIA_BOLETO_VENCIDO.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Apólice/Certificado/Endosso","2ª via de Boleto carnê","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.ENDOSSO_BOLETO_EMISSAO2VIA.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Apólice/Certificado/Endosso","2ª via de boleto carnê","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.APOLICE_SOLICITACAO_CARNE.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Apólice/Certificado/Endosso","2ª via de Boleto carnê","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.PROPOSTA_BOLETO_EMISSAO2VIA.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Proposta","2ª via da FCA","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.PROPOSTA_RECUSA_A_PEDIDO.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Proposta","Recusa - pedido do corretor","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.PROPOSTA_DOCUMENTO_EMISSAO2VIA.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Proposta","2º via de documento","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.USO_DO_VEICULO_UBER.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Proposta","Orientação sobre enquadramento","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.APOLICE_DOCUMENTO_EMISSAO2VIA.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Apólice/Certificado/Endosso","2ª via de documento","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.ENDOSSO_DOCUMENTO_EMISSAO2VIA.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Apólice/Certificado/Endosso","2ª via de documento","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.APOLICE_CARTEIRINHA_EMISSAO2VIA.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Apólice/Certificado/Endosso","2ª via de documento","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.APOLICE_KIT_SEGURADO_EMISSAO2VIA.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Apólice/Certificado/Endosso","2ª via de Kit","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.INFORMACOES_DIVERSAS.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Informações Diversas","Manual do Produto","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.CONSULTA_MANUAL.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Informações Diversas","Manual do Produto","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.CONSULTA_POSTOS_DE_VISTORIA.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Informações Diversas","Manual do Produto","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.TROCA_VIDROS_FAROIS_LANTERNAS_RETROVISORES.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Informações Diversas","Manual do Produto","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.COTACAO_FACIL.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Proposta","Informação","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.DADOS_CORRETOR.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Informações Diversas","Informações / Dúvidas Manuseio do Portal","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.DEMAIS_CANAIS.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Informações Diversas","Demais canais","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.DEMAIS_CANAIS_SINISTRO.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Informações Diversas","Demais canais","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.DEMAIS_CANAIS_MASSIFICADOS.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Informações Diversas","Demais canais","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.DEMAIS_CANAIS_VIDA.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Informações Diversas","Demais canais","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.DEMAIS_CANAIS_ASSISTÊNCIA_24_HORAS.getCodigo(),"Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Informações Diversas","Demais canais","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.DEMAIS_CANAIS_CENTRAL_DE_COMISSAO.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Informações Diversas","Demais canais","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.DEMAIS_CANAIS_CENTRAL_DE_DEVOLUCAO.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Informações Diversas","Demais canais","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.DEMAIS_CANAIS_PREVIDENCIA.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Informações Diversas","Demais canais","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.DEMAIS_CANAIS_SAUDE.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Informações Diversas","Demais canais","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.PEDIDO_DE_ENDOSSO_RECUSA_A_PEDIDO.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Pedido de endosso","Recusa - pedido do corretor","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.APOLICE_CONSULTA_PAGAMENTO.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Apólice/Certificado/Endosso","Consulta de Pagamento","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.ENDOSSO_CONSULTA_PAGAMENTO.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Apólice/Certificado/Endosso","Consulta de Pagamento","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));
//            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.APOLICE_ALTERACAO_DCC_PARA_BOLETO.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Apólice/Certificado/Endosso","Consulta/Informações do Documento","sim","concluído","Chat inteligente","cópia da transcrição do chat","Email","Chat Inteligente"));

            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.DEFAULT.getCodigo(),"Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Informações Diversas","Manual do Produto","Sim","concluído","Chat inteligente","conversation","Email","Chat Inteligente"));
            //Action fake apenas para identificar encerramento por inatividade no chat
            payloads.add(new MapeamentoPayloadManifestacao(ActionWatsonEnum.INATIVIDADE.getCodigo(), "Atendimento Auto Individual","Solicitação - Atendimento Emissão Auto Individual","Informações Diversas","Inatividade sem servico","Sim","concluído","Chat inteligente","conversation","Email","Chat Inteligente"));

            for (MapeamentoPayloadManifestacao payload : payloads) {
                if (mapeamentoPayloadManifestacaoDao.getByProperty("action", payload.getAction()) == null) {
                    mapeamentoPayloadManifestacaoDao.insert(payload);
                }
            }

			// Parâmetros do Chat
			ArrayList<ParametroChat> listaParametros = new ArrayList<ParametroChat>();

            if (SystemProperty.environment.value() == SystemProperty.Environment.Value.Production) {
                // Appengine Dev, HML and Production
                listaParametros.add(new ParametroChat(Constants.Parametros.HORA_INICIO, "09:00"));
                listaParametros.add(new ParametroChat(Constants.Parametros.HORA_FIM, "18:30"));
                listaParametros.add(new ParametroChat(Constants.Parametros.CHECK_ATENDENTES, "true"));
                listaParametros.add(new ParametroChat(Constants.Parametros.EMAIL_TELEFONE_OBRIGATORIO, "true"));
                listaParametros.add(new ParametroChat(Constants.Parametros.TELEFONE_ATENDIMENTO, "0800-727-5555"));
                listaParametros.add(new ParametroChat(Constants.Parametros.HORARIO_ATENDIMENTO_TELEFONE, "2ª a 6ª feira - 09h às 18h30min (exceto feriados nacionais)"));
            }else{
                // Appengine localhost
                listaParametros.add(new ParametroChat(Constants.Parametros.HORA_INICIO, "00:00"));
                listaParametros.add(new ParametroChat(Constants.Parametros.HORA_FIM, "23:59"));
                listaParametros.add(new ParametroChat(Constants.Parametros.CHECK_ATENDENTES, "false"));
                listaParametros.add(new ParametroChat(Constants.Parametros.EMAIL_TELEFONE_OBRIGATORIO, "false"));
                listaParametros.add(new ParametroChat(Constants.Parametros.TELEFONE_ATENDIMENTO, "0800-727-5555"));
                listaParametros.add(new ParametroChat(Constants.Parametros.HORARIO_ATENDIMENTO_TELEFONE, "2ª a 6ª feira - 09h às 18h30min (exceto feriados nacionais)"));
            }



			for (ParametroChat parametro : listaParametros) {
				if (parametroChatDao.getByProperty(Constants.Parametros.CHAVE, parametro.getChave()) == null) {
					parametroChatDao.insert(parametro);
				}
			}
        }
    }
}
