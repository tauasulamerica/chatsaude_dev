package com.ciandt.watson_orquestrator.orquestrator.entity;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;

import java.io.Serializable;

@Entity
public class ParametroChat extends BaseEntity implements Serializable {
    
	@Index
    private String chave;
    private String valor;

    public ParametroChat() {
    }
    
    public ParametroChat(String chave, String valor) {
		super();
		this.chave = chave;
		this.valor = valor;
	}
    
    public String getChave() {
		return chave;
	}

	public void setChave(String chave) {
		this.chave = chave;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}
}
