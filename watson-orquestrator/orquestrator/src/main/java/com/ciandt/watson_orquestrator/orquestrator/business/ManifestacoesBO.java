package com.ciandt.watson_orquestrator.orquestrator.business;

import com.ciandt.watson_orquestrator.orquestrator.constants.Constants;
import com.ciandt.watson_orquestrator.orquestrator.dao.SessaoDao;
import com.ciandt.watson_orquestrator.orquestrator.entity.Sessao;
import com.ciandt.watson_orquestrator.orquestrator.helper.LoggerHelper;
import com.ciandt.watson_orquestrator.orquestrator.service.salesforce.ManifestacoesApi;
import com.ciandt.watson_orquestrator.orquestrator.util.DateUtil;
import com.google.api.server.spi.response.ConflictException;
import com.google.appengine.api.datastore.Query;

import org.glassfish.jersey.internal.util.ExceptionUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by rodrigogs on 06/03/17.
 */

public class ManifestacoesBO {
    private static final Logger log = Logger.getLogger(ManifestacoesBO.class.getName());
    private SessaoDao sessaoDao = null;
    private ManifestacoesApi manifestacoesApi = null;
    private ChatCorretorBO chatCorretorBO = null;

    public ManifestacoesBO() {
        sessaoDao = new SessaoDao();
        manifestacoesApi = new ManifestacoesApi();
        chatCorretorBO = new ChatCorretorBO();
    }

    public Boolean integrarManifestacoes() {
        List<Sessao> sessoes = new ArrayList<Sessao>();
        
        Query.Filter filter = criarFiltros();

        sessoes = sessaoDao.listByFilter(filter);

        Date dataAtual = DateUtil.getBRTTimeZoneDate();
        log.info("ManifestacoesBO - IntegrarManifestacoes, Data: " + dataAtual.toString());
        log.info("ManifestacoesBO - IntegrarManifestacoes, Itens para integrar: " + sessoes.size());
        
        manifestacoesApi.redefinirContadorTentativas();

        for (Sessao sessao : sessoes) {
            LoggerHelper.getInstance(log).log("ManifestacoesBO - IntegrarManifestacoes, Sessão a enviar: ", sessao);

            try {
				if (sessao.getFinalizada() == null || !sessao.getFinalizada()) {
					chatCorretorBO.inserirLogInatividade(sessao.getId());
					sessao.setFinalizada(true);
					sessao.setDataFinalizada(new Date());
					sessao.setAbandono(true);
				}
            	
                manifestacoesApi.integrar(sessao.getId());

                sessao.setEnviadoSalesForce(true);
                sessaoDao.update(sessao);

            } catch (ConflictException e) {
                log.severe("ManifestacoesBO - IntegrarManifestacoes, Erro ao integrar: " + ExceptionUtils.exceptionStackTraceAsString(e));
            }
        }

        return true;
    }
    
	private Query.Filter criarFiltros() {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MINUTE, -Constants.ManifestacoesApi.MINUTOS_INATIVIDADE);

		Date dataLimite = calendar.getTime();

		// Combinado com o cliente que serão consideradas apenas as sessões do dia
		calendar.add(Calendar.HOUR_OF_DAY, -calendar.get(Calendar.HOUR_OF_DAY));
		Date dataInicio = calendar.getTime();

		Query.Filter filterNaoEnviado = new Query.FilterPredicate("enviadoSalesForce", Query.FilterOperator.EQUAL, false);

		Query.Filter filterDataLimite = new Query.FilterPredicate("dataInicio", Query.FilterOperator.LESS_THAN, dataLimite);
		Query.Filter filterDataInicio = new Query.FilterPredicate("dataInicio", Query.FilterOperator.GREATER_THAN, dataInicio);
		Query.Filter filterAbandono = Query.CompositeFilterOperator.and(filterDataLimite, filterDataInicio);

		Query.Filter filterFinalizados = new Query.FilterPredicate("finalizada", Query.FilterOperator.EQUAL, true);
		Query.Filter filterFinalizadoOuAbandono = Query.CompositeFilterOperator.or(filterFinalizados, filterAbandono);

		return Query.CompositeFilterOperator.and(filterNaoEnviado, filterFinalizadoOuAbandono);
	}
}
