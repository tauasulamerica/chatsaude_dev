package com.ciandt.watson_orquestrator.orquestrator.model;

import java.io.Serializable;

/**
 * Created by rodrigogs on 24/01/17.
 */

public class FeedbacksInput implements Serializable {
    public Long identificadorSessao;
    public Boolean satisfacaoAtentimento;

    public FeedbacksInput() {
    }

    public Long getIdentificadorSessao() {
        return identificadorSessao;
    }

    public void setIdentificadorSessao(Long identificadorSessao) {
        this.identificadorSessao = identificadorSessao;
    }

    public Boolean getSatisfacaoAtentimento() {
        return satisfacaoAtentimento;
    }

    public void setSatisfacaoAtentimento(Boolean satisfacaoAtentimento) {
        this.satisfacaoAtentimento = satisfacaoAtentimento;
    }
}
