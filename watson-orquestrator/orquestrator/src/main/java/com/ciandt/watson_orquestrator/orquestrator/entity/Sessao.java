package com.ciandt.watson_orquestrator.orquestrator.entity;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by rodrigosclosa on 21/12/16.
 */
@Entity
public class Sessao extends BaseEntity implements Serializable {
    @Index
    private Date dataInicio;
    private String razaoSocial;
    private String nome;
    @Index
    private String codigoNAC;
    private String codigoCorretor;
    @Index
    private Boolean finalizada;
    @Index
    private Date dataFinalizada;
    private Long idAtendente;
    private Boolean satisfeitoAtendimento;

    @Index
    private Boolean enviadoSalesForce;

    @Index
    private String idManifestacao;
    @Index
    private String workspaceId;
    @Index
    private String conversationId;
    private Boolean abandono;
    public String email;
	public String telefone;

    public String carteirinha;

    public String origem;

    public String tipoProduto;

    public String codEmpresa;

    public String getCodEmpresa() {
        return codEmpresa;
    }

    public void setCodEmpresa(String codEmpresa) {
        this.codEmpresa = codEmpresa;
    }

    public Sessao() {
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public String getCodigoNAC() {
        return codigoNAC;
    }

    public void setCodigoNAC(String codigoNAC) {
        this.codigoNAC = codigoNAC;
    }

    public Boolean getFinalizada() {
        return finalizada;
    }

    public void setFinalizada(Boolean finalizada) {
        this.finalizada = finalizada;
    }

    public Date getDataFinalizada() {
        return dataFinalizada;
    }

    public void setDataFinalizada(Date dataFinalizada) {
        this.dataFinalizada = dataFinalizada;
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

    public String getCodigoCorretor() {
        return codigoCorretor;
    }

    public void setCodigoCorretor(String codigoCorretor) {
        this.codigoCorretor = codigoCorretor;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Long getIdAtendente() {
        return idAtendente;
    }

    public void setIdAtendente(Long idAtendente) {
        this.idAtendente = idAtendente;
    }

    public String getIdManifestacao() {
        return idManifestacao;
    }

    public void setIdManifestacao(String idManifestacao) {
        this.idManifestacao = idManifestacao;
    }

    public Boolean getSatisfeitoAtendimento() {
        return satisfeitoAtendimento;
    }

    public void setSatisfeitoAtendimento(Boolean satisfeitoAtendimento) {
        this.satisfeitoAtendimento = satisfeitoAtendimento;
    }

    public String getWorkspaceId() {
        return workspaceId;
    }

    public void setWorkspaceId(String workspaceId) {
        this.workspaceId = workspaceId;
    }

    public String getConversationId() {
        return conversationId;
    }

    public void setConversationId(String conversationId) {
        this.conversationId = conversationId;
    }

    public Boolean getEnviadoSalesForce() {
        return enviadoSalesForce;
    }

    public void setEnviadoSalesForce(Boolean enviadoSalesForce) {
        this.enviadoSalesForce = enviadoSalesForce;
    }

	public Boolean getAbandono() {
		return abandono;
	}

	public void setAbandono(Boolean abandono) {
		this.abandono = abandono;
	}

    public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

    public String getCarteirinha() {
        return carteirinha;
    }

    public void setCarteirinha(String carteirinha) {
        this.carteirinha = carteirinha;
    }

    public String getOrigem() {
        return origem;
    }

    public void setOrigem(String origem) {
        this.origem = origem;
    }

    public String getTipoProduto() {
        return tipoProduto;
    }

    public void setTipoProduto(String tipoProduto) {
        this.tipoProduto = tipoProduto;
    }
}
