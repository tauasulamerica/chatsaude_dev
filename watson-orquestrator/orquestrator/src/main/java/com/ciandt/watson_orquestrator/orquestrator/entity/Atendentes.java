package com.ciandt.watson_orquestrator.orquestrator.entity;

import com.googlecode.objectify.annotation.Entity;

import java.io.Serializable;

/**
 * Created by rodrigogs on 03/01/17.
 */
@Entity
public class Atendentes extends BaseEntity implements Serializable {
    private String nome;

    public Atendentes() {
    }

    public Atendentes(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
