package com.ciandt.watson_orquestrator.orquestrator.entity;

import com.ciandt.watson_orquestrator.orquestrator.util.StringUtil;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;

import java.io.Serializable;

/**
 * Created by gsanchez on 18/01/2017.
 */
@Entity
public class MapeamentoPayloadManifestacao extends BaseEntity implements Serializable {
    @Index
    private Integer action;
    private String Type;
    private String CATEGORIAC;
    private String TIPOC;
    private String SUBTIPOCPESQUISAC;
    private String FECHAREMTEMPODEATENDIMENTOC;
    private String Status;
    private String Subject;
    private String Coments;
    private String FORMADECONTATOC;
    private String Origin;


    public MapeamentoPayloadManifestacao() {
    }

    public MapeamentoPayloadManifestacao(Integer action, String type, String CATEGORIAC, String TIPOC, String SUBTIPOCPESQUISAC, String FECHAREMTEMPODEATENDIMENTOC, String status, String subject,String Coments, String FORMADECONTATOC, String origin) {
        this.action = action;
        Type = type;
        this.CATEGORIAC = CATEGORIAC;
        this.TIPOC = TIPOC;
        this.SUBTIPOCPESQUISAC = SUBTIPOCPESQUISAC;
        this.FECHAREMTEMPODEATENDIMENTOC = FECHAREMTEMPODEATENDIMENTOC;
        Status = status;
        Subject = subject;
        this.Coments = Coments;
        this.FORMADECONTATOC = FORMADECONTATOC;
        Origin = origin;
    }

    public Integer getAction() {
        return action;
    }

    public void setAction(Integer action) {
        this.action = action;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getCATEGORIAC() {
        return CATEGORIAC;
    }

    public void setCATEGORIAC(String CATEGORIAC) {
        this.CATEGORIAC = CATEGORIAC;
    }

    public String getTIPOC() {
        return TIPOC;
    }

    public void setTIPOC(String TIPOC) {
        this.TIPOC = TIPOC;
    }

    public String getSUBTIPOCPESQUISAC() {
        return SUBTIPOCPESQUISAC;
    }

    public void setSUBTIPOCPESQUISAC(String SUBTIPOCPESQUISAC) {
        this.SUBTIPOCPESQUISAC = SUBTIPOCPESQUISAC;
    }

    public String getFECHAREMTEMPODEATENDIMENTOC() {
        return FECHAREMTEMPODEATENDIMENTOC;
    }

    public void setFECHAREMTEMPODEATENDIMENTOC(String FECHAREMTEMPODEATENDIMENTOC) {
        this.FECHAREMTEMPODEATENDIMENTOC = FECHAREMTEMPODEATENDIMENTOC;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getSubject() {
        return Subject;
    }

    public void setSubject(String subject) {
        Subject = subject;
    }

    public String getComents() {
        return Coments;
    }

    public void setComents(String coments) {
        Coments = coments;
    }

    public String getFORMADECONTATOC() {
        return FORMADECONTATOC;
    }

    public void setFORMADECONTATOC(String FORMADECONTATOC) {
        this.FORMADECONTATOC = FORMADECONTATOC;
    }

    public String getOrigin() {
        return Origin;
    }

    public void setOrigin(String origin) {
        Origin = origin;
    }
}
