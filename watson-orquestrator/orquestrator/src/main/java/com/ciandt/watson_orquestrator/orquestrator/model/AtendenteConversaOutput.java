package com.ciandt.watson_orquestrator.orquestrator.model;

import java.io.Serializable;

/**
 * Created by rodrigogs on 11/01/17.
 */

public class AtendenteConversaOutput implements Serializable {
    public Long id;
    public String nome;

    public AtendenteConversaOutput() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
