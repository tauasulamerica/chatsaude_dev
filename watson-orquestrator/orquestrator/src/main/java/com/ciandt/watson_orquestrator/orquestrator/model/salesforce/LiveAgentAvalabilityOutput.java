package com.ciandt.watson_orquestrator.orquestrator.model.salesforce;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rodrigogs on 19/04/17.
 */

public class LiveAgentAvalabilityOutput implements Serializable {
    private List<LiveAgentAvailabilityMessages> messages;

    public LiveAgentAvalabilityOutput() {
        messages = new ArrayList<LiveAgentAvailabilityMessages>();
    }

    public List<LiveAgentAvailabilityMessages> getMessages() {
        return messages;
    }

    public void setMessages(List<LiveAgentAvailabilityMessages> messages) {
        this.messages = messages;
    }
}
