package com.ciandt.watson_orquestrator.orquestrator.model.memcache;

import java.io.Serializable;

/**
 * Created by rodrigogs on 10/03/17.
 */

public class SFRecordTypeModel implements Serializable {
    private String recordTypeId;

    public SFRecordTypeModel() {
    }

    public String getRecordTypeId() {
        return recordTypeId;
    }

    public void setRecordTypeId(String recordTypeId) {
        this.recordTypeId = recordTypeId;
    }
}
