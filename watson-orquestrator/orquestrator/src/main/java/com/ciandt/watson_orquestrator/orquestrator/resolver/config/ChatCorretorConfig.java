package com.ciandt.watson_orquestrator.orquestrator.resolver.config;

/**
 * Created by rodrigogs on 24/04/17.
 */

public class ChatCorretorConfig {
    private Integer duracaoURL;

    public ChatCorretorConfig() {
    }

    public Integer getDuracaoURL() {
        return duracaoURL;
    }

    public void setDuracaoURL(Integer duracao) {
        this.duracaoURL = duracao;
    }
}
