package com.ciandt.watson_orquestrator.orquestrator.resolver;

import com.ciandt.watson_orquestrator.orquestrator.util.ConfigReader;

import org.glassfish.jersey.internal.util.ExceptionUtils;

import java.lang.reflect.Field;
import java.util.logging.Logger;

/**
 * Classe para resolver automaticamente e retornar um objeto de configuração de cada serviço
 * Created by rodrigosclosa on 19/12/16.
 */
public class ConfigResolver<T> {
    private static Logger log = null;

    protected Class<T> clazz;

    public ConfigResolver(Class<?> clazz) {
        log = Logger.getLogger(getClass().getName());
        this.clazz = (Class<T>) clazz;
    }

    public T getConfig() throws IllegalAccessException {
        Object config = instanceNew(clazz);

        for (Field field : config.getClass().getDeclaredFields()) {
            System.out.print(field.getName()+": ");

            field.setAccessible(true);

            Object paramValue = ConfigReader.getInstance().getParam(field.getName());

            if(paramValue != null) {
                field.set(config, paramValue);
            }
        }

        return (T)config;
    }

    public Object instanceNew(Class<?> clazz) {
        Object o = null;

        try {
            o = Class.forName(clazz.getName()).newInstance();
        } catch (Exception e) {
            log.severe("Erro ao instanciar um novo objeto. " + ExceptionUtils.exceptionStackTraceAsString(e));
        }

        return o;
    }
}
