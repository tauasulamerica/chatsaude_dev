package com.ciandt.watson_orquestrator.orquestrator.model.sulamerica;

import java.io.Serializable;

public class ConsultarDadosBoletoSaudeInput extends ServiceInputBase implements Serializable  {
    public String getCarteirinha() {
        return carteirinha;
    }

    public void setCarteirinha(String carteirinha) {
        this.carteirinha = carteirinha;
    }

    private String carteirinha;

    public String getTipoProduto() {
        return tipoProduto;
    }

    public void setTipoProduto(String tipoProduto) {
        this.tipoProduto = tipoProduto;
    }

    private String tipoProduto;

    public String getCodEmpresa() {
        return codEmpresa;
    }

    public void setCodEmpresa(String codEmpresa) {
        this.codEmpresa = codEmpresa;
    }

    private String codEmpresa;
}
