package com.ciandt.watson_orquestrator.orquestrator.service.salesforce;

import com.ciandt.watson_orquestrator.orquestrator.constants.Constants;
import com.ciandt.watson_orquestrator.orquestrator.dao.LogDao;
import com.ciandt.watson_orquestrator.orquestrator.dao.MapeamentoPayloadManifestacaoDao;
import com.ciandt.watson_orquestrator.orquestrator.dao.MapeamentoServicosDao;
import com.ciandt.watson_orquestrator.orquestrator.dao.SessaoDao;
import com.ciandt.watson_orquestrator.orquestrator.entity.Log;
import com.ciandt.watson_orquestrator.orquestrator.entity.MapeamentoPayloadManifestacao;
import com.ciandt.watson_orquestrator.orquestrator.entity.Sessao;
import com.ciandt.watson_orquestrator.orquestrator.enums.ActionWatsonEnum;
import com.ciandt.watson_orquestrator.orquestrator.enums.TokenTypesEnum;
import com.ciandt.watson_orquestrator.orquestrator.exceptions.AccessTokenNullException;
import com.ciandt.watson_orquestrator.orquestrator.exceptions.GeneralErrorServiceException;
import com.ciandt.watson_orquestrator.orquestrator.exceptions.NotFoundServiceException;
import com.ciandt.watson_orquestrator.orquestrator.exceptions.SaSServicesException;
import com.ciandt.watson_orquestrator.orquestrator.helper.LoggerHelper;
import com.ciandt.watson_orquestrator.orquestrator.model.memcache.SFCorretorModel;
import com.ciandt.watson_orquestrator.orquestrator.model.memcache.SFRecordTypeModel;
import com.ciandt.watson_orquestrator.orquestrator.model.memcache.SFSubTipoModel;
import com.ciandt.watson_orquestrator.orquestrator.resolver.config.SalesforceConfig;
import com.ciandt.watson_orquestrator.orquestrator.resolver.memcache.SFCorretorCacheResolver;
import com.ciandt.watson_orquestrator.orquestrator.resolver.memcache.SFRecordTypeCacheResolver;
import com.ciandt.watson_orquestrator.orquestrator.resolver.memcache.SFSubTipoCacheResolver;
import com.ciandt.watson_orquestrator.orquestrator.service.BaseServices;
import com.ciandt.watson_orquestrator.orquestrator.service.sensedia.SensediaSevice;
import com.google.api.server.spi.response.ConflictException;
import com.google.api.server.spi.response.UnauthorizedException;
import com.google.apphosting.api.ApiProxy;

import org.glassfish.jersey.internal.util.ExceptionUtils;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import io.swagger.client.salesforce.manifestacao.ApiClient;
import io.swagger.client.salesforce.manifestacao.ApiException;
import io.swagger.client.salesforce.manifestacao.api.ManifestacaoApi;
import io.swagger.client.salesforce.manifestacao.model.Atendimento;
import io.swagger.client.salesforce.manifestacao.model.Corretor;
import io.swagger.client.salesforce.manifestacao.model.Layouts;
import io.swagger.client.salesforce.manifestacao.model.Manifestacao;
import io.swagger.client.salesforce.manifestacao.model.Record;
import io.swagger.client.salesforce.manifestacao.model.RetornoManifestacao;
import io.swagger.client.salesforce.manifestacao.model.SubTipo;


/**
 * Created by rodrigosclosa on 21/12/16.
 */

public class ManifestacoesApi extends BaseServices<SalesforceConfig> {
    private static final String CLASS_NAME = ManifestacoesApi.class.getName();
    private static Logger log = Logger.getLogger(ManifestacoesApi.class.getName());
    int numRetries = 0;
    private String accessToken = "";
    private SensediaSevice sensediaSevice = null;
    private ApiClient clientConfig = null;
    private MapeamentoServicosDao mapeamentoServicosDao = null;
    private ManifestacaoApi manifestacoesApi = null;
    private SessaoDao sessaoDao = null;
    private LogDao logDao = null;
    private MapeamentoPayloadManifestacaoDao mapeamentoPayloadManifestacaoDao = null;
    private SFCorretorCacheResolver sfCorretorCacheResolver = null;
    private SFSubTipoCacheResolver sfSubTipoCacheResolver = null;
    private SFRecordTypeCacheResolver sfRecordTypeCacheResolver = null;
    private String nameRecord = "Solicitação";
    private String contentType = "application/json";

    public ManifestacoesApi() {
        super();
        log.entering(CLASS_NAME, "<init>");

        sensediaSevice = new SensediaSevice(config.getUrlManifestacao());
        mapeamentoServicosDao = new MapeamentoServicosDao();
        mapeamentoPayloadManifestacaoDao = new MapeamentoPayloadManifestacaoDao();
        sessaoDao = new SessaoDao();
        logDao = new LogDao();

        manifestacoesApi = new ManifestacaoApi();

        clientConfig = new ApiClient();
        clientConfig.setConnectTimeout(config.getTimeout());
        clientConfig.setBasePath(config.getUrlManifestacao());

        manifestacoesApi.setApiClient(clientConfig);
        numRetries = 0;

        sfCorretorCacheResolver = new SFCorretorCacheResolver();
        sfSubTipoCacheResolver = new SFSubTipoCacheResolver();
        sfRecordTypeCacheResolver = new SFRecordTypeCacheResolver();

        log.exiting(CLASS_NAME, "<init>");
    }

    public void requestAccessToken(Long idSessao) throws AccessTokenNullException, ConflictException {
        log.entering(CLASS_NAME, "requestAccessToken");
        try {
            log.info("ManifestacoesApi - Chamada Serviço: sensediaSevice.getAccessToken");
            accessToken = sensediaSevice.getAccessToken(idSessao, TokenTypesEnum.SalesForce);
            log.info("ManifestacoesApi - Retorno Serviço: sensediaSevice.getAccessToken");

            if (accessToken == null) {
                throw new AccessTokenNullException();
            }
        } catch (UnauthorizedException e) {
            log.severe("ManifestacoesApi - requestAccessToken: Não foi possível gerar um token de acesso para Sensedia. Stack trace: " + ExceptionUtils.exceptionStackTraceAsString(e));
            throw new ConflictException("ManifestacoesApi - requestAccessToken: Não foi possível gerar um token de acesso para Sensedia. Stack trace: " + ExceptionUtils.exceptionStackTraceAsString(e));
        }
        log.exiting(CLASS_NAME, "requestAccessToken", accessToken);
    }

    public void integrar(Long idSessao) throws ConflictException {
        log.entering(CLASS_NAME, "integrar");

        try {
            requestAccessToken(idSessao);

            Sessao sessao = sessaoDao.getByKey(idSessao);

            if (sessao != null) {
                Integer action = logDao.getActionByidSessao(idSessao);

                MapeamentoPayloadManifestacao mapeamento = getMapeamentoPayloadManifestacao(action);

                // Se for inatividade devemos considerar a ultima action executada
				if (ActionWatsonEnum.INATIVIDADE.getCodigo().equals(action)) {
					Log ultimoLog = logDao.getUltimoLogPreenchidoSessao(idSessao);

					//if(ultimoLog.getAction() != null && ultimoLog.getRetornoServico() != null && !ultimoLog.getRetornoServico().isEmpty()){
                    if(ultimoLog.getAction() != null
                            && ultimoLog.getRetornoServico() != null
                            && (ultimoLog.getRetornoServico().equals(Constants.ManifestacoesApi.RETORNO_SERVICO_SUCESSO)
                                || ultimoLog.getRetornoServico().equals(Constants.ManifestacoesApi.RESPOSTA_ESTATICA))) {
                        mapeamento = getMapeamentoPayloadManifestacao(ultimoLog.getAction());
                    }
				}

                if (mapeamento == null) {
                    //Considera o mapeamento padrão
                    mapeamento = getMapeamentoPayloadManifestacao(ActionWatsonEnum.DEFAULT.getCodigo());
                    log.info("ManifestacoesApi - mapeamento da action: " + action + " não encontrado na entidade MapeamentoPayloadManifestacao. Usando mapeamento DEFAULT(7000).");
                }

                LoggerHelper.getInstance(log).log("ManifestacoesApi - Mapeamento Payload", mapeamento);

                Corretor corretor = buscarAccountCorretor(idSessao, sessao.getCodigoCorretor());
                String recordTypeId = buscarRecordTypeIdSolicitacao(idSessao);
                SubTipo subtipo = buscarSubTipoByMapeamento(idSessao, mapeamento);
                RetornoManifestacao retorno = enviaManifestacao(corretor, recordTypeId, subtipo, mapeamento, idSessao, sessao);

                atualizaIdManifestacaoNaSessao(sessao, retorno);
            } else {
                throw new ConflictException("ManifestacoesApi - integrar: Erro ao executar o envio da manifestacao: sessão com id: " + idSessao + " não existe ");
            }
        } catch (AccessTokenNullException an) {
            log.severe("ManifestacoesApi: Não foi possível gerar um token de acesso para Sensedia. Stack trace: " + ExceptionUtils.exceptionStackTraceAsString(an));
            throw new ConflictException("ManifestacoesApi: Não foi possível gerar um token de acesso para Sensedia. Stack trace: " + ExceptionUtils.exceptionStackTraceAsString(an));
        } catch (SaSServicesException | ConflictException ex) {
            log.severe("ManifestacoesApi - integrar: Erro em serviços: " + ExceptionUtils.exceptionStackTraceAsString(ex));
            throw new ConflictException("ManifestacoesApi - integrar: " + ExceptionUtils.exceptionStackTraceAsString(ex));
        } catch (Exception e) {
            log.severe("ManifestacoesApi - integrar: Erro geral: " + ExceptionUtils.exceptionStackTraceAsString(e));
            throw new ConflictException("ManifestacoesApi - integrar: Erro geral: " + ExceptionUtils.exceptionStackTraceAsString(e));
        }

        log.exiting(CLASS_NAME, "integrar");
    }

	public String obterMensagemPreChat(Long idSessao) throws AccessTokenNullException, ConflictException {
		log.entering(CLASS_NAME, "obterMensagemPreChat");

		while (numRetries <= config.getTentativas()
				&& ApiProxy.getCurrentEnvironment().getRemainingMillis() > config.getTempoMinimoTentativa()) {
			try {
				requestAccessToken(idSessao);

				Atendimento atendimento = manifestacoesApi.atendimentosGet(config.getClientId(), accessToken, String.valueOf(idSessao), String.valueOf(idSessao));
				
				if(atendimento != null && atendimento.getDiasUteis() != null) {
					log.exiting(CLASS_NAME, "obterMensagemPreChat");
					return atendimento.getDiasUteis().getMensagemPreChat();
				}

				log.exiting(CLASS_NAME, "obterMensagemPreChat");
				return null;
			} catch (ApiException e) {
				if (e.getCode() == Constants.ApiErrorCodes.FORBIDDEN) {
					log.info("ManifestacoesApi - obterMensagemPreChat: FORBIDDEN. " + ExceptionUtils.exceptionStackTraceAsString(e));
					numRetries++;
					sleep(config.getIntervaloTentativas());
					return obterMensagemPreChat(idSessao);
				} else if (e.getCode() == Constants.ApiErrorCodes.UNAUTHORIZED) {
					log.info("ManifestacoesApi - obterMensagemPreChat: UNAUTHORIZED. " + ExceptionUtils.exceptionStackTraceAsString(e));
					numRetries++;
					sleep(config.getIntervaloTentativas());
					return obterMensagemPreChat(idSessao);
				} else {
					log.severe("ManifestacoesApi - obterMensagemPreChat: Não foi possível obter a mensagem. Stack trace: " + ExceptionUtils.exceptionStackTraceAsString(e));
				}
			} catch (Exception e) {
				log.severe("ManifestacoesApi - obterMensagemPreChat: Não foi possível obter a mensagem. Stack trace: " + ExceptionUtils.exceptionStackTraceAsString(e));
			}
		}

		log.exiting(CLASS_NAME, "obterMensagemPreChat");
		return null;
	}

    private void atualizaIdManifestacaoNaSessao(Sessao sessao, RetornoManifestacao retorno) {
        log.entering(CLASS_NAME, "atualizaIdManifestacaoNaSessao");
        if (retorno != null && retorno.getId() != null && !retorno.getId().isEmpty()) {
            sessao.setIdManifestacao(retorno.getId());
            sessaoDao.update(sessao);
        } else {
            sessao.setIdManifestacao(Constants.ManifestacoesApi.NAO_RETORNADO);
            sessaoDao.update(sessao);
        }
        log.exiting(CLASS_NAME, "atualizaIdManifestacaoNaSessao");
    }

    private MapeamentoPayloadManifestacao getMapeamentoPayloadManifestacao(Integer action) {
        log.entering(CLASS_NAME, "MapeamentoPayloadManifestacao");
        MapeamentoPayloadManifestacao mapeamento;
        if (action == null) {
            log.info("MapeamentoPayloadManifestacao - Chamada Serviço: mapeamentoPayloadManifestacaoDao.getByProperty");
            mapeamento = mapeamentoPayloadManifestacaoDao.getByProperty(Constants.ManifestacoesApi.ACTION, ActionWatsonEnum.DEFAULT.getCodigo());
            log.info("MapeamentoPayloadManifestacao - Chamada Serviço: mapeamentoPayloadManifestacaoDao.getByProperty");
        } else {
            mapeamento = mapeamentoPayloadManifestacaoDao.getByProperty(Constants.ManifestacoesApi.ACTION, action);
        }
        log.exiting(CLASS_NAME, "MapeamentoPayloadManifestacao", mapeamento);
        return mapeamento;
    }

    private RetornoManifestacao enviaManifestacao(Corretor corretor, String recordTypeId, SubTipo subtipo, MapeamentoPayloadManifestacao mapeamento, Long idSessao, Sessao sessao) throws NotFoundServiceException, GeneralErrorServiceException, SaSServicesException, ConflictException {
        log.entering(CLASS_NAME, "enviaManifestacao");
        while (numRetries <= config.getTentativas()) {
            try {
                Manifestacao manifestacao = new Manifestacao();
                preencherManifestacao(corretor, recordTypeId, subtipo, manifestacao, mapeamento, idSessao, sessao);

                LoggerHelper.getInstance(log).log("ManifestacoesApi - enviaManifestacao - Manifestação", manifestacao);
                log.info("enviaManifestacao - Chamada Serviço: manifestacoesApi.manifestacoesPost");
                RetornoManifestacao response = manifestacoesApi.manifestacoesPost(config.getClientId(), accessToken, contentType, String.valueOf(idSessao), String.valueOf(idSessao), manifestacao);
                log.info("enviaManifestacao - Chamada Serviço: manifestacoesApi.manifestacoesPost");
                LoggerHelper.getInstance(log).log("ManifestacoesApi - enviaManifestacao - Response", response);

                log.exiting(CLASS_NAME, "enviaManifestacao", response);
                return response;
            } catch (ApiException e) {
                if (e.getCode() == Constants.ApiErrorCodes.FORBIDDEN) {
                    requestAccessToken(null);
                    numRetries++;
                    sleep(config.getIntervaloTentativas());
                    return enviaManifestacao(corretor, recordTypeId, subtipo, mapeamento, idSessao, sessao);
                } else if (e.getCode() == Constants.ApiErrorCodes.UNAUTHORIZED) {
                    log.info("ManifestacoesApi - enviaManifestacao: UNAUTHORIZED. " + ExceptionUtils.exceptionStackTraceAsString(e));
                    numRetries++;
                    sleep(config.getIntervaloTentativas());
                    return enviaManifestacao(corretor, recordTypeId, subtipo, mapeamento, idSessao, sessao);
                } else {
                    LoggerHelper.getInstance(log).log(Level.SEVERE, "ManifestacoesApi - enviaManifestacao - ApiException", e.getResponseBody());

                    if (e.getResponseBody().contains(Constants.ApiErrorCodes.ERROR)) {
                        log.severe("ManifestacoesApi - enviaManifestacao: Erro no envio " + ExceptionUtils.exceptionStackTraceAsString(e));
                        log.exiting(CLASS_NAME, "enviaManifestacao");
                        throw new NotFoundServiceException();
                    } else {
                        log.severe("ManifestacoesApi - enviaManifestacao: Problema ao executar a chamada a API de enviaManifestacao. " + ExceptionUtils.exceptionStackTraceAsString(e));
                        log.exiting(CLASS_NAME, "enviaManifestacao");
                        throw new GeneralErrorServiceException();
                    }
                }
            } catch (Exception e) {
                log.severe("ManifestacoesApi - enviaManifestacao: Erro geral: " + ExceptionUtils.exceptionStackTraceAsString(e));
                log.exiting(CLASS_NAME, "enviaManifestacao");
                throw new GeneralErrorServiceException();
            }
        }

        log.exiting(CLASS_NAME, "enviaManifestacao");
        log.severe("ManifestacoesApi - enviaManifestacao: Número de tentativas excedidas. Tentativas: " + numRetries);
        return null;
    }

    private void preencherManifestacao(Corretor corretor, String recordTypeId, SubTipo subtipo, Manifestacao manifestacao, MapeamentoPayloadManifestacao mapeamento, Long idSessao, Sessao sessao) {
        log.entering(CLASS_NAME, "preencherManifestacao");
        manifestacao.setRecordTypeId(recordTypeId);
        manifestacao.setAccountId(corretor.getIdentificador());
        manifestacao.setType(mapeamento.getType());
        manifestacao.setCaTEGORIAC(mapeamento.getCATEGORIAC());
        manifestacao.setTiPOC(mapeamento.getTIPOC());
        manifestacao.setSuBTIPOPESQUISAC(subtipo.getIdentificador());
        manifestacao.setFeCHAREMTEMPODEATENDIMENTOC(mapeamento.getFECHAREMTEMPODEATENDIMENTOC());
        manifestacao.setStatus(mapeamento.getStatus());
        manifestacao.setSubject(mapeamento.getSubject());
        manifestacao.setDescription(logDao.getConversaByidSessao(idSessao));
        manifestacao.setFoRMADECONTATOC(mapeamento.getFORMADECONTATOC());
        manifestacao.setOrigin(mapeamento.getOrigin());
        manifestacao.setNoMEDAPESSOAC(sessao.getNome());
        manifestacao.setNmEPESSOACONTATOEMAILC(sessao.getEmail());
        manifestacao.setTeLEFONEPESSOAC(sessao.getTelefone());
        manifestacao.setIdSessionC(sessao.getId().toString());
        
        Log ultimoLog = logDao.getUltimoLogPreenchidoSessao(idSessao);
        if(ultimoLog != null){

            if(ultimoLog.getRetencaoParcial() != null) {
                manifestacao.setRetencaoParcialC(ultimoLog.getRetencaoParcial().toString());
            }

            if(ultimoLog.getAction() != null) {
                manifestacao.setActionC(ultimoLog.getAction().toString());
            }

            if(ultimoLog.getContadorDialogo() != null) {
                manifestacao.setContadorDialogoC(ultimoLog.getContadorDialogo().toString());
            }

            manifestacao.setIntencaoC(ultimoLog.getIntent());
            manifestacao.setRetornoDoServicoC(ultimoLog.getRetornoServico());
        }

        log.exiting(CLASS_NAME, "preencherManifestacao", manifestacao);
    }

    private SubTipo buscarSubTipoByMapeamento(Long idSessao, MapeamentoPayloadManifestacao mapeamento) throws NotFoundServiceException, GeneralErrorServiceException, ConflictException, AccessTokenNullException {
        log.entering(CLASS_NAME, "buscarSubTipoByMapeamento");

        SubTipo retorno = new SubTipo();

        while (numRetries <= config.getTentativas()) {
            try {
                String chave = String.format("%s%s%s", mapeamento.getCATEGORIAC(), mapeamento.getTIPOC(), mapeamento.getSUBTIPOCPESQUISAC());
                SFSubTipoModel sfSubTipoModel = sfSubTipoCacheResolver.getSubTipo(chave);

                if (sfSubTipoModel != null) {
                    log.info("buscarSubTipoByMapeamento - Recuperado do Cache: " + chave);
                    retorno.setIdentificador(sfSubTipoModel.getIdentificador());
                    retorno.setNome(sfSubTipoModel.getNome());

                    log.exiting(CLASS_NAME, "buscarSubTipoByMapeamento", retorno);
                    return retorno;
                } else {
                    log.info("buscarSubTipoByMapeamento - Chamada Serviço: manifestacoesApi.manifestacoesSubtiposGet");
                    List<SubTipo> subTipos = manifestacoesApi.manifestacoesSubtiposGet(config.getClientId(), accessToken, String.valueOf(idSessao), String.valueOf(idSessao), mapeamento.getCATEGORIAC(), mapeamento.getTIPOC(), mapeamento.getSUBTIPOCPESQUISAC());
                    log.info("buscarSubTipoByMapeamento - Retorno Serviço: manifestacoesApi.manifestacoesSubtiposGet");

                    LoggerHelper.getInstance(log).log("ManifestacoesApi - buscarSubTipoByMapeamento - subTipos", subTipos);

                    if (subTipos != null && !subTipos.isEmpty() && subTipos.get(0) != null) {
                        LoggerHelper.getInstance(log).log("ManifestacoesApi - buscarSubTipoByMapeamento - Sub tipos", subTipos);

                        retorno = subTipos.get(0);

                        //Guarda no Cache
                        sfSubTipoModel = new SFSubTipoModel();
                        sfSubTipoModel.setIdentificador(retorno.getIdentificador());
                        sfSubTipoModel.setNome(retorno.getNome());
                        sfSubTipoCacheResolver.saveSubTipo(chave, sfSubTipoModel);

                        log.exiting(CLASS_NAME, "buscarSubTipoByMapeamento", retorno);
                        return retorno;
                    } else {
                        log.severe("ManifestacoesApi - buscarSubTipoByMapeamento: Sub tipos não encontrados.");
                        log.exiting(CLASS_NAME, "buscarSubTipoByMapeamento");
                        throw new NotFoundServiceException();
                    }
                }
            } catch (ApiException e) {
                if (e.getCode() == Constants.ApiErrorCodes.FORBIDDEN) {
                    requestAccessToken(null);

                    numRetries++;
                    sleep(config.getIntervaloTentativas());

                    return buscarSubTipoByMapeamento(idSessao, mapeamento);
                } else if (e.getCode() == Constants.ApiErrorCodes.UNAUTHORIZED) {
                    log.info("ManifestacoesApi - buscarSubTipoByMapeamento: UNAUTHORIZED. " + ExceptionUtils.exceptionStackTraceAsString(e));

                    numRetries++;
                    sleep(config.getIntervaloTentativas());

                    return buscarSubTipoByMapeamento(idSessao, mapeamento);
                } else {
                    LoggerHelper.getInstance(log).log(Level.SEVERE, "ManifestacoesApi - buscarSubTipoByMapeamento - ApiException", e.getResponseBody());

                    if (e.getResponseBody().contains(Constants.ApiErrorCodes.ERROR)) {
                        log.severe("ManifestacoesApi - buscarSubTipoByMapeamento: Record não encontrado. " + ExceptionUtils.exceptionStackTraceAsString(e));
                        log.exiting(CLASS_NAME, "buscarSubTipoByMapeamento");
                        throw new NotFoundServiceException();
                    } else {
                        log.severe("ManifestacoesApi - buscarSubTipoByMapeamento: Problema ao executar a chamada a API de buscarRecordTypeIdSolicitacao. " + ExceptionUtils.exceptionStackTraceAsString(e));
                        log.exiting(CLASS_NAME, "buscarSubTipoByMapeamento");
                        throw new GeneralErrorServiceException();
                    }
                }
            }
        }

        log.exiting(CLASS_NAME, "buscarSubTipoByMapeamento");
        log.severe("ManifestacoesApi - buscarSubTipoByMapeamento: Número de tentativas excedidas. Tentativas: " + numRetries);
        return null;
    }

    private String buscarRecordTypeIdSolicitacao(Long idSessao) throws NotFoundServiceException, GeneralErrorServiceException, ConflictException, AccessTokenNullException {
        log.exiting(CLASS_NAME, "buscarRecordTypeIdSolicitacao");

        String retorno = null;

        while (numRetries <= config.getTentativas()) {
            try {
                SFRecordTypeModel sfRecordTypeModel = sfRecordTypeCacheResolver.getRecordType(nameRecord);

                if (sfRecordTypeModel != null) {
                    log.info("buscarRecordTypeIdSolicitacao - Recuperado do Cache: " + nameRecord);
                    retorno = sfRecordTypeModel.getRecordTypeId();

                    log.exiting(CLASS_NAME, "buscarRecordTypeIdSolicitacao", retorno);
                    return retorno;
                } else {
                    log.info("buscarRecordTypeIdSolicitacao - Chamada Serviço: manifestacoesApi.manifestacoesDescricaoLayoutsGet");
                    Layouts layout = manifestacoesApi.manifestacoesDescricaoLayoutsGet(config.getClientId(), accessToken, String.valueOf(idSessao), String.valueOf(idSessao));
                    log.info("buscarRecordTypeIdSolicitacao - Retorno Serviço: manifestacoesApi.manifestacoesDescricaoLayoutsGet");

                    LoggerHelper.getInstance(log).log("ManifestacoesApi - buscarRecordTypeIdSolicitacao - Layout", layout);

                    if (layout != null) {
                        for (Record record : layout.getRecordTypeMappings()) {
                            if (record.getName().equals(nameRecord)) {
                                retorno = record.getRecordTypeId();

                                //Salva no Cache
                                sfRecordTypeModel = new SFRecordTypeModel();
                                sfRecordTypeModel.setRecordTypeId(retorno);
                                sfRecordTypeCacheResolver.saveRecordType(nameRecord, sfRecordTypeModel);

                                log.exiting(CLASS_NAME, "buscarRecordTypeIdSolicitacao", record);
                                return retorno;
                            }
                        }
                        log.exiting(CLASS_NAME, "buscarRecordTypeIdSolicitacao");
                        log.severe("ManifestacoesApi - buscarRecordTypeIdSolicitacao: Record não encontrado. ");
                        throw new NotFoundServiceException();
                    } else {
                        log.exiting(CLASS_NAME, "buscarRecordTypeIdSolicitacao");
                        log.severe("ManifestacoesApi - buscarRecordTypeIdSolicitacao: lista de layouts vazia. ");
                        throw new NotFoundServiceException();
                    }
                }
            } catch (ApiException e) {
                if (e.getCode() == Constants.ApiErrorCodes.FORBIDDEN) {
                    requestAccessToken(null);

                    numRetries++;
                    sleep(config.getIntervaloTentativas());

                    return buscarRecordTypeIdSolicitacao(idSessao);
                } else if (e.getCode() == Constants.ApiErrorCodes.UNAUTHORIZED) {
                    log.info("ManifestacoesApi - buscarRecordTypeIdSolicitacao: UNAUTHORIZED. " + ExceptionUtils.exceptionStackTraceAsString(e));

                    numRetries++;
                    sleep(config.getIntervaloTentativas());

                    return buscarRecordTypeIdSolicitacao(idSessao);
                } else {
                    if (e.getResponseBody().contains(Constants.ApiErrorCodes.ERROR)) {
                        log.severe("ManifestacoesApi - buscarRecordTypeIdSolicitacao: Record não encontrado. " + ExceptionUtils.exceptionStackTraceAsString(e));
                        log.exiting(CLASS_NAME, "buscarRecordTypeIdSolicitacao");
                        throw new NotFoundServiceException();
                    } else {
                        LoggerHelper.getInstance(log).log(Level.SEVERE, "ManifestacoesApi - buscarRecordTypeIdSolicitacao - ApiException", e.getResponseBody());
                        log.severe("ManifestacoesApi - buscarRecordTypeIdSolicitacao: Problema ao executar a chamada a API de buscarRecordTypeIdSolicitacao. " + ExceptionUtils.exceptionStackTraceAsString(e));
                        log.exiting(CLASS_NAME, "buscarRecordTypeIdSolicitacao");
                        throw new GeneralErrorServiceException();
                    }
                }
            } catch (Exception e) {
                log.severe("ManifestacoesApi - buscarRecordTypeIdSolicitacao: Erro geral: " + ExceptionUtils.exceptionStackTraceAsString(e));
                log.exiting(CLASS_NAME, "buscarRecordTypeIdSolicitacao");
                throw new GeneralErrorServiceException();
            }
        }

        log.exiting(CLASS_NAME, "buscarRecordTypeIdSolicitacao");
        log.severe("ManifestacoesApi - buscarRecordTypeIdSolicitacao: Número de tentativas excedidas. Tentativas: " + numRetries);
        return null;
    }

    private Corretor buscarAccountCorretor(Long idSessao, String codigoCorretor) throws NotFoundServiceException, GeneralErrorServiceException, ConflictException, AccessTokenNullException {
        log.exiting(CLASS_NAME, "buscarAccountCorretor");
        Corretor retorno = new Corretor();

        while (numRetries <= config.getTentativas()) {
            try {
                LoggerHelper.getInstance(log).log("ManifestacoesApi - buscarAccountCorretor", codigoCorretor);
                SFCorretorModel corretorModel = sfCorretorCacheResolver.getCorretor(codigoCorretor);

                if (corretorModel != null) {
                    log.info("buscarAccountCorretor - Recuperado do Cache: " + codigoCorretor);
                    retorno.setIdentificador(corretorModel.getIdentificador());
                    retorno.setNome(corretorModel.getNome());

                    log.exiting(CLASS_NAME, "buscarAccountCorretor", retorno);
                    return retorno;
                } else {
                    log.info("buscarAccountCorretor - Chamada Serviço: manifestacoesApi.corretoresGet - " + config.getClientId() + " - " + codigoCorretor);
                    List<Corretor> corretores = manifestacoesApi.corretoresGet(config.getClientId(), accessToken, codigoCorretor, String.valueOf(idSessao), String.valueOf(idSessao));
                    log.info("buscarAccountCorretor - Retorno Serviço: manifestacoesApi.corretoresGet");

                    if (corretores != null && !corretores.isEmpty() && corretores.get(0) != null) {
                        LoggerHelper.getInstance(log).log("ManifestacoesApi - buscarAccountCorretor - Lista corretores", corretores);
                        retorno = corretores.get(0);

                        //Grava no Cache
                        corretorModel = new SFCorretorModel();
                        corretorModel.setIdentificador(retorno.getIdentificador());
                        corretorModel.setNome(retorno.getNome());
                        sfCorretorCacheResolver.saveCorretor(codigoCorretor, corretorModel);

                        log.exiting(CLASS_NAME, "buscarAccountCorretor", retorno);
                        return retorno;
                    } else {
                        log.severe("ManifestacoesApi - buscarAccountCorretor: Corretor não encontrado. ");
                        log.exiting(CLASS_NAME, "buscarAccountCorretor");
                        throw new NotFoundServiceException();
                    }
                }
            } catch (ApiException e) {
                if (e.getCode() == Constants.ApiErrorCodes.FORBIDDEN) {
                    requestAccessToken(null);

                    numRetries++;
                    sleep(config.getIntervaloTentativas());

                    return buscarAccountCorretor(idSessao, codigoCorretor);
                } else if (e.getCode() == Constants.ApiErrorCodes.UNAUTHORIZED) {
                    log.info("ManifestacoesApi - buscarAccountCorretor: UNAUTHORIZED. " + ExceptionUtils.exceptionStackTraceAsString(e));

                    numRetries++;
                    sleep(config.getIntervaloTentativas());

                    return buscarAccountCorretor(idSessao, codigoCorretor);
                } else {
                    if (e.getResponseBody().contains(Constants.ApiErrorCodes.ERROR)) {
                        log.severe("ManifestacoesApi - buscarAccountCorretor: Corretor não encontrado. " + ExceptionUtils.exceptionStackTraceAsString(e));
                        log.exiting(CLASS_NAME, "buscarAccountCorretor");
                        throw new NotFoundServiceException();
                    } else {
                        LoggerHelper.getInstance(log).log(Level.SEVERE, "ManifestacoesApi - buscarAccountCorretor - ApiException", e.getResponseBody());
                        log.severe("ManifestacoesApi - buscarAccountCorretor: Problema ao executar a chamada a API de buscarAccountCorretor. " + ExceptionUtils.exceptionStackTraceAsString(e));
                        log.exiting(CLASS_NAME, "buscarAccountCorretor");
                        throw new GeneralErrorServiceException();
                    }
                }
            } catch (Exception e) {
                log.severe("ManifestacoesApi - buscarAccountCorretor: Erro geral: " + ExceptionUtils.exceptionStackTraceAsString(e));
                log.exiting(CLASS_NAME, "buscarAccountCorretor");
                throw new GeneralErrorServiceException();
            }
        }

        log.exiting(CLASS_NAME, "buscarAccountCorretor");
        log.severe("ManifestacoesApi - buscarAccountCorretor: Número de tentativas excedidas. Tentativas: " + numRetries);
        return null;
    }
    
    public void redefinirContadorTentativas() {
    		numRetries = 0;
    }
}
