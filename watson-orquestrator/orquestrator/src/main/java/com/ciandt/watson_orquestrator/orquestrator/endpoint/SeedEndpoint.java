package com.ciandt.watson_orquestrator.orquestrator.endpoint;

import com.ciandt.watson_orquestrator.orquestrator.business.SeedDataBO;
import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.response.UnauthorizedException;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by rodrigosclosa on 13/12/16.
 */

@Api(
        name = "seed",
        version = "v1",
        namespace = @ApiNamespace(
                ownerDomain = "endpoint.orquestrator.watson_orquestrator.ciandt.com",
                ownerName = "endpoint.orquestrator.watson_orquestrator.ciandt.com",
                packagePath = ""
        )
)
public class SeedEndpoint {

    private SeedDataBO seedDataBO;

    public SeedEndpoint() {
        seedDataBO = new SeedDataBO();
    }

    @ApiMethod(name = "seedServicosWatson", path = "seed", httpMethod = ApiMethod.HttpMethod.POST)
    public void seedServicosWatson(HttpServletRequest req) throws UnauthorizedException {
        seedDataBO.seedServicosWatson(req);
    }

}
