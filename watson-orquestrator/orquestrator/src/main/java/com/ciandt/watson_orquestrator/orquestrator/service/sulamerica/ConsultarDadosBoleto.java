package com.ciandt.watson_orquestrator.orquestrator.service.sulamerica;

import com.ciandt.watson_orquestrator.orquestrator.constants.Constants;
import com.ciandt.watson_orquestrator.orquestrator.dao.SessaoDao;
import com.ciandt.watson_orquestrator.orquestrator.entity.Sessao;
import com.ciandt.watson_orquestrator.orquestrator.enums.ActionWatsonEnum;
import com.ciandt.watson_orquestrator.orquestrator.enums.TokenTypesEnum;
import com.ciandt.watson_orquestrator.orquestrator.exceptions.AccessTokenNullException;
import com.ciandt.watson_orquestrator.orquestrator.exceptions.EmptyErrorServiceException;
import com.ciandt.watson_orquestrator.orquestrator.exceptions.GeneralErrorServiceException;
import com.ciandt.watson_orquestrator.orquestrator.exceptions.NotFoundServiceException;
import com.ciandt.watson_orquestrator.orquestrator.exceptions.SaSServicesException;
import com.ciandt.watson_orquestrator.orquestrator.helper.LoggerHelper;
import com.ciandt.watson_orquestrator.orquestrator.model.sulamerica.ConsultarDadosBoletoInput;
import com.ciandt.watson_orquestrator.orquestrator.model.sulamerica.ConsultarDadosBoletoOutput;
import com.ciandt.watson_orquestrator.orquestrator.resolver.config.SaSServicesConfig;
import com.ciandt.watson_orquestrator.orquestrator.service.BaseServices;
import com.ciandt.watson_orquestrator.orquestrator.service.sensedia.SensediaSevice;
import com.google.api.server.spi.response.UnauthorizedException;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import org.glassfish.jersey.internal.util.ExceptionUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.text.DecimalFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import io.swagger.client.sulamerica.pagamentos.model.Parcela;
import io.swagger.client.sulamerica.pagamentos.ApiClient;
import io.swagger.client.sulamerica.pagamentos.ApiException;
import io.swagger.client.sulamerica.pagamentos.api.ParcelasApi;


/**
 * Created by rodrigogs on 03/01/17.
 */

public class ConsultarDadosBoleto extends BaseServices<SaSServicesConfig> implements SaSService<ConsultarDadosBoletoInput, ConsultarDadosBoletoOutput> {

    private static Logger log = null;
    private String accessToken = "";
    private SensediaSevice sensediaSevice = null;
    private ParcelasApi parcelasApi = null;
    private ApiClient clientConfig = null;
    private SessaoDao sessaoDao = null;
    int numRetries = 0;
    private String funcaoGetBoleto = "openBoletoPdf";

    public ConsultarDadosBoleto() {
        super();
        log = Logger.getLogger(getClass().getName());
        sensediaSevice = new SensediaSevice(config.getBaseURLPagamentosSas());
        sessaoDao = new SessaoDao();
        parcelasApi = new ParcelasApi();

        clientConfig = new ApiClient();
        clientConfig.setConnectTimeout(config.getTimeout());
        clientConfig.setBasePath(config.getBaseURLPagamentosSas());

        parcelasApi.setApiClient(clientConfig);
    }

    @Override
    public void requestAccessToken(Long idSessao) throws AccessTokenNullException, GeneralErrorServiceException {
        try {
            accessToken = sensediaSevice.getAccessToken(idSessao, TokenTypesEnum.Documentos);

            if(accessToken == null) {
                throw new AccessTokenNullException();
            }
        } catch (UnauthorizedException e) {
            log.severe("ConsultarDadosBoleto - requestAccessToken: Não foi possível gerar um token de acesso para Sensedia. Stack trace: " + ExceptionUtils.exceptionStackTraceAsString(e));
            throw new GeneralErrorServiceException();
        }
    }

    @Override
    public ConsultarDadosBoletoOutput integrar(ConsultarDadosBoletoInput objetoEntrada, Long idSessao, String requestId) throws SaSServicesException {
        ConsultarDadosBoletoOutput retorno = new ConsultarDadosBoletoOutput();

        while (numRetries <= config.getTentativas()) {
            try {
                requestAccessToken(idSessao);

                Sessao sessao = sessaoDao.getByKey(idSessao);

                LoggerHelper.getInstance(log).log("ConsultarDadosBoleto - Objeto entrada", objetoEntrada);

                if (sessao == null) {
                    log.severe("ConsultarDadosBoleto - integrar: Sessão não encontrada para o ID: " + idSessao);
                    throw new NotFoundServiceException();
                }

                String numeroApolice = null;
                String numeroEndosso = null;

                if(ActionWatsonEnum.APOLICE_BOLETO_EMISSAO2VIA.getCodigo().equals(objetoEntrada.getAction()) ||
                        ActionWatsonEnum.PROPOSTA_BOLETO_EMISSAO2VIA.getCodigo().equals(objetoEntrada.getAction()) ||
                        ActionWatsonEnum.APOLICE_CONSULTA_PAGAMENTO.getCodigo().equals(objetoEntrada.getAction())) {
                    numeroApolice = objetoEntrada.getNumDocumento();
                } else if (ActionWatsonEnum.ENDOSSO_BOLETO_EMISSAO2VIA.getCodigo().equals(objetoEntrada.getAction()) ||
                        ActionWatsonEnum.PEDIDO_DE_ENDOSSO_BOLETO_EMISSAO2VIA.getCodigo().equals(objetoEntrada.getAction()) ||
                        ActionWatsonEnum.ENDOSSO_CONSULTA_PAGAMENTO.getCodigo().equals(objetoEntrada.getAction())) {
                    numeroEndosso = objetoEntrada.getNumEndosso();
                    numeroApolice = objetoEntrada.getNumDocumento();

                }

                List<Parcela> parcelas = parcelasApi.getParcelas(config.getClientId(), accessToken, String.valueOf(idSessao), requestId, Constants.ApiInfo.ORIGEM, Constants.ApiInfo.SISTEMA_ORIGEM, objetoEntrada.getNumSucursal(), numeroApolice, numeroEndosso, null, null, null);

				if (parcelas != null && parcelas.size() > 0 &&
                        (ActionWatsonEnum.APOLICE_CONSULTA_PAGAMENTO.getCodigo().equals(objetoEntrada.getAction()) ||
                                ActionWatsonEnum.ENDOSSO_CONSULTA_PAGAMENTO.getCodigo().equals(objetoEntrada.getAction())
                        ) ) {
					String tabela = obterTabelaParcelas(objetoEntrada, numeroApolice, numeroEndosso, parcelas);
					retorno.setTabelaParcelas(tabela);

					return retorno;
				}
	                
                if (parcelas == null || parcelas.size() == 0 || !verificarExistenciaParcelasPendentes(parcelas)) {
                    log.severe("ConsultarDadosBoleto - integrar: Não existem parcelas pendentes.");
                    throw new EmptyErrorServiceException();
                }

                LoggerHelper.getInstance(log).log("ConsultarDadosBoleto - response", parcelas);

                Parcela parcelaPendente = null;
                Parcela parcelaAux;
                Boolean debitoConta = false;

                for (int i = 0; i < parcelas.size(); i++) {
                    parcelaAux = parcelas.get(i);

                    if (parcelaAux.getStatus().equals(Constants.Status.PENDENTE)) {
                        if (i == 0 && parcelaAux.getTipoPagamento().equals(Constants.TipoPagamento.DEBITO_EM_CONTA)) {
                            retorno.setIdentificadorMensagem(Constants.MensagensAlternativas.PRIMEIRA_PARCELA_DCC);
                            return retorno;
                        }
                        
                        if (i == 0 && parcelaAux.getTipoPagamento().equals(Constants.TipoPagamento.CARTAO_DE_CREDITO)) {
                            retorno.setIdentificadorMensagem(Constants.MensagensAlternativas.PRIMEIRA_PARCELA_CARTAO_CREDITO);
                            return retorno;
                        }
                        
                        if (parcelaAux.getTipoPagamento().equals(Constants.TipoPagamento.CARTAO_DE_CREDITO)) {
                        		retorno.setIdentificadorMensagem(Constants.MensagensAlternativas.DEMAIS_PARCELAS_CARTAO_CREDITO);
                            return retorno;
                        }

                        if (parcelaAux.getStatus().equals(Constants.Status.PAGAMENTO_A_MENOR)) {
                            LoggerHelper.getInstance(log).log("ConsultarDadosBoleto - Status PAGAMENTO A MENOR", parcelaAux);
                            throw new GeneralErrorServiceException();
                        }

                        parcelaPendente = parcelaAux;
                        debitoConta = parcelaAux.getTipoPagamento().equals(Constants.TipoPagamento.DEBITO_EM_CONTA);
                        break;
                    }
                }

                retorno.setLink(montaUrlBoleto(parcelaPendente, numeroApolice, numeroEndosso, objetoEntrada.getNumSucursal(), debitoConta));
                retorno.setNumParcela(String.valueOf(parcelaPendente.getNumeroParcela()));
                retorno.setDataVencimento(parcelaPendente.getDataVencimento());

            } catch (AccessTokenNullException an) {
                log.severe("ConsultarDadosBoleto: Não foi possível gerar um token de acesso para Sensedia. Stack trace: " + ExceptionUtils.exceptionStackTraceAsString(an));
                throw new GeneralErrorServiceException();
            } catch (ApiException e) {
                if (e.getCode() == Constants.ApiErrorCodes.FORBIDDEN) {
                    requestAccessToken(idSessao);
                    numRetries++;
                    sleep(config.getIntervaloTentativas());

                    integrar(objetoEntrada, idSessao, requestId);
                } else if (e.getCode() == Constants.ApiErrorCodes.UNAUTHORIZED) {
                    log.info("ConsultarDadosBoleto - integrar: UNAUTHORIZED. " + ExceptionUtils.exceptionStackTraceAsString(e));

                    numRetries++;
                    sleep(config.getIntervaloTentativas());

                    integrar(objetoEntrada, idSessao, requestId);
                } else {
                    LoggerHelper.getInstance(log).log(Level.SEVERE, "ConsultarDadosBoleto - Erro", e.getResponseBody());

                    if (e.getResponseBody().contains(Constants.ApiErrorCodes.ERROR)) {
                        log.info("ConsultarDadosBoleto - integrar: Documento não encontrado. " + ExceptionUtils.exceptionStackTraceAsString(e));
                        throw new NotFoundServiceException();
                    } else {
                        log.info("ConsultarDadosBoleto - integrar: Problema ao executar a chamada a API de Documentos. " + ExceptionUtils.exceptionStackTraceAsString(e));
                        throw new NotFoundServiceException();
                    }
                }
            } catch (SaSServicesException se) {
                log.info("ConsultarDadosBoleto - integrar: Retorno do serviço: " + ExceptionUtils.exceptionStackTraceAsString(se));
                throw se;
            } catch (Exception e) {
                log.severe("ConsultarDadosBoleto - integrar: Erro geral: " + ExceptionUtils.exceptionStackTraceAsString(e));
                throw new GeneralErrorServiceException();
            }

            return retorno;
        }

        log.severe("ConsultarDadosBoleto - integrar: Número de tentativas excedidas. Tentativas: " + numRetries);
        return null;
    }

	private String obterTabelaParcelas(ConsultarDadosBoletoInput objetoEntrada, String numeroApolice, String numeroEndosso, List<Parcela> parcelas) throws IOException {
		StringBuilder strBuilder = new StringBuilder();

	    DecimalFormat formatter = new DecimalFormat("#0.00");

        String templateSemBoleto = obterTemplateParcela(Constants.Templates.APOLICE_CONSULTA_PAGAMENTO);
        String templateComBoleto = obterTemplateParcela(Constants.Templates.APOLICE_CONSULTA_PAGAMENTO_PENDENTE);

		for(Parcela parcela : parcelas) {
		    String template;

		    if(!parcela.getStatus().equals(Constants.Status.PENDENTE)
                    || parcela.getTipoPagamento().equals(Constants.TipoPagamento.CARTAO_DE_CREDITO)){
		        template = templateSemBoleto;
            } else {
                template = templateComBoleto;
            }

            String status = parcela.getStatus();

		    if(status.toLowerCase().startsWith(Constants.Status.PAGO.toLowerCase())){
                status = Constants.Status.PAGO;
            } else if(status.toLowerCase().equals(Constants.Status.CANCELADO_FALTA_PAGAMENTO.toLowerCase()) ||
                    status.toLowerCase().equals(Constants.Status.CANCELAMENTO_PEDIDO.toLowerCase())){
                status = Constants.Status.CANCELADO;
            }

            boolean exibirPopupDebitoConta = parcela.getTipoPagamento().equals(Constants.TipoPagamento.DEBITO_EM_CONTA);

		    template = template.replace("{{numeroParcela}}", String.valueOf(parcela.getNumeroParcela()))
                .replace("{{tipoPagamento}}", parcela.getTipoPagamento())
                .replace("{{valor}}", formatter.format(parcela.getValor()).replace(".", ","))
                .replace("{{vencimento}}", parcela.getDataVencimento() != "" ? parcela.getDataVencimento().substring(0,5) : "")
                .replace("{{dataPagamento}}", parcela.getDataPagamento() != "" ? parcela.getDataPagamento().substring(0,5) : "")
                .replace("{{situacao}}", status)
                .replace("{{botaoDownload}}", "<button ng-click=\"" + montaUrlBoleto(parcela, numeroApolice, numeroEndosso, objetoEntrada.getNumSucursal(), exibirPopupDebitoConta) + "\" >BOLETO</button>");

            strBuilder.append(template);
		}

		return strBuilder.toString();
	}

	private String obterTemplateParcela(String fileName) throws IOException {
        return Resources.toString(Resources.getResource(fileName), Charsets.UTF_8);
    }

    private String montaUrlBoleto(Parcela parcela, String numeroApolice, String numeroEndosso, String numSucursal, boolean exibirPopupDebitoConta) throws UnsupportedEncodingException {
        String baseUrl = config.getBaseURLPagamentosSas() + "/boletos/auto/segundaVia?tipoRetorno=1&origem=CH";
        String paramApolice = "&numeroApolice=";
        String paramEndosso = "&numeroEndosso=";
        String paramSucursal = "&codigoSucursal=";
        String paramParcela = "&numeroParcela=";
        String paramDataVencimento = "&dataVencimento=";
        String paramVencido = "&vencido=";

        if (numeroApolice != null) {
             paramApolice +=  numeroApolice;
        }

        if (numeroEndosso != null) {
            paramEndosso += numeroEndosso;
        }

        if (numSucursal != null) {
            paramSucursal += numSucursal;
        }

        if (parcela.getNumeroParcela() != null) {
            paramParcela += parcela.getNumeroParcela();
        }

        if (parcela.getTipoTransacao() != null) {
            if (parcela.getTipoTransacao().equals(Constants.TipoTransacao.VENCIDO)) {
                paramDataVencimento += parcela.getDataVencimento().replace("/", "");
                paramVencido += true;
            } else {
                paramVencido += false;
            }
        }

        return funcaoGetBoleto + "('" +  baseUrl + paramApolice + paramEndosso+ paramSucursal+ paramParcela +  paramVencido + paramDataVencimento + "', " + exibirPopupDebitoConta + ")";
    }

    private boolean verificarExistenciaParcelasPendentes(List<Parcela> parcelas) {
        for (Parcela parcela : parcelas){
            if(parcela.getStatus().equals(Constants.Status.PENDENTE)){
                return true;
            }
        }
        return false;
    }


    @Override
    public Class<ConsultarDadosBoletoInput> getTypeParameterInputClass() {
        return ConsultarDadosBoletoInput.class;
    }

    @Override
    public Class<ConsultarDadosBoletoOutput> getTypeParameterOutputClass() {
        return ConsultarDadosBoletoOutput.class;
    }
}
