package com.ciandt.watson_orquestrator.orquestrator.service.salesforce;

import com.ciandt.watson_orquestrator.orquestrator.constants.Constants;
import com.ciandt.watson_orquestrator.orquestrator.dao.ParametroChatDao;
import com.ciandt.watson_orquestrator.orquestrator.entity.ParametroChat;
import com.ciandt.watson_orquestrator.orquestrator.enums.TokenTypesEnum;
import com.ciandt.watson_orquestrator.orquestrator.exceptions.AccessTokenNullException;
import com.ciandt.watson_orquestrator.orquestrator.helper.LoggerHelper;
import com.ciandt.watson_orquestrator.orquestrator.model.salesforce.LiveAgentAvalabilityOutput;
import com.ciandt.watson_orquestrator.orquestrator.model.salesforce.LiveAgentMessageList;
import com.ciandt.watson_orquestrator.orquestrator.resolver.config.SalesforceConfig;
import com.ciandt.watson_orquestrator.orquestrator.service.BaseServices;
import com.ciandt.watson_orquestrator.orquestrator.service.sensedia.SensediaSevice;
import com.google.api.server.spi.response.ConflictException;
import com.google.api.server.spi.response.InternalServerErrorException;
import com.google.api.server.spi.response.UnauthorizedException;
import com.google.gson.Gson;
import com.google.gson.JsonElement;

import org.apache.commons.httpclient.HttpStatus;
import org.glassfish.jersey.internal.util.ExceptionUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import io.swagger.client.salesforce.manifestacao.ApiClient;
import io.swagger.client.salesforce.manifestacao.ApiException;
import io.swagger.client.salesforce.manifestacao.api.LiveAgentApi;

/**
 * Created by rodrigogs on 19/04/17.
 */

public class LiveAgentServices extends BaseServices<SalesforceConfig> {
    private static final String CLASS_NAME = LiveAgentServices.class.getName();
    private static Logger log = Logger.getLogger(LiveAgentServices.class.getName());
    int numRetries = 0;
    private String accessToken = "";
    private SensediaSevice sensediaSevice = null;
    private ApiClient clientConfig = null;
    private io.swagger.client.salesforce.manifestacao.api.LiveAgentApi liveAgentApi = null;
    private ParametroChatDao parametroChatDao;

    public LiveAgentServices() {
        super();
        log.entering(CLASS_NAME, "<init>");

        sensediaSevice = new SensediaSevice(config.getUrlManifestacao());
        liveAgentApi = new LiveAgentApi();
        parametroChatDao = new ParametroChatDao();

        clientConfig = new ApiClient();
        clientConfig.setConnectTimeout(config.getTimeout());
        clientConfig.setBasePath(config.getUrlManifestacao());

        liveAgentApi.setApiClient(clientConfig);
        numRetries = 0;

        log.exiting(CLASS_NAME, "<init>");
    }

    public void requestAccessToken() throws AccessTokenNullException, ConflictException {
        log.entering(CLASS_NAME, "requestAccessToken");
        try {
            log.info("LiveAgentServices - Chamada Serviço: sensediaSevice.getAccessToken");
            accessToken = sensediaSevice.getAccessToken(null, TokenTypesEnum.SalesForce);
            log.info("LiveAgentServices - Retorno Serviço: sensediaSevice.getAccessToken");

            if (accessToken == null) {
                throw new AccessTokenNullException();
            }
        } catch (UnauthorizedException e) {
            log.severe("LiveAgentServices - requestAccessToken: Não foi possível gerar um token de acesso para Sensedia. Stack trace: " + ExceptionUtils.exceptionStackTraceAsString(e));
            throw new ConflictException("LiveAgentServices - requestAccessToken: Não foi possível gerar um token de acesso para Sensedia. Stack trace: " + ExceptionUtils.exceptionStackTraceAsString(e));
        }
        log.exiting(CLASS_NAME, "requestAccessToken", accessToken);
    }

    public Boolean checkAvailability(String organizationId, String deploymentId, String buttonId) throws ConflictException, AccessTokenNullException {
        log.entering(CLASS_NAME, "checkAvailability");
        while (numRetries <= config.getTentativas()) {
            try {
                
            		ParametroChat parametroCheckAtendentes = parametroChatDao.getByProperty(Constants.Parametros.CHAVE, Constants.Parametros.CHECK_ATENDENTES);

                if(parametroCheckAtendentes != null) {
                    if(Boolean.parseBoolean(parametroCheckAtendentes.getValor())) {
                        //Valida se está habilitado para verificar os atendentes
                        requestAccessToken();

                        log.info("checkAvailability - Chamada Serviço: liveAgentApi.agentesDisponiveisGet");
                        Object retorno = liveAgentApi.agentesDisponiveisGet(config.getClientId(), accessToken, config.getApiVersion(), "", "", organizationId, deploymentId, buttonId);
                        log.info("checkAvailability - Chamada Serviço: liveAgentApi.agentesDisponiveisGet");
                        LoggerHelper.getInstance(log).log("LiveAgentServices - checkAvailability - Response", retorno);

                        Gson gson = new Gson();
                        JsonElement jsonElement = gson.toJsonTree(retorno);
                        LiveAgentAvalabilityOutput liveAgentAvalabilityOutput = gson.fromJson(jsonElement, LiveAgentAvalabilityOutput.class);

                        if (liveAgentAvalabilityOutput != null) {
                            if (liveAgentAvalabilityOutput.getMessages().size() > 0 &&
                                    liveAgentAvalabilityOutput.getMessages().get(0).getType().equals("Availability")) {
                                if (liveAgentAvalabilityOutput.getMessages().get(0).getMessage() != null &&
                                        liveAgentAvalabilityOutput.getMessages().get(0).getMessage().getResults().size() > 0) {
                                    if (liveAgentAvalabilityOutput.getMessages().get(0).getMessage().getResults().get(0).getAvailable() != null &&
                                            liveAgentAvalabilityOutput.getMessages().get(0).getMessage().getResults().get(0).getAvailable()) {
                                        log.exiting(CLASS_NAME, "checkAvailability", liveAgentAvalabilityOutput);
                                        return true;
                                    }
                                }
                            }
                        }

                        log.exiting(CLASS_NAME, "checkAvailability", liveAgentAvalabilityOutput);
                        return false;
                    } else {
                        //Retorna sempre True se não estiver habilitado
                        return true;
                    }
                } else {
                    //Retorna sempre True se não estiver habilitado
                    return true;
                }
            } catch (ApiException e) {
                if (e.getCode() == Constants.ApiErrorCodes.FORBIDDEN) {
                    requestAccessToken();
                    numRetries++;
                    sleep(config.getIntervaloTentativas());
                    return checkAvailability(organizationId, deploymentId, buttonId);
                } else if (e.getCode() == Constants.ApiErrorCodes.UNAUTHORIZED) {
                    log.info("LiveAgentServices - checkAvailability: UNAUTHORIZED. " + ExceptionUtils.exceptionStackTraceAsString(e));
                    numRetries++;
                    sleep(config.getIntervaloTentativas());
                    return checkAvailability(organizationId, deploymentId, buttonId);
                } else {
                    LoggerHelper.getInstance(log).log(Level.SEVERE, "LiveAgentServices - checkAvailability - ApiException", e.getResponseBody());

                    log.severe("LiveAgentServices - checkAvailability: Problema ao executar a chamada a API de enviaManifestacao. " + ExceptionUtils.exceptionStackTraceAsString(e));
                    log.exiting(CLASS_NAME, "checkAvailability");
                    throw new ConflictException("LiveAgentServices - checkAvailability: Problema ao executar a chamada a API de enviaManifestacao. " + ExceptionUtils.exceptionStackTraceAsString(e));
                }
            } catch (Exception e) {
                log.severe("LiveAgentServices - checkAvailability: Erro geral: " + ExceptionUtils.exceptionStackTraceAsString(e));
                log.exiting(CLASS_NAME, "checkAvailability");
                throw new ConflictException("LiveAgentServices - checkAvailability: Erro geral: " + ExceptionUtils.exceptionStackTraceAsString(e));
            }
        }

        log.exiting(CLASS_NAME, "checkAvailability");
        log.severe("LiveAgentServices - checkAvailability: Número de tentativas excedidas. Tentativas: " + numRetries);
        return false;
    }

	public LiveAgentMessageList getMessages(HttpServletRequest request) throws ConflictException, IOException, InternalServerErrorException {
		log.entering(CLASS_NAME, "mensagens");

		URL endpoint = new URL(config.getUrlLiveAgent() + "/System/Messages");
		HttpURLConnection con = (HttpURLConnection) endpoint.openConnection();
		con.setConnectTimeout(config.getGetMessagesTimeout());
		con.setRequestMethod("GET");

		con.setRequestProperty("X-LIVEAGENT-API-VERSION", request.getHeader("X-LIVEAGENT-API-VERSION"));
		con.setRequestProperty("X-LIVEAGENT-AFFINITY", request.getHeader("X-LIVEAGENT-AFFINITY"));
		con.setRequestProperty("X-LIVEAGENT-SESSION-KEY", request.getHeader("X-LIVEAGENT-SESSION-KEY"));

		BufferedReader streamReader = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
		StringBuilder strBuilder = new StringBuilder();

		String inputStr;
		while ((inputStr = streamReader.readLine()) != null) {
			strBuilder.append(inputStr);
		}

		log.exiting(CLASS_NAME, "mensagens");

		if (con.getResponseCode() == HttpStatus.SC_OK || con.getResponseCode() == HttpStatus.SC_NO_CONTENT) {
			String message = strBuilder.toString();
			
			if(message.contains("QueueUpdate")) {
				log.info("getMessages - QueueUpdate: " + message);
			}
			
			return new Gson().fromJson(message, LiveAgentMessageList.class);
		} else if (con.getResponseCode() == HttpStatus.SC_CONFLICT) {
			throw new ConflictException(strBuilder.toString());
		} else {
			throw new InternalServerErrorException(strBuilder.toString());
		}
	}
}
