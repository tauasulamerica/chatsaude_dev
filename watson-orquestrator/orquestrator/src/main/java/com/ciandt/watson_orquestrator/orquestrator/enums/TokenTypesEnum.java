package com.ciandt.watson_orquestrator.orquestrator.enums;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by rodrigogs on 12/01/17.
 *
 * Classe de tipos de Tokens para armazenamento e recuperação do Memcache
 */
public enum TokenTypesEnum {

    Watson("WT", "Watson"),
    Corretor("CR", "Corretor"),
    Documentos("DC", "Documentos"),
    SalesForce("SF", "SalesForce");

    private static final Map<String, TokenTypesEnum> lookup = new HashMap<String, TokenTypesEnum>();

    static {
        for(TokenTypesEnum s : EnumSet.allOf(TokenTypesEnum.class))
            lookup.put(s.getSigla(), s);
    }

    private String sigla;
    private String descricao;

    private TokenTypesEnum(String sigla, String descricao) {
        this.sigla = sigla;
        this.descricao = descricao;
    }

    public String getSigla() { return sigla; }
    public String getDescricao() { return descricao; }

    public static TokenTypesEnum get(String sigla) {
        return lookup.get(sigla);
    }
}
