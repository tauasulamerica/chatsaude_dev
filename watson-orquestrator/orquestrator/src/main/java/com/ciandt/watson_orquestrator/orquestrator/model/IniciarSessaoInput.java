package com.ciandt.watson_orquestrator.orquestrator.model;

import java.io.Serializable;

/**
 * Created by rodrigogs on 03/01/17.
 */

public class IniciarSessaoInput implements Serializable {
    public String params;
    public String nome;
    public String carteirinha;
    public String requestId;
    public String email;
	public String telefone;
    public String tipoproduto;
    public String codempresa;


    public String getTipoproduto() {
        return tipoproduto;
    }

    public void setTipoproduto(String tipoproduto) {
        this.tipoproduto = tipoproduto;
    }

    public String getCodempresa() {
        return codempresa;
    }

    public void setCodempresa(String codempresa) {
        this.codempresa = codempresa;
    }

    public IniciarSessaoInput() {
    }

    public String getParams() {
        params = params.replace(" ", "+");
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

    public String getCarteirinha() {
        return carteirinha;
    }

    public void setCarteirinha(String carteirinha) {
        this.carteirinha = carteirinha;
    }

    public String getTipoProduto() {
        return tipoproduto;
    }

    public void setTipoProduto(String tipoproduto) {
        this.tipoproduto = tipoproduto;
    }
}
