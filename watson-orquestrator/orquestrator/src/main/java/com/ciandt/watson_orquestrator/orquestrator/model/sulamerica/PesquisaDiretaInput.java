package com.ciandt.watson_orquestrator.orquestrator.model.sulamerica;

import java.io.Serializable;

/**
 * Created by rodrigogs on 03/01/17.
 */

public class PesquisaDiretaInput extends ServiceInputBase implements Serializable {
    private String codigoApolice;
    private String codigoEndosso;
    private String codigoSucursal;

    public PesquisaDiretaInput() {
    }

    public String getCodigoApolice() {
        return codigoApolice;
    }

    public void setCodigoApolice(String codigoApolice) {
        this.codigoApolice = codigoApolice;
    }

    public String getCodigoSucursal() {
        return codigoSucursal;
    }

    public void setCodigoSucursal(String codigoSucursal) {
        this.codigoSucursal = codigoSucursal;
    }

    public String getCodigoEndosso() {
        return codigoEndosso;
    }

    public void setCodigoEndosso(String codigoEndosso) {
        this.codigoEndosso = codigoEndosso;
    }
}
