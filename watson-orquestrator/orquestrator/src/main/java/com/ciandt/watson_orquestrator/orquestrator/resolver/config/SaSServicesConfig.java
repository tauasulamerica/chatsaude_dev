package com.ciandt.watson_orquestrator.orquestrator.resolver.config;

/**
 * Created by rodrigosclosa on 19/12/16.
 */

public class SaSServicesConfig {
    private Integer timeout;
    private String baseURLServicosSaS;
    private String baseURLCoretorSaS;
    private String baseURLPagamentosSas;

    private String baseURLPagamentos;

    private String clientId;
    private Integer tentativas;
    private Integer intervaloTentativas;
    public SaSServicesConfig() {
    }

    public Integer getTimeout() {
        return timeout;
    }

    public void setTimeout(Integer timeout) {
        this.timeout = timeout;
    }

    public String getBaseURLServicosSaS() {
        return baseURLServicosSaS;
    }

    public void setBaseURLServicosSaS(String baseURLServicosSaS) {
        this.baseURLServicosSaS = baseURLServicosSaS;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getBaseURLCoretorSaS() {
        return baseURLCoretorSaS;
    }

    public void setBaseURLCoretorSaS(String baseURLCoretorSaS) {
        this.baseURLCoretorSaS = baseURLCoretorSaS;
    }

    public Integer getTentativas() {
        return tentativas;
    }

    public void setTentativas(Integer tentativas) {
        this.tentativas = tentativas;
    }

    public Integer getIntervaloTentativas() {
        return intervaloTentativas;
    }

    public void setIntervaloTentativas(Integer intervaloTentativas) {
        this.intervaloTentativas = intervaloTentativas;
    }

    public String getBaseURLPagamentosSas() {
        return baseURLPagamentosSas;
    }

    public void setBaseURLPagamentosSas(String baseURLPagamentosSas) {
        this.baseURLPagamentosSas = baseURLPagamentosSas;
    }

    public String getBaseURLPagamentos() {
        return baseURLPagamentos;
    }

    public void setBaseURLPagamentos(String baseURLPagamentos) {
        this.baseURLPagamentos = baseURLPagamentos;
    }
}
