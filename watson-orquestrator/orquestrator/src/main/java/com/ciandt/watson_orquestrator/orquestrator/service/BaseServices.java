package com.ciandt.watson_orquestrator.orquestrator.service;

import com.ciandt.watson_orquestrator.orquestrator.resolver.ConfigResolver;

import org.glassfish.jersey.internal.util.ExceptionUtils;

import java.lang.reflect.ParameterizedType;
import java.util.logging.Logger;

/**
 * Created by rodrigosclosa on 19/12/16.
 */

public abstract class BaseServices<T> {
    private static Logger log = null;
    protected Class<T> clazz;
    public ConfigResolver<T> configResolver = null;
    public T config = null;

    public BaseServices() {
        log = Logger.getLogger(getClass().getName());
        clazz = (Class<T>) ((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        Object instance = instanceNew(clazz);
        configResolver = new ConfigResolver<T>(instance.getClass());

        try {
            config = configResolver.getConfig();
        } catch (Exception e) {
            log.severe("Erro ao recuperar as configurações do serviço. " + ExceptionUtils.exceptionStackTraceAsString(e));
        }
    }

    public Object instanceNew(Class<?> clazz) {
        Object o = null;

        try {
            o = Class.forName(clazz.getName()).newInstance();
        } catch (Exception e) {
            log.severe("Erro ao instanciar um novo objeto. " + ExceptionUtils.exceptionStackTraceAsString(e));
        }

        return o;
    }

    protected void sleep(Integer miliseconds) {
        try {
            Thread.sleep(miliseconds);
        } catch (InterruptedException ie) {}
    }
}
