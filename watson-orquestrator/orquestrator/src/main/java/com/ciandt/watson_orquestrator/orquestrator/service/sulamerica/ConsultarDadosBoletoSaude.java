package com.ciandt.watson_orquestrator.orquestrator.service.sulamerica;

import com.ciandt.watson_orquestrator.orquestrator.constants.Constants;
import com.ciandt.watson_orquestrator.orquestrator.dao.SessaoDao;
import com.ciandt.watson_orquestrator.orquestrator.entity.Sessao;
import com.ciandt.watson_orquestrator.orquestrator.enums.ActionWatsonEnum;
import com.ciandt.watson_orquestrator.orquestrator.enums.TokenTypesEnum;
import com.ciandt.watson_orquestrator.orquestrator.exceptions.AccessTokenNullException;
import com.ciandt.watson_orquestrator.orquestrator.exceptions.EmptyErrorServiceException;
import com.ciandt.watson_orquestrator.orquestrator.exceptions.GeneralErrorServiceException;
import com.ciandt.watson_orquestrator.orquestrator.exceptions.NotFoundServiceException;
import com.ciandt.watson_orquestrator.orquestrator.exceptions.SaSServicesException;
import com.ciandt.watson_orquestrator.orquestrator.helper.LoggerHelper;
import com.ciandt.watson_orquestrator.orquestrator.model.sulamerica.ConsultarDadosBoletoInput;
import com.ciandt.watson_orquestrator.orquestrator.model.sulamerica.ConsultarDadosBoletoOutput;
import com.ciandt.watson_orquestrator.orquestrator.model.sulamerica.ConsultarDadosBoletoSaudeInput;
import com.ciandt.watson_orquestrator.orquestrator.model.sulamerica.ConsultarDadosBoletoSaudeOutput;
import com.ciandt.watson_orquestrator.orquestrator.resolver.config.SaSServicesConfig;
import com.ciandt.watson_orquestrator.orquestrator.service.BaseServices;
import com.ciandt.watson_orquestrator.orquestrator.service.sensedia.SensediaSevice;
import com.google.api.server.spi.response.UnauthorizedException;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import io.swagger.client.sulamerica.pagamentosssaude.ApiClient;
import io.swagger.client.sulamerica.pagamentosssaude.ApiException;
import io.swagger.client.sulamerica.pagamentosssaude.api.SaudeApi;
import io.swagger.client.sulamerica.pagamentosssaude.model.Parcela;
import io.swagger.client.sulamerica.pagamentosssaude.model.ParcelaFatura;
import io.swagger.client.sulamerica.pagamentosssaude.model.ParcelaIndividualSaude;
import org.glassfish.jersey.internal.util.ExceptionUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;




/**
 * Created by rodrigogs on 03/01/17.
 */

public class ConsultarDadosBoletoSaude extends BaseServices<SaSServicesConfig> implements SaSService<ConsultarDadosBoletoSaudeInput, ConsultarDadosBoletoSaudeOutput> {

    private static Logger log = null;
    private String accessToken = "";
    private SensediaSevice sensediaSevice = null;
    private SaudeApi parcelasApi = null;
    private ApiClient clientConfig = null;
    private SessaoDao sessaoDao = null;
    int numRetries = 0;
    private String funcaoGetBoleto = "openBoletoPdf";

    public ConsultarDadosBoletoSaude() {
        super();
        log = Logger.getLogger(getClass().getName());
        sensediaSevice = new SensediaSevice(config.getBaseURLPagamentosSas());
        sessaoDao = new SessaoDao();
        parcelasApi = new SaudeApi();

        clientConfig = new ApiClient();
        clientConfig.setConnectTimeout(config.getTimeout());
        clientConfig.setBasePath(config.getBaseURLPagamentosSas());

        parcelasApi.setApiClient(clientConfig);
    }

    @Override
    public void requestAccessToken(Long idSessao) throws AccessTokenNullException, GeneralErrorServiceException {
        try {
            accessToken = sensediaSevice.getAccessToken(idSessao, TokenTypesEnum.Documentos);

            if(accessToken == null) {
                throw new AccessTokenNullException();
            }
        } catch (UnauthorizedException e) {
            log.severe("ConsultarDadosBoleto - requestAccessToken: Não foi possível gerar um token de acesso para Sensedia. Stack trace: " + ExceptionUtils.exceptionStackTraceAsString(e));
            throw new GeneralErrorServiceException();
        }
    }

    @Override
    public ConsultarDadosBoletoSaudeOutput integrar(ConsultarDadosBoletoSaudeInput objetoEntrada, Long idSessao, String requestId) throws SaSServicesException {
        ConsultarDadosBoletoSaudeOutput retorno = new ConsultarDadosBoletoSaudeOutput();

        while (numRetries <= config.getTentativas()) {
            try {
                requestAccessToken(idSessao);

                Sessao sessao = sessaoDao.getByKey(idSessao);

                LoggerHelper.getInstance(log).log("ConsultarDadosBoleto - Objeto entrada", objetoEntrada);

                if (sessao == null) {
                    log.severe("ConsultarDadosBoleto - integrar: Sessão não encontrada para o ID: " + idSessao);
                    throw new NotFoundServiceException();
                }

                if (sessao.getTipoProduto().equals("PME")) {
                    List<ParcelaFatura> parcelas = parcelasApi.getFaturasSaude(config.getClientId(), accessToken, "CH", sessao.getCodEmpresa());
                    if (parcelas != null && parcelas.size() > 0) {

                        Collections.sort(parcelas, new Comparator<ParcelaFatura>() {
                            DateFormat f = new SimpleDateFormat("dd/MM/yyyy");//or your pattern
                            @Override
                            public int compare(ParcelaFatura o1, ParcelaFatura o2) {
                                try {
                                    return f.parse(o1.getCompetencia()).compareTo(f.parse(o2.getCompetencia()));
                                } catch (ParseException e) {
                                    throw new IllegalArgumentException(e);
                                }
                            }
                        });

                        List<ParcelaFatura> parcelasTop = parcelas.subList(parcelas.size()>=4?parcelas.size()-4:0,parcelas.size());


                        String tabela = obterTabelaParcelasPME(objetoEntrada, sessao.getCodEmpresa(), parcelasTop);
                        retorno.setTabelaParcelas(tabela);

                        return retorno;
                    }

                }
                else if (sessao.getTipoProduto().equals("Individual")) {
                    List<ParcelaIndividualSaude> parcelas = parcelasApi.getParcelasSaude(config.getClientId(), accessToken, "CH", sessao.getCarteirinha());
                    if (parcelas != null && parcelas.size() > 0) {
                        parcelas.sort(Comparator.comparing(ParcelaIndividualSaude::getNumeroParcela));
                        List<ParcelaIndividualSaude> parcelasTop = parcelas.subList(parcelas.size()>=4?parcelas.size()-4:0,parcelas.size());


                        String tabela = obterTabelaParcelasIndividual(objetoEntrada, sessao.getCarteirinha(), parcelasTop);
                        retorno.setTabelaParcelas(tabela);

                        return retorno;
                    }
                }

//                if (parcelas == null || parcelas.size() == 0 || !verificarExistenciaParcelasPendentes(parcelas)) {
//                    log.severe("ConsultarDadosBoleto - integrar: Não existem parcelas pendentes.");
//                    throw new EmptyErrorServiceException();
//                }

//                LoggerHelper.getInstance(log).log("ConsultarDadosBoleto - response", parcelas);
//
//                Parcela parcelaPendente = null;
//                Parcela parcelaAux;
//                Boolean debitoConta = false;
//
//                for (int i = 0; i < parcelas.size(); i++) {
//                    parcelaAux = parcelas.get(i);
//
//                    if (parcelaAux.getStatus().equals(Constants.Status.PENDENTE)) {
//                        if (i == 0 && parcelaAux.getTipoPagamento().equals(Constants.TipoPagamento.DEBITO_EM_CONTA)) {
//                            retorno.setIdentificadorMensagem(Constants.MensagensAlternativas.PRIMEIRA_PARCELA_DCC);
//                            return retorno;
//                        }
//
//                        if (i == 0 && parcelaAux.getTipoPagamento().equals(Constants.TipoPagamento.CARTAO_DE_CREDITO)) {
//                            retorno.setIdentificadorMensagem(Constants.MensagensAlternativas.PRIMEIRA_PARCELA_CARTAO_CREDITO);
//                            return retorno;
//                        }
//
//                        if (parcelaAux.getTipoPagamento().equals(Constants.TipoPagamento.CARTAO_DE_CREDITO)) {
//                            retorno.setIdentificadorMensagem(Constants.MensagensAlternativas.DEMAIS_PARCELAS_CARTAO_CREDITO);
//                            return retorno;
//                        }
//
//                        if (parcelaAux.getStatus().equals(Constants.Status.PAGAMENTO_A_MENOR)) {
//                            LoggerHelper.getInstance(log).log("ConsultarDadosBoleto - Status PAGAMENTO A MENOR", parcelaAux);
//                            throw new GeneralErrorServiceException();
//                        }
//
//                        parcelaPendente = parcelaAux;
//                        debitoConta = parcelaAux.getTipoPagamento().equals(Constants.TipoPagamento.DEBITO_EM_CONTA);
//                        break;
//                    }
//                }
//
//                retorno.setLink(montaUrlBoleto(parcelaPendente, numeroApolice, numeroEndosso, objetoEntrada.getNumSucursal(), debitoConta));
//                retorno.setNumParcela(String.valueOf(parcelaPendente.getNumeroParcela()));
//                retorno.setDataVencimento(parcelaPendente.getDataVencimento());

            } catch (AccessTokenNullException an) {
                log.severe("ConsultarDadosBoleto: Não foi possível gerar um token de acesso para Sensedia. Stack trace: " + ExceptionUtils.exceptionStackTraceAsString(an));
                throw new GeneralErrorServiceException();
            } catch (ApiException e) {
                if (e.getCode() == Constants.ApiErrorCodes.FORBIDDEN) {
                    requestAccessToken(idSessao);
                    numRetries++;
                    sleep(config.getIntervaloTentativas());

                    integrar(objetoEntrada, idSessao, requestId);
                } else if (e.getCode() == Constants.ApiErrorCodes.UNAUTHORIZED) {
                    log.info("ConsultarDadosBoleto - integrar: UNAUTHORIZED. " + ExceptionUtils.exceptionStackTraceAsString(e));

                    numRetries++;
                    sleep(config.getIntervaloTentativas());

                    integrar(objetoEntrada, idSessao, requestId);
                } else {
                    LoggerHelper.getInstance(log).log(Level.SEVERE, "ConsultarDadosBoleto - Erro", e.getResponseBody());

                    if (e.getResponseBody().contains(Constants.ApiErrorCodes.ERROR)) {
                        log.info("ConsultarDadosBoleto - integrar: Documento não encontrado. " + ExceptionUtils.exceptionStackTraceAsString(e));
                        throw new NotFoundServiceException();
                    } else {
                        log.info("ConsultarDadosBoleto - integrar: Problema ao executar a chamada a API de Documentos. " + ExceptionUtils.exceptionStackTraceAsString(e));
                        throw new NotFoundServiceException();
                    }
                }
            } catch (SaSServicesException se) {
                log.info("ConsultarDadosBoleto - integrar: Retorno do serviço: " + ExceptionUtils.exceptionStackTraceAsString(se));
                throw se;
            } catch (Exception e) {
                log.severe("ConsultarDadosBoleto - integrar: Erro geral: " + ExceptionUtils.exceptionStackTraceAsString(e));
                throw new GeneralErrorServiceException();
            }

            return retorno;
        }

        log.severe("ConsultarDadosBoleto - integrar: Número de tentativas excedidas. Tentativas: " + numRetries);
        return null;
    }

    private String obterTabelaParcelasPME(ConsultarDadosBoletoSaudeInput objetoEntrada, String codEmpresa, List<ParcelaFatura> parcelas) throws IOException {
        StringBuilder strBuilder = new StringBuilder();

        DecimalFormat formatter = new DecimalFormat("#0.00");

        String templateSemBoleto = obterTemplateParcela(Constants.Templates.CONSULTA_PAGAMENTO_PME);
        String templateComBoleto = obterTemplateParcela(Constants.Templates.CONSULTA_PAGAMENTO_PME_PENDENTE);

        for(ParcelaFatura parcela : parcelas) {
            String template;
            String status = parcela.getSituacao();

            if(!status.toLowerCase().equals(Constants.Status.PENDENTE.toLowerCase())){
                template = templateSemBoleto;
            } else {
                template = templateComBoleto;
            }

            if(status.toLowerCase().startsWith(Constants.Status.PAGO.toLowerCase())){
                status = Constants.Status.PAGO;
            } else if(status.toLowerCase().equals(Constants.Status.CANCELADO_FALTA_PAGAMENTO.toLowerCase()) ||
                    status.toLowerCase().equals(Constants.Status.CANCELAMENTO_PEDIDO.toLowerCase())){
                status = Constants.Status.CANCELADO;
            }

            //boolean exibirPopupDebitoConta = parcela.getTipoPagamento().equals(Constants.TipoPagamento.DEBITO_EM_CONTA);
            boolean exibirPopupDebitoConta = false;

            template = template.replace("{{numeroParcela}}", String.valueOf(parcela.getCompetencia()))
                    .replace("{{tipoPagamento}}", "FATURA")
                    .replace("{{valor}}", formatter.format(parcela.getValorFaturado()).replace(".", ","))
                    .replace("{{vencimento}}", parcela.getDataVencimento() != "" ? parcela.getDataVencimento().substring(0,5) : "")
                    .replace("{{dataPagamento}}", parcela.getDataPagamento() != "" ? parcela.getDataPagamento().substring(0,5) : "")
                    .replace("{{situacao}}", status)
                    .replace("{{botaoDownload}}", "<button ng-click=\"" + montaUrlBoletoPME(parcela, codEmpresa, "IND") + "\" >FATURA</button>");

            strBuilder.append(template);
        }

        return strBuilder.toString();
    }

    private String obterTabelaParcelasIndividual(ConsultarDadosBoletoSaudeInput objetoEntrada, String carteirinha, List<ParcelaIndividualSaude> parcelas) throws IOException {
        StringBuilder strBuilder = new StringBuilder();

        DecimalFormat formatter = new DecimalFormat("#0.00");

        String templateSemBoleto = obterTemplateParcela(Constants.Templates.CONSULTA_PAGAMENTO_INDIVIDUAL);
        String templateComBoleto = obterTemplateParcela(Constants.Templates.CONSULTA_PAGAMENTO_INDIVIDUAL_PENDENTE);

        for(ParcelaIndividualSaude parcela : parcelas) {
            String template;
            String status = parcela.getSituacao();

            if(!status.toLowerCase().equals(Constants.Status.PENDENTE.toLowerCase())){
                template = templateSemBoleto;
            } else {
                template = templateComBoleto;
            }

            if(status.toLowerCase().startsWith(Constants.Status.PAGO.toLowerCase())){
                status = Constants.Status.PAGO;
            } else if(status.toLowerCase().equals(Constants.Status.CANCELADO_FALTA_PAGAMENTO.toLowerCase()) ||
                    status.toLowerCase().equals(Constants.Status.CANCELAMENTO_PEDIDO.toLowerCase())){
                status = Constants.Status.CANCELADO;
            }

            //boolean exibirPopupDebitoConta = parcela.getTipoPagamento().equals(Constants.TipoPagamento.DEBITO_EM_CONTA);
            boolean exibirPopupDebitoConta = false;

            template = template.replace("{{numeroParcela}}", String.valueOf(parcela.getNumeroParcela()))
                    .replace("{{tipoPagamento}}", "BOLETO")
                    .replace("{{valor}}", formatter.format(parcela.getValorEmitido()).replace(".", ","))
                    .replace("{{vencimento}}", parcela.getDataVencimento() != "" ? parcela.getDataVencimento().substring(0,5) : "")
                    .replace("{{dataPagamento}}", parcela.getDataPagamento() != "" ? parcela.getDataPagamento().substring(0,5) : "")
                    .replace("{{situacao}}", status)
                    .replace("{{botaoDownload}}", "<button ng-click=\"" + montaUrlBoletoIndividual(parcela, carteirinha, "IND") + "\" >BOLETO</button>");
            //.replace("{{botaoDownload}}", "<button>BOLETO</button>");

            strBuilder.append(template);
        }

        return strBuilder.toString();
    }

    private String obterTemplateParcela(String fileName) throws IOException {
        return Resources.toString(Resources.getResource(fileName), Charsets.UTF_8);
    }
//
    private String montaUrlBoletoIndividual(ParcelaIndividualSaude parcela, String carteirinha, String tipoRequisicao) throws UnsupportedEncodingException {
        String baseUrl = config.getBaseURLPagamentosSas() + "/boletos/saude/segundaVia?tipoRetorno=1&origem=CH";

        String paramParcela = "&numeroParcela=";
        String paramTipoRequisicao = "&tipoRequisicao=";
        String paramCarteirinha = "&codigoBeneficiario=";
        String paramStatus = "&status=";

        if (parcela.getNumeroParcela() != null) {
            paramParcela += parcela.getNumeroParcela();
        }

        if (carteirinha != null) {
            paramCarteirinha += carteirinha;
        }

        paramTipoRequisicao += "IND";
        paramStatus += "IND";

//        if (parcela.getTipoTransacao() != null) {
//            if (parcela.getTipoTransacao().equals(Constants.TipoTransacao.VENCIDO)) {
//                paramDataVencimento += parcela.getDataVencimento().replace("/", "");
//                paramVencido += true;
//            } else {
//                paramVencido += false;
//            }
//        }

        return funcaoGetBoleto + "('" +  baseUrl + paramParcela +  paramTipoRequisicao + paramCarteirinha + paramStatus + "', " + false + ")";
    }

    private String montaUrlBoletoPME(ParcelaFatura parcela, String codEmpresa, String tipoRequisicao) throws UnsupportedEncodingException {
        String baseUrl = config.getBaseURLPagamentosSas() + "/boletos/saude/segundaVia?tipoRetorno=1&origem=CH";

        String paramNossoNumero = "&nossoNumero=";
        String paramTipoRequisicao = "&tipoRequisicao=";
        String paramCodEmpresa = "&codigoEmpresa=";
        String paramStatus = "&status=";

        if (parcela.getNossoNumero() != null) {
            paramNossoNumero += parcela.getNossoNumero();
        }

        if (codEmpresa != null) {
            paramCodEmpresa += codEmpresa;
        }

        paramTipoRequisicao += "PME";
        paramStatus += "0";

//        if (parcela.getTipoTransacao() != null) {
//            if (parcela.getTipoTransacao().equals(Constants.TipoTransacao.VENCIDO)) {
//                paramDataVencimento += parcela.getDataVencimento().replace("/", "");
//                paramVencido += true;
//            } else {
//                paramVencido += false;
//            }
//        }

        return funcaoGetBoleto + "('" +  baseUrl + paramNossoNumero +  paramTipoRequisicao + paramCodEmpresa + paramStatus + "', " + false + ")";
    }
//
//    private boolean verificarExistenciaParcelasPendentes(List<Parcela> parcelas) {
//        for (Parcela parcela : parcelas){
//            if(parcela.getStatus().equals(Constants.Status.PENDENTE)){
//                return true;
//            }
//        }
//        return false;
//    }


    @Override
    public Class<ConsultarDadosBoletoSaudeInput> getTypeParameterInputClass() {
        return ConsultarDadosBoletoSaudeInput.class;
    }

    @Override
    public Class<ConsultarDadosBoletoSaudeOutput> getTypeParameterOutputClass() {
        return ConsultarDadosBoletoSaudeOutput.class;
    }
}
