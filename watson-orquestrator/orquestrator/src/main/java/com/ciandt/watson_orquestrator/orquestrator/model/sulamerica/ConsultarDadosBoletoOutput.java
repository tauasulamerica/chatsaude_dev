package com.ciandt.watson_orquestrator.orquestrator.model.sulamerica;

import java.io.Serializable;

/**
 * Created by rodrigogs on 03/01/17.
 */

public class ConsultarDadosBoletoOutput extends MensagemAlternativaBaseOutput implements Serializable {
    private String link;
    private String numParcela;
    private String dataVencimento;
    private String tabelaParcelas;


    public ConsultarDadosBoletoOutput() {}

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getNumParcela() {
        return numParcela;
    }

    public void setNumParcela(String numParcela) {
        this.numParcela = numParcela;
    }

    public String getDataVencimento() {
        return dataVencimento;
    }

    public void setDataVencimento(String dataVencimento) {
        this.dataVencimento = dataVencimento;
    }

	public String getTabelaParcelas() {
		return tabelaParcelas;
	}

	public void setTabelaParcelas(String tabelaParcelas) {
		this.tabelaParcelas = tabelaParcelas;
	}
}
