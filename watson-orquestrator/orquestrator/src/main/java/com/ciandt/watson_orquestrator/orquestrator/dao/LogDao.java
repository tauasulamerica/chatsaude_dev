package com.ciandt.watson_orquestrator.orquestrator.dao;

import com.ciandt.watson_orquestrator.orquestrator.constants.Constants;
import com.ciandt.watson_orquestrator.orquestrator.entity.Log;
import com.ciandt.watson_orquestrator.orquestrator.enums.ActionWatsonEnum;
import com.ciandt.watson_orquestrator.orquestrator.util.DateUtil;

import java.util.List;
import java.util.logging.Logger;

/**
 * Created by rodrigosclosa on 29/08/16.
 */
public class LogDao extends GenericDao<Log> {
    private static final Logger logger = Logger.getLogger(LogDao.class.getName());

    public Integer getActionByidSessao(Long idSessao){
        List<Log> logList = this.listByPropertyWithOrder("idSessao",idSessao,"-dataHora");

        for(Log log: logList){
            // Não deve ser considerado actions de finalização e redirecionamento na abertura de caso
            if(log.getAction() != null
                    && !ActionWatsonEnum.FIM_ATENDIMENTO_WATSON.getCodigo().equals(log.getAction())
                    && !ActionWatsonEnum.REDIRECIONAR_ATENDENTE.getCodigo().equals(log.getAction())) {
                return log.getAction();
            }
        }
        return null;
    }

    public String getConversaByidSessao(Long idSessao) {
        List<Log> logList = this.listByPropertyWithOrder("idSessao",idSessao,"dataHora");

        StringBuilder stb = new StringBuilder();

        for(Log log: logList) {
            if(log.getDados() != null) {
                String dataLog = DateUtil.dateToString(DateUtil.corrigeTimezone(log.getDataHora()));

                logger.info("getConversaByidSessao - Data Log: " + dataLog);

                stb.append("( " + log.getOrigem() + " - " + dataLog + " ) ");
                stb.append(log.getDados().getValue().replace("[","").replace("]","") + "\n");
            }
        }
        return stb.toString();
    }

    public Log getUltimoLogPreenchidoSessao(Long idSessao){
        List<Log> logList = this.listByPropertyWithOrder("idSessao",idSessao,"-dataHora");

        if(logList == null || logList.size() == 0){
            return null;
        }

        Log retorno = new Log();

        for(Log log: logList){
            if(retorno.getIntent() == null && log.getIntent() != null && !log.getIntent().isEmpty()) {
                retorno.setIntent(log.getIntent());
            }

            // Não deve ser considerado actions de finalização e redirecionamento na abertura de caso
            if(retorno.getAction() == null
                    && log.getAction() != null
                    && !ActionWatsonEnum.FIM_ATENDIMENTO_WATSON.getCodigo().equals(log.getAction())
                    && !ActionWatsonEnum.REDIRECIONAR_ATENDENTE.getCodigo().equals(log.getAction())
                    && !ActionWatsonEnum.INATIVIDADE.getCodigo().equals(log.getAction())
                    && (Constants.ManifestacoesApi.RETORNO_SERVICO_SUCESSO.equals(log.getRetornoServico())
                    || Constants.ManifestacoesApi.RESPOSTA_ESTATICA.equals(log.getRetornoServico()))) {
                retorno.setAction(log.getAction());
            }

            if(retorno.getRetornoServico() == null && log.getRetornoServico() != null && !log.getRetornoServico().isEmpty()) {
                retorno.setRetornoServico(log.getRetornoServico());
            }
            
            if(retorno.getContadorDialogo() == null && log.getContadorDialogo() != null) {
                retorno.setContadorDialogo(log.getContadorDialogo());
            }
            
            if(retorno.getRetencaoParcial() == null && log.getRetencaoParcial() != null) {
                retorno.setRetencaoParcial(log.getRetencaoParcial());
            }
        }

        return retorno;
    }

}