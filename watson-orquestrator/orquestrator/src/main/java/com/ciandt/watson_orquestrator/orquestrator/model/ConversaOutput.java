package com.ciandt.watson_orquestrator.orquestrator.model;

import com.ciandt.watson_orquestrator.orquestrator.entity.MapeamentoCampos;
import com.google.appengine.api.datastore.Text;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.swagger.client.sensedia.watson.model.OutputData;

/**
 * Created by rodrigosclosa on 29/12/16.
 */

public class ConversaOutput implements Serializable {
    private Map<String, Object> context;
    private OutputData output;
    private Long idSessao;
    private String razaoSocial;
    private Date data;
    private AtendenteConversaOutput atendente;
    private Text logConversa;
    private Boolean enviarSalesforce;
    private Boolean encerradoWatson;
    private String workspaceId;
    private IniciarSessaoParamsInput parametros;
    private String requestId;
    private String retornoServico;
    private List<MapeamentoCampos> camposFormulario;
    private String jsonIntents;
    private String jsonEntities;

    public ConversaOutput() {
        this.enviarSalesforce = false;
        this.encerradoWatson = false;
        atendente = new AtendenteConversaOutput();
        output = new OutputData();
        context = new HashMap<String, Object>();
    }

    public Map<String, Object> getContext() {
        return context;
    }

    public void setContext(Map<String, Object> context) {
        this.context = context;
    }

    public OutputData getOutput() {
        return output;
    }

    public void setOutput(OutputData output) {
        this.output = output;
    }

    public Long getIdSessao() {
        return idSessao;
    }

    public void setIdSessao(Long idSessao) {
        this.idSessao = idSessao;
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public AtendenteConversaOutput getAtendente() {
        return atendente;
    }

    public void setAtendente(AtendenteConversaOutput atendente) {
        this.atendente = atendente;
    }

    public Boolean getEnviarSalesforce() {
        return enviarSalesforce;
    }

    public void setEnviarSalesforce(Boolean enviarSalesforce) {
        this.enviarSalesforce = enviarSalesforce;
    }

    public Boolean getEncerradoWatson() {
        return encerradoWatson;
    }

    public void setEncerradoWatson(Boolean encerradoWatson) {
        this.encerradoWatson = encerradoWatson;
    }

    public Text getLogConversa() {
        return logConversa;
    }

    public void setLogConversa(Text logConversa) {
        this.logConversa = logConversa;
    }

    public String getWorkspaceId() {
        return workspaceId;
    }

    public void setWorkspaceId(String workspaceId) {
        this.workspaceId = workspaceId;
    }

    public IniciarSessaoParamsInput getParametros() {
        return parametros;
    }

    public void setParametros(IniciarSessaoParamsInput parametros) {
        this.parametros = parametros;
    }

    public String getIdSessaoAsString() {
        return String.valueOf(idSessao);
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getRetornoServico() { return retornoServico; }

    public void setRetornoServico(String retornoServico) { this.retornoServico = retornoServico; }

    public List<MapeamentoCampos> getCamposFormulario() {
        return camposFormulario;
    }

    public void setCamposFormulario(List<MapeamentoCampos> camposFormulario) {
        this.camposFormulario = camposFormulario;
    }

    public String getJsonIntents() {
        return jsonIntents;
    }

    public void setJsonIntents(String jsonIntents) {
        this.jsonIntents = jsonIntents;
    }

    public String getJsonEntities() {
        return jsonEntities;
    }

    public void setJsonEntities(String jsonEntities) {
        this.jsonEntities = jsonEntities;
    }
}
