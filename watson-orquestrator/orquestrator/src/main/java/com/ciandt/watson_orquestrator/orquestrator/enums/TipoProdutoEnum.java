package com.ciandt.watson_orquestrator.orquestrator.enums;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

public enum TipoProdutoEnum {

    PME("P"),
    INDIVIDUAL("I"),
    ADESAO("A"),
    GRUPAL("G");

    private static final Map<String, TipoProdutoEnum> lookup = new HashMap<String, TipoProdutoEnum>();

    static {
        for(TipoProdutoEnum s : EnumSet.allOf(TipoProdutoEnum.class))
            lookup.put(s.getSigla(), s);
    }

    private String sigla;

    private TipoProdutoEnum(String sigla) {
        this.sigla = sigla;
    }

    public String getSigla() { return sigla; }

    public static TipoProdutoEnum get(String sigla) {
        return lookup.get(sigla);
    }
}
