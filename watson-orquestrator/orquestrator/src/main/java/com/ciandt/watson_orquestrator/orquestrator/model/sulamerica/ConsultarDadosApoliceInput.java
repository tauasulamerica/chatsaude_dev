package com.ciandt.watson_orquestrator.orquestrator.model.sulamerica;

import java.io.Serializable;

/**
 * Created by gsanchez on 13/01/2017.
 */

public class ConsultarDadosApoliceInput extends ServiceInputBase implements Serializable {
    private String documentoApolice;
    private String codigoSucursal;
    private String numCnpj;
    private String dataNascimento;

    public ConsultarDadosApoliceInput() {
    }

    public String getDocumentoApolice() {
        return documentoApolice;
    }

    public void setDocumentoApolice(String documentoApolice) {
        this.documentoApolice = documentoApolice;
    }

    public String getCodigoSucursal() {
        return codigoSucursal;
    }

    public void setCodigoSucursal(String codigoSucursal) {
        this.codigoSucursal = codigoSucursal;
    }

    public String getNumCnpj() {
        return numCnpj;
    }

    public void setNumCnpj(String numCnpj) {
        this.numCnpj = numCnpj;
    }

    public String getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(String dataNascimento) {
        this.dataNascimento = dataNascimento;
    }
}
