package com.ciandt.watson_orquestrator.orquestrator.resolver.memcache;

import com.ciandt.watson_orquestrator.orquestrator.constants.Constants;
import com.ciandt.watson_orquestrator.orquestrator.enums.TokenTypesEnum;
import com.ciandt.watson_orquestrator.orquestrator.factory.memcache.MemcacheFactory;
import com.ciandt.watson_orquestrator.orquestrator.model.memcache.TokenDataModel;
import com.ciandt.watson_orquestrator.orquestrator.model.memcache.TokenListModel;

import java.util.logging.Logger;

/**
 * Created by rodrigogs on 09/03/17.
 */

public class TokenCacheResolver {
    private static final String CLASS_NAME = TokenCacheResolver.class.getName();
    private static Logger log = Logger.getLogger(TokenCacheResolver.class.getName());
    private MemcacheFactory<Long, TokenListModel> memcacheFactory = null;

    public TokenCacheResolver() {
        log.entering(CLASS_NAME, "<init>");
        memcacheFactory = new MemcacheFactory<Long, TokenListModel>(Constants.Memcache.EXPIRATION_TOKEN);
        log.exiting(CLASS_NAME, "<init>");
    }

    public TokenListModel getTokens(Long key) {
        log.entering(CLASS_NAME, "getTokens");
        TokenListModel tokenListModel = null;

        if(memcacheFactory != null && key != null) {
            tokenListModel = memcacheFactory.getFromCache(key);
        }

        log.exiting(CLASS_NAME, "getTokens", tokenListModel);
        return tokenListModel;
    }

    public void saveToken(Long key, TokenDataModel tokenDataModel) {
        log.entering(CLASS_NAME, "saveToken");
        if(memcacheFactory != null && key != null) {
            if(memcacheFactory.exists(key)) {
                //Atualiza o objeto Token
                TokenListModel tokens = this.getTokens(key);

                if(tokens != null) {
                    tokens.addToken(tokenDataModel);
                    memcacheFactory.saveToCache(key, tokens);
                }
            } else {
                TokenListModel tokenListModel = new TokenListModel();
                tokenListModel.addToken(tokenDataModel);
                memcacheFactory.saveToCache(key, tokenListModel);
            }
        }
        log.exiting(CLASS_NAME, "saveToken");
    }

    public TokenDataModel getTokenByType(Long key, TokenTypesEnum tipo) {
        log.entering(CLASS_NAME, "getTokenByType");
        TokenDataModel tokenDataModel = null;

        if(memcacheFactory != null && key != null) {
            if(memcacheFactory.exists(key)) {
                TokenListModel tokens = this.getTokens(key);

                if(tokens != null) {
                    for (TokenDataModel token : tokens.getTokens()) {
                        if(token.getTipo().getSigla().equals(tipo.getSigla())) {
                            tokenDataModel = token;
                        }
                    }
                }
            }
        }

        log.exiting(CLASS_NAME, "getTokenByType", tokenDataModel);
        return tokenDataModel;
    }
}
