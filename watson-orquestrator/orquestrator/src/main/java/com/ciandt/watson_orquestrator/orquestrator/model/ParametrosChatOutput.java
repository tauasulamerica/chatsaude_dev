package com.ciandt.watson_orquestrator.orquestrator.model;

import java.io.Serializable;

/**
 * Created by rodrigogs on 03/02/17.
 */

public class ParametrosChatOutput implements Serializable {
    private Boolean horarioDentroAtendimento;
    private String horaInicial;
    private String horaFinal;
    private Boolean emailTelefoneObrigatorio;
    private Boolean agenteDisponivel;
    private String telefoneAtendimento;
    private String horarioAtendimentoTelefone;

    public ParametrosChatOutput() {
    }

    public Boolean getHorarioDentroAtendimento() {
        return horarioDentroAtendimento;
    }

    public void setHorarioDentroAtendimento(Boolean horarioDentroAtendimento) {
        this.horarioDentroAtendimento = horarioDentroAtendimento;
    }

    public String getHoraInicial() {
        return horaInicial;
    }

    public void setHoraInicial(String horaInicial) {
        this.horaInicial = horaInicial;
    }

    public String getHoraFinal() {
        return horaFinal;
    }

    public void setHoraFinal(String horaFinal) {
        this.horaFinal = horaFinal;
    }

	public Boolean getEmailTelefoneObrigatorio() {
		return emailTelefoneObrigatorio;
	}

	public void setEmailTelefoneObrigatorio(Boolean emailTelefoneObrigatorio) {
		this.emailTelefoneObrigatorio = emailTelefoneObrigatorio;
	}

	public String getTelefoneAtendimento() {
		return telefoneAtendimento;
	}

	public void setTelefoneAtendimento(String telefoneAtendimento) {
		this.telefoneAtendimento = telefoneAtendimento;
	}

	public String getHorarioAtendimentoTelefone() {
		return horarioAtendimentoTelefone;
	}

	public void setHorarioAtendimentoTelefone(String horarioAtendimentoTelefone) {
		this.horarioAtendimentoTelefone = horarioAtendimentoTelefone;
	}

	public Boolean getAgenteDisponivel() {
		return agenteDisponivel;
	}

	public void setAgenteDisponivel(Boolean agenteDisponivel) {
		this.agenteDisponivel = agenteDisponivel;
	}
}
