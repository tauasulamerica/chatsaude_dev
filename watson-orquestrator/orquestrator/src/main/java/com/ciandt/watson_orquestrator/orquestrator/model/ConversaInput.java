package com.ciandt.watson_orquestrator.orquestrator.model;

import java.io.Serializable;
import java.util.Map;

import javax.annotation.Nullable;

/**
 * Created by rodrigosclosa on 19/12/16.
 */

public class ConversaInput implements Serializable {
    private Long id;
    private String texto;
    @Nullable
    private Map<String, Object> context;
    @Nullable
    private Long idSessao;
    private String requestId;

    @Nullable
    private ConversaOutput respostaAnterior;

    public ConversaInput() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    @Nullable
    public Map<String, Object> getContext() {
        return context;
    }

    public void setContext(@Nullable Map<String, Object> context) {
        this.context = context;
    }

    @Nullable
    public Long getIdSessao() {
        return idSessao;
    }

    public void setIdSessao(@Nullable Long idSessao) {
        this.idSessao = idSessao;
    }

    public String getIdSessaoAsString() {
        return String.valueOf(idSessao);
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    @Nullable
    public ConversaOutput getRespostaAnterior() {
        return respostaAnterior;
    }

    public void setRespostaAnterior(@Nullable ConversaOutput respostaAnterior) {
        this.respostaAnterior = respostaAnterior;
    }
}
