package com.ciandt.watson_orquestrator.orquestrator.service.sensedia;

import com.ciandt.watson_orquestrator.orquestrator.enums.TokenTypesEnum;
import com.ciandt.watson_orquestrator.orquestrator.exceptions.AccessTokenNullException;
import com.ciandt.watson_orquestrator.orquestrator.factory.memcache.MemcacheFactory;
import com.ciandt.watson_orquestrator.orquestrator.helper.LoggerHelper;
import com.ciandt.watson_orquestrator.orquestrator.model.memcache.TokenDataModel;
import com.ciandt.watson_orquestrator.orquestrator.resolver.config.SensediaConfig;
import com.ciandt.watson_orquestrator.orquestrator.resolver.memcache.TokenCacheResolver;
import com.ciandt.watson_orquestrator.orquestrator.service.BaseServices;
import org.apache.commons.codec.binary.Base64;
import com.google.api.server.spi.response.UnauthorizedException;
import com.google.apphosting.api.ApiProxy;

import org.glassfish.jersey.internal.util.ExceptionUtils;

import java.util.logging.Logger;

import io.swagger.client.sensedia.watson.ApiClient;
import io.swagger.client.sensedia.watson.ApiException;
import io.swagger.client.sensedia.watson.api.AccessTokenApi;
import io.swagger.client.sensedia.watson.model.AccessToken;
import io.swagger.client.sensedia.watson.model.RequestAccessToken;

/**
 * Created by rodrigosclosa on 21/12/16.
 */

public class SensediaSevice extends BaseServices<SensediaConfig> {
    private static final String CLASS_NAME = SensediaSevice.class.getName();
    private static Logger log = Logger.getLogger(SensediaSevice.class.getName());
    private AccessTokenApi accessTokenApi = null;
    private ApiClient clientConfig = null;
    TokenCacheResolver tokenCacheResolver = null;

    public SensediaSevice(String baseURL) {
        super();
        log.entering(CLASS_NAME, "<init>");
        accessTokenApi = new AccessTokenApi();

        clientConfig = new ApiClient();
        clientConfig.setConnectTimeout(config.getTimeout());
        clientConfig.setBasePath(baseURL);

        accessTokenApi.setApiClient(clientConfig);
        tokenCacheResolver = new TokenCacheResolver();
        log.exiting(CLASS_NAME, "<init>");
    }

    public String getAccessToken(Long sessionID, TokenTypesEnum tipo) throws UnauthorizedException {
        log.entering(CLASS_NAME, "getAccessToken");
        String token = null;
        String authToken = "Basic " + new String(Base64.encodeBase64((config.getClientId() + ":" + config.getClientSecret()).getBytes()));

        int numRetries = 0;
        while (numRetries <= config.getTentativas()) {
            RequestAccessToken input = new RequestAccessToken();
            input.setGrantType(config.getGrantType());

            try {
                if (sessionID == null) {
                    log.info("getAccessToken - Chamada Serviço: accessTokenApi.oauthAccessTokenPost");
                    AccessToken response = accessTokenApi.oauthAccessTokenPost(authToken, input);
                    log.info("getAccessToken - Retorno Serviço: accessTokenApi.oauthAccessTokenPost");

                    LoggerHelper.getInstance(log).log("SensediaSevice - Response: ", response);

                    if (response == null) {
                        throw new AccessTokenNullException();
                    }

                    token = response.getAccessToken();
                } else {
                    log.info("getAccessToken - Chamada Memcache: memcacheFactory.getTokenByType");
                    TokenDataModel tokenCache = tokenCacheResolver.getTokenByType(sessionID, tipo);
                    log.info("getAccessToken - Retorno Memcache: memcacheFactory.getTokenByType");

                    if (tokenCache != null) {
                        //Reutiliza o token em cache
                        token = tokenCache.getToken();
                    } else {
                        //Gera um token novo
                        log.info("getAccessToken - Chamada Serviço: accessTokenApi.oauthAccessTokenPost");
                         AccessToken response = accessTokenApi.oauthAccessTokenPost(authToken, input);
                        log.info("getAccessToken - Retorno Serviço: accessTokenApi.oauthAccessTokenPost");

                        LoggerHelper.getInstance(log).log("SensediaSevice - Response: ", response);

                        if (response == null) {
                            throw new AccessTokenNullException();
                        }

                        token = response.getAccessToken();

                        //guarda no Cache
                        TokenDataModel tokenNovo = new TokenDataModel(tipo, token);
                        tokenCacheResolver.saveToken(sessionID, tokenNovo);
                    }
                }

                log.exiting(CLASS_NAME, "getAccessToken", token);
                return token;
            } catch (ApiException e) {
                log.severe("SensediaSevice - getAccessToken: Não foi possível gerar um token de acesso. " + ExceptionUtils.exceptionStackTraceAsString(e));
            } catch (AccessTokenNullException an) {
                log.severe("SensediaSevice: Não foi possível gerar um token de acesso para Sensedia. Stack trace: " + ExceptionUtils.exceptionStackTraceAsString(an));
                throw new UnauthorizedException("SensediaSevice: Não foi possível gerar um token de acesso para Sensedia. Stack trace: " + ExceptionUtils.exceptionStackTraceAsString(an));
            } catch (Exception e) {
                log.severe("SensediaSevice - getAccessToken: Erro geral: " + ExceptionUtils.exceptionStackTraceAsString(e));
            }

            long tempoRestante = ApiProxy.getCurrentEnvironment().getRemainingMillis();
            if(tempoRestante <= config.getTempoMinimoTentativa()) {
            		log.severe("SensediaSevice - getAccessToken: Não foi possível obter o token dentro do tempo. Tempo restante: " + tempoRestante);
            		throw new UnauthorizedException("SensediaSevice - getAccessToken: Não foi possível obter o token dentro do tempo. Tempo restante: " + tempoRestante);
            }
            
            log.warning("SensediaSevice - getAccessToken: Sleep por não conseguir pegar o token.");
            numRetries++;
            sleep(config.getIntervaloTentativas());
        }

        log.exiting(CLASS_NAME, "getAccessToken");
        log.severe("SensediaSevice - getAccessToken: Número de tentativas excedidas. Tentativas: " + numRetries);
        throw new UnauthorizedException("SensediaSevice - getAccessToken: Número de tentativas excedidas. Tentativas: " + numRetries);
    }
}