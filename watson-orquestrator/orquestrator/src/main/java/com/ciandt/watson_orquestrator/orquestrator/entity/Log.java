package com.ciandt.watson_orquestrator.orquestrator.entity;

import com.google.appengine.api.datastore.Text;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;

import java.io.Serializable;
import java.util.Date;

import javax.annotation.Nullable;

/**
 * Created by rodrigosclosa on 21/12/16.
 */
@Entity
public class Log extends BaseEntity implements Serializable {
    @Index
    private Long idSessao;
    @Index
    private Date dataHora;
    private String origem;
    private Text dados;
    private String intent;
    @Nullable
    private String feedback;

    private Integer retencaoParcial;
    private Integer action;
    private String retornoServico;
    private Integer contadorDialogo;

    public Log() {
    }

    public Long getIdSessao() {
        return idSessao;
    }

    public void setIdSessao(Long idSessao) {
        this.idSessao = idSessao;
    }

    public Date getDataHora() {
        return dataHora;
    }

    public void setDataHora(Date dataHora) {
        this.dataHora = dataHora;
    }

    public Text getDados() {
        return dados;
    }

    public void setDados(Text dados) {
        this.dados = dados;
    }

    public String getOrigem() {
        return origem;
    }

    public void setOrigem(String origem) {
        this.origem = origem;
    }

    public String getIntent() {
        return intent;
    }

    public void setIntent(String intent) {
        this.intent = intent;
    }

    @Nullable
    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(@Nullable String feedback) {
        this.feedback = feedback;
    }

    public Integer getRetencaoParcial() {
        return retencaoParcial;
    }

    public void setRetencaoParcial(Integer retencaoParcial) {
        this.retencaoParcial = retencaoParcial;
    }

    public Integer getAction() {
        return action;
    }

    public void setAction(Integer action) {
        this.action = action;
    }

    public String getRetornoServico() {
        return retornoServico;
    }

    public void setRetornoServico(String retornoServico) {
        this.retornoServico = retornoServico;
    }

    public Integer getContadorDialogo() {
        return contadorDialogo;
    }

    public void setContadorDialogo(Integer contadorDialogo) {
        this.contadorDialogo = contadorDialogo;
    }
}
