package com.ciandt.watson_orquestrator.orquestrator.business;

import com.ciandt.watson_orquestrator.orquestrator.constants.Constants;
import com.ciandt.watson_orquestrator.orquestrator.dao.LogDao;
import com.ciandt.watson_orquestrator.orquestrator.dao.ParametroChatDao;
import com.ciandt.watson_orquestrator.orquestrator.dao.SessaoDao;
import com.ciandt.watson_orquestrator.orquestrator.entity.Log;
import com.ciandt.watson_orquestrator.orquestrator.entity.ParametroChat;
import com.ciandt.watson_orquestrator.orquestrator.entity.Sessao;
import com.ciandt.watson_orquestrator.orquestrator.enums.ActionWatsonEnum;
import com.ciandt.watson_orquestrator.orquestrator.enums.TipoProdutoEnum;
import com.ciandt.watson_orquestrator.orquestrator.exceptions.AccessTokenNullException;
import com.ciandt.watson_orquestrator.orquestrator.exceptions.SaSServicesException;
import com.ciandt.watson_orquestrator.orquestrator.factory.crypto.AES;
import com.ciandt.watson_orquestrator.orquestrator.helper.TokenHelper;
import com.ciandt.watson_orquestrator.orquestrator.model.salesforce.LiveAgentMessageList;
import com.ciandt.watson_orquestrator.orquestrator.model.watson.DadosSeguradoWatsonBean;
import com.ciandt.watson_orquestrator.orquestrator.resolver.config.ChatCorretorConfig;
import com.ciandt.watson_orquestrator.orquestrator.model.AtendenteConversaOutput;
import com.ciandt.watson_orquestrator.orquestrator.model.ParametrosChatOutput;
import com.ciandt.watson_orquestrator.orquestrator.model.ConversaInput;
import com.ciandt.watson_orquestrator.orquestrator.model.ConversaOutput;
import com.ciandt.watson_orquestrator.orquestrator.model.IniciarSessaoInput;
import com.ciandt.watson_orquestrator.orquestrator.model.IniciarSessaoParamsInput;
import com.ciandt.watson_orquestrator.orquestrator.model.watson.DadosCorretorWatsonBean;
import com.ciandt.watson_orquestrator.orquestrator.service.ibm.WatsonConversationServices;
import com.ciandt.watson_orquestrator.orquestrator.service.salesforce.LiveAgentServices;
import com.ciandt.watson_orquestrator.orquestrator.service.salesforce.ManifestacoesApi;
import com.ciandt.watson_orquestrator.orquestrator.service.sulamerica.VerificaCorretor;
import com.ciandt.watson_orquestrator.orquestrator.resolver.WatsonOutputFormatterResolver;
import com.ciandt.watson_orquestrator.orquestrator.util.DateUtil;
import com.google.api.server.spi.response.ConflictException;
import com.google.api.server.spi.response.InternalServerErrorException;
import com.google.api.server.spi.response.NotFoundException;
import com.google.api.server.spi.response.UnauthorizedException;
import com.google.appengine.api.datastore.Text;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.glassfish.jersey.internal.util.ExceptionUtils;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.Logger;

/**
 * Created by rodrigosclosa on 21/12/16.
 */

public class ChatCorretorBO extends BaseBusiness<ChatCorretorConfig> {
    private static final String CLASS_NAME = ChatCorretorBO.class.getName();
    private final String ORIGEM_WATSON = "Atendimento Automático";
    private final String ORIGEM_CORRETOR = "Corretor";

    private static final Logger log = Logger.getLogger(ChatCorretorBO.class.getName());
    private WatsonConversationServices watsonConversationServices = null;
    private SessaoDao sessaoDao = null;
    private VerificaCorretor verificaCorretor = null;
    private LogDao logDao = null;
    private AtendentesBO atendentesBO = null;
    private ParametroChatDao parametroChatDao = null;
    private AES aes = null;
    private LiveAgentServices liveAgentServices = null;
    private ManifestacoesApi manifestacoesApi = null;
    private WatsonOutputFormatterResolver watsonOutputFormatterResolver = null;

    public ChatCorretorBO() {
        super();
        log.entering(CLASS_NAME, "<init>");

        watsonConversationServices = new WatsonConversationServices();
        sessaoDao = new SessaoDao();
        verificaCorretor = new VerificaCorretor();
        logDao = new LogDao();
        atendentesBO = new AtendentesBO();
        parametroChatDao = new ParametroChatDao();
        aes = new AES(Constants.Crypto.AES_KEY);
        liveAgentServices = new LiveAgentServices();
        manifestacoesApi = new ManifestacoesApi();
        watsonOutputFormatterResolver = new WatsonOutputFormatterResolver();

        log.exiting(CLASS_NAME, "<init>");
    }

    public ConversaOutput iniciarSessao(HttpServletRequest request, IniciarSessaoInput iniciarSessaoInput) throws ConflictException, NotFoundException, UnauthorizedException, SaSServicesException {
        log.entering(CLASS_NAME, "iniciarSessao");
        ConversaOutput iniciarWatson = new ConversaOutput();

        if(TokenHelper.getInstance().tokenValido(request)) {
            if (iniciarSessaoInput == null) {
                log.info("iniciarSessao - Parâmetros de inicialização da sessão não informados.");
                throw new ConflictException("Parâmetros de inicialização da sessão não informados.");
            }

            ParametrosChatOutput parametros = obterParametrosChat();
            
            if(!parametros.getHorarioDentroAtendimento()) {
            		throw new ConflictException("Chat fora do horário padrão de atendimento.");
            }
            
            if(parametros.getEmailTelefoneObrigatorio()) {
            		validarEmailTelefone(iniciarSessaoInput);
            }

            //Descriptografa a string dos Params
            String jsonParams = aes.decrypt(iniciarSessaoInput.getParams());
            log.info("iniciarSessao - Valores de parâmetros: " + jsonParams);

            Gson gson = new Gson();
            IniciarSessaoParamsInput params = gson.fromJson(jsonParams, IniciarSessaoParamsInput.class);

            if(params == null) {
                log.info("iniciarSessao - Parâmetros de inicialização incorretos ou inválidos. ");
                throw new ConflictException("Parâmetros de inicialização incorretos ou inválidos.");
            }

            params.setDuracao(config.getDuracaoURL());

            //Valida o tempo da URL
            if(urlExpirou(params)) {
                log.info("iniciarSessao - URL expirada.");
                throw new ConflictException("Tempo de acesso à sessão do Chat expirou. Por favor, acesse novamente o link pela área do Corretor no Portal.");
            }

//@@saude@@ - VERIFICA USUARIO
//            if (!verificaCorretor.corretorValido(params.getCodigoNAC())) {
//                //Se não achou o corretor, redireciona automaticamente para o Salesforce
//                log.warning("iniciarSessao - Código NAC não encontrado ou inválido. Enviando para o SalesForce.");
//                iniciarWatson.setEnviarSalesforce(true);
//                return iniciarWatson;
//            }

            //Cria uma nova sessão
            Sessao sessao = new Sessao();
            //sessao.setCodigoNAC(params.getCodigoNAC());
            sessao.setDataInicio(new Date());
            //sessao.setRazaoSocial(params.getRazaoSocial());
            //sessao.setCodigoCorretor(params.getCodigoProdutor());
            sessao.setNome(iniciarSessaoInput.getNome());
            sessao.setEnviadoSalesForce(false);
            sessao.setEmail(iniciarSessaoInput.getEmail());
            sessao.setTelefone(iniciarSessaoInput.getTelefone());
            sessao.setCarteirinha(iniciarSessaoInput.getCarteirinha().replace(" ","")); //pegando carteirinha do pre chat
            sessao.setTipoProduto(iniciarSessaoInput.getTipoProduto()); //pegando produto do pre chat
            sessao.setOrigem(params.getOrigem());
            sessao.setCodEmpresa(iniciarSessaoInput.getCodempresa());

            AtendenteConversaOutput atendente = atendentesBO.getAtendente();
            sessao.setIdAtendente(atendente.getId());

            sessao = sessaoDao.insert(sessao);
            
            manifestacoesApi.redefinirContadorTentativas();
            //@@saude@@
            //String mensagemPreChat = manifestacoesApi.obterMensagemPreChat(sessao.getId());
            String mensagemPreChat = "Oi mensagem pré chat teste";

            DadosSeguradoWatsonBean dadosSegurado = new DadosSeguradoWatsonBean();

            dadosSegurado.setCarteirinha(sessao.getCarteirinha());
            dadosSegurado.setProduto(sessao.getTipoProduto());
            dadosSegurado.setOrigem(sessao.getOrigem());
            dadosSegurado.setNome(sessao.getNome());


            log.info("ChatCorretorBO - Chamada Serviço: watsonConversationServices.iniciarSessao");
            iniciarWatson = watsonConversationServices.iniciarSessao(sessao.getId(), dadosSegurado, mensagemPreChat);
            log.info("ChatCorretorBO - Retorno Serviço: watsonConversationServices.iniciarSessao");

            if (sessao.getId() != null) {
                iniciarWatson.setIdSessao(sessao.getId());
                iniciarWatson.setAtendente(atendente);

                //Atualiza o WorkspaceId na sessão
                sessao.setWorkspaceId(iniciarWatson.getWorkspaceId());

                //Atualiza o conversationID na sessão
                String conversationId = (String)iniciarWatson.getContext().get("conversation_id");
                sessao.setConversationId(conversationId);

                sessaoDao.update(sessao);
            }

            Log logg = new Log();
            logg.setIdSessao(sessao.getId());
            logg.setDataHora(new Date());
            logg.setDados(new Text(iniciarWatson.getOutput().getText().toString()));
            logg.setOrigem(ORIGEM_WATSON);

            logDao.insert(logg);

            //Parâmetros descriptografados do Chat
            iniciarWatson.setParametros(params);
            
            log.exiting(CLASS_NAME, "iniciarSessao", iniciarWatson);

            return iniciarWatson;
        }

        log.exiting(CLASS_NAME, "iniciarSessao");
        return null;
    }

	private void validarEmailTelefone(IniciarSessaoInput iniciarSessaoInput) throws ConflictException {
		if (iniciarSessaoInput.getEmail() == null || iniciarSessaoInput.getEmail().isEmpty()
				|| iniciarSessaoInput.getTelefone() == null || iniciarSessaoInput.getTelefone().isEmpty()) {
			throw new ConflictException("Parâmetros de inicialização da sessão não informados.");
		}
	}

	public ConversaOutput conversa(HttpServletRequest request, ConversaInput conversaInput) throws ConflictException, UnauthorizedException, SaSServicesException {
        log.entering(CLASS_NAME, "conversa");

        if(TokenHelper.getInstance().tokenValido(request)) {
            if (conversaInput.getIdSessao() == null) {
                log.info("conversa - ID de sessão não informado.");
                throw new ConflictException("ID de sessão não informado.");
            }

            if (conversaInput.getContext() == null) {
                log.info("conversa - Context não informado.");
                throw new ConflictException("Context não informado.");
            }

            Log logg = new Log();
            logg.setIdSessao(conversaInput.getIdSessao());
            logg.setDataHora(new Date());
            logg.setDados(new Text(conversaInput.getTexto()));
            logg.setOrigem(ORIGEM_CORRETOR);

            logDao.insert(logg);

            log.info("ChatCorretorBO - Chamada Serviço: watsonConversationServices.integrar");
            ConversaOutput output = watsonConversationServices.integrar(conversaInput);
            log.info("ChatCorretorBO - Retorno Serviço: watsonConversationServices.integrar");

            output.setIdSessao(conversaInput.getIdSessao());
            output.setRequestId(conversaInput.getRequestId());

            Log logResposta = new Log();
            logResposta.setIdSessao(output.getIdSessao());
            logResposta.setDataHora(new Date());
            //logResposta.setDados(new Text(output.getOutput().getText().toString()));
            //logResposta.setDados(new Text(watsonOutputFormatterResolver.htmlToText(output.getOutput().getText().toString())));
            logResposta.setIntent(output.getContext() != null ? (String) output.getContext().get("intent") : null);
            logResposta.setOrigem(ORIGEM_WATSON);

            //Verifica se precisa salvar o Feedback
            Map<String, Object> feedback = (Map<String, Object>)output.getContext().get("feedback");

            if(feedback != null) {
                Boolean save = (Boolean)feedback.get("save");

                if(save) {
                    Gson gson = new GsonBuilder().create();
                    String json = gson.toJson(feedback);
                    logResposta.setFeedback(json);
                }
            }

            logResposta.setRetornoServico(output.getRetornoServico());

            if (output.getContext() != null) {
                logResposta.setRetencaoParcial((Integer) output.getContext().get("ret_parcial_count"));
                logResposta.setIntent((String) output.getContext().get("intent"));
                logResposta.setAction((Integer) output.getOutput().getAction());

                if (output.getContext().get("system") != null) {
                    Map<String, Object> system = (Map<String, Object>) output.getContext().get("system");
                    logResposta.setContadorDialogo((Integer) system.get("dialog_request_counter"));
                }

                //MARCA FLAG EXIBIR FORMULARIO - ACTION COM SERVIÇO E CAMPOS NÃO PREENCHIDOS
                if (output.getContext().get("fg_formulario_exibido") == "P") {
                    logResposta.setDados(new Text("Formulário Exibido."));
                    logResposta.setRetornoServico(Constants.ManifestacoesApi.RETORNO_SERVICO_FORMULARIO);
                    //logResposta.setRetencaoParcial((Integer) output.getContext().get("ret_parcial_count")-1);
                }
            }

            logDao.insert(logResposta);

            if (output.getEncerradoWatson()) {
                finalizarSessao(output.getIdSessao(), true, false);
            }
            if (output.getEnviarSalesforce()) {
                output.setLogConversa(new Text(logDao.getConversaByidSessao(output.getIdSessao())));
                finalizarSessao(output.getIdSessao(), false, false);
            }

            //TODO: Remover antes de ir pra Produção
            //Para testes com o Gatling
            //output.getContext().remove("feedback");

            log.exiting(CLASS_NAME, "conversa", output);
            return output;
        }

        log.exiting(CLASS_NAME, "conversa");
        return null;
    }

    public ConversaOutput getLogConversa (HttpServletRequest request, Long idSessao) throws UnauthorizedException, ConflictException {
        if(TokenHelper.getInstance().tokenValido(request)) {
            if (idSessao == null) {
                log.info("conversa - ID de sessão não informado.");
                throw new ConflictException("ID de sessão não informado.");
            }

            ConversaOutput output = new ConversaOutput();
            output.setIdSessao(idSessao);
            output.setLogConversa(new Text(logDao.getConversaByidSessao(output.getIdSessao())));
            output.setEnviarSalesforce(true);

            return output;
        }

        return null;
    }

    private void finalizarSessao(Long idSession, Boolean abrirManifestacao, Boolean abandono) {
        log.entering(CLASS_NAME, "finalizarSessao");

        Sessao session = sessaoDao.getByKey(idSession);
        if(session != null) {
            session.setFinalizada(true);
            session.setDataFinalizada(new Date());
            session.setAbandono(abandono);

            if(abrirManifestacao) {
                session.setEnviadoSalesForce(false);
            } else {
                session.setEnviadoSalesForce(true);
            }

            sessaoDao.update(session);
        }
        log.exiting(CLASS_NAME, "finalizarSessao");
    }
    
    public void sendFeedback(HttpServletRequest request, Long idSessao, Boolean satisfeito) throws UnauthorizedException {
        log.entering(CLASS_NAME, "sendFeedback");

        if(TokenHelper.getInstance().tokenValido(request)) {
            Sessao sessao = sessaoDao.getByKey(idSessao);

            if (sessao != null) {
                sessao.setSatisfeitoAtendimento(satisfeito);

                sessaoDao.update(sessao);
            }
        }
        log.exiting(CLASS_NAME, "sendFeedback");
    }

    private ParametrosChatOutput obterParametrosChat() throws ConflictException {
        log.entering(CLASS_NAME, "obterParametrosChat");

        ParametrosChatOutput retorno = new ParametrosChatOutput();

        Date dataAtual = Calendar.getInstance(TimeZone.getTimeZone(DateUtil.TIMEZONE_BRT)).getTime();
        ParametroChat parametroHoraInicio = parametroChatDao.getByProperty(Constants.Parametros.CHAVE, Constants.Parametros.HORA_INICIO);
        ParametroChat parametroHoraFim = parametroChatDao.getByProperty(Constants.Parametros.CHAVE, Constants.Parametros.HORA_FIM);
        
        log.info("obterParametrosChat - Data atual: " + dataAtual.toString());

        if(parametroHoraInicio == null || parametroHoraFim == null) {
			retorno.setHorarioDentroAtendimento(false);
			return retorno;
        }

        Date dataInicio = DateUtil.createNewDateWithTime(parametroHoraInicio.getValor());
        Date dataFim = DateUtil.createNewDateWithTime(parametroHoraFim.getValor());

        log.info("obterParametrosChat - dataInicio: " + dataInicio.toString());
        log.info("obterParametrosChat - dataFim: " + dataFim.toString());

        retorno.setHoraInicial(parametroHoraInicio.getValor());
        retorno.setHoraFinal(parametroHoraFim.getValor());

        if ((DateUtil.compareDate(dataAtual, dataInicio, true) == 1) &&
                (DateUtil.compareDate(dataFim, dataAtual, true) == 1)) {
            retorno.setHorarioDentroAtendimento(true);
        } else {
			retorno.setHorarioDentroAtendimento(false);
        }

        ParametroChat parametroEmailTelefoneObrigatorio = parametroChatDao.getByProperty(Constants.Parametros.CHAVE, Constants.Parametros.EMAIL_TELEFONE_OBRIGATORIO);
        ParametroChat parametroTelefoneAtendimento = parametroChatDao.getByProperty(Constants.Parametros.CHAVE, Constants.Parametros.TELEFONE_ATENDIMENTO);
        ParametroChat parametroHorarioAtendimentoTelefone = parametroChatDao.getByProperty(Constants.Parametros.CHAVE, Constants.Parametros.HORARIO_ATENDIMENTO_TELEFONE);
        
        retorno.setEmailTelefoneObrigatorio(Boolean.parseBoolean(parametroEmailTelefoneObrigatorio.getValor()));
        retorno.setTelefoneAtendimento(parametroTelefoneAtendimento.getValor());
        retorno.setHorarioAtendimentoTelefone(parametroHorarioAtendimentoTelefone.getValor());
        
        log.exiting(CLASS_NAME, "obterParametrosChat", retorno);
        return retorno;
    }

    public ParametrosChatOutput obterParametrosChat(String organizationId, String deploymentId, String buttonId) throws ConflictException {
		ParametrosChatOutput parametros = obterParametrosChat();

		try {
			parametros.setAgenteDisponivel(liveAgentServices.checkAvailability(organizationId, deploymentId, buttonId));

			return parametros;
		} catch (AccessTokenNullException e) {
			log.severe("LiveAgentServices - horarioDentroDoAtendimento: Não foi possível gerar um token de acesso para Sensedia. Stack trace: " + ExceptionUtils.exceptionStackTraceAsString(e));
			parametros.setAgenteDisponivel(false);
		}

		return parametros;
    }

    public void encerrarChat(HttpServletRequest request, Long idSessao) throws UnauthorizedException, ConflictException {
        log.entering(CLASS_NAME, "encerrarChat");

        if (TokenHelper.getInstance().tokenValido(request)) {

            if(verificaSeInatividade(request) && idSessao != null) {
                inserirLogInatividade(idSessao);
            }

            finalizarSessao(idSessao, true, true);
        }

        log.exiting(CLASS_NAME, "encerrarChat");
    }
    
    public LiveAgentMessageList getMessages(HttpServletRequest request) throws ConflictException, IOException, InternalServerErrorException {
    		return liveAgentServices.getMessages(request);
    }

	public void inserirLogInatividade(Long idSessao) {
		log.entering(CLASS_NAME, "inserirLogInatividade");

		Log logg = new Log();
		logg.setIdSessao(idSessao);
		logg.setDataHora(new Date());
		logg.setDados(new Text("Encerrado por inatividade."));
		logg.setOrigem(ORIGEM_WATSON);
        logg.setAction(ActionWatsonEnum.INATIVIDADE.getCodigo());

		logDao.insert(logg);

		log.exiting(CLASS_NAME, "inserirLogInatividade");
	}

    private Boolean verificaSeInatividade(HttpServletRequest request) {
        String strIdle = request.getHeader("chatIdle");

        if(strIdle == null || strIdle.isEmpty()) {
            return false;
        }

        return Boolean.valueOf(strIdle);
    }

    private Boolean urlExpirou(IniciarSessaoParamsInput params) {
        log.entering(CLASS_NAME, "urlExpirou");
        Boolean retorno = false;

        try {
            Date dataInicio = DateUtil.createDateTime(params.dataInicio);
            Date dataFinal = DateUtil.incrementDate(dataInicio, Calendar.SECOND, params.duracao);
            Date dataAtual = DateUtil.getBRTTimeZoneDate();

            log.info("urlExpirou - dataInicio: " + dataInicio.toString());
            log.info("urlExpirou - dataFinal: " + dataFinal.toString());
            log.info("urlExpirou - dataAtual: " + dataAtual.toString());

            if (DateUtil.compareDate(dataAtual, dataFinal, true) == 1) {
                //Expirou
                retorno = true;
            }
        } catch (ParseException e) {
            log.info("urlExpirou - Erro ao converter a data: " + ExceptionUtils.exceptionStackTraceAsString(e));
        }

        log.exiting(CLASS_NAME, "urlExpirou", retorno);
        return retorno;
    }

}