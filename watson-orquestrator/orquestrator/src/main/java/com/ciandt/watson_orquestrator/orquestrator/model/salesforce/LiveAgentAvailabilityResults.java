package com.ciandt.watson_orquestrator.orquestrator.model.salesforce;

import java.io.Serializable;

/**
 * Created by rodrigogs on 19/04/17.
 */

public class LiveAgentAvailabilityResults implements Serializable {
    private String id;
    private Boolean isAvailable;

    public LiveAgentAvailabilityResults() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean getAvailable() {
        return isAvailable;
    }

    public void setAvailable(Boolean available) {
        isAvailable = available;
    }
}
