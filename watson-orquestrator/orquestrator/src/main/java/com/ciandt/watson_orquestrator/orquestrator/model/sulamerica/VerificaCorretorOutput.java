package com.ciandt.watson_orquestrator.orquestrator.model.sulamerica;

import java.io.Serializable;

/**
 * Created by rodrigogs on 03/01/17.
 */

public class VerificaCorretorOutput implements Serializable {
    private String codigoProdutor;

    public VerificaCorretorOutput() {
    }

    public String getCodigoProdutor() {
        return codigoProdutor;
    }

    public void setCodigoProdutor(String codigoProdutor) {
        this.codigoProdutor = codigoProdutor;
    }
}
