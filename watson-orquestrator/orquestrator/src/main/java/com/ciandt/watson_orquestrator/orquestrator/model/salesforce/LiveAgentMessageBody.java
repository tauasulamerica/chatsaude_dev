package com.ciandt.watson_orquestrator.orquestrator.model.salesforce;

import java.io.Serializable;

public class LiveAgentMessageBody implements Serializable {
	private String text;
	private String name;
	private String agentId;
	private String uploadServletUrl;
	private String fileToken;
	private String cdmServletUrl;
	private String type;
	private String position;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}
	
	public String getUploadServletUrl() {
		return uploadServletUrl;
	}

	public void setUploadServletUrl(String uploadServletUrl) {
		this.uploadServletUrl = uploadServletUrl;
	}

	public String getFileToken() {
		return fileToken;
	}

	public void setFileToken(String fileToken) {
		this.fileToken = fileToken;
	}

	public String getCdmServletUrl() {
		return cdmServletUrl;
	}

	public void setCdmServletUrl(String cdmServletUrl) {
		this.cdmServletUrl = cdmServletUrl;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}
}
