package com.ciandt.watson_orquestrator.orquestrator.service.sulamerica;

import com.ciandt.watson_orquestrator.orquestrator.constants.Constants;
import com.ciandt.watson_orquestrator.orquestrator.dao.MapeamentoServicosDao;
import com.ciandt.watson_orquestrator.orquestrator.dao.SessaoDao;
import com.ciandt.watson_orquestrator.orquestrator.enums.TokenTypesEnum;
import com.ciandt.watson_orquestrator.orquestrator.exceptions.AccessTokenNullException;
import com.ciandt.watson_orquestrator.orquestrator.exceptions.GeneralErrorServiceException;
import com.ciandt.watson_orquestrator.orquestrator.exceptions.NotFoundServiceException;
import com.ciandt.watson_orquestrator.orquestrator.exceptions.SaSServicesException;
import com.ciandt.watson_orquestrator.orquestrator.helper.LoggerHelper;
import com.ciandt.watson_orquestrator.orquestrator.model.sulamerica.VerificaCorretorInput;
import com.ciandt.watson_orquestrator.orquestrator.model.sulamerica.VerificaCorretorOutput;
import com.ciandt.watson_orquestrator.orquestrator.resolver.config.SaSServicesConfig;
import com.ciandt.watson_orquestrator.orquestrator.service.BaseServices;
import com.ciandt.watson_orquestrator.orquestrator.service.sensedia.SensediaSevice;
import com.google.api.server.spi.response.UnauthorizedException;

import org.glassfish.jersey.internal.util.ExceptionUtils;

import java.util.logging.Level;
import java.util.logging.Logger;

import io.swagger.client.corretor.auto.ApiClient;
import io.swagger.client.corretor.auto.ApiException;
import io.swagger.client.corretor.auto.api.CorretoresApi;
import io.swagger.client.corretor.auto.model.CorrecaoStatus;

/**
 * Created by rodrigogs on 03/01/17.
 */

public class VerificaCorretor extends BaseServices<SaSServicesConfig> implements SaSService<VerificaCorretorInput, VerificaCorretorOutput> {

    private static Logger log = null;
    private String accessToken = "";
    private SensediaSevice sensediaSevice = null;
    private CorretoresApi corretoresApi = null;
    private ApiClient clientConfig = null   ;
    private MapeamentoServicosDao mapeamentoServicosDao = null;
    private SessaoDao sessaoDao = null;
    int numRetries = 0;

    public VerificaCorretor() {
        super();
        log = Logger.getLogger(getClass().getName());
        sensediaSevice = new SensediaSevice(config.getBaseURLCoretorSaS());
        mapeamentoServicosDao = new MapeamentoServicosDao();
        sessaoDao = new SessaoDao();

        corretoresApi = new CorretoresApi();

        clientConfig = new ApiClient();
        clientConfig.setConnectTimeout(config.getTimeout());
        clientConfig.setBasePath(config.getBaseURLCoretorSaS());

        corretoresApi.setApiClient(clientConfig);
    }

    @Override
    public void requestAccessToken(Long idSessao) throws AccessTokenNullException {
        try {
            accessToken = sensediaSevice.getAccessToken(idSessao, TokenTypesEnum.Corretor);

            if(accessToken == null) {
                throw new AccessTokenNullException();
            }
        } catch (UnauthorizedException e) {
            log.severe("VerificaCorretor - requestAccessToken: Não foi possível gerar um token de acesso para Sensedia. Stack trace: " + ExceptionUtils.exceptionStackTraceAsString(e));
            throw new AccessTokenNullException();
        }
    }

    @Override
    public VerificaCorretorOutput integrar(VerificaCorretorInput objetoEntrada, Long idSessao, String requestId) throws SaSServicesException {
        VerificaCorretorOutput retorno = new VerificaCorretorOutput();

        while (numRetries <= config.getTentativas()) {
            try {
                requestAccessToken(idSessao);

                LoggerHelper.getInstance(log).log("VerificaCorretor - Objeto entrada", objetoEntrada);

                CorrecaoStatus response = corretoresApi.corretoresCodigoNacStatusGet(config.getClientId(), accessToken, objetoEntrada.getCodigoNac());

                if (response == null) {
                    log.severe("VerificaCorretor - integrar: Retorno da execução do serviço vazio ou nulo.");
                    throw new GeneralErrorServiceException();
                }

                LoggerHelper.getInstance(log).log("VerificaCorretor - response", response);

                retorno.setCodigoProdutor(response.getCodigoProdutor().toString());

            } catch (AccessTokenNullException an) {
                log.severe("VerificaCorretor: Não foi possível gerar um token de acesso para Sensedia. Stack trace: " + ExceptionUtils.exceptionStackTraceAsString(an));
                throw new GeneralErrorServiceException();
            } catch (ApiException e) {
                if (e.getCode() == Constants.ApiErrorCodes.FORBIDDEN) {
                    requestAccessToken(idSessao);
                    numRetries++;
                    sleep(config.getIntervaloTentativas());

                    integrar(objetoEntrada, idSessao, requestId);
                } else if (e.getCode() == Constants.ApiErrorCodes.UNAUTHORIZED) {
                    log.warning("VerificaCorretor - integrar: UNAUTHORIZED. " + ExceptionUtils.exceptionStackTraceAsString(e));
                    numRetries++;
                    sleep(config.getIntervaloTentativas());

                    integrar(objetoEntrada, idSessao, requestId);
                } else {
                    LoggerHelper.getInstance(log).log(Level.SEVERE, "VerificaCorretor - Erro", e.getResponseBody());

                    if (null != e.getResponseBody() && e.getResponseBody().contains(Constants.ApiErrorCodes.ERROR)) {
                        log.warning("VerificaCorretor - integrar: Corretor não encontrado. " + ExceptionUtils.exceptionStackTraceAsString(e));
                        throw new NotFoundServiceException();
                    } else {
                        log.severe("VerificaCorretor - integrar: Problema ao executar a chamada a API de VerificaCorretor. " + ExceptionUtils.exceptionStackTraceAsString(e));
                        throw new GeneralErrorServiceException();
                    }
                }
            } catch (Exception e) {
                log.severe("VerificaCorretor - integrar: Erro geral: " + ExceptionUtils.exceptionStackTraceAsString(e));
                throw new GeneralErrorServiceException();
            }

            return retorno;
        }

        log.severe("VerificaCorretor - integrar: Número de tentativas excedidas. Tentativas: " + numRetries);
        return null;
    }

    @Override
    public Class<VerificaCorretorInput> getTypeParameterInputClass() {
        return VerificaCorretorInput.class;
    }

    @Override
    public Class<VerificaCorretorOutput> getTypeParameterOutputClass() {
        return VerificaCorretorOutput.class;
    }

    public Boolean corretorValido(String codigoNAC) {
        VerificaCorretorInput verificaCorretorInput = new VerificaCorretorInput();
        verificaCorretorInput.setCodigoNac(codigoNAC);
        VerificaCorretorOutput verificaCorretorOutput;

        try {
            verificaCorretorOutput = integrar(verificaCorretorInput, null, null);
        } catch (Exception e) {
            return false;
        }
        return validarPreenchimentoCodigoProdutor(verificaCorretorOutput);
    }

    public boolean validarPreenchimentoCodigoProdutor(VerificaCorretorOutput verificaCorretorOutput){
        return verificaCorretorOutput != null && verificaCorretorOutput.getCodigoProdutor() != null
                && !verificaCorretorOutput.getCodigoProdutor().isEmpty();
    }
}