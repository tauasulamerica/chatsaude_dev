package com.ciandt.watson_orquestrator.orquestrator.endpoint;

import com.ciandt.watson_orquestrator.orquestrator.business.TokenBO;
import com.ciandt.watson_orquestrator.orquestrator.entity.Token;
import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.response.ConflictException;
import com.google.api.server.spi.response.NotFoundException;

import java.util.List;

import javax.inject.Named;

/**
 * Created by rodrigosclosa on 29/08/16.
 */
@Api(
        name = "token",
        version = "v1",
        namespace = @ApiNamespace(
            ownerDomain = "endpoint.orquestrator.watson_orquestrator.ciandt.com",
            ownerName = "endpoint.orquestrator.watson_orquestrator.ciandt.com",
            packagePath = ""
        )
)
public class TokenEndpoint {

    private TokenBO tokensService;

    public TokenEndpoint() {
        tokensService = new TokenBO();
    }

    @ApiMethod(name = "getByCodigo", path = "get/{codigo}", httpMethod = ApiMethod.HttpMethod.GET)
    public Token getByCodigo(@Named("codigo") String codigo) throws NotFoundException {
        return tokensService.getByCodigo(codigo);
    }
}
