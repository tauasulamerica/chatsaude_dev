package com.ciandt.watson_orquestrator.orquestrator.resolver.config;

/**
 * Created by rodrigosclosa on 19/12/16.
 */
public class WatsonConfig {
    private Integer timeout;
    private String baseURLWatson;
    private String clientId;
    private Integer tentativas;
    private Integer intervaloTentativas;
    private Integer tempoMinimoTentativa;

    public WatsonConfig() {
    }

    public String getBaseURLWatson() {
        return baseURLWatson;
    }

    public void setBaseURLWatson(String baseURLWatson) {
        this.baseURLWatson = baseURLWatson;
    }

    public Integer getTimeout() {
        return timeout;
    }

    public void setTimeout(Integer timeout) {
        this.timeout = timeout;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public Integer getTentativas() {
        return tentativas;
    }

    public void setTentativas(Integer tentativas) {
        this.tentativas = tentativas;
    }

    public Integer getIntervaloTentativas() {
        return intervaloTentativas;
    }

    public void setIntervaloTentativas(Integer intervaloTentativas) {
        this.intervaloTentativas = intervaloTentativas;
    }

	public Integer getTempoMinimoTentativa() {
		return tempoMinimoTentativa;
	}

	public void setTempoMinimoTentativa(Integer tempoMinimoTentativa) {
		this.tempoMinimoTentativa = tempoMinimoTentativa;
	}
}
