package com.ciandt.watson_orquestrator.orquestrator.router.sulamerica;

import com.ciandt.watson_orquestrator.orquestrator.constants.Constants;
import com.ciandt.watson_orquestrator.orquestrator.dao.LogDao;
import com.ciandt.watson_orquestrator.orquestrator.dao.MapeamentoCamposDao;
import com.ciandt.watson_orquestrator.orquestrator.dao.MapeamentoServicosDao;
import com.ciandt.watson_orquestrator.orquestrator.entity.Log;
import com.ciandt.watson_orquestrator.orquestrator.entity.MapeamentoCampos;
import com.ciandt.watson_orquestrator.orquestrator.entity.MapeamentoServicos;
import com.ciandt.watson_orquestrator.orquestrator.enums.ActionWatsonEnum;
import com.ciandt.watson_orquestrator.orquestrator.exceptions.EmptyErrorServiceException;
import com.ciandt.watson_orquestrator.orquestrator.exceptions.GeneralErrorServiceException;
import com.ciandt.watson_orquestrator.orquestrator.exceptions.NotFoundServiceException;
import com.ciandt.watson_orquestrator.orquestrator.factory.sulamerica.ServicesFactory;
import com.ciandt.watson_orquestrator.orquestrator.model.ConversaOutput;
import com.ciandt.watson_orquestrator.orquestrator.model.sulamerica.ServiceInputBase;
import com.ciandt.watson_orquestrator.orquestrator.resolver.ServicesPropertiesResolver;
import com.ciandt.watson_orquestrator.orquestrator.resolver.WatsonOutputFormatterResolver;
import com.ciandt.watson_orquestrator.orquestrator.service.sulamerica.SaSService;
import com.google.api.server.spi.response.ConflictException;
import com.google.appengine.api.datastore.Query;

import com.google.appengine.api.datastore.Text;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.glassfish.jersey.internal.util.ExceptionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import io.swagger.client.sensedia.watson.model.MessageResponse;

/**
 * Created by rodrigogs on 05/01/17.
 */

public class ServicesRouter {
    private static Logger log = null;
    private MapeamentoServicosDao mapeamentoServicosDao = null;
    private MapeamentoCamposDao mapeamentoCamposDao = null;
    private ServicesPropertiesResolver servicesPropertiesResolver = null;
    private WatsonOutputFormatterResolver watsonOutputFormatterResolver = null;
    private LogDao logDao = null;

    public ServicesRouter() {
        log = Logger.getLogger(getClass().getName());
        mapeamentoServicosDao = new MapeamentoServicosDao();
        mapeamentoCamposDao = new MapeamentoCamposDao();
        watsonOutputFormatterResolver = new WatsonOutputFormatterResolver();
        logDao = new LogDao();
    }

    public ConversaOutput formatOutput(MessageResponse responseWatson, Long idSessao, String requestId) throws ConflictException {
        ConversaOutput retorno = new ConversaOutput();

        Gson gson = new GsonBuilder().create();

        retorno.setContext(responseWatson.getContext());
        retorno.setOutput(responseWatson.getOutput());
        if(responseWatson.getEntities() != null && !responseWatson.getEntities().isEmpty()){
            retorno.setJsonEntities(gson.toJson(responseWatson.getEntities()));
        }
        if(responseWatson.getIntents() != null && !responseWatson.getIntents().isEmpty()){
            retorno.setJsonIntents(gson.toJson(responseWatson.getIntents()));
        }

        if(responseWatson == null ) {
            throw new ConflictException("MessageResponse do Watson não informado.");
        }

//@@SAUDE TESTE
//        if (responseWatson.getInput().getText() != null) {
//            responseWatson.getOutput().setAction(ActionWatsonEnum.STATUS_PAGAMENTO.getCodigo());
//        }

        if(responseWatson.getOutput().getAction() != null &&
                responseWatson.getOutput().getAction() > 0 &&
                !responseWatson.getOutput().getAction().equals(ActionWatsonEnum.FIM_ATENDIMENTO_WATSON.getCodigo()) &&
                !responseWatson.getOutput().getAction().equals(ActionWatsonEnum.REDIRECIONAR_ATENDENTE.getCodigo())) {

            MapeamentoServicos servico = mapeamentoServicosDao.getByProperty("codigoIntencao", responseWatson.getOutput().getAction());

            if(servico != null && !servico.getServico().isEmpty()) {
                try {
                    SaSService servicoInstance = ServicesFactory.getInstance().getService(servico.getServico());

                    if(servicoInstance != null) {
                        Class<? extends ServiceInputBase> tipoEntrada = servicoInstance.getTypeParameterInputClass();
                        Class<?> tipoSaida = servicoInstance.getTypeParameterOutputClass();

                        ServiceInputBase paramEntrada = tipoEntrada.newInstance();
                        Object paramSaida = tipoSaida.newInstance();

                        //Recupera os parâmetros de entrada para invocar o integrar
                        Query.Filter f1 = new Query.FilterPredicate("codigoIntencao", Query.FilterOperator.EQUAL, responseWatson.getOutput().getAction());
                        Query.Filter f2 = new Query.FilterPredicate("tipo", Query.FilterOperator.EQUAL, "E");
                        Query.Filter filter = Query.CompositeFilterOperator.and(f1, f2);

                        List<MapeamentoCampos> camposEntrada = mapeamentoCamposDao.listByFilter(filter);

                        if(camposEntrada != null && camposEntrada.size() > 0) {
                            //Preenche o objeto e invoca o integrar do serviço
                            servicesPropertiesResolver = new ServicesPropertiesResolver();

                            boolean camposPreenchidos = servicesPropertiesResolver.camposPreenchidos(camposEntrada, responseWatson.getContext());

                            //EXIBE O FORMULARIO CASO OS CAMPOS NÃO ESTEJAM PREENCHIDOS OU SEJA A PRIMEIRA VEZ QUE O FORMULARIO É EXIBIDO
                            if(!camposPreenchidos || (responseWatson.getContext().get("fg_formulario_exibido") == null)){

                                // Set Flag Exibe Formulário
                                Map<String, Object> context = new HashMap<String, Object>();
                                context = responseWatson.getContext();
                                context.put("fg_formulario_exibido", "P");
                                responseWatson.setContext(context);

                                retorno.setCamposFormulario(camposEntrada);
                                return retorno;
                            }

                            paramEntrada = servicesPropertiesResolver.populateFields(paramEntrada, camposEntrada, responseWatson.getContext());
                            paramEntrada.setAction(responseWatson.getOutput().getAction());

                            if (paramEntrada != null) {
                                try {

                                    // LIMPA A FLAG FORMULARIO AO CHAMAR O SERVIÇO
                                    Map<String, Object> context = new HashMap<String, Object>();
                                    context = responseWatson.getContext();
                                    context.put("fg_formulario_exibido", null);
                                    responseWatson.setContext(context);

                                    paramSaida = servicoInstance.integrar(paramEntrada, idSessao, requestId);

                                    if (paramSaida != null) {
                                        //Recupera os parametros de saída para substituir as variáveis pelos valores
                                        f1 = new Query.FilterPredicate("codigoIntencao", Query.FilterOperator.EQUAL, responseWatson.getOutput().getAction());
                                        f2 = new Query.FilterPredicate("tipo", Query.FilterOperator.EQUAL, "S");
                                        filter = Query.CompositeFilterOperator.and(f1, f2);

                                        List<MapeamentoCampos> camposSaida = mapeamentoCamposDao.listByFilterWithOrder(filter, "ordem");

                                        if (camposSaida != null && camposSaida.size() > 0) {
                                            //Procura as variáveis no texto de saída e substitui pelos campos de valores
                                            retorno.setOutput(watsonOutputFormatterResolver.onSuccess(servico, paramSaida, responseWatson.getOutput(), camposSaida));
                                        }

                                        retorno.setRetornoServico(Constants.ManifestacoesApi.RETORNO_SERVICO_SUCESSO);
                                    } else {
                                        throw new NotFoundServiceException();
                                    }
                                } catch (NotFoundServiceException | GeneralErrorServiceException e) {
                                    retorno.setOutput(watsonOutputFormatterResolver.onError(e, responseWatson.getOutput()));
                                    retorno.setEnviarSalesforce(true);

                                    if(e.getClass().equals(NotFoundServiceException.class)){
                                        retorno.setRetornoServico(Constants.ManifestacoesApi.RETORNO_SERVICO_INSUCESSO);
                                    } else {
                                        retorno.setRetornoServico(Constants.ManifestacoesApi.RETORNO_SERVICO_ERRO);
                                    }

                                } catch (EmptyErrorServiceException e) {
                                    if (responseWatson.getOutput().getAction().equals(ActionWatsonEnum.APOLICE_BOLETO_EMISSAO2VIA.getCodigo()))
                                    {
                                        retorno.setOutput(watsonOutputFormatterResolver.onError(e, responseWatson.getOutput()));
                                        retorno.setRetornoServico("sucesso");
                                    }
                                    else
                                    {
                                        retorno.setOutput(watsonOutputFormatterResolver.onError(e, responseWatson.getOutput()));
                                        retorno.setRetornoServico("insucesso");
                                    }
                                }
                            }
                        }
                        //CHAMADA SERVIÇOS SEM PARAMETRO DE ENTRADA
                        else
                        {
                            try {
                                paramEntrada.setAction(responseWatson.getOutput().getAction());
                                paramSaida = servicoInstance.integrar(paramEntrada, idSessao, requestId);

                                if (paramSaida != null) {
                                    //Recupera os parametros de saída para substituir as variáveis pelos valores
                                    f1 = new Query.FilterPredicate("codigoIntencao", Query.FilterOperator.EQUAL, responseWatson.getOutput().getAction());
                                    f2 = new Query.FilterPredicate("tipo", Query.FilterOperator.EQUAL, "S");
                                    filter = Query.CompositeFilterOperator.and(f1, f2);

                                    List<MapeamentoCampos> camposSaida = mapeamentoCamposDao.listByFilterWithOrder(filter, "ordem");

                                    if (camposSaida != null && camposSaida.size() > 0) {
                                        //Procura as variáveis no texto de saída e substitui pelos campos de valores
                                        retorno.setOutput(watsonOutputFormatterResolver.onSuccess(servico, paramSaida, responseWatson.getOutput(), camposSaida));
                                    }

                                    retorno.setRetornoServico(Constants.ManifestacoesApi.RETORNO_SERVICO_SUCESSO);
                                } else {
                                    throw new NotFoundServiceException();
                                }
                            } catch (NotFoundServiceException | GeneralErrorServiceException e) {
                                retorno.setOutput(watsonOutputFormatterResolver.onError(e, responseWatson.getOutput()));
                                retorno.setEnviarSalesforce(true);

                                if(e.getClass().equals(NotFoundServiceException.class)){
                                    retorno.setRetornoServico(Constants.ManifestacoesApi.RETORNO_SERVICO_INSUCESSO);
                                } else {
                                    retorno.setRetornoServico(Constants.ManifestacoesApi.RETORNO_SERVICO_ERRO);
                                }

                            } catch (EmptyErrorServiceException e) {
                                if (responseWatson.getOutput().getAction().equals(ActionWatsonEnum.APOLICE_BOLETO_EMISSAO2VIA.getCodigo()))
                                {
                                    retorno.setOutput(watsonOutputFormatterResolver.onError(e, responseWatson.getOutput()));
                                    retorno.setRetornoServico("sucesso");
                                }
                                else
                                {
                                    retorno.setOutput(watsonOutputFormatterResolver.onError(e, responseWatson.getOutput()));
                                    retorno.setRetornoServico("insucesso");
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    log.severe("ServicesRouter : formatOutput: Erro geral. StackTrace: " + ExceptionUtils.exceptionStackTraceAsString(e));
                    throw new ConflictException("ServicesRouter : formatOutput: Erro geral: " + ExceptionUtils.exceptionStackTraceAsString(e));
                }
            } else {
                //SE TIVER ACTION E NAO FOR SERVICO, É RESPOSTA ESTATICA.
                //DEFINE RETORNO SERVIÇO PARA CONTABILIZAR.
                retorno.setRetornoServico(Constants.ManifestacoesApi.RESPOSTA_ESTATICA);
            }
        } else {
            if (responseWatson.getOutput().getAction() != null) {
                                if (responseWatson.getOutput().getAction().equals(ActionWatsonEnum.FIM_ATENDIMENTO_WATSON.getCodigo())) {
                    retorno.setEncerradoWatson(true);
                } else if (responseWatson.getOutput().getAction().equals(ActionWatsonEnum.REDIRECIONAR_ATENDENTE.getCodigo())) {
                    retorno.setEnviarSalesforce(true);
                }
            }
        }

        return retorno;
    }
}
