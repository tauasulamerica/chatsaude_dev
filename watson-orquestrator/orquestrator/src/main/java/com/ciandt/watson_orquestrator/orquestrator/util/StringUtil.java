package com.ciandt.watson_orquestrator.orquestrator.util;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * Created by gsanchez on 19/01/2017.
 */

public class StringUtil {

    public static String toUTF8Encode(String string){
        return new String(string.getBytes(), UTF_8);
    }
}
