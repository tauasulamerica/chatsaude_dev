package com.ciandt.watson_orquestrator.orquestrator.service.ibm;

import com.ciandt.watson_orquestrator.orquestrator.constants.Constants;
import com.ciandt.watson_orquestrator.orquestrator.dao.LogDao;
import com.ciandt.watson_orquestrator.orquestrator.enums.TokenTypesEnum;
import com.ciandt.watson_orquestrator.orquestrator.exceptions.AccessTokenNullException;
import com.ciandt.watson_orquestrator.orquestrator.exceptions.SaSServicesException;
import com.ciandt.watson_orquestrator.orquestrator.helper.LoggerHelper;
import com.ciandt.watson_orquestrator.orquestrator.model.ConversaInput;
import com.ciandt.watson_orquestrator.orquestrator.model.ConversaOutput;
import com.ciandt.watson_orquestrator.orquestrator.model.watson.DadosCorretorWatsonBean;
import com.ciandt.watson_orquestrator.orquestrator.model.watson.DadosSeguradoWatsonBean;
import com.ciandt.watson_orquestrator.orquestrator.resolver.config.WatsonConfig;
import com.ciandt.watson_orquestrator.orquestrator.router.sulamerica.ServicesRouter;
import com.ciandt.watson_orquestrator.orquestrator.service.BaseServices;
import com.ciandt.watson_orquestrator.orquestrator.service.sensedia.SensediaSevice;
import com.ciandt.watson_orquestrator.orquestrator.util.DateUtil;
import com.google.api.server.spi.response.ConflictException;
import com.google.api.server.spi.response.UnauthorizedException;
import com.google.apphosting.api.ApiProxy;

import io.swagger.client.sensedia.watson.ApiClient;
import io.swagger.client.sensedia.watson.ApiException;
import io.swagger.client.sensedia.watson.api.ConversaApi;
import io.swagger.client.sensedia.watson.model.InputData;
import io.swagger.client.sensedia.watson.model.MessageRequest;
import io.swagger.client.sensedia.watson.model.MessageResponse;
import org.glassfish.jersey.internal.util.ExceptionUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by rodrigosclosa on 19/12/16.
 */

public class WatsonConversationServices extends BaseServices<WatsonConfig> {
    private static final String CLASS_NAME = WatsonConversationServices.class.getName();
    private static Logger log = Logger.getLogger(WatsonConversationServices.class.getName());
    private SensediaSevice sensediaSevice = null;
    private ConversaApi conversaApi = null;
    private LogDao logDao = null;
    private String accessToken = "";
    private ServicesRouter servicesRouter = null;
    private ApiClient clientConfig = null;
    private final String TIMEZONE = "America/Sao_Paulo";
    private final String DATEFORMAT = "dd/mm/yyyy HH:mm:ss";

    public WatsonConversationServices() {
        super();
        log.entering(CLASS_NAME, "<init>");

        sensediaSevice = new SensediaSevice(config.getBaseURLWatson());
        conversaApi = new ConversaApi();
        logDao = new LogDao();
        servicesRouter = new ServicesRouter();

        clientConfig = new ApiClient();
        clientConfig.setConnectTimeout(config.getTimeout());
        clientConfig.setBasePath(config.getBaseURLWatson());

        conversaApi.setApiClient(clientConfig);
        log.exiting(CLASS_NAME, "<init>");
    }

    public void requestAccessToken(Long idSessao) throws AccessTokenNullException, ConflictException {
        log.entering(CLASS_NAME, "requestAccessToken");
        try {
            log.info("WatsonConversationServices - Chamada Serviço: sensediaSevice.getAccessToken");
            accessToken = sensediaSevice.getAccessToken(idSessao, TokenTypesEnum.Watson);
            log.info("WatsonConversationServices - Retorno Serviço: sensediaSevice.getAccessToken");

            if (accessToken == null) {
                throw new AccessTokenNullException();
            }
        } catch (UnauthorizedException e) {
            log.severe("WatsonConversationServices - requestAccessToken: Não foi possível gerar um token de acesso para Sensedia. Stack trace: " + ExceptionUtils.exceptionStackTraceAsString(e));
            throw new ConflictException("WatsonConversationServices - requestAccessToken: Não foi possível gerar um token de acesso para Sensedia. Stack trace: " + ExceptionUtils.exceptionStackTraceAsString(e));
        }
        log.exiting(CLASS_NAME, "requestAccessToken", accessToken);
    }

    public ConversaOutput integrar(ConversaInput conversaInput) throws ConflictException, SaSServicesException {
    		return integrar(conversaInput, 0);
    }
    
    private ConversaOutput integrar(ConversaInput conversaInput, int numRetries) throws ConflictException, SaSServicesException {
        log.entering(CLASS_NAME, "integrar");
        ConversaOutput retorno = new ConversaOutput();

        while (numRetries <= config.getTentativas() 
        		&& ApiProxy.getCurrentEnvironment().getRemainingMillis() > config.getTempoMinimoTentativa()) {
            try {
                requestAccessToken(conversaInput.getIdSessao());

                LoggerHelper.getInstance(log).log("WatsonConversationServices - Objeto entrada", conversaInput);

                MessageResponse response;

                if(conversaInput.getRespostaAnterior() != null
                        && conversaInput.getRespostaAnterior().getOutput() != null
                        && conversaInput.getRespostaAnterior().getOutput().getAction() != null) {
                    ConversaOutput outputAnterior = conversaInput.getRespostaAnterior();

                    response = new MessageResponse();
                    response.setContext(outputAnterior.getContext());
                    response.setOutput(outputAnterior.getOutput());
                    response.setWorkspaceId(outputAnterior.getWorkspaceId());
                } else {
                    response = conversation(config.getClientId(), accessToken, conversaInput);
                }

                Long idSessao = conversaInput != null ? conversaInput.getIdSessao() : null;

                LoggerHelper.getInstance(log).log("WatsonConversationServices - response", response);

                try {
                    retorno = servicesRouter.formatOutput(response, idSessao, conversaInput.getRequestId());
                    retorno.setWorkspaceId(response.getWorkspaceId());

                    LoggerHelper.getInstance(log).log("WatsonConversationServices - retorno", retorno);
                } catch (ConflictException e) {
                    log.severe("WatsonConversationServices - integrar: Não foi possível formatar a saída para o usuário " + ExceptionUtils.exceptionStackTraceAsString(e));
                    throw new ConflictException("WatsonConversationServices - integrar: Erro ao executar o processamento das mensagens: " + ExceptionUtils.exceptionStackTraceAsString(e));
                }
            } catch (AccessTokenNullException an) {
                log.severe("WatsonConversationServices: Não foi possível gerar um token de acesso para Sensedia. Stack trace: " + ExceptionUtils.exceptionStackTraceAsString(an));
                throw new ConflictException("WatsonConversationServices: Não foi possível gerar um token de acesso para Sensedia. Stack trace: " + ExceptionUtils.exceptionStackTraceAsString(an));
            } catch (ApiException e) {
                if (e.getCode() == Constants.ApiErrorCodes.FORBIDDEN) {
                    requestAccessToken(conversaInput.getIdSessao());

                    log.warning("WatsonConversationServices - integrar: Sleep por não conseguir pegar o token.");
                    numRetries++;
                    sleep(config.getIntervaloTentativas());

                    return integrar(conversaInput, numRetries);
                } else if (e.getCode() == Constants.ApiErrorCodes.UNAUTHORIZED) {
                    log.info("WatsonConversationServices - integrar: UNAUTHORIZED. " + ExceptionUtils.exceptionStackTraceAsString(e));

                    log.warning("WatsonConversationServices - integrar: Sleep por não conseguir pegar o token.");
                    numRetries++;
                    sleep(config.getIntervaloTentativas());

                    return integrar(conversaInput, numRetries);
                } else {
                    LoggerHelper.getInstance(log).log(Level.SEVERE, "WatsonConversationServices - ApiException", e.getResponseBody());
                    throw new ConflictException("WatsonConversationServices - integrar: ApiException: " + ExceptionUtils.exceptionStackTraceAsString(e));
                }
            } catch (Exception e) {
                log.severe("WatsonConversationServices - integrar: Erro geral: " + ExceptionUtils.exceptionStackTraceAsString(e));
                throw new ConflictException("WatsonConversationServices - integrar: Erro geral: " + ExceptionUtils.exceptionStackTraceAsString(e));
            }

            log.exiting(CLASS_NAME, "integrar", retorno);
            return retorno;
        }

        log.exiting(CLASS_NAME, "integrar");
        log.severe("WatsonConversationServices - integrar: Não foi possível recuperar o token. Tentativas: " + numRetries + " Tempo restante:" + ApiProxy.getCurrentEnvironment().getRemainingMillis());
        return null;
    }

    public ConversaOutput iniciarSessao(Long idSessao, DadosSeguradoWatsonBean dadosSegurado, String mensagemPreChat) throws ConflictException, SaSServicesException {
        log.entering(CLASS_NAME, "iniciarSessao");
        ConversaInput conversaInput = new ConversaInput();
        conversaInput.setIdSessao(idSessao);
        
        Map<String, Object> context = new HashMap<String, Object>();
        context.put(Constants.Watson.MENSAGEM_PRE_CHAT, mensagemPreChat);
        context.put("dados_segurado", dadosSegurado);

        context.put("nome", dadosSegurado.getNome());
        context.put("origem", dadosSegurado.getOrigem()); // Portal / Mobile
        context.put("produto", dadosSegurado.getProduto()); //Individual / PME / Grupal
        context.put("carteirinha", dadosSegurado.getCarteirinha());
        context.put("codEmpresa", dadosSegurado.getCodEmpresa());

        conversaInput.setContext(context);
        
        log.exiting(CLASS_NAME, "iniciarSessao");

        return integrar(conversaInput);
    }

    private MessageResponse conversation(String clientId, String accessToken, ConversaInput conversaInput) throws ApiException {
        log.entering(CLASS_NAME, "conversation");
        MessageRequest body = new MessageRequest();

        if (conversaInput != null) {
            InputData input = new InputData();
            body.setInput(input.text(conversaInput.getTexto()));
            body.setAlternateIntents(true);
            body.setIntents(null);

            if (conversaInput.getContext() != null) {
                body.setContext(conversaInput.getContext());
            }
        } else {
            //Iniciar sessão, passa a Data/Hora
            Map<String, Object> context = new HashMap<String, Object>();
            context.put("datetime", DateUtil.corrigeTimezoneWithFormat(new Date(), DATEFORMAT));

            body.setContext(context);
        }

        log.info("WatsonConversationServices - Chamada Serviço: ConversaApi.conversationPost");
        MessageResponse response = conversaApi.conversationPost(clientId, accessToken, conversaInput.getIdSessaoAsString(), conversaInput.getRequestId(), body);
        log.info("WatsonConversationServices - Chamada Serviço: ConversaApi.conversationPost");

        log.exiting(CLASS_NAME, "conversation", response);
        return response;
    }

}
