package com.ciandt.watson_orquestrator.orquestrator.model.sulamerica;

import java.io.Serializable;

/**
 * Created by rodrigogs on 03/01/17.
 */

public class ConsultarDadosBoletoInput extends ServiceInputBase implements Serializable {
    private String numDocumento;
    private String numSucursal;
    private String numEndosso;
    private String cpf;

    public ConsultarDadosBoletoInput() {}

    public String getNumSucursal() {
        return numSucursal;
    }

    public void setNumSucursal(String numSucursal) {
        this.numSucursal = numSucursal;
    }

    public String getNumDocumento() {
        return numDocumento;
    }

    public void setNumDocumento(String numDocumento) {
        this.numDocumento = numDocumento;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getNumEndosso() {
        return numEndosso;
    }

    public void setNumEndosso(String numEndosso) {
        this.numEndosso = numEndosso;
    }
}
