package com.ciandt.watson_orquestrator.orquestrator.factory.memcache;

import com.ciandt.watson_orquestrator.orquestrator.constants.Constants;
import com.ciandt.watson_orquestrator.orquestrator.enums.TokenTypesEnum;
import com.ciandt.watson_orquestrator.orquestrator.helper.LoggerHelper;
import com.ciandt.watson_orquestrator.orquestrator.model.memcache.TokenDataModel;
import com.ciandt.watson_orquestrator.orquestrator.model.memcache.TokenListModel;
import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import org.glassfish.jersey.internal.util.ExceptionUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import javax.cache.Cache;
import javax.cache.CacheException;
import javax.cache.CacheFactory;
import javax.cache.CacheManager;

/**
 * Created by rodrigogs on 01/03/17.
 */
public class MemcacheFactory<K, T> {
    private static Logger log = Logger.getLogger(MemcacheFactory.class.getName());
    private Cache cache;

    protected Class<T> clazz;

    public MemcacheFactory(int expirationDelta) {
        this.clazz = (Class<T>) clazz;

        try {
            CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
            Map properties = new HashMap<>();
            properties.put(GCacheFactory.EXPIRATION_DELTA, expirationDelta);
            cache = cacheFactory.createCache(properties);
        } catch (CacheException e) {
            LoggerHelper.getInstance(log).log("MemcacheFactory - Constructor error: ", ExceptionUtils.exceptionStackTraceAsString(e));
        }
    }

    public Boolean exists(K key) {
        return cache.containsKey(key);
    }

    public T getFromCache(K key) {
        Object retorno = null;

        if(cache != null && key != null) {
            retorno = (T)cache.get(key);
        }

        return (T)retorno;
    }

    public void saveToCache(K key, T model) {
        if(cache != null && key != null) {
            cache.put(key, model);
        }
    }

}
