package com.ciandt.watson_orquestrator.orquestrator.model;

import java.io.Serializable;

/**
 * Created by rodrigogs on 02/03/17.
 */

public class EncryptOutput implements Serializable {
    private String cryptografado;

    public EncryptOutput() {
    }

    public String getCryptografado() {
        return cryptografado;
    }

    public void setCryptografado(String cryptografado) {
        this.cryptografado = cryptografado;
    }
}
