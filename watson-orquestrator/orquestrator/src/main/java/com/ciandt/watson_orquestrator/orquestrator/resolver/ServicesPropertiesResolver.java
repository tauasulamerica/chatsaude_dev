package com.ciandt.watson_orquestrator.orquestrator.resolver;

import com.ciandt.watson_orquestrator.orquestrator.entity.MapeamentoCampos;
import com.ciandt.watson_orquestrator.orquestrator.model.sulamerica.ServiceInputBase;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

/**
 * Created by rodrigogs on 09/01/17.
 */

public class ServicesPropertiesResolver {

    public ServicesPropertiesResolver() {
    }

    public ServiceInputBase populateFields(ServiceInputBase entrada, List<MapeamentoCampos> campos, Map<String, Object> context) throws IllegalAccessException {

        for (MapeamentoCampos campo : campos) {
            for (Field field : entrada.getClass().getDeclaredFields()) {

                if(field.getName().equals(campo.getCampoValorDestino())) {
                    // make private fields accessible
                    field.setAccessible(true);

                    String paramValue = (String)context.get(campo.getVariavelOrigem());

                    if(paramValue != null) {
                        field.set(entrada, paramValue);
                    }
                }
            }
        }

        return entrada;
    }

    public boolean camposPreenchidos(List<MapeamentoCampos> campos, Map<String, Object> context) throws IllegalAccessException {

        for (MapeamentoCampos campo : campos) {
            Object paramValue = context.get(campo.getVariavelOrigem());
            if(paramValue == null){
                return false;
            }
        }

        return true;
    }
}
