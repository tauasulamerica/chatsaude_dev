#!/usr/bin/env bash

# Copyright 2017 Rodrigo Sclosa <rodrigogs@ciandt.com>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
set -xe

projectId=$1
appVersion=$2

#Limpa todo o build
./gradlew clean

#Faz o build das interfaces
cd ../Client\ Chat/
npm install
bower install
grunt build --force

#Gera a documentação Discovery das APIs
cd ../watson-orquestrator
./gradlew endpointsDiscoveryDocs -DprojectId=$projectId

#Gera a documentação OpenAPI das APIs
./gradlew endpointsOpenApiDocs -DprojectId=$projectId

#Copia o arquivo OpenAPI gerado
cp orquestrator/build/endpointsOpenApiDocs/openapi.json orquestrator/src/main/webapp/openapi.json

#Faz o deploy do aplicativo no GAE
./gradlew appengineDeploy -DprojectId=$projectId -DappVersion=$appVersion

#Configura o projeto
gcloud config set project $projectId

#Faz a atualização da especificação OpenAPI no Endpoints
#gcloud endpoints services enable endpoints.googleapis.com
gcloud endpoints services deploy orquestrator/build/endpointsOpenApiDocs/openapi.json