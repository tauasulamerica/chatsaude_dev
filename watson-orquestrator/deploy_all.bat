gradlew clean
cd ..\"Client Chat"
npm install
bower install
grunt build --force
cd ..\watson-orquestrator
gradlew endpointsDiscoveryDocs -DprojectId=%1
gradlew endpointsOpenApiDocs -DprojectId=%1
copy orquestrator\build\endpointsOpenApiDocs\openapi.json orquestrator\src\main\webapp\openapi.json
gradlew appengineDeploy -DprojectId=%1 -DappVersion=%2
gcloud config set project %1
gcloud endpoints services enable endpoints.googleapis.com
gcloud endpoints services deploy orquestrator/build/endpointsOpenApiDocs/openapi.json