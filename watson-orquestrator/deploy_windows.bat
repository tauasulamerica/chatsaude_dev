echo %1
call gradlew clean
cd ..\"Client Chat"
call npm install
call bower install
call grunt build --force
cd ..\watson-orquestrator
call gradlew endpointsDiscoveryDocs -DprojectId=%1
call gradlew endpointsOpenApiDocs -DprojectId=%1
copy orquestrator\build\endpointsOpenApiDocs\openapi.json orquestrator\src\main\webapp\openapi.json
call gradlew appengineDeploy -DprojectId=%1 -DappVersion=%2
call gcloud config set project %1
call gcloud endpoints services deploy orquestrator/build/endpointsOpenApiDocs/openapi.json