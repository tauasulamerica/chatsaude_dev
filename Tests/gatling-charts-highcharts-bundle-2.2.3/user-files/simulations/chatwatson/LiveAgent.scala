package chatwatson

import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

class LiveAgent extends Simulation {

	val num_users = 30
	val tempo_users = 30

	//def mapToJson = (obj: Any) => com.fasterxml.jackson.databind.ObjectMapper.writeValueAsString(obj)
	def mapToJson = (obj: Any) => io.gatling.core.json.Json.stringify(obj)
	//def mapToJson = (obj: Any) => io.gatling.core.json.Boon.parse(obj)

	object Chat {

		val httpProtocol = http
			.baseURL("https://apisulamerica.sensedia.com")
			.inferHtmlResources(BlackList(""".*\.js""", """.*\.css""", """.*\.gif""", """.*\.jpeg""", """.*\.jpg""", """.*\.ico""", """.*\.woff""", """.*\.(t|o)tf""", """.*\.png""", """.*\.wav"""), WhiteList())
			.userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36")

		val access_token = ""
		val client_id = "f6bcf772-13fb-3d5c-b86f-ede8f64bbdca"
		val client_secret = "90153e42-79d5-310f-bc0c-084476688b7e"
		val context = ""
		val idSessao = ""
		val organizationId = "00D3C000000H6AF" //HOM: 00D3C000000H6AF  DEV: 00Dg0000006HfGS

		val headers_accesstoken = Map(
			"Origin" -> "https://projeto-cognitivo-h.appspot.com",
			"client_id" -> client_id)

		val headers_iniciar = Map(
			"Content-Type" -> "application/json;charset=UTF-8",
			"access_token" -> "${access_token}",
			"client_id" -> client_id)

		val body_iniciar = StringBody("""{ 
			"codigoNAC": "11392",
			"codigoProdutor": "283924",
			"razaoSocial": "CI&T",
			"nome": "Teste Automatizado"}""")

		val headers_mensagens = Map(
			"Content-Type" -> "application/json;charset=UTF-8",
			"access_token" -> "${access_token}",
			"client_id" -> client_id)

		val body_conversa = StringBody("""{
			"context": ${context},
			"idSessao": "${idSessao}",
			"texto": "${texto_conversa}"
			}""")

		val headers_access_token_liveagent = Map(
			"Origin" -> "https://projeto-cognitivo-h.appspot.com",
			"X-LIVEAGENT-AFFINITY" -> "null",
			"client_id" -> client_id)

		val headers_session_liveagent = Map(
			"Origin" -> "https://projeto-cognitivo-h.appspot.com",
			"X-LIVEAGENT-AFFINITY" -> "null",
			"X-LIVEAGENT-API-VERSION" -> "37",
			"access_token" -> "${access_token}",
			"client_id" -> client_id)

		val headers_init_chat_liveagent = Map(
			"Origin" -> "https://projeto-cognitivo-h.appspot.com",
			"X-LIVEAGENT-AFFINITY" -> "${affinityToken}",
			"X-LIVEAGENT-API-VERSION" -> "37",
			"X-LIVEAGENT-SEQUENCE" -> "1",
			"X-LIVEAGENT-SESSION-KEY" -> "${key}",
			"access_token" -> "${access_token}",
			"client_id" -> client_id)

		val body_init_chat_liveagent = StringBody("""
			{"sessionId":"${id}","organizationId":""" + organizationId + ""","deploymentId":"57231000000NXL9","buttonId":"57331000000NZyN","userAgent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36","language":"pt-BR","screenResolution":"1900x1080","visitorName":"TESTE AUTOMATIZADO","prechatDetails":[{"label":"Nome","value":"rodrigo","entityMaps":[{"entityName":"Contact","fieldName":"Name","isFastFillable":false,"isAutoQueryable":false,"isExactMatchable":false}],"transcriptFields":["NOME__c"],"displayToAgent":true},{"label":"Area","value":"Auto Emissão Individual","entityMaps":[{"entityName":"Case","fieldName":"IDC_AREA__c","isFastFillable":false,"isAutoQueryable":false,"isExactMatchable":false}],"transcriptFields":["IDC_AREA__c"],"displayToAgent":true},{"label":"CodigodoProdutor","value":"283924","entityMaps":[{"entityName":"LiveChatTranscript","fieldName":"CodigodoPlano","isFastFillable":false,"isAutoQueryable":false,"isExactMatchable":false}],"transcriptFields":["Codigo_do_Produtor__c"],"displayToAgent":true},{"label":"CodigoNAC","value":"11392","entityMaps":[{"entityName":"LiveChatTranscript","fieldName":"CodigodoCliente","isFastFillable":false,"isAutoQueryable":false,"isExactMatchable":false}],"transcriptFields":["Codigo_NAC__c"],"displayToAgent":true},{"label":"Origem","value":"Chat","entityMaps":[{"entityName":"Case","fieldName":"Origin","isFastFillable":false,"isAutoQueryable":false,"isExactMatchable":false}],"transcriptFields":["Origin"],"displayToAgent":true},{"label":"NomeDoProdutor","value":"CI&T","entityMaps":[{"entityName":"LiveChatTranscript","fieldName":"Nome_do_produtor__c","isFastFillable":false,"isAutoQueryable":false,"isExactMatchable":false}],"transcriptFields":["Nome_do_produtor__c"],"displayToAgent":true}],"prechatEntities":[{"entityName":"Case","showOnCreate":true,"linkToEntityName":"Account","saveToTranscript":"LiveChatTranscript","entityFieldsMaps":[{"fieldName":"NOME_DA_PESSOA__c","label":"NomeDoProdutor","doFind":false,"isExactMatch":false,"doCreate":true},{"fieldName":"Origin","label":"Origem","doFind":false,"isExactMatch":true,"doCreate":true}]}],"receiveQueueUpdates":true,"isPost":true}
			""")

		val headers_getmessage_liveagent = Map(
			"Origin" -> "https://projeto-cognitivo-h.appspot.com",
			"X-LIVEAGENT-AFFINITY" -> "${affinityToken}",
			"X-LIVEAGENT-API-VERSION" -> "37",
			"X-LIVEAGENT-SESSION-KEY" -> "${key}",
			"access_token" -> "${access_token}",
			"client_id" -> client_id)

		val ambiente = "/" // /homolog    /dev/
		var prefAmbiente = "/homolog/";

		val urlAccessTokenCorretor = prefAmbiente + "auto/corretor/chat/bot/api/v1/oauth/access-token"
		val urlConversas = prefAmbiente + "auto/corretor/chat/bot/api/v1/conversas"
		val urlAccessTokenLiveagent = prefAmbiente + "auto/corretor/chat/bot/salesforce/api/v1" + ambiente + "oauth/access-token"
		val urlGetSessionLiveagent = prefAmbiente + "auto/corretor/chat/bot/salesforce/api/v1" + ambiente + "sessao"
		val urlInitChatLiveagent = prefAmbiente + "auto/corretor/chat/bot/salesforce/api/v1" + ambiente + "conversas"
		val urlGetMessagesLiveagent = prefAmbiente + "auto/corretor/chat/bot/salesforce/api/v1" + ambiente + "mensagens"

		val liveAgent = scenario("LiveAgent")
			.exec(http("Google Analytics")
				.get("https://www.google-analytics.com/collect?v=1&_v=j47&a=627520423&t=pageview&_s=1&dl=https%3A%2F%2Fprojeto-cognitivo-h.appspot.com%2F&ul=pt-br&de=UTF-8&sd=24-bit&sr=1920x1080&vp=1143x983&je=0&fl=24.0%20r0&_u=AACAAEABI~&jid=&cid=557017007.1486646386&tid=UA-68316501-8&z=1464662247"))
			.pause(2)
			.exec(http("Get Access Token Corretor")
				.post(urlAccessTokenCorretor)
				.headers(headers_accesstoken)
				.basicAuth(client_id, client_secret)
				.check(jsonPath("$.access_token").saveAs("access_token")))
			.pause(1)
			.exec(session => {
				val access_token = session.get("access_token").asOption[String]
				println(access_token.getOrElse("Não foi possível obter o AccessToken da Sensedia"))
				session })
			.pause(2)
			.exec(http("Iniciar Conversa")
				.post(urlConversas)
				.headers(headers_iniciar)
				.body(body_iniciar).asJSON
				.basicAuth(client_id, client_secret)
				.check(jsonPath("$.context").ofType[String].transform(mapToJson).saveAs("context"))
				.check(jsonPath("$.idSessao").saveAs("idSessao")))
			//.exec(s => {println(s.attributes("context")); s})
			.pause(1)
			.exec(session => {
				val context = session.get("context").asOption[String]
				val idSessao = session.get("idSessao").asOption[String]
				println(context.getOrElse("Não foi possível salvar o Context do Watson."))
				println(idSessao.getOrElse("Não foi possível salvar o idSessao do Orquestrador."))
				session.set("texto_conversa", "critica") })
			.pause(2)
			.exec(http("Enviar para Liveagent")
				.put(urlConversas)
				.headers(headers_mensagens)
				.body(body_conversa)
				.basicAuth(client_id, client_secret)
				.check(jsonPath("$.context").ofType[String].transform(mapToJson).saveAs("context"))
				.check(jsonPath("$.idSessao").saveAs("idSessao")))
			.pause(1)
			.exec(session => {
				val context = session.get("context").asOption[String]
				val idSessao = session.get("idSessao").asOption[String]
				println(context.getOrElse("Não foi possível salvar o Context do Watson."))
				println(idSessao.getOrElse("Não foi possível salvar o idSessao do Orquestrador."))
				//session.set("texto_conversa", num_proposta) })
				session })
			.pause(3)
			.exec(http("Get Access Token LiveAgent")
				.post(urlAccessTokenLiveagent)
				.headers(headers_access_token_liveagent)
				.basicAuth(client_id, client_secret)
				.check(jsonPath("$.access_token").saveAs("access_token")))
			.pause(1)
			.exec(session => {
				val access_token = session.get("access_token").asOption[String]
				println(access_token.getOrElse("Não foi possível obter o AccessToken do LiveAgent da Sensedia"))
				session })
			.pause(2)
			.exec(http("Get Session LiveAgent")
				.get(urlGetSessionLiveagent)
				.headers(headers_session_liveagent)
				.basicAuth(client_id, client_secret)
				.check(jsonPath("$.id").saveAs("id"))
				.check(jsonPath("$.key").saveAs("key"))
				.check(jsonPath("$.affinityToken").saveAs("affinityToken")))
			.pause(1)
			.exec(session => {
				val id = session.get("id").asOption[String]
				val key = session.get("key").asOption[String]
				val affinityToken = session.get("affinityToken").asOption[String]
				println(id.getOrElse("Não foi possível salvar o ID do LiveAgent."))
				println(key.getOrElse("Não foi possível salvar o KEY do LiveAgent."))
				println(affinityToken.getOrElse("Não foi possível salvar o AFFINITYTOKEN do LiveAgent."))
				session })
			.pause(2)
			.exec(http("Init Chat LiveAgent")
				.post(urlInitChatLiveagent)
				.headers(headers_init_chat_liveagent)
				.body(body_init_chat_liveagent)
				.basicAuth(client_id, client_secret)
				.check(status.is(201)))
			.pause(3)
			.exec(http("Get Message LiveAgent")
				.get(urlGetMessagesLiveagent)
				.headers(headers_getmessage_liveagent)
				.basicAuth(client_id, client_secret)
				.check(jsonPath("$.messages[0].type").is("ChatRequestSuccess").saveAs("ChatRequestSuccess"))
				.check(jsonPath("$.messages[2].type").is("ChatEstablished").saveAs("ChatEstablished"))
				.check(status.is(200)))
			.pause(30)
			.doIf("${ChatRequestSuccess.exists()}") {
				doIfOrElse("${ChatEstablished.exists()}") {
					//Atendeu
					exec(s => {println("Atendeu conversa no LiveAgent"); s})
				} {
					//Caiu na fila
					exec(s => {println("Caiu na fila do LiveAgent"); s})
				}	
			}
			.pause(2)
	}

	val scenario_liveagent = scenario("Encaminhar para LiveAgent").exec(Chat.liveAgent)

	setUp(scenario_liveagent.inject(rampUsers(num_users) over (tempo_users seconds))).protocols(Chat.httpProtocol)
}