package chatwatson

import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

class Consulta extends Simulation {

	val num_users = 100
	val tempo_users = 120

	//def mapToJson = (obj: Any) => com.fasterxml.jackson.databind.ObjectMapper.writeValueAsString(obj)
	def mapToJson = (obj: Any) => io.gatling.core.json.Json.stringify(obj)
	//def mapToJson = (obj: Any) => io.gatling.core.json.Boon.parse(obj)

	object Chat {

		val httpProtocol = http
			.baseURL("https://apisulamerica.sensedia.com")
			.inferHtmlResources(BlackList(""".*\.js""", """.*\.css""", """.*\.gif""", """.*\.jpeg""", """.*\.jpg""", """.*\.ico""", """.*\.woff""", """.*\.(t|o)tf""", """.*\.png""", """.*\.wav"""), WhiteList())
			.userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36")

		val access_token = ""
		val client_id = "f6bcf772-13fb-3d5c-b86f-ede8f64bbdca"
		val client_secret = "90153e42-79d5-310f-bc0c-084476688b7e"
		val context = ""
		val idSessao = ""

		val headers_accesstoken = Map("client_id" -> client_id)

		val headers_iniciar = Map(
			"Content-Type" -> "application/json;charset=UTF-8",
			"access_token" -> "${access_token}",
			"client_id" -> client_id)

//		val body_iniciar = StringBody("""{ 
//			"codigoNAC": "11392",
//			"codigoProdutor": "283924",
//			"razaoSocial": "CI&T",
//			"nome": "Teste Automatizado"}""")

		val body_iniciar = StringBody("""{ 
			"codigoNAC": "16927-7",
			"codigoProdutor": "194840",
			"razaoSocial": "CORRETORA DE SEGUROS HONDA LTDA",
			"nome": "Teste Automatizado"}""")

		val headers_mensagens = Map(
			"Content-Type" -> "application/json;charset=UTF-8",
			"access_token" -> "${access_token}",
			"client_id" -> client_id)

		val body_conversa = StringBody("""{
			"context": ${context},
			"idSessao": "${idSessao}",
			"texto": "${texto_conversa}"
			}""")

		val proposta = csv("propostas.csv").random
		val viaApolice = csv("2via-apolice.csv").random
		val viaBoleto = csv("2via-boleto.csv").random
		val statusApolice = csv("status-apolice.csv").random

		val consultaStatusProposta = exec(http("Get Access Token Corretor")
				.post("/homolog/auto/corretor/chat/bot/api/v1/oauth/access-token")
				.headers(headers_accesstoken)
				.basicAuth(client_id, client_secret)
				.check(jsonPath("$.access_token").saveAs("access_token")))
			.pause(2)
			.exec(session => {
				val access_token = session.get("access_token").asOption[String]
				println(access_token.getOrElse("Não foi possível obter o AccessToken da Sensedia"))
				session })
			.pause(1)
			.exec(http("Iniciar Conversa")
				.post("/homolog/auto/corretor/chat/bot/api/v1/conversas")
				.headers(headers_iniciar)
				.body(body_iniciar).asJSON
				.basicAuth(client_id, client_secret)
				//.check(jsonPath("$.context").saveAs("context"))
				.check(jsonPath("$.context").ofType[String].transform(mapToJson).saveAs("context"))
				.check(jsonPath("$.idSessao").saveAs("idSessao")))
			//.exec(s => {println(s.attributes("context")); s})
			.pause(2)
			.exec(session => {
				val context = session.get("context").asOption[String]
				val idSessao = session.get("idSessao").asOption[String]
				println(context.getOrElse("Não foi possível salvar o Context do Watson."))
				println(idSessao.getOrElse("Não foi possível salvar o idSessao do Orquestrador."))
				session.set("texto_conversa", "Consultar status proposta") })
			.pause(5)
			.feed(proposta)
			.exec(http("Mensagem 1")
				.put("/homolog/auto/corretor/chat/bot/api/v1/conversas")
				.headers(headers_mensagens)
				.body(body_conversa).asJSON
				.basicAuth(client_id, client_secret)
				//.check(jsonPath("$.context").saveAs("context"))
				.check(jsonPath("$.context").ofType[String].transform(mapToJson).saveAs("context"))
				.check(jsonPath("$.idSessao").saveAs("idSessao")))
			//.exec(s => {println(s.attributes("context")); s})
			.pause(1)
			.exec(session => {
				val context = session.get("context").asOption[String]
				val idSessao = session.get("idSessao").asOption[String]
				val num_proposta = session.get("num_proposta").as[String]
				println(context.getOrElse("Não foi possível salvar o Context do Watson."))
				println(idSessao.getOrElse("Não foi possível salvar o idSessao do Orquestrador."))
				session.set("texto_conversa", num_proposta) })
			.pause(5)
			.exec(http("Mensagem 2")
				.put("/homolog/auto/corretor/chat/bot/api/v1/conversas")
				.headers(headers_mensagens)
				.body(body_conversa).asJSON
				.basicAuth(client_id, client_secret)
				//.check(jsonPath("$.context").saveAs("context"))
				.check(jsonPath("$.context").ofType[String].transform(mapToJson).saveAs("context"))
				.check(jsonPath("$.idSessao").saveAs("idSessao")))
			//.exec(s => {println(s.attributes("context")); s})
			.pause(1)
			.exec(session => {
				val context = session.get("context").asOption[String]
				val idSessao = session.get("idSessao").asOption[String]
				println(context.getOrElse("Não foi possível salvar o Context do Watson."))
				println(idSessao.getOrElse("Não foi possível salvar o idSessao do Orquestrador."))
				session.set("texto_conversa", "sim") })
			.pause(3)
			.exec(http("Mensagem 3")
				.put("/homolog/auto/corretor/chat/bot/api/v1/conversas")
				.headers(headers_mensagens)
				.body(body_conversa).asJSON
				.basicAuth(client_id, client_secret)
				//.check(jsonPath("$.context").saveAs("context"))
				.check(jsonPath("$.context").ofType[String].transform(mapToJson).saveAs("context"))
				.check(jsonPath("$.idSessao").saveAs("idSessao")))
			//.exec(s => {println(s.attributes("context")); s})
			.pause(1)
			.exec(session => {
				val context = session.get("context").asOption[String]
				val idSessao = session.get("idSessao").asOption[String]
				println(context.getOrElse("Não foi possível salvar o Context do Watson."))
				println(idSessao.getOrElse("Não foi possível salvar o idSessao do Orquestrador."))
				session.set("texto_conversa", "obrigado") })
			.pause(5)
			.exec(http("Mensagem Final")
				.put("/homolog/auto/corretor/chat/bot/api/v1/conversas")
				.headers(headers_mensagens)
				.body(body_conversa).asJSON
				.basicAuth(client_id, client_secret)
				//.check(jsonPath("$.context").saveAs("context"))
				.check(jsonPath("$.context").ofType[String].transform(mapToJson).saveAs("context"))
				.check(jsonPath("$.idSessao").saveAs("idSessao")))
			//.exec(s => {println(s.attributes("context")); s})
			.pause(1)
			.exec(session => {
				val context = session.get("context").asOption[String]
				val idSessao = session.get("idSessao").asOption[String]
				println(context.getOrElse("Não foi possível salvar o Context do Watson."))
				println(idSessao.getOrElse("Não foi possível salvar o idSessao do Orquestrador."))
				session })

// *************************************** Consulta Status Apolice ***************************************************

		val consultaStatusApolice = exec(http("Get Access Token Corretor")
				.post("/homolog/auto/corretor/chat/bot/api/v1/oauth/access-token")
				.headers(headers_accesstoken)
				.basicAuth(client_id, client_secret)
				.check(jsonPath("$.access_token").saveAs("access_token")))
			.pause(2)
			.exec(session => {
				val access_token = session.get("access_token").asOption[String]
				println(access_token.getOrElse("Não foi possível obter o AccessToken da Sensedia"))
				session })
			.pause(1)
			.exec(http("Iniciar Conversa")
				.post("/homolog/auto/corretor/chat/bot/api/v1/conversas")
				.headers(headers_iniciar)
				.body(body_iniciar).asJSON
				.basicAuth(client_id, client_secret)
				//.check(jsonPath("$.context").saveAs("context"))
				.check(jsonPath("$.context").ofType[String].transform(mapToJson).saveAs("context"))
				.check(jsonPath("$.idSessao").saveAs("idSessao")))
			//.exec(s => {println(s.attributes("context")); s})
			.pause(2)
			.exec(session => {
				val context = session.get("context").asOption[String]
				val idSessao = session.get("idSessao").asOption[String]
				println(context.getOrElse("Não foi possível salvar o Context do Watson."))
				println(idSessao.getOrElse("Não foi possível salvar o idSessao do Orquestrador."))
				session.set("texto_conversa", "Consultar status apolice") })
			.pause(5)
			.exec(http("Mensagem 1")
				.put("/homolog/auto/corretor/chat/bot/api/v1/conversas")
				.headers(headers_mensagens)
				.body(body_conversa).asJSON
				.basicAuth(client_id, client_secret)
				//.check(jsonPath("$.context").saveAs("context"))
				.check(jsonPath("$.context").ofType[String].transform(mapToJson).saveAs("context"))
				.check(jsonPath("$.idSessao").saveAs("idSessao")))
			//.exec(s => {println(s.attributes("context")); s})
			.pause(1)
			.feed(statusApolice)
			.exec(session => {
				val context = session.get("context").asOption[String]
				val idSessao = session.get("idSessao").asOption[String]
				val num_apolice = session.get("num_apolice").as[String]
				println(context.getOrElse("Não foi possível salvar o Context do Watson."))
				println(idSessao.getOrElse("Não foi possível salvar o idSessao do Orquestrador."))
				session.set("texto_conversa", num_apolice) })
			.pause(5)
			.exec(http("Mensagem 2")
				.put("/homolog/auto/corretor/chat/bot/api/v1/conversas")
				.headers(headers_mensagens)
				.body(body_conversa).asJSON
				.basicAuth(client_id, client_secret)
				//.check(jsonPath("$.context").saveAs("context"))
				.check(jsonPath("$.context").ofType[String].transform(mapToJson).saveAs("context"))
				.check(jsonPath("$.idSessao").saveAs("idSessao")))
			//.exec(s => {println(s.attributes("context")); s})
			.pause(1)
			.exec(session => {
				val context = session.get("context").asOption[String]
				val idSessao = session.get("idSessao").asOption[String]
				val num_sucursal = session.get("num_sucursal").as[String]
				println(context.getOrElse("Não foi possível salvar o Context do Watson."))
				println(idSessao.getOrElse("Não foi possível salvar o idSessao do Orquestrador."))
				session.set("texto_conversa", num_sucursal) })
			.pause(3)
			.exec(http("Mensagem 3")
				.put("/homolog/auto/corretor/chat/bot/api/v1/conversas")
				.headers(headers_mensagens)
				.body(body_conversa).asJSON
				.basicAuth(client_id, client_secret)
				//.check(jsonPath("$.context").saveAs("context"))
				.check(jsonPath("$.context").ofType[String].transform(mapToJson).saveAs("context"))
				.check(jsonPath("$.idSessao").saveAs("idSessao")))
			//.exec(s => {println(s.attributes("context")); s})
			.pause(2)
			.exec(session => {
				val context = session.get("context").asOption[String]
				val idSessao = session.get("idSessao").asOption[String]
				println(context.getOrElse("Não foi possível salvar o Context do Watson."))
				println(idSessao.getOrElse("Não foi possível salvar o idSessao do Orquestrador."))
				session.set("texto_conversa", "sim") })
			.pause(3)
			.exec(http("Mensagem 4")
				.put("/homolog/auto/corretor/chat/bot/api/v1/conversas")
				.headers(headers_mensagens)
				.body(body_conversa).asJSON
				.basicAuth(client_id, client_secret)
				//.check(jsonPath("$.context").saveAs("context"))
				.check(jsonPath("$.context").ofType[String].transform(mapToJson).saveAs("context"))
				.check(jsonPath("$.idSessao").saveAs("idSessao")))
			//.exec(s => {println(s.attributes("context")); s})
			.pause(2)
			.exec(session => {
				val context = session.get("context").asOption[String]
				val idSessao = session.get("idSessao").asOption[String]
				println(context.getOrElse("Não foi possível salvar o Context do Watson."))
				println(idSessao.getOrElse("Não foi possível salvar o idSessao do Orquestrador."))
				session.set("texto_conversa", "obrigado") })
			.pause(5)
			.exec(http("Mensagem Final")
				.put("/homolog/auto/corretor/chat/bot/api/v1/conversas")
				.headers(headers_mensagens)
				.body(body_conversa).asJSON
				.basicAuth(client_id, client_secret)
				//.check(jsonPath("$.context").saveAs("context"))
				.check(jsonPath("$.context").ofType[String].transform(mapToJson).saveAs("context"))
				.check(jsonPath("$.idSessao").saveAs("idSessao")))
			//.exec(s => {println(s.attributes("context")); s})
			.pause(1)
			.exec(session => {
				val context = session.get("context").asOption[String]
				val idSessao = session.get("idSessao").asOption[String]
				println(context.getOrElse("Não foi possível salvar o Context do Watson."))
				println(idSessao.getOrElse("Não foi possível salvar o idSessao do Orquestrador."))
				session })


// *************************************** Consulta 2a via carteirinha apolice ***************************************************

		val consulta2ViaApolice = exec(http("Get Access Token Corretor")
				.post("/homolog/auto/corretor/chat/bot/api/v1/oauth/access-token")
				.headers(headers_accesstoken)
				.basicAuth(client_id, client_secret)
				.check(jsonPath("$.access_token").saveAs("access_token")))
			.pause(2)
			.exec(session => {
				val access_token = session.get("access_token").asOption[String]
				println(access_token.getOrElse("Não foi possível obter o AccessToken da Sensedia"))
				session })
			.pause(1)
			.exec(http("Iniciar Conversa")
				.post("/homolog/auto/corretor/chat/bot/api/v1/conversas")
				.headers(headers_iniciar)
				.body(body_iniciar).asJSON
				.basicAuth(client_id, client_secret)
				//.check(jsonPath("$.context").saveAs("context"))
				.check(jsonPath("$.context").ofType[String].transform(mapToJson).saveAs("context"))
				.check(jsonPath("$.idSessao").saveAs("idSessao")))
			//.exec(s => {println(s.attributes("context")); s})
			.pause(2)
			.feed(viaApolice)
			.exec(session => {
				val context = session.get("context").asOption[String]
				val idSessao = session.get("idSessao").asOption[String]
				val num_apolice = session.get("num_apolice").as[String]
				println(context.getOrElse("Não foi possível salvar o Context do Watson."))
				println(idSessao.getOrElse("Não foi possível salvar o idSessao do Orquestrador."))
				session.set("texto_conversa", "Gostaria da segunda via de carteirinha da apólice " + num_apolice) })
			.pause(5)
			.exec(http("Mensagem 1")
				.put("/homolog/auto/corretor/chat/bot/api/v1/conversas")
				.headers(headers_mensagens)
				.body(body_conversa).asJSON
				.basicAuth(client_id, client_secret)
				//.check(jsonPath("$.context").saveAs("context"))
				.check(jsonPath("$.context").ofType[String].transform(mapToJson).saveAs("context"))
				.check(jsonPath("$.idSessao").saveAs("idSessao")))
			//.exec(s => {println(s.attributes("context")); s})
			.pause(1)
			.exec(session => {
				val context = session.get("context").asOption[String]
				val idSessao = session.get("idSessao").asOption[String]
				val cpf_apolice = session.get("cpf_apolice").as[String]
				println(context.getOrElse("Não foi possível salvar o Context do Watson."))
				println(idSessao.getOrElse("Não foi possível salvar o idSessao do Orquestrador."))
				session.set("texto_conversa", cpf_apolice) })
			.pause(5)
			.exec(http("Mensagem 2")
				.put("/homolog/auto/corretor/chat/bot/api/v1/conversas")
				.headers(headers_mensagens)
				.body(body_conversa).asJSON
				.basicAuth(client_id, client_secret)
				//.check(jsonPath("$.context").saveAs("context"))
				.check(jsonPath("$.context").ofType[String].transform(mapToJson).saveAs("context"))
				.check(jsonPath("$.idSessao").saveAs("idSessao")))
			//.exec(s => {println(s.attributes("context")); s})
			.pause(1)
			.exec(session => {
				val context = session.get("context").asOption[String]
				val idSessao = session.get("idSessao").asOption[String]
				val nasc_apolice = session.get("nasc_apolice").as[String]
				println(context.getOrElse("Não foi possível salvar o Context do Watson."))
				println(idSessao.getOrElse("Não foi possível salvar o idSessao do Orquestrador."))
				session.set("texto_conversa", nasc_apolice) })
			.pause(3)
			.exec(http("Mensagem 3")
				.put("/homolog/auto/corretor/chat/bot/api/v1/conversas")
				.headers(headers_mensagens)
				.body(body_conversa).asJSON
				.basicAuth(client_id, client_secret)
				//.check(jsonPath("$.context").saveAs("context"))
				.check(jsonPath("$.context").ofType[String].transform(mapToJson).saveAs("context"))
				.check(jsonPath("$.idSessao").saveAs("idSessao")))
			//.exec(s => {println(s.attributes("context")); s})
			.pause(2)
			.exec(session => {
				val context = session.get("context").asOption[String]
				val idSessao = session.get("idSessao").asOption[String]
				println(context.getOrElse("Não foi possível salvar o Context do Watson."))
				println(idSessao.getOrElse("Não foi possível salvar o idSessao do Orquestrador."))
				session.set("texto_conversa", "sim") })
			.pause(3)
			.exec(http("Mensagem 4")
				.put("/homolog/auto/corretor/chat/bot/api/v1/conversas")
				.headers(headers_mensagens)
				.body(body_conversa).asJSON
				.basicAuth(client_id, client_secret)
				//.check(jsonPath("$.context").saveAs("context"))
				.check(jsonPath("$.context").ofType[String].transform(mapToJson).saveAs("context"))
				.check(jsonPath("$.idSessao").saveAs("idSessao")))
			//.exec(s => {println(s.attributes("context")); s})
			.pause(2)
			.exec(session => {
				val context = session.get("context").asOption[String]
				val idSessao = session.get("idSessao").asOption[String]
				println(context.getOrElse("Não foi possível salvar o Context do Watson."))
				println(idSessao.getOrElse("Não foi possível salvar o idSessao do Orquestrador."))
				session.set("texto_conversa", "obrigado") })
			.pause(5)
			.exec(http("Mensagem Final")
				.put("/homolog/auto/corretor/chat/bot/api/v1/conversas")
				.headers(headers_mensagens)
				.body(body_conversa).asJSON
				.basicAuth(client_id, client_secret)
				//.check(jsonPath("$.context").saveAs("context"))
				.check(jsonPath("$.context").ofType[String].transform(mapToJson).saveAs("context"))
				.check(jsonPath("$.idSessao").saveAs("idSessao")))
			//.exec(s => {println(s.attributes("context")); s})
			.pause(1)
			.exec(session => {
				val context = session.get("context").asOption[String]
				val idSessao = session.get("idSessao").asOption[String]
				println(context.getOrElse("Não foi possível salvar o Context do Watson."))
				println(idSessao.getOrElse("Não foi possível salvar o idSessao do Orquestrador."))
				session })

// *************************************** 2a via boleto ***************************************************

		val consulta2ViaBoleto = exec(http("Get Access Token Corretor")
				.post("/homolog/auto/corretor/chat/bot/api/v1/oauth/access-token")
				.headers(headers_accesstoken)
				.basicAuth(client_id, client_secret)
				.check(jsonPath("$.access_token").saveAs("access_token")))
			.pause(2)
			.exec(session => {
				val access_token = session.get("access_token").asOption[String]
				println(access_token.getOrElse("Não foi possível obter o AccessToken da Sensedia"))
				session })
			.pause(1)
			.exec(http("Iniciar Conversa")
				.post("/homolog/auto/corretor/chat/bot/api/v1/conversas")
				.headers(headers_iniciar)
				.body(body_iniciar).asJSON
				.basicAuth(client_id, client_secret)
				//.check(jsonPath("$.context").saveAs("context"))
				.check(jsonPath("$.context").ofType[String].transform(mapToJson).saveAs("context"))
				.check(jsonPath("$.idSessao").saveAs("idSessao")))
			//.exec(s => {println(s.attributes("context")); s})
			.pause(2)
			.exec(session => {
				val context = session.get("context").asOption[String]
				val idSessao = session.get("idSessao").asOption[String]
				println(context.getOrElse("Não foi possível salvar o Context do Watson."))
				println(idSessao.getOrElse("Não foi possível salvar o idSessao do Orquestrador."))
				session.set("texto_conversa", "Boa tarde, gostaria de uma segunda um boleto") })
			.pause(5)
			.exec(http("Mensagem 1")
				.put("/homolog/auto/corretor/chat/bot/api/v1/conversas")
				.headers(headers_mensagens)
				.body(body_conversa).asJSON
				.basicAuth(client_id, client_secret)
				//.check(jsonPath("$.context").saveAs("context"))
				.check(jsonPath("$.context").ofType[String].transform(mapToJson).saveAs("context"))
				.check(jsonPath("$.idSessao").saveAs("idSessao")))
			//.exec(s => {println(s.attributes("context")); s})
			.pause(1)
			.feed(viaBoleto)
			.exec(session => {
				val context = session.get("context").asOption[String]
				val idSessao = session.get("idSessao").asOption[String]
				val boleto_2via_apolice = session.get("boleto_2via_apolice").as[String]
				println(context.getOrElse("Não foi possível salvar o Context do Watson."))
				println(idSessao.getOrElse("Não foi possível salvar o idSessao do Orquestrador."))
				session.set("texto_conversa", "Apolice " + boleto_2via_apolice) })
			.pause(5)
			.exec(http("Mensagem 2")
				.put("/homolog/auto/corretor/chat/bot/api/v1/conversas")
				.headers(headers_mensagens)
				.body(body_conversa).asJSON
				.basicAuth(client_id, client_secret)
				//.check(jsonPath("$.context").saveAs("context"))
				.check(jsonPath("$.context").ofType[String].transform(mapToJson).saveAs("context"))
				.check(jsonPath("$.idSessao").saveAs("idSessao")))
			//.exec(s => {println(s.attributes("context")); s})
			.pause(1)
			.exec(session => {
				val context = session.get("context").asOption[String]
				val idSessao = session.get("idSessao").asOption[String]
				val boleto_2via_cpf = session.get("boleto_2via_cpf").as[String]
				println(context.getOrElse("Não foi possível salvar o Context do Watson."))
				println(idSessao.getOrElse("Não foi possível salvar o idSessao do Orquestrador."))
				session.set("texto_conversa", boleto_2via_cpf) })
			.pause(3)
			.exec(http("Mensagem 3")
				.put("/homolog/auto/corretor/chat/bot/api/v1/conversas")
				.headers(headers_mensagens)
				.body(body_conversa).asJSON
				.basicAuth(client_id, client_secret)
				//.check(jsonPath("$.context").saveAs("context"))
				.check(jsonPath("$.context").ofType[String].transform(mapToJson).saveAs("context"))
				.check(jsonPath("$.idSessao").saveAs("idSessao")))
			//.exec(s => {println(s.attributes("context")); s})
			.pause(2)
			.exec(session => {
				val context = session.get("context").asOption[String]
				val idSessao = session.get("idSessao").asOption[String]
				println(context.getOrElse("Não foi possível salvar o Context do Watson."))
				println(idSessao.getOrElse("Não foi possível salvar o idSessao do Orquestrador."))
				session.set("texto_conversa", "sim") })
			.pause(3)
			.exec(http("Mensagem 4")
				.put("/homolog/auto/corretor/chat/bot/api/v1/conversas")
				.headers(headers_mensagens)
				.body(body_conversa).asJSON
				.basicAuth(client_id, client_secret)
				//.check(jsonPath("$.context").saveAs("context"))
				.check(jsonPath("$.context").ofType[String].transform(mapToJson).saveAs("context"))
				.check(jsonPath("$.idSessao").saveAs("idSessao")))
			//.exec(s => {println(s.attributes("context")); s})
			.pause(2)
			.exec(session => {
				val context = session.get("context").asOption[String]
				val idSessao = session.get("idSessao").asOption[String]
				println(context.getOrElse("Não foi possível salvar o Context do Watson."))
				println(idSessao.getOrElse("Não foi possível salvar o idSessao do Orquestrador."))
				session.set("texto_conversa", "obrigado") })
			.pause(5)
			.exec(http("Mensagem Final")
				.put("/homolog/auto/corretor/chat/bot/api/v1/conversas")
				.headers(headers_mensagens)
				.body(body_conversa).asJSON
				.basicAuth(client_id, client_secret)
				//.check(jsonPath("$.context").saveAs("context"))
				.check(jsonPath("$.context").ofType[String].transform(mapToJson).saveAs("context"))
				.check(jsonPath("$.idSessao").saveAs("idSessao")))
			//.exec(s => {println(s.attributes("context")); s})
			.pause(1)
			.exec(session => {
				val context = session.get("context").asOption[String]
				val idSessao = session.get("idSessao").asOption[String]
				println(context.getOrElse("Não foi possível salvar o Context do Watson."))
				println(idSessao.getOrElse("Não foi possível salvar o idSessao do Orquestrador."))
				session })

	}

	val scenario_status_proposta = scenario("Consulta Status Proposta").exec(Chat.consultaStatusProposta)
	val scenario_status_apolice = scenario("Consulta Status Apolice").exec(Chat.consultaStatusApolice)
	val scenario_2via_apolice = scenario("Consulta 2via Apolice").exec(Chat.consulta2ViaApolice)
	val scenario_2via_boleto = scenario("Consulta 2via Boleto").exec(Chat.consulta2ViaBoleto)

	setUp(
		scenario_status_proposta.inject(rampUsers(num_users) over (tempo_users seconds)),
		scenario_status_apolice.inject(rampUsers(num_users) over (tempo_users seconds)),
		scenario_2via_apolice.inject(rampUsers(num_users) over (tempo_users seconds)),
		scenario_2via_boleto.inject(rampUsers(num_users) over (tempo_users seconds))
	).protocols(Chat.httpProtocol)
}