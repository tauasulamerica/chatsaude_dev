
# DetailLayout

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**collapsed** | **Boolean** |  |  [optional]
**columns** | **Integer** | Número de Colunas |  [optional]
**heading** | **String** |  |  [optional]
**layoutRows** | [**List&lt;LayoutRow&gt;**](LayoutRow.md) |  |  [optional]
**layoutSectionId** | **String** |  |  [optional]
**parentLayoutId** | **String** |  |  [optional]
**rows** | **Integer** | Colunas |  [optional]
**tabOrder** | **String** |  |  [optional]
**useCollapsibleSection** | **Boolean** |  |  [optional]
**useHeading** | **Boolean** |  |  [optional]



