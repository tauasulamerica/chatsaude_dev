
# DetelhesPreChat

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**label** | **String** | A etiqueta personalizada para o detalhe. |  [optional]
**value** | **String** | O valor personalizado para o detalhe. |  [optional]
**transcriptFields** | **List&lt;String&gt;** | Os nomes dos campos para os quais serão salvos os detalhes do cliente na transcrição do bate-papo. |  [optional]
**displayToAgent** | **Boolean** | Especifica se o detalhe personalizado deve ser exibido no agente (true) ou não (false). |  [optional]



