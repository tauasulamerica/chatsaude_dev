
# Sessao

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **String** | A chave para a nova sessão. |  [optional]
**id** | **String** | O Identificador para a nova sessão. |  [optional]
**affinityToken** | **String** | O token de afinidade para a sessão |  [optional]
**clientPollTimeout** | **Integer** | O número de segundos antes de você deve fazer uma solicitação de Mensagens antes que o ciclo de pesquisa longa de Mensagens expire e é encerrado. |  [optional]



