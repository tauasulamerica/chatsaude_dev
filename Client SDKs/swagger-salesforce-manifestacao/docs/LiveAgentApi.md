# LiveAgentApi

All URIs are relative to *http://apisulamerica.sensedia.com/auto/corretor/chat/bot/salesforce/api/v1/dev*

Method | HTTP request | Description
------------- | ------------- | -------------
[**agentesDisponiveisGet**](LiveAgentApi.md#agentesDisponiveisGet) | **GET** /agentes/disponiveis | 
[**conversasDelete**](LiveAgentApi.md#conversasDelete) | **DELETE** /conversas | 
[**conversasPost**](LiveAgentApi.md#conversasPost) | **POST** /conversas | 
[**conversasPut**](LiveAgentApi.md#conversasPut) | **PUT** /conversas | 
[**mensagensGet**](LiveAgentApi.md#mensagensGet) | **GET** /mensagens | 
[**sessaoChavesessaoDelete**](LiveAgentApi.md#sessaoChavesessaoDelete) | **DELETE** /sessao/{chavesessao} | 
[**sessaoGet**](LiveAgentApi.md#sessaoGet) | **GET** /sessao | 


<a name="agentesDisponiveisGet"></a>
# **agentesDisponiveisGet**
> Object agentesDisponiveisGet(clientId, accessToken, X_LIVEAGENT_API_VERSION, xGatewayProcessId, xGatewayRequestId, identificadorOrganizacao, identificadorImplantacao, identificadoresDisponibilidade)



Indica se um botão de chat está disponível para receber novas solicitações de bate-papo.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.LiveAgentApi;


LiveAgentApi apiInstance = new LiveAgentApi();
String clientId = "clientId_example"; // String | Identificador do Cliente utilizado na autenticação.
String accessToken = "accessToken_example"; // String | Token de acesso utilizado na autenticação.
Integer X_LIVEAGENT_API_VERSION = 56; // Integer | A versão da API do Salesforce para a solicitação.
String xGatewayProcessId = "xGatewayProcessId_example"; // String | Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão.
String xGatewayRequestId = "xGatewayRequestId_example"; // String | Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador.
String identificadorOrganizacao = "identificadorOrganizacao_example"; // String | O Identificador da organização do Salesforce associada à implantação do Live Agent.
String identificadorImplantacao = "identificadorImplantacao_example"; // String | O Identificador de 15 dígitos da implantação do Live Agent da qual a solicitação de cgat foi iniciada.
String identificadoresDisponibilidade = "identificadoresDisponibilidade_example"; // String | Uma matriz de IDs de objeto para os quais é possível verificar a disponibilidade.
try {
    Object result = apiInstance.agentesDisponiveisGet(clientId, accessToken, X_LIVEAGENT_API_VERSION, xGatewayProcessId, xGatewayRequestId, identificadorOrganizacao, identificadorImplantacao, identificadoresDisponibilidade);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LiveAgentApi#agentesDisponiveisGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**| Identificador do Cliente utilizado na autenticação. |
 **accessToken** | **String**| Token de acesso utilizado na autenticação. |
 **X_LIVEAGENT_API_VERSION** | **Integer**| A versão da API do Salesforce para a solicitação. |
 **xGatewayProcessId** | **String**| Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão. |
 **xGatewayRequestId** | **String**| Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador. |
 **identificadorOrganizacao** | **String**| O Identificador da organização do Salesforce associada à implantação do Live Agent. |
 **identificadorImplantacao** | **String**| O Identificador de 15 dígitos da implantação do Live Agent da qual a solicitação de cgat foi iniciada. |
 **identificadoresDisponibilidade** | **String**| Uma matriz de IDs de objeto para os quais é possível verificar a disponibilidade. |

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="conversasDelete"></a>
# **conversasDelete**
> conversasDelete(clientId, accessToken, X_LIVEAGENT_SESSION_KEY, X_LIVEAGENT_API_VERSION, X_LIVEAGENT_AFFINITY, X_LIVEAGENT_SEQUENCE, finalizaConversa, xGatewayProcessId, xGatewayRequestId)



Finaliza a Conversa

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.LiveAgentApi;


LiveAgentApi apiInstance = new LiveAgentApi();
String clientId = "clientId_example"; // String | Identificador do Cliente utilizado na autenticação.
String accessToken = "accessToken_example"; // String | Token de acesso utilizado na autenticação.
String X_LIVEAGENT_SESSION_KEY = "X_LIVEAGENT_SESSION_KEY_example"; // String | O ID exclusivo associado à sua sessão do LIve Agent.
Integer X_LIVEAGENT_API_VERSION = 56; // Integer | A versão da API do Salesforce para a solicitação.
String X_LIVEAGENT_AFFINITY = "X_LIVEAGENT_AFFINITY_example"; // String | O ID gerado pelo sistema usado para identificar a sessão do Live Agent nos servidores do Live Agent
String X_LIVEAGENT_SEQUENCE = "X_LIVEAGENT_SEQUENCE_example"; // String | A sequência de mensagens enviadas ao servidor do Live Agent para ajudar o servidor do Live Agent a evitar o processamento de mensagens duplicadas.
FinalConversa finalizaConversa = new FinalConversa(); // FinalConversa | body da conversa.
String xGatewayProcessId = "xGatewayProcessId_example"; // String | Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão.
String xGatewayRequestId = "xGatewayRequestId_example"; // String | Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador.
try {
    apiInstance.conversasDelete(clientId, accessToken, X_LIVEAGENT_SESSION_KEY, X_LIVEAGENT_API_VERSION, X_LIVEAGENT_AFFINITY, X_LIVEAGENT_SEQUENCE, finalizaConversa, xGatewayProcessId, xGatewayRequestId);
} catch (ApiException e) {
    System.err.println("Exception when calling LiveAgentApi#conversasDelete");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**| Identificador do Cliente utilizado na autenticação. |
 **accessToken** | **String**| Token de acesso utilizado na autenticação. |
 **X_LIVEAGENT_SESSION_KEY** | **String**| O ID exclusivo associado à sua sessão do LIve Agent. |
 **X_LIVEAGENT_API_VERSION** | **Integer**| A versão da API do Salesforce para a solicitação. |
 **X_LIVEAGENT_AFFINITY** | **String**| O ID gerado pelo sistema usado para identificar a sessão do Live Agent nos servidores do Live Agent |
 **X_LIVEAGENT_SEQUENCE** | **String**| A sequência de mensagens enviadas ao servidor do Live Agent para ajudar o servidor do Live Agent a evitar o processamento de mensagens duplicadas. |
 **finalizaConversa** | [**FinalConversa**](FinalConversa.md)| body da conversa. |
 **xGatewayProcessId** | **String**| Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão. |
 **xGatewayRequestId** | **String**| Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador. |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="conversasPost"></a>
# **conversasPost**
> conversasPost(clientId, accessToken, X_LIVEAGENT_SESSION_KEY, X_LIVEAGENT_API_VERSION, X_LIVEAGENT_AFFINITY, X_LIVEAGENT_SEQUENCE, xGatewayProcessId, xGatewayRequestId, iniciarConversa)



Inicia uma nova sessão de visitante de bate-papo

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.LiveAgentApi;


LiveAgentApi apiInstance = new LiveAgentApi();
String clientId = "clientId_example"; // String | Identificador do Cliente utilizado na autenticação.
String accessToken = "accessToken_example"; // String | Token de acesso utilizado na autenticação.
String X_LIVEAGENT_SESSION_KEY = "X_LIVEAGENT_SESSION_KEY_example"; // String | O ID exclusivo associado à sua sessão do LIve Agent.
Integer X_LIVEAGENT_API_VERSION = 56; // Integer | A versão da API do Salesforce para a solicitação.
String X_LIVEAGENT_AFFINITY = "X_LIVEAGENT_AFFINITY_example"; // String | O ID gerado pelo sistema usado para identificar a sessão do Live Agent nos servidores do Live Agent
String X_LIVEAGENT_SEQUENCE = "X_LIVEAGENT_SEQUENCE_example"; // String | A sequência de mensagens enviadas ao servidor do Live Agent para ajudar o servidor do Live Agent a evitar o processamento de mensagens duplicadas.
String xGatewayProcessId = "xGatewayProcessId_example"; // String | Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão.
String xGatewayRequestId = "xGatewayRequestId_example"; // String | Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador.
IniciarConversa iniciarConversa = new IniciarConversa(); // IniciarConversa | Body para iniciar a conversa
try {
    apiInstance.conversasPost(clientId, accessToken, X_LIVEAGENT_SESSION_KEY, X_LIVEAGENT_API_VERSION, X_LIVEAGENT_AFFINITY, X_LIVEAGENT_SEQUENCE, xGatewayProcessId, xGatewayRequestId, iniciarConversa);
} catch (ApiException e) {
    System.err.println("Exception when calling LiveAgentApi#conversasPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**| Identificador do Cliente utilizado na autenticação. |
 **accessToken** | **String**| Token de acesso utilizado na autenticação. |
 **X_LIVEAGENT_SESSION_KEY** | **String**| O ID exclusivo associado à sua sessão do LIve Agent. |
 **X_LIVEAGENT_API_VERSION** | **Integer**| A versão da API do Salesforce para a solicitação. |
 **X_LIVEAGENT_AFFINITY** | **String**| O ID gerado pelo sistema usado para identificar a sessão do Live Agent nos servidores do Live Agent |
 **X_LIVEAGENT_SEQUENCE** | **String**| A sequência de mensagens enviadas ao servidor do Live Agent para ajudar o servidor do Live Agent a evitar o processamento de mensagens duplicadas. |
 **xGatewayProcessId** | **String**| Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão. |
 **xGatewayRequestId** | **String**| Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador. |
 **iniciarConversa** | [**IniciarConversa**](IniciarConversa.md)| Body para iniciar a conversa |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="conversasPut"></a>
# **conversasPut**
> conversasPut(clientId, accessToken, X_LIVEAGENT_SESSION_KEY, X_LIVEAGENT_API_VERSION, X_LIVEAGENT_AFFINITY, X_LIVEAGENT_SEQUENCE, xGatewayProcessId, xGatewayRequestId, conversa)



Realiza o chat

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.LiveAgentApi;


LiveAgentApi apiInstance = new LiveAgentApi();
String clientId = "clientId_example"; // String | Identificador do Cliente utilizado na autenticação.
String accessToken = "accessToken_example"; // String | Token de acesso utilizado na autenticação.
String X_LIVEAGENT_SESSION_KEY = "X_LIVEAGENT_SESSION_KEY_example"; // String | O ID exclusivo associado à sua sessão do LIve Agent.
Integer X_LIVEAGENT_API_VERSION = 56; // Integer | A versão da API do Salesforce para a solicitação.
String X_LIVEAGENT_AFFINITY = "X_LIVEAGENT_AFFINITY_example"; // String | O ID gerado pelo sistema usado para identificar a sessão do Live Agent nos servidores do Live Agent
String X_LIVEAGENT_SEQUENCE = "X_LIVEAGENT_SEQUENCE_example"; // String | A sequência de mensagens enviadas ao servidor do Live Agent para ajudar o servidor do Live Agent a evitar o processamento de mensagens duplicadas.
String xGatewayProcessId = "xGatewayProcessId_example"; // String | Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão.
String xGatewayRequestId = "xGatewayRequestId_example"; // String | Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador.
Conversa conversa = new Conversa(); // Conversa | body da conversa.
try {
    apiInstance.conversasPut(clientId, accessToken, X_LIVEAGENT_SESSION_KEY, X_LIVEAGENT_API_VERSION, X_LIVEAGENT_AFFINITY, X_LIVEAGENT_SEQUENCE, xGatewayProcessId, xGatewayRequestId, conversa);
} catch (ApiException e) {
    System.err.println("Exception when calling LiveAgentApi#conversasPut");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**| Identificador do Cliente utilizado na autenticação. |
 **accessToken** | **String**| Token de acesso utilizado na autenticação. |
 **X_LIVEAGENT_SESSION_KEY** | **String**| O ID exclusivo associado à sua sessão do LIve Agent. |
 **X_LIVEAGENT_API_VERSION** | **Integer**| A versão da API do Salesforce para a solicitação. |
 **X_LIVEAGENT_AFFINITY** | **String**| O ID gerado pelo sistema usado para identificar a sessão do Live Agent nos servidores do Live Agent |
 **X_LIVEAGENT_SEQUENCE** | **String**| A sequência de mensagens enviadas ao servidor do Live Agent para ajudar o servidor do Live Agent a evitar o processamento de mensagens duplicadas. |
 **xGatewayProcessId** | **String**| Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão. |
 **xGatewayRequestId** | **String**| Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador. |
 **conversa** | [**Conversa**](Conversa.md)| body da conversa. |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="mensagensGet"></a>
# **mensagensGet**
> Object mensagensGet(clientId, accessToken, X_LIVEAGENT_SESSION_KEY, X_LIVEAGENT_API_VERSION, X_LIVEAGENT_AFFINITY, xGatewayProcessId, xGatewayRequestId)



Retorna todas as mensagens enviadas entre agentes e visitantes de bate-papo durante uma sessão de bate-papo.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.LiveAgentApi;


LiveAgentApi apiInstance = new LiveAgentApi();
String clientId = "clientId_example"; // String | Identificador do Cliente utilizado na autenticação.
String accessToken = "accessToken_example"; // String | Token de acesso utilizado na autenticação.
String X_LIVEAGENT_SESSION_KEY = "X_LIVEAGENT_SESSION_KEY_example"; // String | O ID exclusivo associado à sua sessão do LIve Agent.
Integer X_LIVEAGENT_API_VERSION = 56; // Integer | A versão da API do Salesforce para a solicitação.
String X_LIVEAGENT_AFFINITY = "X_LIVEAGENT_AFFINITY_example"; // String | O ID gerado pelo sistema usado para identificar a sessão do Live Agent nos servidores do Live Agent
String xGatewayProcessId = "xGatewayProcessId_example"; // String | Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão.
String xGatewayRequestId = "xGatewayRequestId_example"; // String | Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador.
try {
    Object result = apiInstance.mensagensGet(clientId, accessToken, X_LIVEAGENT_SESSION_KEY, X_LIVEAGENT_API_VERSION, X_LIVEAGENT_AFFINITY, xGatewayProcessId, xGatewayRequestId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LiveAgentApi#mensagensGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**| Identificador do Cliente utilizado na autenticação. |
 **accessToken** | **String**| Token de acesso utilizado na autenticação. |
 **X_LIVEAGENT_SESSION_KEY** | **String**| O ID exclusivo associado à sua sessão do LIve Agent. |
 **X_LIVEAGENT_API_VERSION** | **Integer**| A versão da API do Salesforce para a solicitação. |
 **X_LIVEAGENT_AFFINITY** | **String**| O ID gerado pelo sistema usado para identificar a sessão do Live Agent nos servidores do Live Agent |
 **xGatewayProcessId** | **String**| Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão. |
 **xGatewayRequestId** | **String**| Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador. |

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="sessaoChavesessaoDelete"></a>
# **sessaoChavesessaoDelete**
> sessaoChavesessaoDelete(clientId, accessToken, chavesessao, X_LIVEAGENT_API_VERSION, X_LIVEAGENT_AFFINITY, xGatewayProcessId, xGatewayRequestId)



Finaliza uma sessão

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.LiveAgentApi;


LiveAgentApi apiInstance = new LiveAgentApi();
String clientId = "clientId_example"; // String | Identificador do Cliente utilizado na autenticação.
String accessToken = "accessToken_example"; // String | Token de acesso utilizado na autenticação.
String chavesessao = "chavesessao_example"; // String | Chave da sessão
Integer X_LIVEAGENT_API_VERSION = 56; // Integer | Versão da APi do Live Agent
String X_LIVEAGENT_AFFINITY = "X_LIVEAGENT_AFFINITY_example"; // String | Affinity do Live Agent
String xGatewayProcessId = "xGatewayProcessId_example"; // String | Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão.
String xGatewayRequestId = "xGatewayRequestId_example"; // String | Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador.
try {
    apiInstance.sessaoChavesessaoDelete(clientId, accessToken, chavesessao, X_LIVEAGENT_API_VERSION, X_LIVEAGENT_AFFINITY, xGatewayProcessId, xGatewayRequestId);
} catch (ApiException e) {
    System.err.println("Exception when calling LiveAgentApi#sessaoChavesessaoDelete");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**| Identificador do Cliente utilizado na autenticação. |
 **accessToken** | **String**| Token de acesso utilizado na autenticação. |
 **chavesessao** | **String**| Chave da sessão |
 **X_LIVEAGENT_API_VERSION** | **Integer**| Versão da APi do Live Agent |
 **X_LIVEAGENT_AFFINITY** | **String**| Affinity do Live Agent |
 **xGatewayProcessId** | **String**| Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão. |
 **xGatewayRequestId** | **String**| Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador. |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="sessaoGet"></a>
# **sessaoGet**
> Sessao sessaoGet(clientId, accessToken, X_LIVEAGENT_API_VERSION, X_LIVEAGENT_AFFINITY, xGatewayProcessId, xGatewayRequestId)



Inicia uma sessão.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.LiveAgentApi;


LiveAgentApi apiInstance = new LiveAgentApi();
String clientId = "clientId_example"; // String | Identificador do Cliente utilizado na autenticação.
String accessToken = "accessToken_example"; // String | Token de acesso utilizado na autenticação.
Integer X_LIVEAGENT_API_VERSION = 56; // Integer | A versão da API do Salesforce para a solicitação.
String X_LIVEAGENT_AFFINITY = "X_LIVEAGENT_AFFINITY_example"; // String | O ID gerado pelo sistema usado para identificar a sessão do Live Agent nos servidores do Live Agent
String xGatewayProcessId = "xGatewayProcessId_example"; // String | Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão.
String xGatewayRequestId = "xGatewayRequestId_example"; // String | Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador.
try {
    Sessao result = apiInstance.sessaoGet(clientId, accessToken, X_LIVEAGENT_API_VERSION, X_LIVEAGENT_AFFINITY, xGatewayProcessId, xGatewayRequestId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LiveAgentApi#sessaoGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**| Identificador do Cliente utilizado na autenticação. |
 **accessToken** | **String**| Token de acesso utilizado na autenticação. |
 **X_LIVEAGENT_API_VERSION** | **Integer**| A versão da API do Salesforce para a solicitação. |
 **X_LIVEAGENT_AFFINITY** | **String**| O ID gerado pelo sistema usado para identificar a sessão do Live Agent nos servidores do Live Agent |
 **xGatewayProcessId** | **String**| Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão. |
 **xGatewayRequestId** | **String**| Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador. |

### Return type

[**Sessao**](Sessao.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

