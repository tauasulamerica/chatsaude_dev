# OAuthApi

All URIs are relative to *http://apisulamerica.sensedia.com/auto/corretor/chat/bot/salesforce/api/v1/dev*

Method | HTTP request | Description
------------- | ------------- | -------------
[**oauthAccessTokenPost**](OAuthApi.md#oauthAccessTokenPost) | **POST** /oauth/access-token | 


<a name="oauthAccessTokenPost"></a>
# **oauthAccessTokenPost**
> AccessToken oauthAccessTokenPost(authorization)



Gerar o access-token.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.OAuthApi;


OAuthApi apiInstance = new OAuthApi();
String authorization = "authorization_example"; // String | Código de Autorização.
try {
    AccessToken result = apiInstance.oauthAccessTokenPost(authorization);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OAuthApi#oauthAccessTokenPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Código de Autorização. |

### Return type

[**AccessToken**](AccessToken.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

