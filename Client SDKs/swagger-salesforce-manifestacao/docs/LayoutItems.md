
# LayoutItems

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**editableForNew** | **Boolean** |  |  [optional]
**editableForUpdate** | **Boolean** |  |  [optional]
**label** | **String** | Rótulo |  [optional]
**layoutComponents** | [**List&lt;LayoutComponents&gt;**](LayoutComponents.md) |  |  [optional]
**placeholder** | **Boolean** |  |  [optional]
**required** | **Boolean** |  |  [optional]



