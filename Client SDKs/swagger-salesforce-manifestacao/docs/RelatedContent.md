
# RelatedContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**relatedContentItems** | [**List&lt;RelatedContentItems&gt;**](RelatedContentItems.md) |  |  [optional]



