
# RelatedLists

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accessLevelRequiredForCreate** | **String** |  |  [optional]
**buttons** | **List&lt;String&gt;** |  |  [optional]
**columns** | [**List&lt;Columns&gt;**](Columns.md) |  |  [optional]
**custom** | **Boolean** |  |  [optional]
**field** | **String** |  |  [optional]
**label** | **String** |  |  [optional]
**limitRows** | **Integer** |  |  [optional]
**name** | **String** |  |  [optional]
**sobject** | **String** |  |  [optional]
**sort** | [**List&lt;Sort&gt;**](Sort.md) |  |  [optional]



