
# MapaEntidade

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fieldName** | **String** | O nome do campo ao qual associar o detalhe. | 
**label** | **String** | A etiqueta personalizada para o detalhe. | 
**doFind** | **Boolean** | Especifica se o campo fieldName deve ser usado para executar uma pesquisa de registros correspondentes (true) ou não (false). | 
**isExactMatch** | **Boolean** | Especifica se deve apenas procurar por registros que têm campos que correspondem exatamente ao campo fieldName (true) ou não (false). | 
**doCreate** | **Boolean** | Especifica se é necessário criar um registro com base no campo fieldName se não existir (true) ou não (false). | 



