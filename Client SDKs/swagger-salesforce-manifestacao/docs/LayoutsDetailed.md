
# LayoutsDetailed

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**buttonLayoutSection** | [**ButtonLayout**](ButtonLayout.md) |  |  [optional]
**detailLayoutSections** | [**List&lt;DetailLayout&gt;**](DetailLayout.md) |  |  [optional]
**editLayoutSections** | [**List&lt;DetailLayout&gt;**](DetailLayout.md) |  |  [optional]
**feedView** | **String** |  |  [optional]
**highlightsPanelLayoutSection** | **String** |  |  [optional]
**id** | **String** |  |  [optional]
**multirowEditLayoutSections** | **List&lt;String&gt;** |  |  [optional]
**offlineLinks** | **List&lt;String&gt;** |  |  [optional]
**quickActionList** | [**QuickActionListItems**](QuickActionListItems.md) |  |  [optional]
**relatedContent** | [**RelatedContent**](RelatedContent.md) |  |  [optional]
**relatedLists** | [**List&lt;RelatedLists&gt;**](RelatedLists.md) |  |  [optional]



