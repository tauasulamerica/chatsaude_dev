
# PicklistValue

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**active** | **Boolean** |   |  [optional]
**defaultValue** | **Boolean** |   |  [optional]
**label** | **String** |   |  [optional]
**validFor** | **String** |   |  [optional]
**value** | **String** |   |  [optional]



