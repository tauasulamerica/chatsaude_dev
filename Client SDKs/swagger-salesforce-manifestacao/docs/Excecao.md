
# Excecao

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**identificador** | **String** | Identificador do horario de exceção |  [optional]
**data** | **String** | Data de Exceção |  [optional]
**horaInicio** | **String** | Hora de Inicio da Exceção |  [optional]
**horaFinal** | **String** | Hora Final da Exceção |  [optional]
**mensagem** | **String** | Mensagem da exceção |  [optional]



