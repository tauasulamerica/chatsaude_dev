
# IniciarConversa

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**organizationId** | **String** | O Identificador da organização Salesforce do visitante do chat. |  [optional]
**deploymentId** | **String** | O Identificador da implantação a partir do qual o chat se originou. |  [optional]
**buttonId** | **String** | A identificação do botão a partir do qual o chat se originou. |  [optional]
**sessionId** | **String** | Identificador da sessão do Live Agent do visitante ao chat. |  [optional]
**userAgent** | **String** | O agente do usuário do navegador do visitante do chat. |  [optional]
**language** | **String** | A língua falada do visitante do chat. |  [optional]
**screenResolution** | **String** | A resolução da tela de computador do visitante do chat. |  [optional]
**visitorName** | **String** | Nome do visitante do chat. |  [optional]
**receiveQueueUpdates** | **Boolean** | Indica se o visitante do chat receberá as atualizações da posição da fila (true) ou não (false). |  [optional]
**isPost** | **Boolean** | Indica se a solicitação de bate-papo foi feita corretamente através de uma solicitação POST (true) ou não (false). |  [optional]
**prechatDetails** | [**List&lt;DetelhesPreChat&gt;**](DetelhesPreChat.md) | Detalhes do Pré chat. |  [optional]
**prechatEntities** | [**List&lt;Entidade&gt;**](Entidade.md) |  |  [optional]



