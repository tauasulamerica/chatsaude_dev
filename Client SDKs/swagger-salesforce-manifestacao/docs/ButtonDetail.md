
# ButtonDetail

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**behavior** | **String** |  |  [optional]
**colors** | [**List&lt;Color&gt;**](Color.md) |  |  [optional]
**content** | **String** |  |  [optional]
**contentSource** | **String** |  |  [optional]
**custom** | **Boolean** |  |  [optional]
**encoding** | **String** |  |  [optional]
**height** | **Integer** | Altura |  [optional]
**icons** | [**List&lt;Icons&gt;**](Icons.md) |  |  [optional]
**label** | **String** | Rótulo |  [optional]
**menubar** | **Boolean** |  |  [optional]
**name** | **String** | Nome |  [optional]
**overridden** | **Boolean** |  |  [optional]
**resizeable** | **Boolean** |  |  [optional]
**scrollbars** | **Boolean** |  |  [optional]
**showsLocation** | **Boolean** |  |  [optional]
**showsStatus** | **Boolean** |  |  [optional]
**toolbar** | **Boolean** |  |  [optional]
**url** | **String** | URL |  [optional]
**width** | **Integer** | Largura |  [optional]
**windowPosition** | **String** |  |  [optional]



