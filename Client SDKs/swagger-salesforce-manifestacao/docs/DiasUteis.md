
# DiasUteis

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**identificador** | **String** |  |  [optional]
**nome** | **String** |  |  [optional]
**mensagemAtendimento** | **String** |  |  [optional]
**domingo** | [**DiasUteisDomingo**](DiasUteisDomingo.md) |  |  [optional]
**segunda** | [**DiasUteisSegunda**](DiasUteisSegunda.md) |  |  [optional]
**terca** | [**DiasUteisSegunda**](DiasUteisSegunda.md) |  |  [optional]
**quarta** | [**DiasUteisSegunda**](DiasUteisSegunda.md) |  |  [optional]
**quinta** | [**DiasUteisSegunda**](DiasUteisSegunda.md) |  |  [optional]
**sexta** | [**DiasUteisSegunda**](DiasUteisSegunda.md) |  |  [optional]
**sabado** | [**DiasUteisDomingo**](DiasUteisDomingo.md) |  |  [optional]



