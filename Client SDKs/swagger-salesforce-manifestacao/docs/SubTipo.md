
# SubTipo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**identificador** | **String** | Identificador do Sub Tipo. |  [optional]
**nome** | **String** | Nome do Sub Tipo. |  [optional]



