
# LayoutComponents

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**details** | [**Campos**](Campos.md) |  |  [optional]
**displayLines** | **Integer** |  |  [optional]
**tabOrder** | **Integer** |  |  [optional]
**type** | **String** | Tipo |  [optional]
**value** | **String** |  |  [optional]



