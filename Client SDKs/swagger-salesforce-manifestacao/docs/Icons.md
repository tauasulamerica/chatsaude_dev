
# Icons

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contentType** | **String** |  |  [optional]
**height** | **Integer** | Altura |  [optional]
**theme** | **String** | Tema |  [optional]
**url** | **String** | URL |  [optional]
**width** | **Integer** | Largura |  [optional]



