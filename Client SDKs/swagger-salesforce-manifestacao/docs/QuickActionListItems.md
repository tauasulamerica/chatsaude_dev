
# QuickActionListItems

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**quickActionListItems** | [**List&lt;QuickActionList&gt;**](QuickActionList.md) |  |  [optional]



