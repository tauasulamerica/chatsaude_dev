
# Relacoes

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cascadeDelete** | **Boolean** |  |  [optional]
**childSObject** | **String** |  |  [optional]
**deprecatedAndHidden** | **Boolean** |  |  [optional]
**field** | **String** | Campo |  [optional]
**junctionIdListNames** | **List&lt;String&gt;** |  |  [optional]
**junctionReferenceTo** | **List&lt;String&gt;** |  |  [optional]
**relationshipName** | **String** | Nome da Relação |  [optional]
**restrictedDelete** | **Boolean** | Flag Delete Restrito |  [optional]



