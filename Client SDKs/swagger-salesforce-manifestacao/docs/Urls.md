
# Urls

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rowTemplate** | **String** |  |  [optional]
**approvalLayouts** | **String** |  |  [optional]
**uiDetailTemplate** | **String** |  |  [optional]
**quickActions** | **String** |  |  [optional]
**layouts** | **String** |  |  [optional]
**compactLayouts** | **String** |  |  [optional]
**uiEditTemplate** | **String** |  |  [optional]
**caseArticleSuggestions** | **String** |  |  [optional]
**caseRowArticleSuggestions** | **String** |  |  [optional]
**defaultValues** | **String** |  |  [optional]
**listviews** | **String** |  |  [optional]
**describe** | **String** |  |  [optional]
**uiNewRecord** | **String** |  |  [optional]
**sobject** | **String** |  |  [optional]



