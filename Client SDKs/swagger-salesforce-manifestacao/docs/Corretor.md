
# Corretor

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**identificador** | **String** | Identificador do Corretor. |  [optional]
**nome** | **String** | Nome do Corretor. |  [optional]



