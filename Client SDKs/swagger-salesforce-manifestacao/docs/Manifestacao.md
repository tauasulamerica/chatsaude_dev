
# Manifestacao

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**recordTypeId** | **String** |  |  [optional]
**accountId** | **String** |  |  [optional]
**type** | **String** |  |  [optional]
**noMEDAPESSOAC** | **String** |  |  [optional]
**caTEGORIAC** | **String** |  |  [optional]
**tiPOC** | **String** |  |  [optional]
**suBTIPOPESQUISAC** | **String** |  |  [optional]
**feCHAREMTEMPODEATENDIMENTOC** | **String** |  |  [optional]
**status** | **String** |  |  [optional]
**subject** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**foRMADECONTATOC** | **String** |  |  [optional]
**origin** | **String** |  |  [optional]
**COMMENTS** | **String** |  |  [optional]
**retencaoParcialC** | **String** |  |  [optional]
**intencaoC** | **String** |  |  [optional]
**actionC** | **String** |  |  [optional]
**retornoDoServicoC** | **String** |  |  [optional]
**contadorDialogoC** | **String** |  |  [optional]



