
# Descrio

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**actionOverrides** | **List&lt;String&gt;** |  |  [optional]
**activateable** | **Boolean** | Flag Ativavel |  [optional]
**childRelationships** | [**List&lt;Relacoes&gt;**](Relacoes.md) |  |  [optional]
**compactLayoutable** | **Boolean** | Flag Layout Compactavel |  [optional]
**createable** | **Boolean** | Flag Criavel |  [optional]
**customSetting** | **Boolean** | Flag Configuração Customizada |  [optional]
**deletable** | **Boolean** | Flag Apagavel |  [optional]
**deprecatedAndHidden** | **Boolean** | Flag Obsoleto e Escondido |  [optional]
**feedEnabled** | **Boolean** | Flag Alimentação Ativada |  [optional]
**fields** | [**List&lt;Campos&gt;**](Campos.md) |  |  [optional]
**hasSubtypes** | **Boolean** | Flag Existem Subtipos |  [optional]
**keyPrefix** | **String** | Prefixo da Chave |  [optional]
**label** | **String** | Rótulo |  [optional]
**labelPlural** | **String** | Plural do Rótulo |  [optional]
**layoutable** | **Boolean** |  |  [optional]
**listviewable** | **String** |  |  [optional]
**lookupLayoutable** | **String** |  |  [optional]
**mergeable** | **Boolean** |  |  [optional]
**mruEnabled** | **Boolean** | Flag MRU Ativo |  [optional]
**name** | **String** | Nome da Manifestação (??) |  [optional]
**namedLayoutInfos** | **List&lt;String&gt;** |  |  [optional]
**networkScopeFieldName** | **String** |  |  [optional]
**queryable** | **Boolean** |  |  [optional]
**recordTypeInfos** | [**List&lt;InformaoRegistro&gt;**](InformaoRegistro.md) |  |  [optional]
**replicateable** | **Boolean** | Flag Replicável |  [optional]
**retrieveable** | **Boolean** | Flag Recuperável |  [optional]
**searchLayoutable** | **Boolean** |  |  [optional]
**searchable** | **Boolean** | Flag Pesqusável |  [optional]
**supportedScopes** | [**List&lt;EscopoSuportado&gt;**](EscopoSuportado.md) |  |  [optional]
**triggerable** | **Boolean** | Flag Acionável |  [optional]
**undeletable** | **Boolean** |  |  [optional]
**updateable** | **Boolean** | Flag Atualizável |  [optional]
**urls** | [**Urls**](Urls.md) |  |  [optional]



