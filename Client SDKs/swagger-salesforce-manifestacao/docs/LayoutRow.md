
# LayoutRow

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**layoutItems** | [**List&lt;LayoutItems&gt;**](LayoutItems.md) |  |  [optional]
**numItems** | **Integer** | Número de Itens |  [optional]



