
# Color

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**color** | **String** | HexCode da Cor |  [optional]
**context** | **String** | Contexto |  [optional]
**theme** | **String** | Tema |  [optional]



