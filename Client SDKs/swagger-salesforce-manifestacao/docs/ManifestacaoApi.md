# ManifestacaoApi

All URIs are relative to *http://apisulamerica.sensedia.com/auto/corretor/chat/bot/salesforce/api/v1/dev*

Method | HTTP request | Description
------------- | ------------- | -------------
[**atendimentosExcecoesDataHoraGet**](ManifestacaoApi.md#atendimentosExcecoesDataHoraGet) | **GET** /atendimentos/excecoes/{dataHora} | 
[**atendimentosGet**](ManifestacaoApi.md#atendimentosGet) | **GET** /atendimentos | 
[**corretoresContasGet**](ManifestacaoApi.md#corretoresContasGet) | **GET** /corretores/contas | 
[**corretoresGet**](ManifestacaoApi.md#corretoresGet) | **GET** /corretores | 
[**manifestacoesArquivosPost**](ManifestacaoApi.md#manifestacoesArquivosPost) | **POST** /manifestacoes/arquivos | 
[**manifestacoesDescricaoGet**](ManifestacaoApi.md#manifestacoesDescricaoGet) | **GET** /manifestacoes/descricao | 
[**manifestacoesDescricaoLayoutsGet**](ManifestacaoApi.md#manifestacoesDescricaoLayoutsGet) | **GET** /manifestacoes/descricao/layouts | 
[**manifestacoesDescricaoLayoutsIdGet**](ManifestacaoApi.md#manifestacoesDescricaoLayoutsIdGet) | **GET** /manifestacoes/descricao/layouts/{id} | 
[**manifestacoesPost**](ManifestacaoApi.md#manifestacoesPost) | **POST** /manifestacoes | 
[**manifestacoesSubtiposGet**](ManifestacaoApi.md#manifestacoesSubtiposGet) | **GET** /manifestacoes/subtipos | 


<a name="atendimentosExcecoesDataHoraGet"></a>
# **atendimentosExcecoesDataHoraGet**
> Excecao atendimentosExcecoesDataHoraGet(clientId, accessToken, xGatewayProcessId, xGatewayRequestId, dataHora)



Retorna a data e o horário de exceção de funcionamento do chat de Emissão Auto.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.ManifestacaoApi;


ManifestacaoApi apiInstance = new ManifestacaoApi();
String clientId = "clientId_example"; // String | Identificador do Cliente utilizado na autenticação.
String accessToken = "accessToken_example"; // String | Token de acesso utilizado na autenticação.
String xGatewayProcessId = "xGatewayProcessId_example"; // String | Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão.
String xGatewayRequestId = "xGatewayRequestId_example"; // String | Código único que identifica uma requisição de chat. Deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador.
String dataHora = "dataHora_example"; // String | Data e hora em que o usuário está tentando acessar o chat para verificar se é uma exceção (Formato: YYYY-MM-DDThh:mm:ss.sZ).
try {
    Excecao result = apiInstance.atendimentosExcecoesDataHoraGet(clientId, accessToken, xGatewayProcessId, xGatewayRequestId, dataHora);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ManifestacaoApi#atendimentosExcecoesDataHoraGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**| Identificador do Cliente utilizado na autenticação. |
 **accessToken** | **String**| Token de acesso utilizado na autenticação. |
 **xGatewayProcessId** | **String**| Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão. |
 **xGatewayRequestId** | **String**| Código único que identifica uma requisição de chat. Deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador. |
 **dataHora** | **String**| Data e hora em que o usuário está tentando acessar o chat para verificar se é uma exceção (Formato: YYYY-MM-DDThh:mm:ss.sZ). |

### Return type

[**Excecao**](Excecao.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="atendimentosGet"></a>
# **atendimentosGet**
> Atendimento atendimentosGet(clientId, accessToken, xGatewayProcessId, xGatewayRequestId)



Retorna os horários de funcionamento do chat de Emissão Auto.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.ManifestacaoApi;


ManifestacaoApi apiInstance = new ManifestacaoApi();
String clientId = "clientId_example"; // String | Identificador do Cliente utilizado na autenticação.
String accessToken = "accessToken_example"; // String | Token de acesso utilizado na autenticação.
String xGatewayProcessId = "xGatewayProcessId_example"; // String | Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão.
String xGatewayRequestId = "xGatewayRequestId_example"; // String | Código único que identifica uma requisição de chat. Deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador.
try {
    Atendimento result = apiInstance.atendimentosGet(clientId, accessToken, xGatewayProcessId, xGatewayRequestId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ManifestacaoApi#atendimentosGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**| Identificador do Cliente utilizado na autenticação. |
 **accessToken** | **String**| Token de acesso utilizado na autenticação. |
 **xGatewayProcessId** | **String**| Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão. |
 **xGatewayRequestId** | **String**| Código único que identifica uma requisição de chat. Deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador. |

### Return type

[**Atendimento**](Atendimento.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="corretoresContasGet"></a>
# **corretoresContasGet**
> List&lt;CorretorConta&gt; corretoresContasGet(clientId, accessToken, xGatewayProcessId, xGatewayRequestId, nome)



Consulta Informações das Contas de um Corretor.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.ManifestacaoApi;


ManifestacaoApi apiInstance = new ManifestacaoApi();
String clientId = "clientId_example"; // String | Identificador do Cliente utilizado na autenticação.
String accessToken = "accessToken_example"; // String | Token de acesso utilizado na autenticação.
String xGatewayProcessId = "xGatewayProcessId_example"; // String | Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão.
String xGatewayRequestId = "xGatewayRequestId_example"; // String | Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador.
String nome = "nome_example"; // String | Nome do Corretor.
try {
    List<CorretorConta> result = apiInstance.corretoresContasGet(clientId, accessToken, xGatewayProcessId, xGatewayRequestId, nome);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ManifestacaoApi#corretoresContasGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**| Identificador do Cliente utilizado na autenticação. |
 **accessToken** | **String**| Token de acesso utilizado na autenticação. |
 **xGatewayProcessId** | **String**| Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão. |
 **xGatewayRequestId** | **String**| Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador. |
 **nome** | **String**| Nome do Corretor. |

### Return type

[**List&lt;CorretorConta&gt;**](CorretorConta.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="corretoresGet"></a>
# **corretoresGet**
> List&lt;Corretor&gt; corretoresGet(clientId, accessToken, codigoProdutor, xGatewayProcessId, xGatewayRequestId)



Consulta Informações dos corretores.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.ManifestacaoApi;


ManifestacaoApi apiInstance = new ManifestacaoApi();
String clientId = "clientId_example"; // String | Identificador do Cliente utilizado na autenticação.
String accessToken = "accessToken_example"; // String | Token de acesso utilizado na autenticação.
String codigoProdutor = "codigoProdutor_example"; // String | Código do Produtor.
String xGatewayProcessId = "xGatewayProcessId_example"; // String | Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão.
String xGatewayRequestId = "xGatewayRequestId_example"; // String | Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador.
try {
    List<Corretor> result = apiInstance.corretoresGet(clientId, accessToken, codigoProdutor, xGatewayProcessId, xGatewayRequestId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ManifestacaoApi#corretoresGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**| Identificador do Cliente utilizado na autenticação. |
 **accessToken** | **String**| Token de acesso utilizado na autenticação. |
 **codigoProdutor** | **String**| Código do Produtor. |
 **xGatewayProcessId** | **String**| Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão. |
 **xGatewayRequestId** | **String**| Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador. |

### Return type

[**List&lt;Corretor&gt;**](Corretor.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="manifestacoesArquivosPost"></a>
# **manifestacoesArquivosPost**
> manifestacoesArquivosPost(clientId, accessToken, xGatewayProcessId, xGatewayRequestId, organizationId, sessionId, fileToken, baseURL, file, encoding)



Retorna os horarios de funcionamento.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.ManifestacaoApi;


ManifestacaoApi apiInstance = new ManifestacaoApi();
String clientId = "clientId_example"; // String | Identificador do Cliente utilizado na autenticação.
String accessToken = "accessToken_example"; // String | Token de acesso utilizado na autenticação.
String xGatewayProcessId = "xGatewayProcessId_example"; // String | Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão.
String xGatewayRequestId = "xGatewayRequestId_example"; // String | Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador.
String organizationId = "organizationId_example"; // String | O Identificador da organização Salesforce do visitante do chat.
String sessionId = "sessionId_example"; // String | Identificador da sessão do Live Agent do visitante ao chat.
String fileToken = "fileToken_example"; // String | token do arquivo
String baseURL = "baseURL_example"; // String | URL base para a requisição ao backend.
File file = new File("/path/to/file.txt"); // File | Arquivo para upload
String encoding = "encoding_example"; // String | Encode da mensagem. (Valor default UTF-8)
try {
    apiInstance.manifestacoesArquivosPost(clientId, accessToken, xGatewayProcessId, xGatewayRequestId, organizationId, sessionId, fileToken, baseURL, file, encoding);
} catch (ApiException e) {
    System.err.println("Exception when calling ManifestacaoApi#manifestacoesArquivosPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**| Identificador do Cliente utilizado na autenticação. |
 **accessToken** | **String**| Token de acesso utilizado na autenticação. |
 **xGatewayProcessId** | **String**| Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão. |
 **xGatewayRequestId** | **String**| Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador. |
 **organizationId** | **String**| O Identificador da organização Salesforce do visitante do chat. |
 **sessionId** | **String**| Identificador da sessão do Live Agent do visitante ao chat. |
 **fileToken** | **String**| token do arquivo |
 **baseURL** | **String**| URL base para a requisição ao backend. |
 **file** | **File**| Arquivo para upload |
 **encoding** | **String**| Encode da mensagem. (Valor default UTF-8) | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: Not defined

<a name="manifestacoesDescricaoGet"></a>
# **manifestacoesDescricaoGet**
> Descrio manifestacoesDescricaoGet(clientId, accessToken, xGatewayProcessId, xGatewayRequestId)



Consulta descrição da manifestação.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.ManifestacaoApi;


ManifestacaoApi apiInstance = new ManifestacaoApi();
String clientId = "clientId_example"; // String | Identificador do Cliente utilizado na autenticação.
String accessToken = "accessToken_example"; // String | Token de acesso utilizado na autenticação.
String xGatewayProcessId = "xGatewayProcessId_example"; // String | Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão.
String xGatewayRequestId = "xGatewayRequestId_example"; // String | Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador.
try {
    Descrio result = apiInstance.manifestacoesDescricaoGet(clientId, accessToken, xGatewayProcessId, xGatewayRequestId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ManifestacaoApi#manifestacoesDescricaoGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**| Identificador do Cliente utilizado na autenticação. |
 **accessToken** | **String**| Token de acesso utilizado na autenticação. |
 **xGatewayProcessId** | **String**| Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão. |
 **xGatewayRequestId** | **String**| Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador. |

### Return type

[**Descrio**](Descrio.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="manifestacoesDescricaoLayoutsGet"></a>
# **manifestacoesDescricaoLayoutsGet**
> Layouts manifestacoesDescricaoLayoutsGet(clientId, accessToken, xGatewayProcessId, xGatewayRequestId)



Consulta descrição dos layouts da manifestação.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.ManifestacaoApi;


ManifestacaoApi apiInstance = new ManifestacaoApi();
String clientId = "clientId_example"; // String | Identificador do Cliente utilizado na autenticação.
String accessToken = "accessToken_example"; // String | Token de acesso utilizado na autenticação.
String xGatewayProcessId = "xGatewayProcessId_example"; // String | Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão.
String xGatewayRequestId = "xGatewayRequestId_example"; // String | Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador.
try {
    Layouts result = apiInstance.manifestacoesDescricaoLayoutsGet(clientId, accessToken, xGatewayProcessId, xGatewayRequestId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ManifestacaoApi#manifestacoesDescricaoLayoutsGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**| Identificador do Cliente utilizado na autenticação. |
 **accessToken** | **String**| Token de acesso utilizado na autenticação. |
 **xGatewayProcessId** | **String**| Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão. |
 **xGatewayRequestId** | **String**| Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador. |

### Return type

[**Layouts**](Layouts.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="manifestacoesDescricaoLayoutsIdGet"></a>
# **manifestacoesDescricaoLayoutsIdGet**
> LayoutsDetailed manifestacoesDescricaoLayoutsIdGet(clientId, accessToken, id, xGatewayProcessId, xGatewayRequestId)



Consulta descrição dos layouts da manifestação.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.ManifestacaoApi;


ManifestacaoApi apiInstance = new ManifestacaoApi();
String clientId = "clientId_example"; // String | Identificador do Cliente utilizado na autenticação.
String accessToken = "accessToken_example"; // String | Token de acesso utilizado na autenticação.
String id = "id_example"; // String | Token de acesso utilizado na autenticação.
String xGatewayProcessId = "xGatewayProcessId_example"; // String | Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão.
String xGatewayRequestId = "xGatewayRequestId_example"; // String | Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador.
try {
    LayoutsDetailed result = apiInstance.manifestacoesDescricaoLayoutsIdGet(clientId, accessToken, id, xGatewayProcessId, xGatewayRequestId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ManifestacaoApi#manifestacoesDescricaoLayoutsIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**| Identificador do Cliente utilizado na autenticação. |
 **accessToken** | **String**| Token de acesso utilizado na autenticação. |
 **id** | **String**| Token de acesso utilizado na autenticação. |
 **xGatewayProcessId** | **String**| Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão. |
 **xGatewayRequestId** | **String**| Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador. |

### Return type

[**LayoutsDetailed**](LayoutsDetailed.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="manifestacoesPost"></a>
# **manifestacoesPost**
> RetornoManifestacao manifestacoesPost(clientId, accessToken, contentType, xGatewayProcessId, xGatewayRequestId, manifestacao)



Cria nova manifestação.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.ManifestacaoApi;


ManifestacaoApi apiInstance = new ManifestacaoApi();
String clientId = "clientId_example"; // String | Identificador do Cliente utilizado na autenticação.
String accessToken = "accessToken_example"; // String | Token de acesso utilizado na autenticação.
String contentType = "contentType_example"; // String | Fixo 'application/json'.
String xGatewayProcessId = "xGatewayProcessId_example"; // String | Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão.
String xGatewayRequestId = "xGatewayRequestId_example"; // String | Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador.
Manifestacao manifestacao = new Manifestacao(); // Manifestacao | 
try {
    RetornoManifestacao result = apiInstance.manifestacoesPost(clientId, accessToken, contentType, xGatewayProcessId, xGatewayRequestId, manifestacao);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ManifestacaoApi#manifestacoesPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**| Identificador do Cliente utilizado na autenticação. |
 **accessToken** | **String**| Token de acesso utilizado na autenticação. |
 **contentType** | **String**| Fixo &#39;application/json&#39;. |
 **xGatewayProcessId** | **String**| Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão. |
 **xGatewayRequestId** | **String**| Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador. |
 **manifestacao** | [**Manifestacao**](Manifestacao.md)|  |

### Return type

[**RetornoManifestacao**](RetornoManifestacao.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="manifestacoesSubtiposGet"></a>
# **manifestacoesSubtiposGet**
> List&lt;SubTipo&gt; manifestacoesSubtiposGet(clientId, accessToken, xGatewayProcessId, xGatewayRequestId, categoria, tipo, nome)



Consulta os subtipos da manifestação.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.ManifestacaoApi;


ManifestacaoApi apiInstance = new ManifestacaoApi();
String clientId = "clientId_example"; // String | Identificador do Cliente utilizado na autenticação.
String accessToken = "accessToken_example"; // String | Token de acesso utilizado na autenticação.
String xGatewayProcessId = "xGatewayProcessId_example"; // String | Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão.
String xGatewayRequestId = "xGatewayRequestId_example"; // String | Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador.
String categoria = "categoria_example"; // String | Categoria da Manifestação.
String tipo = "tipo_example"; // String | Tipo da Manifestação.
String nome = "nome_example"; // String | Nome do Subtipo.
try {
    List<SubTipo> result = apiInstance.manifestacoesSubtiposGet(clientId, accessToken, xGatewayProcessId, xGatewayRequestId, categoria, tipo, nome);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ManifestacaoApi#manifestacoesSubtiposGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**| Identificador do Cliente utilizado na autenticação. |
 **accessToken** | **String**| Token de acesso utilizado na autenticação. |
 **xGatewayProcessId** | **String**| Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão. |
 **xGatewayRequestId** | **String**| Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador. |
 **categoria** | **String**| Categoria da Manifestação. |
 **tipo** | **String**| Tipo da Manifestação. |
 **nome** | **String**| Nome do Subtipo. |

### Return type

[**List&lt;SubTipo&gt;**](SubTipo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

