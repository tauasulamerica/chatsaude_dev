
# Entidade

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**entityName** | **String** | O registro a ser pesquisado ou criado. | 
**showOnCreate** | **Boolean** | Especifica se o registro deve ser exibido depois de criado (true) ou não (false). |  [optional]
**linkToEntityName** | **String** | O nome do registro ao qual vincular o detalhe. |  [optional]
**linkToEntityField** | **String** | O campo dentro do registro ao qual vincular o detalhe. |  [optional]
**saveToTranscript** | **String** | O nome do campo de transcrição para o qual salvar o registro. |  [optional]
**entityFieldsMaps** | [**List&lt;MapaEntidade&gt;**](MapaEntidade.md) | Os campos aos quais associar o detalhe em um registro. | 



