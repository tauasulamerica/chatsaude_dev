
# InformaoRegistro

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**available** | **Boolean** | Flag Disponivel |  [optional]
**defaultRecordTypeMapping** | **Boolean** | Tipo de registro padrão para mapeamento (??) |  [optional]
**master** | **Boolean** | Flag de layout mestre |  [optional]
**name** | **String** | Nome do Layout |  [optional]
**recordTypeId** | **String** | Id do tipo de registro |  [optional]
**urls** | [**Url**](Url.md) |  |  [optional]



