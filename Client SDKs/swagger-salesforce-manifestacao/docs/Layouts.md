
# Layouts

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**layouts** | **String** |   |  [optional]
**recordTypeMappings** | [**List&lt;Record&gt;**](Record.md) |   |  [optional]
**recordTypeSelectorRequired** | **List&lt;Boolean&gt;** |  |  [optional]



