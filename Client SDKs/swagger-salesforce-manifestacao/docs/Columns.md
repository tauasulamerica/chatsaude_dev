
# Columns

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**field** | **String** |  |  [optional]
**fieldApiName** | **String** |  |  [optional]
**format** | **String** |  |  [optional]
**label** | **String** |  |  [optional]
**lookupId** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**sortable** | **Boolean** |  |  [optional]



