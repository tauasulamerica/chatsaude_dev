
# QuickActionList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accessLevelRequired** | **String** |  |  [optional]
**colors** | [**List&lt;Color&gt;**](Color.md) |  |  [optional]
**iconUrl** | **String** |  |  [optional]
**icons** | [**List&lt;Icons&gt;**](Icons.md) |  |  [optional]
**label** | **String** |  |  [optional]
**miniIconUrl** | **String** |  |  [optional]
**quickActionName** | **String** |  |  [optional]
**targetSobjectType** | **String** |  |  [optional]
**type** | **String** |  |  [optional]
**urls** | [**Url**](Url.md) |  |  [optional]



