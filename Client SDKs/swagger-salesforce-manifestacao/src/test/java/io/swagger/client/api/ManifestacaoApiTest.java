/*
 * API de Manifestação
 * API com operações para gerenciar Manifestações
 *
 * OpenAPI spec version: 1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.swagger.client.api;

import io.swagger.client.salesforce.manifestacao.model.Atendimento;
import io.swagger.client.salesforce.manifestacao.model.Corretor;
import io.swagger.client.salesforce.manifestacao.model.CorretorConta;
import io.swagger.client.salesforce.manifestacao.model.Descrio;
import io.swagger.client.salesforce.manifestacao.model.Excecao;
import java.io.File;
import io.swagger.client.salesforce.manifestacao.model.Layouts;
import io.swagger.client.salesforce.manifestacao.model.LayoutsDetailed;
import io.swagger.client.salesforce.manifestacao.model.Manifestacao;
import io.swagger.client.salesforce.manifestacao.model.RetornoManifestacao;
import io.swagger.client.salesforce.manifestacao.model.SubTipo;
import io.swagger.client.salesforce.manifestacao.ApiException;
import io.swagger.client.salesforce.manifestacao.api.ManifestacaoApi;

import org.junit.Test;
import org.junit.Ignore;

import java.util.List;

/**
 * API tests for ManifestacaoApi
 */
@Ignore
public class ManifestacaoApiTest {

    private final ManifestacaoApi api = new ManifestacaoApi();

    
    /**
     * 
     *
     * Retorna a data e o horário de exceção de funcionamento do chat de Emissão Auto.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void atendimentosExcecoesDataHoraGetTest() throws ApiException {
        String clientId = null;
        String accessToken = null;
        String xGatewayProcessId = null;
        String xGatewayRequestId = null;
        String dataHora = null;
        Excecao response = api.atendimentosExcecoesDataHoraGet(clientId, accessToken, xGatewayProcessId, xGatewayRequestId, dataHora);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * Retorna os horários de funcionamento do chat de Emissão Auto.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void atendimentosGetTest() throws ApiException {
        String clientId = null;
        String accessToken = null;
        String xGatewayProcessId = null;
        String xGatewayRequestId = null;
        Atendimento response = api.atendimentosGet(clientId, accessToken, xGatewayProcessId, xGatewayRequestId);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * Consulta Informações das Contas de um Corretor.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void corretoresContasGetTest() throws ApiException {
        String clientId = null;
        String accessToken = null;
        String xGatewayProcessId = null;
        String xGatewayRequestId = null;
        String nome = null;
        List<CorretorConta> response = api.corretoresContasGet(clientId, accessToken, xGatewayProcessId, xGatewayRequestId, nome);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * Consulta Informações dos corretores.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void corretoresGetTest() throws ApiException {
        String clientId = null;
        String accessToken = null;
        String codigoProdutor = null;
        String xGatewayProcessId = null;
        String xGatewayRequestId = null;
        List<Corretor> response = api.corretoresGet(clientId, accessToken, codigoProdutor, xGatewayProcessId, xGatewayRequestId);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * Retorna os horarios de funcionamento.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void manifestacoesArquivosPostTest() throws ApiException {
        String clientId = null;
        String accessToken = null;
        String xGatewayProcessId = null;
        String xGatewayRequestId = null;
        String organizationId = null;
        String sessionId = null;
        String fileToken = null;
        String baseURL = null;
        File file = null;
        String encoding = null;
        api.manifestacoesArquivosPost(clientId, accessToken, xGatewayProcessId, xGatewayRequestId, organizationId, sessionId, fileToken, baseURL, file, encoding);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * Consulta descrição da manifestação.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void manifestacoesDescricaoGetTest() throws ApiException {
        String clientId = null;
        String accessToken = null;
        String xGatewayProcessId = null;
        String xGatewayRequestId = null;
        Descrio response = api.manifestacoesDescricaoGet(clientId, accessToken, xGatewayProcessId, xGatewayRequestId);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * Consulta descrição dos layouts da manifestação.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void manifestacoesDescricaoLayoutsGetTest() throws ApiException {
        String clientId = null;
        String accessToken = null;
        String xGatewayProcessId = null;
        String xGatewayRequestId = null;
        Layouts response = api.manifestacoesDescricaoLayoutsGet(clientId, accessToken, xGatewayProcessId, xGatewayRequestId);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * Consulta descrição dos layouts da manifestação.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void manifestacoesDescricaoLayoutsIdGetTest() throws ApiException {
        String clientId = null;
        String accessToken = null;
        String id = null;
        String xGatewayProcessId = null;
        String xGatewayRequestId = null;
        LayoutsDetailed response = api.manifestacoesDescricaoLayoutsIdGet(clientId, accessToken, id, xGatewayProcessId, xGatewayRequestId);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * Cria nova manifestação.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void manifestacoesPostTest() throws ApiException {
        String clientId = null;
        String accessToken = null;
        String contentType = null;
        String xGatewayProcessId = null;
        String xGatewayRequestId = null;
        Manifestacao manifestacao = null;
        RetornoManifestacao response = api.manifestacoesPost(clientId, accessToken, contentType, xGatewayProcessId, xGatewayRequestId, manifestacao);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * Consulta os subtipos da manifestação.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void manifestacoesSubtiposGetTest() throws ApiException {
        String clientId = null;
        String accessToken = null;
        String xGatewayProcessId = null;
        String xGatewayRequestId = null;
        String categoria = null;
        String tipo = null;
        String nome = null;
        List<SubTipo> response = api.manifestacoesSubtiposGet(clientId, accessToken, xGatewayProcessId, xGatewayRequestId, categoria, tipo, nome);

        // TODO: test validations
    }
    
}
