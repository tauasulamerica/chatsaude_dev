/*
 * API de Manifestação
 * API com operações para gerenciar Manifestações
 *
 * OpenAPI spec version: 1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.swagger.client.salesforce.manifestacao.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * IniciarConversa
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2017-08-23T15:25:36.303-03:00")
public class IniciarConversa {
  @JsonProperty("organizationId")
  private String organizationId = null;

  @JsonProperty("deploymentId")
  private String deploymentId = null;

  @JsonProperty("buttonId")
  private String buttonId = null;

  @JsonProperty("sessionId")
  private String sessionId = null;

  @JsonProperty("userAgent")
  private String userAgent = null;

  @JsonProperty("language")
  private String language = null;

  @JsonProperty("screenResolution")
  private String screenResolution = null;

  @JsonProperty("visitorName")
  private String visitorName = null;

  @JsonProperty("receiveQueueUpdates")
  private Boolean receiveQueueUpdates = null;

  @JsonProperty("isPost")
  private Boolean isPost = null;

  @JsonProperty("prechatDetails")
  private List<DetelhesPreChat> prechatDetails = null;

  @JsonProperty("prechatEntities")
  private List<Entidade> prechatEntities = null;

  public IniciarConversa organizationId(String organizationId) {
    this.organizationId = organizationId;
    return this;
  }

   /**
   * O Identificador da organização Salesforce do visitante do chat.
   * @return organizationId
  **/
  @ApiModelProperty(value = "O Identificador da organização Salesforce do visitante do chat.")
  public String getOrganizationId() {
    return organizationId;
  }

  public void setOrganizationId(String organizationId) {
    this.organizationId = organizationId;
  }

  public IniciarConversa deploymentId(String deploymentId) {
    this.deploymentId = deploymentId;
    return this;
  }

   /**
   * O Identificador da implantação a partir do qual o chat se originou.
   * @return deploymentId
  **/
  @ApiModelProperty(value = "O Identificador da implantação a partir do qual o chat se originou.")
  public String getDeploymentId() {
    return deploymentId;
  }

  public void setDeploymentId(String deploymentId) {
    this.deploymentId = deploymentId;
  }

  public IniciarConversa buttonId(String buttonId) {
    this.buttonId = buttonId;
    return this;
  }

   /**
   * A identificação do botão a partir do qual o chat se originou.
   * @return buttonId
  **/
  @ApiModelProperty(value = "A identificação do botão a partir do qual o chat se originou.")
  public String getButtonId() {
    return buttonId;
  }

  public void setButtonId(String buttonId) {
    this.buttonId = buttonId;
  }

  public IniciarConversa sessionId(String sessionId) {
    this.sessionId = sessionId;
    return this;
  }

   /**
   * Identificador da sessão do Live Agent do visitante ao chat.
   * @return sessionId
  **/
  @ApiModelProperty(value = "Identificador da sessão do Live Agent do visitante ao chat.")
  public String getSessionId() {
    return sessionId;
  }

  public void setSessionId(String sessionId) {
    this.sessionId = sessionId;
  }

  public IniciarConversa userAgent(String userAgent) {
    this.userAgent = userAgent;
    return this;
  }

   /**
   * O agente do usuário do navegador do visitante do chat.
   * @return userAgent
  **/
  @ApiModelProperty(value = "O agente do usuário do navegador do visitante do chat.")
  public String getUserAgent() {
    return userAgent;
  }

  public void setUserAgent(String userAgent) {
    this.userAgent = userAgent;
  }

  public IniciarConversa language(String language) {
    this.language = language;
    return this;
  }

   /**
   * A língua falada do visitante do chat.
   * @return language
  **/
  @ApiModelProperty(value = "A língua falada do visitante do chat.")
  public String getLanguage() {
    return language;
  }

  public void setLanguage(String language) {
    this.language = language;
  }

  public IniciarConversa screenResolution(String screenResolution) {
    this.screenResolution = screenResolution;
    return this;
  }

   /**
   * A resolução da tela de computador do visitante do chat.
   * @return screenResolution
  **/
  @ApiModelProperty(value = "A resolução da tela de computador do visitante do chat.")
  public String getScreenResolution() {
    return screenResolution;
  }

  public void setScreenResolution(String screenResolution) {
    this.screenResolution = screenResolution;
  }

  public IniciarConversa visitorName(String visitorName) {
    this.visitorName = visitorName;
    return this;
  }

   /**
   * Nome do visitante do chat.
   * @return visitorName
  **/
  @ApiModelProperty(value = "Nome do visitante do chat.")
  public String getVisitorName() {
    return visitorName;
  }

  public void setVisitorName(String visitorName) {
    this.visitorName = visitorName;
  }

  public IniciarConversa receiveQueueUpdates(Boolean receiveQueueUpdates) {
    this.receiveQueueUpdates = receiveQueueUpdates;
    return this;
  }

   /**
   * Indica se o visitante do chat receberá as atualizações da posição da fila (true) ou não (false).
   * @return receiveQueueUpdates
  **/
  @ApiModelProperty(value = "Indica se o visitante do chat receberá as atualizações da posição da fila (true) ou não (false).")
  public Boolean getReceiveQueueUpdates() {
    return receiveQueueUpdates;
  }

  public void setReceiveQueueUpdates(Boolean receiveQueueUpdates) {
    this.receiveQueueUpdates = receiveQueueUpdates;
  }

  public IniciarConversa isPost(Boolean isPost) {
    this.isPost = isPost;
    return this;
  }

   /**
   * Indica se a solicitação de bate-papo foi feita corretamente através de uma solicitação POST (true) ou não (false).
   * @return isPost
  **/
  @ApiModelProperty(value = "Indica se a solicitação de bate-papo foi feita corretamente através de uma solicitação POST (true) ou não (false).")
  public Boolean getIsPost() {
    return isPost;
  }

  public void setIsPost(Boolean isPost) {
    this.isPost = isPost;
  }

  public IniciarConversa prechatDetails(List<DetelhesPreChat> prechatDetails) {
    this.prechatDetails = prechatDetails;
    return this;
  }

  public IniciarConversa addPrechatDetailsItem(DetelhesPreChat prechatDetailsItem) {
    if (this.prechatDetails == null) {
      this.prechatDetails = new ArrayList<DetelhesPreChat>();
    }
    this.prechatDetails.add(prechatDetailsItem);
    return this;
  }

   /**
   * Detalhes do Pré chat.
   * @return prechatDetails
  **/
  @ApiModelProperty(value = "Detalhes do Pré chat.")
  public List<DetelhesPreChat> getPrechatDetails() {
    return prechatDetails;
  }

  public void setPrechatDetails(List<DetelhesPreChat> prechatDetails) {
    this.prechatDetails = prechatDetails;
  }

  public IniciarConversa prechatEntities(List<Entidade> prechatEntities) {
    this.prechatEntities = prechatEntities;
    return this;
  }

  public IniciarConversa addPrechatEntitiesItem(Entidade prechatEntitiesItem) {
    if (this.prechatEntities == null) {
      this.prechatEntities = new ArrayList<Entidade>();
    }
    this.prechatEntities.add(prechatEntitiesItem);
    return this;
  }

   /**
   * Get prechatEntities
   * @return prechatEntities
  **/
  @ApiModelProperty(value = "")
  public List<Entidade> getPrechatEntities() {
    return prechatEntities;
  }

  public void setPrechatEntities(List<Entidade> prechatEntities) {
    this.prechatEntities = prechatEntities;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    IniciarConversa iniciarConversa = (IniciarConversa) o;
    return Objects.equals(this.organizationId, iniciarConversa.organizationId) &&
        Objects.equals(this.deploymentId, iniciarConversa.deploymentId) &&
        Objects.equals(this.buttonId, iniciarConversa.buttonId) &&
        Objects.equals(this.sessionId, iniciarConversa.sessionId) &&
        Objects.equals(this.userAgent, iniciarConversa.userAgent) &&
        Objects.equals(this.language, iniciarConversa.language) &&
        Objects.equals(this.screenResolution, iniciarConversa.screenResolution) &&
        Objects.equals(this.visitorName, iniciarConversa.visitorName) &&
        Objects.equals(this.receiveQueueUpdates, iniciarConversa.receiveQueueUpdates) &&
        Objects.equals(this.isPost, iniciarConversa.isPost) &&
        Objects.equals(this.prechatDetails, iniciarConversa.prechatDetails) &&
        Objects.equals(this.prechatEntities, iniciarConversa.prechatEntities);
  }

  @Override
  public int hashCode() {
    return Objects.hash(organizationId, deploymentId, buttonId, sessionId, userAgent, language, screenResolution, visitorName, receiveQueueUpdates, isPost, prechatDetails, prechatEntities);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class IniciarConversa {\n");
    
    sb.append("    organizationId: ").append(toIndentedString(organizationId)).append("\n");
    sb.append("    deploymentId: ").append(toIndentedString(deploymentId)).append("\n");
    sb.append("    buttonId: ").append(toIndentedString(buttonId)).append("\n");
    sb.append("    sessionId: ").append(toIndentedString(sessionId)).append("\n");
    sb.append("    userAgent: ").append(toIndentedString(userAgent)).append("\n");
    sb.append("    language: ").append(toIndentedString(language)).append("\n");
    sb.append("    screenResolution: ").append(toIndentedString(screenResolution)).append("\n");
    sb.append("    visitorName: ").append(toIndentedString(visitorName)).append("\n");
    sb.append("    receiveQueueUpdates: ").append(toIndentedString(receiveQueueUpdates)).append("\n");
    sb.append("    isPost: ").append(toIndentedString(isPost)).append("\n");
    sb.append("    prechatDetails: ").append(toIndentedString(prechatDetails)).append("\n");
    sb.append("    prechatEntities: ").append(toIndentedString(prechatEntities)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

