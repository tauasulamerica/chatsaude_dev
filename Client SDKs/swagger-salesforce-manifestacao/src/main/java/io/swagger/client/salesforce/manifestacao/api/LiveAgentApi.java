package io.swagger.client.salesforce.manifestacao.api;

import javax.ws.rs.core.GenericType;

import io.swagger.client.salesforce.manifestacao.model.Conversa;
import io.swagger.client.salesforce.manifestacao.model.FinalConversa;
import io.swagger.client.salesforce.manifestacao.model.IniciarConversa;
import io.swagger.client.salesforce.manifestacao.model.Sessao;
import io.swagger.client.salesforce.manifestacao.ApiClient;
import io.swagger.client.salesforce.manifestacao.ApiException;
import io.swagger.client.salesforce.manifestacao.Configuration;
import io.swagger.client.salesforce.manifestacao.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2017-08-23T15:25:36.303-03:00")
public class LiveAgentApi {
  private ApiClient apiClient;

  public LiveAgentApi() {
    this(Configuration.getDefaultApiClient());
  }

  public LiveAgentApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * 
   * Indica se um botão de chat está disponível para receber novas solicitações de bate-papo.
   * @param clientId Identificador do Cliente utilizado na autenticação. (required)
   * @param accessToken Token de acesso utilizado na autenticação. (required)
   * @param X_LIVEAGENT_API_VERSION A versão da API do Salesforce para a solicitação. (required)
   * @param xGatewayProcessId Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão. (required)
   * @param xGatewayRequestId Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador. (required)
   * @param identificadorOrganizacao O Identificador da organização do Salesforce associada à implantação do Live Agent. (required)
   * @param identificadorImplantacao O Identificador de 15 dígitos da implantação do Live Agent da qual a solicitação de cgat foi iniciada. (required)
   * @param identificadoresDisponibilidade Uma matriz de IDs de objeto para os quais é possível verificar a disponibilidade. (required)
   * @return Object
   * @throws ApiException if fails to make API call
   */
  public Object agentesDisponiveisGet(String clientId, String accessToken, Integer X_LIVEAGENT_API_VERSION, String xGatewayProcessId, String xGatewayRequestId, String identificadorOrganizacao, String identificadorImplantacao, String identificadoresDisponibilidade) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'clientId' is set
    if (clientId == null) {
      throw new ApiException(400, "Missing the required parameter 'clientId' when calling agentesDisponiveisGet");
    }
    
    // verify the required parameter 'accessToken' is set
    if (accessToken == null) {
      throw new ApiException(400, "Missing the required parameter 'accessToken' when calling agentesDisponiveisGet");
    }
    
    // verify the required parameter 'X_LIVEAGENT_API_VERSION' is set
    if (X_LIVEAGENT_API_VERSION == null) {
      throw new ApiException(400, "Missing the required parameter 'X_LIVEAGENT_API_VERSION' when calling agentesDisponiveisGet");
    }
    
    // verify the required parameter 'xGatewayProcessId' is set
    if (xGatewayProcessId == null) {
      throw new ApiException(400, "Missing the required parameter 'xGatewayProcessId' when calling agentesDisponiveisGet");
    }
    
    // verify the required parameter 'xGatewayRequestId' is set
    if (xGatewayRequestId == null) {
      throw new ApiException(400, "Missing the required parameter 'xGatewayRequestId' when calling agentesDisponiveisGet");
    }
    
    // verify the required parameter 'identificadorOrganizacao' is set
    if (identificadorOrganizacao == null) {
      throw new ApiException(400, "Missing the required parameter 'identificadorOrganizacao' when calling agentesDisponiveisGet");
    }
    
    // verify the required parameter 'identificadorImplantacao' is set
    if (identificadorImplantacao == null) {
      throw new ApiException(400, "Missing the required parameter 'identificadorImplantacao' when calling agentesDisponiveisGet");
    }
    
    // verify the required parameter 'identificadoresDisponibilidade' is set
    if (identificadoresDisponibilidade == null) {
      throw new ApiException(400, "Missing the required parameter 'identificadoresDisponibilidade' when calling agentesDisponiveisGet");
    }
    
    // create path and map variables
    String localVarPath = "/agentes/disponiveis";

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();

    localVarQueryParams.addAll(apiClient.parameterToPairs("", "identificadorOrganizacao", identificadorOrganizacao));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "identificadorImplantacao", identificadorImplantacao));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "identificadoresDisponibilidade", identificadoresDisponibilidade));

    if (clientId != null)
      localVarHeaderParams.put("client_id", apiClient.parameterToString(clientId));
if (accessToken != null)
      localVarHeaderParams.put("access_token", apiClient.parameterToString(accessToken));
if (X_LIVEAGENT_API_VERSION != null)
      localVarHeaderParams.put("X-LIVEAGENT-API-VERSION", apiClient.parameterToString(X_LIVEAGENT_API_VERSION));
if (xGatewayProcessId != null)
      localVarHeaderParams.put("x-gateway-process-id", apiClient.parameterToString(xGatewayProcessId));
if (xGatewayRequestId != null)
      localVarHeaderParams.put("x-gateway-request-id", apiClient.parameterToString(xGatewayRequestId));

    
    final String[] localVarAccepts = {
      
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<Object> localVarReturnType = new GenericType<Object>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * 
   * Finaliza a Conversa
   * @param clientId Identificador do Cliente utilizado na autenticação. (required)
   * @param accessToken Token de acesso utilizado na autenticação. (required)
   * @param X_LIVEAGENT_SESSION_KEY O ID exclusivo associado à sua sessão do LIve Agent. (required)
   * @param X_LIVEAGENT_API_VERSION A versão da API do Salesforce para a solicitação. (required)
   * @param X_LIVEAGENT_AFFINITY O ID gerado pelo sistema usado para identificar a sessão do Live Agent nos servidores do Live Agent (required)
   * @param X_LIVEAGENT_SEQUENCE A sequência de mensagens enviadas ao servidor do Live Agent para ajudar o servidor do Live Agent a evitar o processamento de mensagens duplicadas. (required)
   * @param finalizaConversa body da conversa. (required)
   * @param xGatewayProcessId Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão. (required)
   * @param xGatewayRequestId Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador. (required)
   * @throws ApiException if fails to make API call
   */
  public void conversasDelete(String clientId, String accessToken, String X_LIVEAGENT_SESSION_KEY, Integer X_LIVEAGENT_API_VERSION, String X_LIVEAGENT_AFFINITY, String X_LIVEAGENT_SEQUENCE, FinalConversa finalizaConversa, String xGatewayProcessId, String xGatewayRequestId) throws ApiException {
    Object localVarPostBody = finalizaConversa;
    
    // verify the required parameter 'clientId' is set
    if (clientId == null) {
      throw new ApiException(400, "Missing the required parameter 'clientId' when calling conversasDelete");
    }
    
    // verify the required parameter 'accessToken' is set
    if (accessToken == null) {
      throw new ApiException(400, "Missing the required parameter 'accessToken' when calling conversasDelete");
    }
    
    // verify the required parameter 'X_LIVEAGENT_SESSION_KEY' is set
    if (X_LIVEAGENT_SESSION_KEY == null) {
      throw new ApiException(400, "Missing the required parameter 'X_LIVEAGENT_SESSION_KEY' when calling conversasDelete");
    }
    
    // verify the required parameter 'X_LIVEAGENT_API_VERSION' is set
    if (X_LIVEAGENT_API_VERSION == null) {
      throw new ApiException(400, "Missing the required parameter 'X_LIVEAGENT_API_VERSION' when calling conversasDelete");
    }
    
    // verify the required parameter 'X_LIVEAGENT_AFFINITY' is set
    if (X_LIVEAGENT_AFFINITY == null) {
      throw new ApiException(400, "Missing the required parameter 'X_LIVEAGENT_AFFINITY' when calling conversasDelete");
    }
    
    // verify the required parameter 'X_LIVEAGENT_SEQUENCE' is set
    if (X_LIVEAGENT_SEQUENCE == null) {
      throw new ApiException(400, "Missing the required parameter 'X_LIVEAGENT_SEQUENCE' when calling conversasDelete");
    }
    
    // verify the required parameter 'finalizaConversa' is set
    if (finalizaConversa == null) {
      throw new ApiException(400, "Missing the required parameter 'finalizaConversa' when calling conversasDelete");
    }
    
    // verify the required parameter 'xGatewayProcessId' is set
    if (xGatewayProcessId == null) {
      throw new ApiException(400, "Missing the required parameter 'xGatewayProcessId' when calling conversasDelete");
    }
    
    // verify the required parameter 'xGatewayRequestId' is set
    if (xGatewayRequestId == null) {
      throw new ApiException(400, "Missing the required parameter 'xGatewayRequestId' when calling conversasDelete");
    }
    
    // create path and map variables
    String localVarPath = "/conversas";

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    if (clientId != null)
      localVarHeaderParams.put("client_id", apiClient.parameterToString(clientId));
if (accessToken != null)
      localVarHeaderParams.put("access_token", apiClient.parameterToString(accessToken));
if (X_LIVEAGENT_SESSION_KEY != null)
      localVarHeaderParams.put("X-LIVEAGENT-SESSION-KEY", apiClient.parameterToString(X_LIVEAGENT_SESSION_KEY));
if (X_LIVEAGENT_API_VERSION != null)
      localVarHeaderParams.put("X-LIVEAGENT-API-VERSION", apiClient.parameterToString(X_LIVEAGENT_API_VERSION));
if (X_LIVEAGENT_AFFINITY != null)
      localVarHeaderParams.put("X-LIVEAGENT-AFFINITY", apiClient.parameterToString(X_LIVEAGENT_AFFINITY));
if (X_LIVEAGENT_SEQUENCE != null)
      localVarHeaderParams.put("X-LIVEAGENT-SEQUENCE", apiClient.parameterToString(X_LIVEAGENT_SEQUENCE));
if (xGatewayProcessId != null)
      localVarHeaderParams.put("x-gateway-process-id", apiClient.parameterToString(xGatewayProcessId));
if (xGatewayRequestId != null)
      localVarHeaderParams.put("x-gateway-request-id", apiClient.parameterToString(xGatewayRequestId));

    
    final String[] localVarAccepts = {
      
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };


    apiClient.invokeAPI(localVarPath, "DELETE", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, null);
  }
  /**
   * 
   * Inicia uma nova sessão de visitante de bate-papo
   * @param clientId Identificador do Cliente utilizado na autenticação. (required)
   * @param accessToken Token de acesso utilizado na autenticação. (required)
   * @param X_LIVEAGENT_SESSION_KEY O ID exclusivo associado à sua sessão do LIve Agent. (required)
   * @param X_LIVEAGENT_API_VERSION A versão da API do Salesforce para a solicitação. (required)
   * @param X_LIVEAGENT_AFFINITY O ID gerado pelo sistema usado para identificar a sessão do Live Agent nos servidores do Live Agent (required)
   * @param X_LIVEAGENT_SEQUENCE A sequência de mensagens enviadas ao servidor do Live Agent para ajudar o servidor do Live Agent a evitar o processamento de mensagens duplicadas. (required)
   * @param xGatewayProcessId Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão. (required)
   * @param xGatewayRequestId Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador. (required)
   * @param iniciarConversa Body para iniciar a conversa (required)
   * @throws ApiException if fails to make API call
   */
  public void conversasPost(String clientId, String accessToken, String X_LIVEAGENT_SESSION_KEY, Integer X_LIVEAGENT_API_VERSION, String X_LIVEAGENT_AFFINITY, String X_LIVEAGENT_SEQUENCE, String xGatewayProcessId, String xGatewayRequestId, IniciarConversa iniciarConversa) throws ApiException {
    Object localVarPostBody = iniciarConversa;
    
    // verify the required parameter 'clientId' is set
    if (clientId == null) {
      throw new ApiException(400, "Missing the required parameter 'clientId' when calling conversasPost");
    }
    
    // verify the required parameter 'accessToken' is set
    if (accessToken == null) {
      throw new ApiException(400, "Missing the required parameter 'accessToken' when calling conversasPost");
    }
    
    // verify the required parameter 'X_LIVEAGENT_SESSION_KEY' is set
    if (X_LIVEAGENT_SESSION_KEY == null) {
      throw new ApiException(400, "Missing the required parameter 'X_LIVEAGENT_SESSION_KEY' when calling conversasPost");
    }
    
    // verify the required parameter 'X_LIVEAGENT_API_VERSION' is set
    if (X_LIVEAGENT_API_VERSION == null) {
      throw new ApiException(400, "Missing the required parameter 'X_LIVEAGENT_API_VERSION' when calling conversasPost");
    }
    
    // verify the required parameter 'X_LIVEAGENT_AFFINITY' is set
    if (X_LIVEAGENT_AFFINITY == null) {
      throw new ApiException(400, "Missing the required parameter 'X_LIVEAGENT_AFFINITY' when calling conversasPost");
    }
    
    // verify the required parameter 'X_LIVEAGENT_SEQUENCE' is set
    if (X_LIVEAGENT_SEQUENCE == null) {
      throw new ApiException(400, "Missing the required parameter 'X_LIVEAGENT_SEQUENCE' when calling conversasPost");
    }
    
    // verify the required parameter 'xGatewayProcessId' is set
    if (xGatewayProcessId == null) {
      throw new ApiException(400, "Missing the required parameter 'xGatewayProcessId' when calling conversasPost");
    }
    
    // verify the required parameter 'xGatewayRequestId' is set
    if (xGatewayRequestId == null) {
      throw new ApiException(400, "Missing the required parameter 'xGatewayRequestId' when calling conversasPost");
    }
    
    // verify the required parameter 'iniciarConversa' is set
    if (iniciarConversa == null) {
      throw new ApiException(400, "Missing the required parameter 'iniciarConversa' when calling conversasPost");
    }
    
    // create path and map variables
    String localVarPath = "/conversas";

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    if (clientId != null)
      localVarHeaderParams.put("client_id", apiClient.parameterToString(clientId));
if (accessToken != null)
      localVarHeaderParams.put("access_token", apiClient.parameterToString(accessToken));
if (X_LIVEAGENT_SESSION_KEY != null)
      localVarHeaderParams.put("X-LIVEAGENT-SESSION-KEY", apiClient.parameterToString(X_LIVEAGENT_SESSION_KEY));
if (X_LIVEAGENT_API_VERSION != null)
      localVarHeaderParams.put("X-LIVEAGENT-API-VERSION", apiClient.parameterToString(X_LIVEAGENT_API_VERSION));
if (X_LIVEAGENT_AFFINITY != null)
      localVarHeaderParams.put("X-LIVEAGENT-AFFINITY", apiClient.parameterToString(X_LIVEAGENT_AFFINITY));
if (X_LIVEAGENT_SEQUENCE != null)
      localVarHeaderParams.put("X-LIVEAGENT-SEQUENCE", apiClient.parameterToString(X_LIVEAGENT_SEQUENCE));
if (xGatewayProcessId != null)
      localVarHeaderParams.put("x-gateway-process-id", apiClient.parameterToString(xGatewayProcessId));
if (xGatewayRequestId != null)
      localVarHeaderParams.put("x-gateway-request-id", apiClient.parameterToString(xGatewayRequestId));

    
    final String[] localVarAccepts = {
      
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };


    apiClient.invokeAPI(localVarPath, "POST", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, null);
  }
  /**
   * 
   * Realiza o chat
   * @param clientId Identificador do Cliente utilizado na autenticação. (required)
   * @param accessToken Token de acesso utilizado na autenticação. (required)
   * @param X_LIVEAGENT_SESSION_KEY O ID exclusivo associado à sua sessão do LIve Agent. (required)
   * @param X_LIVEAGENT_API_VERSION A versão da API do Salesforce para a solicitação. (required)
   * @param X_LIVEAGENT_AFFINITY O ID gerado pelo sistema usado para identificar a sessão do Live Agent nos servidores do Live Agent (required)
   * @param X_LIVEAGENT_SEQUENCE A sequência de mensagens enviadas ao servidor do Live Agent para ajudar o servidor do Live Agent a evitar o processamento de mensagens duplicadas. (required)
   * @param xGatewayProcessId Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão. (required)
   * @param xGatewayRequestId Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador. (required)
   * @param conversa body da conversa. (required)
   * @throws ApiException if fails to make API call
   */
  public void conversasPut(String clientId, String accessToken, String X_LIVEAGENT_SESSION_KEY, Integer X_LIVEAGENT_API_VERSION, String X_LIVEAGENT_AFFINITY, String X_LIVEAGENT_SEQUENCE, String xGatewayProcessId, String xGatewayRequestId, Conversa conversa) throws ApiException {
    Object localVarPostBody = conversa;
    
    // verify the required parameter 'clientId' is set
    if (clientId == null) {
      throw new ApiException(400, "Missing the required parameter 'clientId' when calling conversasPut");
    }
    
    // verify the required parameter 'accessToken' is set
    if (accessToken == null) {
      throw new ApiException(400, "Missing the required parameter 'accessToken' when calling conversasPut");
    }
    
    // verify the required parameter 'X_LIVEAGENT_SESSION_KEY' is set
    if (X_LIVEAGENT_SESSION_KEY == null) {
      throw new ApiException(400, "Missing the required parameter 'X_LIVEAGENT_SESSION_KEY' when calling conversasPut");
    }
    
    // verify the required parameter 'X_LIVEAGENT_API_VERSION' is set
    if (X_LIVEAGENT_API_VERSION == null) {
      throw new ApiException(400, "Missing the required parameter 'X_LIVEAGENT_API_VERSION' when calling conversasPut");
    }
    
    // verify the required parameter 'X_LIVEAGENT_AFFINITY' is set
    if (X_LIVEAGENT_AFFINITY == null) {
      throw new ApiException(400, "Missing the required parameter 'X_LIVEAGENT_AFFINITY' when calling conversasPut");
    }
    
    // verify the required parameter 'X_LIVEAGENT_SEQUENCE' is set
    if (X_LIVEAGENT_SEQUENCE == null) {
      throw new ApiException(400, "Missing the required parameter 'X_LIVEAGENT_SEQUENCE' when calling conversasPut");
    }
    
    // verify the required parameter 'xGatewayProcessId' is set
    if (xGatewayProcessId == null) {
      throw new ApiException(400, "Missing the required parameter 'xGatewayProcessId' when calling conversasPut");
    }
    
    // verify the required parameter 'xGatewayRequestId' is set
    if (xGatewayRequestId == null) {
      throw new ApiException(400, "Missing the required parameter 'xGatewayRequestId' when calling conversasPut");
    }
    
    // verify the required parameter 'conversa' is set
    if (conversa == null) {
      throw new ApiException(400, "Missing the required parameter 'conversa' when calling conversasPut");
    }
    
    // create path and map variables
    String localVarPath = "/conversas";

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    if (clientId != null)
      localVarHeaderParams.put("client_id", apiClient.parameterToString(clientId));
if (accessToken != null)
      localVarHeaderParams.put("access_token", apiClient.parameterToString(accessToken));
if (X_LIVEAGENT_SESSION_KEY != null)
      localVarHeaderParams.put("X-LIVEAGENT-SESSION-KEY", apiClient.parameterToString(X_LIVEAGENT_SESSION_KEY));
if (X_LIVEAGENT_API_VERSION != null)
      localVarHeaderParams.put("X-LIVEAGENT-API-VERSION", apiClient.parameterToString(X_LIVEAGENT_API_VERSION));
if (X_LIVEAGENT_AFFINITY != null)
      localVarHeaderParams.put("X-LIVEAGENT-AFFINITY", apiClient.parameterToString(X_LIVEAGENT_AFFINITY));
if (X_LIVEAGENT_SEQUENCE != null)
      localVarHeaderParams.put("X-LIVEAGENT-SEQUENCE", apiClient.parameterToString(X_LIVEAGENT_SEQUENCE));
if (xGatewayProcessId != null)
      localVarHeaderParams.put("x-gateway-process-id", apiClient.parameterToString(xGatewayProcessId));
if (xGatewayRequestId != null)
      localVarHeaderParams.put("x-gateway-request-id", apiClient.parameterToString(xGatewayRequestId));

    
    final String[] localVarAccepts = {
      
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };


    apiClient.invokeAPI(localVarPath, "PUT", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, null);
  }
  /**
   * 
   * Retorna todas as mensagens enviadas entre agentes e visitantes de bate-papo durante uma sessão de bate-papo.
   * @param clientId Identificador do Cliente utilizado na autenticação. (required)
   * @param accessToken Token de acesso utilizado na autenticação. (required)
   * @param X_LIVEAGENT_SESSION_KEY O ID exclusivo associado à sua sessão do LIve Agent. (required)
   * @param X_LIVEAGENT_API_VERSION A versão da API do Salesforce para a solicitação. (required)
   * @param X_LIVEAGENT_AFFINITY O ID gerado pelo sistema usado para identificar a sessão do Live Agent nos servidores do Live Agent (required)
   * @param xGatewayProcessId Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão. (required)
   * @param xGatewayRequestId Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador. (required)
   * @return Object
   * @throws ApiException if fails to make API call
   */
  public Object mensagensGet(String clientId, String accessToken, String X_LIVEAGENT_SESSION_KEY, Integer X_LIVEAGENT_API_VERSION, String X_LIVEAGENT_AFFINITY, String xGatewayProcessId, String xGatewayRequestId) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'clientId' is set
    if (clientId == null) {
      throw new ApiException(400, "Missing the required parameter 'clientId' when calling mensagensGet");
    }
    
    // verify the required parameter 'accessToken' is set
    if (accessToken == null) {
      throw new ApiException(400, "Missing the required parameter 'accessToken' when calling mensagensGet");
    }
    
    // verify the required parameter 'X_LIVEAGENT_SESSION_KEY' is set
    if (X_LIVEAGENT_SESSION_KEY == null) {
      throw new ApiException(400, "Missing the required parameter 'X_LIVEAGENT_SESSION_KEY' when calling mensagensGet");
    }
    
    // verify the required parameter 'X_LIVEAGENT_API_VERSION' is set
    if (X_LIVEAGENT_API_VERSION == null) {
      throw new ApiException(400, "Missing the required parameter 'X_LIVEAGENT_API_VERSION' when calling mensagensGet");
    }
    
    // verify the required parameter 'X_LIVEAGENT_AFFINITY' is set
    if (X_LIVEAGENT_AFFINITY == null) {
      throw new ApiException(400, "Missing the required parameter 'X_LIVEAGENT_AFFINITY' when calling mensagensGet");
    }
    
    // verify the required parameter 'xGatewayProcessId' is set
    if (xGatewayProcessId == null) {
      throw new ApiException(400, "Missing the required parameter 'xGatewayProcessId' when calling mensagensGet");
    }
    
    // verify the required parameter 'xGatewayRequestId' is set
    if (xGatewayRequestId == null) {
      throw new ApiException(400, "Missing the required parameter 'xGatewayRequestId' when calling mensagensGet");
    }
    
    // create path and map variables
    String localVarPath = "/mensagens";

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    if (clientId != null)
      localVarHeaderParams.put("client_id", apiClient.parameterToString(clientId));
if (accessToken != null)
      localVarHeaderParams.put("access_token", apiClient.parameterToString(accessToken));
if (X_LIVEAGENT_SESSION_KEY != null)
      localVarHeaderParams.put("X-LIVEAGENT-SESSION-KEY", apiClient.parameterToString(X_LIVEAGENT_SESSION_KEY));
if (X_LIVEAGENT_API_VERSION != null)
      localVarHeaderParams.put("X-LIVEAGENT-API-VERSION", apiClient.parameterToString(X_LIVEAGENT_API_VERSION));
if (X_LIVEAGENT_AFFINITY != null)
      localVarHeaderParams.put("X-LIVEAGENT-AFFINITY", apiClient.parameterToString(X_LIVEAGENT_AFFINITY));
if (xGatewayProcessId != null)
      localVarHeaderParams.put("x-gateway-process-id", apiClient.parameterToString(xGatewayProcessId));
if (xGatewayRequestId != null)
      localVarHeaderParams.put("x-gateway-request-id", apiClient.parameterToString(xGatewayRequestId));

    
    final String[] localVarAccepts = {
      
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<Object> localVarReturnType = new GenericType<Object>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * 
   * Finaliza uma sessão
   * @param clientId Identificador do Cliente utilizado na autenticação. (required)
   * @param accessToken Token de acesso utilizado na autenticação. (required)
   * @param chavesessao Chave da sessão (required)
   * @param X_LIVEAGENT_API_VERSION Versão da APi do Live Agent (required)
   * @param X_LIVEAGENT_AFFINITY Affinity do Live Agent (required)
   * @param xGatewayProcessId Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão. (required)
   * @param xGatewayRequestId Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador. (required)
   * @throws ApiException if fails to make API call
   */
  public void sessaoChavesessaoDelete(String clientId, String accessToken, String chavesessao, Integer X_LIVEAGENT_API_VERSION, String X_LIVEAGENT_AFFINITY, String xGatewayProcessId, String xGatewayRequestId) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'clientId' is set
    if (clientId == null) {
      throw new ApiException(400, "Missing the required parameter 'clientId' when calling sessaoChavesessaoDelete");
    }
    
    // verify the required parameter 'accessToken' is set
    if (accessToken == null) {
      throw new ApiException(400, "Missing the required parameter 'accessToken' when calling sessaoChavesessaoDelete");
    }
    
    // verify the required parameter 'chavesessao' is set
    if (chavesessao == null) {
      throw new ApiException(400, "Missing the required parameter 'chavesessao' when calling sessaoChavesessaoDelete");
    }
    
    // verify the required parameter 'X_LIVEAGENT_API_VERSION' is set
    if (X_LIVEAGENT_API_VERSION == null) {
      throw new ApiException(400, "Missing the required parameter 'X_LIVEAGENT_API_VERSION' when calling sessaoChavesessaoDelete");
    }
    
    // verify the required parameter 'X_LIVEAGENT_AFFINITY' is set
    if (X_LIVEAGENT_AFFINITY == null) {
      throw new ApiException(400, "Missing the required parameter 'X_LIVEAGENT_AFFINITY' when calling sessaoChavesessaoDelete");
    }
    
    // verify the required parameter 'xGatewayProcessId' is set
    if (xGatewayProcessId == null) {
      throw new ApiException(400, "Missing the required parameter 'xGatewayProcessId' when calling sessaoChavesessaoDelete");
    }
    
    // verify the required parameter 'xGatewayRequestId' is set
    if (xGatewayRequestId == null) {
      throw new ApiException(400, "Missing the required parameter 'xGatewayRequestId' when calling sessaoChavesessaoDelete");
    }
    
    // create path and map variables
    String localVarPath = "/sessao/{chavesessao}"
      .replaceAll("\\{" + "chavesessao" + "\\}", apiClient.escapeString(chavesessao.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    if (clientId != null)
      localVarHeaderParams.put("client_id", apiClient.parameterToString(clientId));
if (accessToken != null)
      localVarHeaderParams.put("access_token", apiClient.parameterToString(accessToken));
if (X_LIVEAGENT_API_VERSION != null)
      localVarHeaderParams.put("X-LIVEAGENT-API-VERSION", apiClient.parameterToString(X_LIVEAGENT_API_VERSION));
if (X_LIVEAGENT_AFFINITY != null)
      localVarHeaderParams.put("X-LIVEAGENT-AFFINITY", apiClient.parameterToString(X_LIVEAGENT_AFFINITY));
if (xGatewayProcessId != null)
      localVarHeaderParams.put("x-gateway-process-id", apiClient.parameterToString(xGatewayProcessId));
if (xGatewayRequestId != null)
      localVarHeaderParams.put("x-gateway-request-id", apiClient.parameterToString(xGatewayRequestId));

    
    final String[] localVarAccepts = {
      
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };


    apiClient.invokeAPI(localVarPath, "DELETE", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, null);
  }
  /**
   * 
   * Inicia uma sessão.
   * @param clientId Identificador do Cliente utilizado na autenticação. (required)
   * @param accessToken Token de acesso utilizado na autenticação. (required)
   * @param X_LIVEAGENT_API_VERSION A versão da API do Salesforce para a solicitação. (required)
   * @param X_LIVEAGENT_AFFINITY O ID gerado pelo sistema usado para identificar a sessão do Live Agent nos servidores do Live Agent (required)
   * @param xGatewayProcessId Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão. (required)
   * @param xGatewayRequestId Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador. (required)
   * @return Sessao
   * @throws ApiException if fails to make API call
   */
  public Sessao sessaoGet(String clientId, String accessToken, Integer X_LIVEAGENT_API_VERSION, String X_LIVEAGENT_AFFINITY, String xGatewayProcessId, String xGatewayRequestId) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'clientId' is set
    if (clientId == null) {
      throw new ApiException(400, "Missing the required parameter 'clientId' when calling sessaoGet");
    }
    
    // verify the required parameter 'accessToken' is set
    if (accessToken == null) {
      throw new ApiException(400, "Missing the required parameter 'accessToken' when calling sessaoGet");
    }
    
    // verify the required parameter 'X_LIVEAGENT_API_VERSION' is set
    if (X_LIVEAGENT_API_VERSION == null) {
      throw new ApiException(400, "Missing the required parameter 'X_LIVEAGENT_API_VERSION' when calling sessaoGet");
    }
    
    // verify the required parameter 'X_LIVEAGENT_AFFINITY' is set
    if (X_LIVEAGENT_AFFINITY == null) {
      throw new ApiException(400, "Missing the required parameter 'X_LIVEAGENT_AFFINITY' when calling sessaoGet");
    }
    
    // verify the required parameter 'xGatewayProcessId' is set
    if (xGatewayProcessId == null) {
      throw new ApiException(400, "Missing the required parameter 'xGatewayProcessId' when calling sessaoGet");
    }
    
    // verify the required parameter 'xGatewayRequestId' is set
    if (xGatewayRequestId == null) {
      throw new ApiException(400, "Missing the required parameter 'xGatewayRequestId' when calling sessaoGet");
    }
    
    // create path and map variables
    String localVarPath = "/sessao";

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    if (clientId != null)
      localVarHeaderParams.put("client_id", apiClient.parameterToString(clientId));
if (accessToken != null)
      localVarHeaderParams.put("access_token", apiClient.parameterToString(accessToken));
if (X_LIVEAGENT_API_VERSION != null)
      localVarHeaderParams.put("X-LIVEAGENT-API-VERSION", apiClient.parameterToString(X_LIVEAGENT_API_VERSION));
if (X_LIVEAGENT_AFFINITY != null)
      localVarHeaderParams.put("X-LIVEAGENT-AFFINITY", apiClient.parameterToString(X_LIVEAGENT_AFFINITY));
if (xGatewayProcessId != null)
      localVarHeaderParams.put("x-gateway-process-id", apiClient.parameterToString(xGatewayProcessId));
if (xGatewayRequestId != null)
      localVarHeaderParams.put("x-gateway-request-id", apiClient.parameterToString(xGatewayRequestId));

    
    final String[] localVarAccepts = {
      
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<Sessao> localVarReturnType = new GenericType<Sessao>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
}
