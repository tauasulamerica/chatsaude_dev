/*
 * API de Manifestação
 * API com operações para gerenciar Manifestações
 *
 * OpenAPI spec version: 1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.swagger.client.salesforce.manifestacao.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * DiasUteisSegunda
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2017-08-23T15:25:36.303-03:00")
public class DiasUteisSegunda {
  @JsonProperty("inicio")
  private String inicio = null;

  @JsonProperty("fim")
  private String fim = null;

  public DiasUteisSegunda inicio(String inicio) {
    this.inicio = inicio;
    return this;
  }

   /**
   * Get inicio
   * @return inicio
  **/
  @ApiModelProperty(example = "08:30", value = "")
  public String getInicio() {
    return inicio;
  }

  public void setInicio(String inicio) {
    this.inicio = inicio;
  }

  public DiasUteisSegunda fim(String fim) {
    this.fim = fim;
    return this;
  }

   /**
   * Get fim
   * @return fim
  **/
  @ApiModelProperty(example = "1080", value = "")
  public String getFim() {
    return fim;
  }

  public void setFim(String fim) {
    this.fim = fim;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DiasUteisSegunda diasUteisSegunda = (DiasUteisSegunda) o;
    return Objects.equals(this.inicio, diasUteisSegunda.inicio) &&
        Objects.equals(this.fim, diasUteisSegunda.fim);
  }

  @Override
  public int hashCode() {
    return Objects.hash(inicio, fim);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DiasUteisSegunda {\n");
    
    sb.append("    inicio: ").append(toIndentedString(inicio)).append("\n");
    sb.append("    fim: ").append(toIndentedString(fim)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

