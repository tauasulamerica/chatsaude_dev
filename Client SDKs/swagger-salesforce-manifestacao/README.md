# swagger-java-client

## Requirements

Building the API client library requires [Maven](https://maven.apache.org/) to be installed.

## Installation

To install the API client library to your local Maven repository, simply execute:

```shell
mvn install
```

To deploy it to a remote Maven repository instead, configure the settings of the repository and execute:

```shell
mvn deploy
```

Refer to the [official documentation](https://maven.apache.org/plugins/maven-deploy-plugin/usage.html) for more information.

### Maven users

Add this dependency to your project's POM:

```xml
<dependency>
    <groupId>io.swagger</groupId>
    <artifactId>swagger-java-client</artifactId>
    <version>1.0.0</version>
    <scope>compile</scope>
</dependency>
```

### Gradle users

Add this dependency to your project's build file:

```groovy
compile "io.swagger:swagger-java-client:1.0.0"
```

### Others

At first generate the JAR by executing:

    mvn package

Then manually install the following JARs:

* target/swagger-java-client-1.0.0.jar
* target/lib/*.jar

## Getting Started

Please follow the [installation](#installation) instruction and execute the following Java code:

```java

import io.swagger.client.*;
import io.swagger.client.auth.*;
import io.swagger.client.model.*;
import io.swagger.client.api.LiveAgentApi;

import java.io.File;
import java.util.*;

public class LiveAgentApiExample {

    public static void main(String[] args) {
        
        LiveAgentApi apiInstance = new LiveAgentApi();
        String clientId = "clientId_example"; // String | Identificador do Cliente utilizado na autenticação.
        String accessToken = "accessToken_example"; // String | Token de acesso utilizado na autenticação.
        Integer X_LIVEAGENT_API_VERSION = 56; // Integer | A versão da API do Salesforce para a solicitação.
        String xGatewayProcessId = "xGatewayProcessId_example"; // String | Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão.
        String xGatewayRequestId = "xGatewayRequestId_example"; // String | Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador.
        String identificadorOrganizacao = "identificadorOrganizacao_example"; // String | O Identificador da organização do Salesforce associada à implantação do Live Agent.
        String identificadorImplantacao = "identificadorImplantacao_example"; // String | O Identificador de 15 dígitos da implantação do Live Agent da qual a solicitação de cgat foi iniciada.
        String identificadoresDisponibilidade = "identificadoresDisponibilidade_example"; // String | Uma matriz de IDs de objeto para os quais é possível verificar a disponibilidade.
        try {
            Object result = apiInstance.agentesDisponiveisGet(clientId, accessToken, X_LIVEAGENT_API_VERSION, xGatewayProcessId, xGatewayRequestId, identificadorOrganizacao, identificadorImplantacao, identificadoresDisponibilidade);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling LiveAgentApi#agentesDisponiveisGet");
            e.printStackTrace();
        }
    }
}

```

## Documentation for API Endpoints

All URIs are relative to *http://apisulamerica.sensedia.com/auto/corretor/chat/bot/salesforce/api/v1/dev*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*LiveAgentApi* | [**agentesDisponiveisGet**](docs/LiveAgentApi.md#agentesDisponiveisGet) | **GET** /agentes/disponiveis | 
*LiveAgentApi* | [**conversasDelete**](docs/LiveAgentApi.md#conversasDelete) | **DELETE** /conversas | 
*LiveAgentApi* | [**conversasPost**](docs/LiveAgentApi.md#conversasPost) | **POST** /conversas | 
*LiveAgentApi* | [**conversasPut**](docs/LiveAgentApi.md#conversasPut) | **PUT** /conversas | 
*LiveAgentApi* | [**mensagensGet**](docs/LiveAgentApi.md#mensagensGet) | **GET** /mensagens | 
*LiveAgentApi* | [**sessaoChavesessaoDelete**](docs/LiveAgentApi.md#sessaoChavesessaoDelete) | **DELETE** /sessao/{chavesessao} | 
*LiveAgentApi* | [**sessaoGet**](docs/LiveAgentApi.md#sessaoGet) | **GET** /sessao | 
*ManifestacaoApi* | [**atendimentosExcecoesDataHoraGet**](docs/ManifestacaoApi.md#atendimentosExcecoesDataHoraGet) | **GET** /atendimentos/excecoes/{dataHora} | 
*ManifestacaoApi* | [**atendimentosGet**](docs/ManifestacaoApi.md#atendimentosGet) | **GET** /atendimentos | 
*ManifestacaoApi* | [**corretoresContasGet**](docs/ManifestacaoApi.md#corretoresContasGet) | **GET** /corretores/contas | 
*ManifestacaoApi* | [**corretoresGet**](docs/ManifestacaoApi.md#corretoresGet) | **GET** /corretores | 
*ManifestacaoApi* | [**manifestacoesArquivosPost**](docs/ManifestacaoApi.md#manifestacoesArquivosPost) | **POST** /manifestacoes/arquivos | 
*ManifestacaoApi* | [**manifestacoesDescricaoGet**](docs/ManifestacaoApi.md#manifestacoesDescricaoGet) | **GET** /manifestacoes/descricao | 
*ManifestacaoApi* | [**manifestacoesDescricaoLayoutsGet**](docs/ManifestacaoApi.md#manifestacoesDescricaoLayoutsGet) | **GET** /manifestacoes/descricao/layouts | 
*ManifestacaoApi* | [**manifestacoesDescricaoLayoutsIdGet**](docs/ManifestacaoApi.md#manifestacoesDescricaoLayoutsIdGet) | **GET** /manifestacoes/descricao/layouts/{id} | 
*ManifestacaoApi* | [**manifestacoesPost**](docs/ManifestacaoApi.md#manifestacoesPost) | **POST** /manifestacoes | 
*ManifestacaoApi* | [**manifestacoesSubtiposGet**](docs/ManifestacaoApi.md#manifestacoesSubtiposGet) | **GET** /manifestacoes/subtipos | 
*OAuthApi* | [**oauthAccessTokenPost**](docs/OAuthApi.md#oauthAccessTokenPost) | **POST** /oauth/access-token | 


## Documentation for Models

 - [AccessToken](docs/AccessToken.md)
 - [Atendimento](docs/Atendimento.md)
 - [ButtonDetail](docs/ButtonDetail.md)
 - [ButtonLayout](docs/ButtonLayout.md)
 - [Campos](docs/Campos.md)
 - [Color](docs/Color.md)
 - [Columns](docs/Columns.md)
 - [Conversa](docs/Conversa.md)
 - [Corretor](docs/Corretor.md)
 - [CorretorConta](docs/CorretorConta.md)
 - [Descrio](docs/Descrio.md)
 - [DetailLayout](docs/DetailLayout.md)
 - [DetelhesPreChat](docs/DetelhesPreChat.md)
 - [DiasUteis](docs/DiasUteis.md)
 - [DiasUteisDomingo](docs/DiasUteisDomingo.md)
 - [DiasUteisSegunda](docs/DiasUteisSegunda.md)
 - [Entidade](docs/Entidade.md)
 - [Error](docs/Error.md)
 - [ErrorAuthorization](docs/ErrorAuthorization.md)
 - [ErrorServer](docs/ErrorServer.md)
 - [EscopoSuportado](docs/EscopoSuportado.md)
 - [Excecao](docs/Excecao.md)
 - [FinalConversa](docs/FinalConversa.md)
 - [Icons](docs/Icons.md)
 - [InformaoRegistro](docs/InformaoRegistro.md)
 - [IniciarConversa](docs/IniciarConversa.md)
 - [LayoutComponents](docs/LayoutComponents.md)
 - [LayoutItems](docs/LayoutItems.md)
 - [LayoutRow](docs/LayoutRow.md)
 - [Layouts](docs/Layouts.md)
 - [LayoutsDetailed](docs/LayoutsDetailed.md)
 - [Manifestacao](docs/Manifestacao.md)
 - [MapaEntidade](docs/MapaEntidade.md)
 - [PicklistValue](docs/PicklistValue.md)
 - [QuickActionList](docs/QuickActionList.md)
 - [QuickActionListItems](docs/QuickActionListItems.md)
 - [Record](docs/Record.md)
 - [Relacoes](docs/Relacoes.md)
 - [RelatedContent](docs/RelatedContent.md)
 - [RelatedContentItems](docs/RelatedContentItems.md)
 - [RelatedLists](docs/RelatedLists.md)
 - [RetornoManifestacao](docs/RetornoManifestacao.md)
 - [Sessao](docs/Sessao.md)
 - [Sort](docs/Sort.md)
 - [SubTipo](docs/SubTipo.md)
 - [Url](docs/Url.md)
 - [Urls](docs/Urls.md)


## Documentation for Authorization

All endpoints do not require authorization.
Authentication schemes defined for the API:

## Recommendation

It's recommended to create an instance of `ApiClient` per thread in a multithreaded environment to avoid any potential issues.

## Author



