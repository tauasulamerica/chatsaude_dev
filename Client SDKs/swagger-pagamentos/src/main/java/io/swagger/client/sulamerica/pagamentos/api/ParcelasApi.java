package io.swagger.client.sulamerica.pagamentos.api;

import io.swagger.client.sulamerica.pagamentos.ApiException;
import io.swagger.client.sulamerica.pagamentos.ApiClient;
import io.swagger.client.sulamerica.pagamentos.Configuration;
import io.swagger.client.sulamerica.pagamentos.Pair;

import javax.ws.rs.core.GenericType;

import io.swagger.client.sulamerica.pagamentos.model.Parcela;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2017-06-28T14:12:18.933-03:00")
public class ParcelasApi {
  private ApiClient apiClient;

  public ParcelasApi() {
    this(Configuration.getDefaultApiClient());
  }

  public ParcelasApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * 
   * Retorna as parcelas referentes a uma apólice ou endosso do segurado.
   * @param clientId Identificador do Cliente utilizado na autenticação. (required)
   * @param accessToken Token de acesso utilizado na autenticação. (required)
   * @param origem Código do consumidor. Aceita os valores MB(Mobile), EC(Espaço Cliente), PC(Portal do Corretor), CH(Chat) e UR(Ura). (required)
   * @param cpfCnpj Número do CPF ou CNPJ do segurado. (required)
   * @param numeroApolice Número da apólice do segurado. (required)
   * @param numeroEndosso Número do endosso relativo à apólice do segurado. (optional)
   * @param codigoSucursal Código da sucursal. (optional)
   * @param codigoProdutor Código do produtor. (optional)
   * @param numeroContrato Número do contrato em caso de frota. (optional)
   * @return List&lt;Parcela&gt;
   * @throws ApiException if fails to make API call
   */
  public List<Parcela> getParcelas(String clientId, String accessToken, String origem, String cpfCnpj, String numeroApolice, String numeroEndosso, String codigoSucursal, String codigoProdutor, String numeroContrato) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'clientId' is set
    if (clientId == null) {
      throw new ApiException(400, "Missing the required parameter 'clientId' when calling getParcelas");
    }
    
    // verify the required parameter 'accessToken' is set
    if (accessToken == null) {
      throw new ApiException(400, "Missing the required parameter 'accessToken' when calling getParcelas");
    }
    
    // verify the required parameter 'origem' is set
    if (origem == null) {
      throw new ApiException(400, "Missing the required parameter 'origem' when calling getParcelas");
    }
    
    // verify the required parameter 'cpfCnpj' is set
    if (cpfCnpj == null) {
      throw new ApiException(400, "Missing the required parameter 'cpfCnpj' when calling getParcelas");
    }
    
    // verify the required parameter 'numeroApolice' is set
    if (numeroApolice == null) {
      throw new ApiException(400, "Missing the required parameter 'numeroApolice' when calling getParcelas");
    }
    
    // create path and map variables
    String localVarPath = "/parcelas/auto".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();

    localVarQueryParams.addAll(apiClient.parameterToPairs("", "origem", origem));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "cpfCnpj", cpfCnpj));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "numeroApolice", numeroApolice));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "numeroEndosso", numeroEndosso));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "codigoSucursal", codigoSucursal));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "codigoProdutor", codigoProdutor));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "numeroContrato", numeroContrato));

    if (clientId != null)
      localVarHeaderParams.put("client_id", apiClient.parameterToString(clientId));
if (accessToken != null)
      localVarHeaderParams.put("access_token", apiClient.parameterToString(accessToken));

    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      "application/json"
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<List<Parcela>> localVarReturnType = new GenericType<List<Parcela>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
}
