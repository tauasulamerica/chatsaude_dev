package io.swagger.client.sulamerica.pagamentos.api;

import io.swagger.client.sulamerica.pagamentos.ApiException;
import io.swagger.client.sulamerica.pagamentos.ApiClient;
import io.swagger.client.sulamerica.pagamentos.Configuration;
import io.swagger.client.sulamerica.pagamentos.Pair;

import javax.ws.rs.core.GenericType;

import io.swagger.client.sulamerica.pagamentos.model.BoletoRequest;
import io.swagger.client.sulamerica.pagamentos.model.BoletoResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2017-06-28T14:12:18.933-03:00")
public class BoletosApi {
  private ApiClient apiClient;

  public BoletosApi() {
    this(Configuration.getDefaultApiClient());
  }

  public BoletosApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * 
   * Gera segunda via de um boleto do canal auto
   * @param clientId Identificador do Cliente utilizado na autenticação. (required)
   * @param accessToken Token de acesso utilizado na autenticação. (required)
   * @param tipoRetorno Tipo de Retorno. Aceita os valores (1 - Gera segunda via de um boleto referente a uma parcela do segurado no formato PDF; 2 - Gera segunda via de um boleto referente a uma parcela do segurado no formato JPG; 3 -Gera a linha digitável da segunda via de um boleto referente a uma parcela do segurado gerada; ou 4 -Gera o link da segunda via de um boleto referente a uma parcela do segurado gerada) (required)
   * @param data Objeto JSON que encapsula os parâmetros para a requisição (required)
   * @return BoletoResponse
   * @throws ApiException if fails to make API call
   */
  public BoletoResponse postBoletosSegundavia(String clientId, String accessToken, String tipoRetorno, BoletoRequest data) throws ApiException {
    Object localVarPostBody = data;
    
    // verify the required parameter 'clientId' is set
    if (clientId == null) {
      throw new ApiException(400, "Missing the required parameter 'clientId' when calling postBoletosSegundavia");
    }
    
    // verify the required parameter 'accessToken' is set
    if (accessToken == null) {
      throw new ApiException(400, "Missing the required parameter 'accessToken' when calling postBoletosSegundavia");
    }
    
    // verify the required parameter 'tipoRetorno' is set
    if (tipoRetorno == null) {
      throw new ApiException(400, "Missing the required parameter 'tipoRetorno' when calling postBoletosSegundavia");
    }
    
    // verify the required parameter 'data' is set
    if (data == null) {
      throw new ApiException(400, "Missing the required parameter 'data' when calling postBoletosSegundavia");
    }
    
    // create path and map variables
    String localVarPath = "/boletos/auto/segundaVia".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();

    localVarQueryParams.addAll(apiClient.parameterToPairs("", "tipoRetorno", tipoRetorno));

    if (clientId != null)
      localVarHeaderParams.put("client_id", apiClient.parameterToString(clientId));
if (accessToken != null)
      localVarHeaderParams.put("access_token", apiClient.parameterToString(accessToken));

    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      "application/json"
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<BoletoResponse> localVarReturnType = new GenericType<BoletoResponse>() {};
    return apiClient.invokeAPI(localVarPath, "POST", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
}
