# ParcelasApi

All URIs are relative to *http://apisulamerica.sensedia.com/dev/pagamentos/api/v1/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getParcelas**](ParcelasApi.md#getParcelas) | **GET** /parcelas/auto | 


<a name="getParcelas"></a>
# **getParcelas**
> List&lt;Parcela&gt; getParcelas(clientId, accessToken, origem, cpfCnpj, numeroApolice, numeroEndosso, codigoSucursal, codigoProdutor, numeroContrato)



Retorna as parcelas referentes a uma apólice ou endosso do segurado.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.ParcelasApi;


ParcelasApi apiInstance = new ParcelasApi();
String clientId = "clientId_example"; // String | Identificador do Cliente utilizado na autenticação.
String accessToken = "accessToken_example"; // String | Token de acesso utilizado na autenticação.
String origem = "origem_example"; // String | Código do consumidor. Aceita os valores MB(Mobile), EC(Espaço Cliente), PC(Portal do Corretor), CH(Chat) e UR(Ura).
String cpfCnpj = "cpfCnpj_example"; // String | Número do CPF ou CNPJ do segurado.
String numeroApolice = "numeroApolice_example"; // String | Número da apólice do segurado.
String numeroEndosso = "numeroEndosso_example"; // String | Número do endosso relativo à apólice do segurado.
String codigoSucursal = "codigoSucursal_example"; // String | Código da sucursal.
String codigoProdutor = "codigoProdutor_example"; // String | Código do produtor.
String numeroContrato = "numeroContrato_example"; // String | Número do contrato em caso de frota.
try {
    List<Parcela> result = apiInstance.getParcelas(clientId, accessToken, origem, cpfCnpj, numeroApolice, numeroEndosso, codigoSucursal, codigoProdutor, numeroContrato);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ParcelasApi#getParcelas");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**| Identificador do Cliente utilizado na autenticação. |
 **accessToken** | **String**| Token de acesso utilizado na autenticação. |
 **origem** | **String**| Código do consumidor. Aceita os valores MB(Mobile), EC(Espaço Cliente), PC(Portal do Corretor), CH(Chat) e UR(Ura). | [enum: MB, EC, PC, CH, UR]
 **cpfCnpj** | **String**| Número do CPF ou CNPJ do segurado. |
 **numeroApolice** | **String**| Número da apólice do segurado. |
 **numeroEndosso** | **String**| Número do endosso relativo à apólice do segurado. | [optional]
 **codigoSucursal** | **String**| Código da sucursal. | [optional]
 **codigoProdutor** | **String**| Código do produtor. | [optional]
 **numeroContrato** | **String**| Número do contrato em caso de frota. | [optional]

### Return type

[**List&lt;Parcela&gt;**](Parcela.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

