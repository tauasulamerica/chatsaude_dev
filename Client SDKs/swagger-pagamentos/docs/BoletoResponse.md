
# BoletoResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**numeroApolice** | **Long** | Número da Apolice |  [optional]
**numeroEndosso** | **Long** | Número do Endosso |  [optional]
**numeroParcela** | **Integer** | Número da Parcela |  [optional]
**pdf** | **String** | Boleto em PDF. Retornado quando o tipoRetorno for igual a 1 (PDF) |  [optional]
**base64Pdf** | **String** | Base 64 do boleto em PDF. Retornado quando o tipoRetorno for igual a 1 (PDF) |  [optional]
**image** | **String** | Imagem do Boleto. Retornado quando o tipoRetorno for igual a 2 (Image) |  [optional]
**ipte** | **String** | Linha digitável do boleto. Retornado quando o tipoRetorno for igual a 3 (Linha Digitável)  |  [optional]
**link** | **String** | Link do boleto. Retornado quando o tipoRetorno for igual a 4 (Link)  |  [optional]



