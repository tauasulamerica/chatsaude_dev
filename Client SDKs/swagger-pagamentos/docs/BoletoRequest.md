
# BoletoRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**origem** | **String** | Origem do Boleto | 
**sistemaOrigem** | **String** | Sistema de Origem | 
**numeroApolice** | **Long** | Número da Apolice | 
**cpfCnpj** | **String** | CPF/CNPJ | 
**numeroEndosso** | **Long** | Numero do Endosso |  [optional]
**numeroParcela** | **Integer** | Número da parcela | 
**codigoSucursal** | **Integer** | Código da Sucursal |  [optional]
**vencido** | **Boolean** | Status da Parcela | 
**dataVencimento** | **String** | Data de Vencimento | 



