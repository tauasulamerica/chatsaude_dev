# BoletosApi

All URIs are relative to *http://apisulamerica.sensedia.com/dev/pagamentos/api/v1/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**postBoletosSegundavia**](BoletosApi.md#postBoletosSegundavia) | **POST** /boletos/auto/segundaVia | 


<a name="postBoletosSegundavia"></a>
# **postBoletosSegundavia**
> BoletoResponse postBoletosSegundavia(clientId, accessToken, tipoRetorno, data)



Gera segunda via de um boleto do canal auto

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.BoletosApi;


BoletosApi apiInstance = new BoletosApi();
String clientId = "clientId_example"; // String | Identificador do Cliente utilizado na autenticação.
String accessToken = "accessToken_example"; // String | Token de acesso utilizado na autenticação.
String tipoRetorno = "tipoRetorno_example"; // String | Tipo de Retorno. Aceita os valores (1 - Gera segunda via de um boleto referente a uma parcela do segurado no formato PDF; 2 - Gera segunda via de um boleto referente a uma parcela do segurado no formato JPG; 3 -Gera a linha digitável da segunda via de um boleto referente a uma parcela do segurado gerada; ou 4 -Gera o link da segunda via de um boleto referente a uma parcela do segurado gerada)
BoletoRequest data = new BoletoRequest(); // BoletoRequest | Objeto JSON que encapsula os parâmetros para a requisição
try {
    BoletoResponse result = apiInstance.postBoletosSegundavia(clientId, accessToken, tipoRetorno, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BoletosApi#postBoletosSegundavia");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**| Identificador do Cliente utilizado na autenticação. |
 **accessToken** | **String**| Token de acesso utilizado na autenticação. |
 **tipoRetorno** | **String**| Tipo de Retorno. Aceita os valores (1 - Gera segunda via de um boleto referente a uma parcela do segurado no formato PDF; 2 - Gera segunda via de um boleto referente a uma parcela do segurado no formato JPG; 3 -Gera a linha digitável da segunda via de um boleto referente a uma parcela do segurado gerada; ou 4 -Gera o link da segunda via de um boleto referente a uma parcela do segurado gerada) |
 **data** | [**BoletoRequest**](BoletoRequest.md)| Objeto JSON que encapsula os parâmetros para a requisição |

### Return type

[**BoletoResponse**](BoletoResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

