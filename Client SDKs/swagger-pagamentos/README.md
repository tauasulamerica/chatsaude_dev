# swagger-java-client

## Requirements

Building the API client library requires [Maven](https://maven.apache.org/) to be installed.

## Installation

To install the API client library to your local Maven repository, simply execute:

```shell
mvn install
```

To deploy it to a remote Maven repository instead, configure the settings of the repository and execute:

```shell
mvn deploy
```

Refer to the [official documentation](https://maven.apache.org/plugins/maven-deploy-plugin/usage.html) for more information.

### Maven users

Add this dependency to your project's POM:

```xml
<dependency>
    <groupId>io.swagger</groupId>
    <artifactId>swagger-java-client</artifactId>
    <version>1.0.0</version>
    <scope>compile</scope>
</dependency>
```

### Gradle users

Add this dependency to your project's build file:

```groovy
compile "io.swagger:swagger-java-client:1.0.0"
```

### Others

At first generate the JAR by executing:

    mvn package

Then manually install the following JARs:

* target/swagger-java-client-1.0.0.jar
* target/lib/*.jar

## Getting Started

Please follow the [installation](#installation) instruction and execute the following Java code:

```java

import io.swagger.client.*;
import io.swagger.client.auth.*;
import io.swagger.client.model.*;
import io.swagger.client.api.BoletosApi;

import java.io.File;
import java.util.*;

public class BoletosApiExample {

    public static void main(String[] args) {
        
        BoletosApi apiInstance = new BoletosApi();
        String clientId = "clientId_example"; // String | Identificador do Cliente utilizado na autenticação.
        String accessToken = "accessToken_example"; // String | Token de acesso utilizado na autenticação.
        String tipoRetorno = "tipoRetorno_example"; // String | Tipo de Retorno. Aceita os valores (1 - Gera segunda via de um boleto referente a uma parcela do segurado no formato PDF; 2 - Gera segunda via de um boleto referente a uma parcela do segurado no formato JPG; 3 -Gera a linha digitável da segunda via de um boleto referente a uma parcela do segurado gerada; ou 4 -Gera o link da segunda via de um boleto referente a uma parcela do segurado gerada)
        BoletoRequest data = new BoletoRequest(); // BoletoRequest | Objeto JSON que encapsula os parâmetros para a requisição
        try {
            BoletoResponse result = apiInstance.postBoletosSegundavia(clientId, accessToken, tipoRetorno, data);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling BoletosApi#postBoletosSegundavia");
            e.printStackTrace();
        }
    }
}

```

## Documentation for API Endpoints

All URIs are relative to *http://apisulamerica.sensedia.com/dev/pagamentos/api/v1/*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*BoletosApi* | [**postBoletosSegundavia**](docs/BoletosApi.md#postBoletosSegundavia) | **POST** /boletos/auto/segundaVia | 
*OAuthApi* | [**oauthAccessTokenPost**](docs/OAuthApi.md#oauthAccessTokenPost) | **POST** /oauth/access-token | 
*ParcelasApi* | [**getParcelas**](docs/ParcelasApi.md#getParcelas) | **GET** /parcelas/auto | 


## Documentation for Models

 - [AccessToken](docs/AccessToken.md)
 - [BoletoRequest](docs/BoletoRequest.md)
 - [BoletoResponse](docs/BoletoResponse.md)
 - [Error](docs/Error.md)
 - [ErrorAuthorization](docs/ErrorAuthorization.md)
 - [ErrorServer](docs/ErrorServer.md)
 - [Parcela](docs/Parcela.md)


## Documentation for Authorization

All endpoints do not require authorization.
Authentication schemes defined for the API:

## Recommendation

It's recommended to create an instance of `ApiClient` per thread in a multithreaded environment to avoid any potential issues.

## Author



