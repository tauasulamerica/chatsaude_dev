# swagger-java-client

## Requirements

Building the API client library requires [Maven](https://maven.apache.org/) to be installed.

## Installation

To install the API client library to your local Maven repository, simply execute:

```shell
mvn install
```

To deploy it to a remote Maven repository instead, configure the settings of the repository and execute:

```shell
mvn deploy
```

Refer to the [official documentation](https://maven.apache.org/plugins/maven-deploy-plugin/usage.html) for more information.

### Maven users

Add this dependency to your project's POM:

```xml
<dependency>
    <groupId>io.swagger</groupId>
    <artifactId>swagger-java-client</artifactId>
    <version>1.0.0</version>
    <scope>compile</scope>
</dependency>
```

### Gradle users

Add this dependency to your project's build file:

```groovy
compile "io.swagger:swagger-java-client:1.0.0"
```

### Others

At first generate the JAR by executing:

    mvn package

Then manually install the following JARs:

* target/swagger-java-client-1.0.0.jar
* target/lib/*.jar

## Getting Started

Please follow the [installation](#installation) instruction and execute the following Java code:

```java

import io.swagger.client.*;
import io.swagger.client.auth.*;
import io.swagger.client.model.*;
import io.swagger.client.api.ApolicesApi;

import java.io.File;
import java.util.*;

public class ApolicesApiExample {

    public static void main(String[] args) {
        
        ApolicesApi apiInstance = new ApolicesApi();
        String clientId = "clientId_example"; // String | Identificador do Cliente utilizado na autenticação.
        String accessToken = "accessToken_example"; // String | Token de acesso utilizado na autenticação.
        String xGatewayProcessId = "xGatewayProcessId_example"; // String | Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão.
        String xGatewayRequestId = "xGatewayRequestId_example"; // String | Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador.
        String cpfCnpj = "cpfCnpj_example"; // String | CPF ou CNPJ do Pagador do Boleto.
        String dataEmissao = "dataEmissao_example"; // String | Data de Emissão do Boleto.
        String numeroApolice = "numeroApolice_example"; // String | Número da Apólice.
        String numeroEndosso = "numeroEndosso_example"; // String | Número do Endosso.
        String numeroParcela = "numeroParcela_example"; // String | Número de Parcelas.
        String placa = "placa_example"; // String | Placa do Automóvel.
        String numeroSucursal = "numeroSucursal_example"; // String | Número do Sucursal.
        String origem = "origem_example"; // String | Origem da Requisição.
        String codigoTipoMensagem = "codigoTipoMensagem_example"; // String | Código de Tipo de Mensagem.
        try {
            List<Apolice> result = apiInstance.apolicesGet(clientId, accessToken, xGatewayProcessId, xGatewayRequestId, cpfCnpj, dataEmissao, numeroApolice, numeroEndosso, numeroParcela, placa, numeroSucursal, origem, codigoTipoMensagem);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling ApolicesApi#apolicesGet");
            e.printStackTrace();
        }
    }
}

```

## Documentation for API Endpoints

All URIs are relative to *http://apisulamerica.sensedia.com/auto/documento/api/v1*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*ApolicesApi* | [**apolicesGet**](docs/ApolicesApi.md#apolicesGet) | **GET** /apolices | 
*ApolicesApi* | [**apolicesHistoricosGet**](docs/ApolicesApi.md#apolicesHistoricosGet) | **GET** /apolices/historicos | 
*BoletosApi* | [**boletosGet**](docs/BoletosApi.md#boletosGet) | **GET** /boletos | 
*DocumentosApi* | [**documentosCodigoNacNumeroDocumentoGet**](docs/DocumentosApi.md#documentosCodigoNacNumeroDocumentoGet) | **GET** /documentos/{codigoNac}/{numeroDocumento} | 
*DocumentosApi* | [**documentosGet**](docs/DocumentosApi.md#documentosGet) | **GET** /documentos | 
*EndossosApi* | [**endossosCodigoNacNumeroDocumentoGet**](docs/EndossosApi.md#endossosCodigoNacNumeroDocumentoGet) | **GET** /endossos/{codigoNac}/{numeroDocumento} | 
*OAuthApi* | [**oauthAccessTokenPost**](docs/OAuthApi.md#oauthAccessTokenPost) | **POST** /oauth/access-token | 
*PropostasApi* | [**propostasCodigoNacNumeroDocumentoGet**](docs/PropostasApi.md#propostasCodigoNacNumeroDocumentoGet) | **GET** /propostas/{codigoNac}/{numeroDocumento} | 


## Documentation for Models

 - [AccessToken](docs/AccessToken.md)
 - [Apolice](docs/Apolice.md)
 - [Boleto](docs/Boleto.md)
 - [Criticas](docs/Criticas.md)
 - [Documento](docs/Documento.md)
 - [DocumentoBasico](docs/DocumentoBasico.md)
 - [Error](docs/Error.md)
 - [ErrorAuthorization](docs/ErrorAuthorization.md)
 - [ErrorServer](docs/ErrorServer.md)
 - [Eventos](docs/Eventos.md)
 - [MotivoRecusa](docs/MotivoRecusa.md)


## Documentation for Authorization

All endpoints do not require authorization.
Authentication schemes defined for the API:

## Recommendation

It's recommended to create an instance of `ApiClient` per thread in a multithreaded environment to avoid any potential issue.

## Author



