/**
 * API de Documentos (Auto)
 * API com operações para consulta de Documentos
 *
 * OpenAPI spec version: 1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package io.swagger.client.api;

import org.junit.Test;

import io.swagger.client.sulamerica.auto.ApiException;
import io.swagger.client.sulamerica.auto.api.DocumentosApi;
import io.swagger.client.sulamerica.auto.model.Documento;
import io.swagger.client.sulamerica.auto.model.DocumentoBasico;
import io.swagger.client.sulamerica.auto.model.Error;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for DocumentosApi
 */
public class DocumentosApiTest {

    private final DocumentosApi api = new DocumentosApi();

    
    /**
     * 
     *
     * Consulta de Documentos.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void documentosCodigoNacNumeroDocumentoGetTest() throws ApiException {
        String clientId = null;
        String accessToken = null;
        String xGatewayProcessId = null;
        String xGatewayRequestId = null;
        String codigoNac = null;
        String numeroDocumento = null;
        // Documento response = api.documentosCodigoNacNumeroDocumentoGet(clientId, accessToken, xGatewayProcessId, xGatewayRequestId, codigoNac, numeroDocumento);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * Consulta de Documentos.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void documentosGetTest() throws ApiException {
        String clientId = null;
        String accessToken = null;
        String xGatewayProcessId = null;
        String xGatewayRequestId = null;
        String cpfCnpj = null;
        String tipoPesquisa = null;
        String companhia = null;
        String sucursal = null;
        String apolice = null;
        String endosso = null;
        String proposta = null;
        String sinistro = null;
        String pedidoEndosso = null;
        String codigoProdutor = null;
        // DocumentoBasico response = api.documentosGet(clientId, accessToken, xGatewayProcessId, xGatewayRequestId, cpfCnpj, tipoPesquisa, companhia, sucursal, apolice, endosso, proposta, sinistro, pedidoEndosso, codigoProdutor);

        // TODO: test validations
    }
    
}
