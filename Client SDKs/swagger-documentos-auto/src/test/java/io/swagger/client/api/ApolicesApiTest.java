/**
 * API de Documentos (Auto)
 * API com operações para consulta de Documentos
 *
 * OpenAPI spec version: 1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package io.swagger.client.api;

import org.junit.Test;

import io.swagger.client.sulamerica.auto.ApiException;
import io.swagger.client.sulamerica.auto.api.ApolicesApi;
import io.swagger.client.sulamerica.auto.model.Apolice;
import io.swagger.client.sulamerica.auto.model.Error;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for ApolicesApi
 */
public class ApolicesApiTest {

    private final ApolicesApi api = new ApolicesApi();

    
    /**
     * 
     *
     * Consulta de Apólices.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void apolicesGetTest() throws ApiException {
        String clientId = null;
        String accessToken = null;
        String xGatewayProcessId = null;
        String xGatewayRequestId = null;
        String cpfCnpj = null;
        String dataEmissao = null;
        String numeroApolice = null;
        String numeroEndosso = null;
        String numeroParcela = null;
        String placa = null;
        String numeroSucursal = null;
        String origem = null;
        String codigoTipoMensagem = null;
        // List<Apolice> response = api.apolicesGet(clientId, accessToken, xGatewayProcessId, xGatewayRequestId, cpfCnpj, dataEmissao, numeroApolice, numeroEndosso, numeroParcela, placa, numeroSucursal, origem, codigoTipoMensagem);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * Consulta o historico de Apolices.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void apolicesHistoricosGetTest() throws ApiException {
        String clientId = null;
        String accessToken = null;
        String xGatewayProcessId = null;
        String xGatewayRequestId = null;
        String cpfCnpj = null;
        String dataEmissao = null;
        String numeroApolice = null;
        String numeroEndosso = null;
        String numeroParcela = null;
        String placa = null;
        String numeroSucursal = null;
        String origem = null;
        String codigoTipoMensagem = null;
        // List<Apolice> response = api.apolicesHistoricosGet(clientId, accessToken, xGatewayProcessId, xGatewayRequestId, cpfCnpj, dataEmissao, numeroApolice, numeroEndosso, numeroParcela, placa, numeroSucursal, origem, codigoTipoMensagem);

        // TODO: test validations
    }
    
}
