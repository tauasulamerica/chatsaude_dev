package io.swagger.client.sulamerica.auto.api;

import javax.ws.rs.core.GenericType;

import io.swagger.client.sulamerica.auto.ApiClient;
import io.swagger.client.sulamerica.auto.ApiException;
import io.swagger.client.sulamerica.auto.Configuration;
import io.swagger.client.sulamerica.auto.Pair;
import io.swagger.client.sulamerica.auto.model.Apolice;
import io.swagger.client.sulamerica.auto.model.Error;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2017-05-03T15:44:06.294-03:00")
public class ApolicesApi {
  private ApiClient apiClient;

  public ApolicesApi() {
    this(Configuration.getDefaultApiClient());
  }

  public ApolicesApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * 
   * Consulta de Apólices.
   * @param clientId Identificador do Cliente utilizado na autenticação. (required)
   * @param accessToken Token de acesso utilizado na autenticação. (required)
   * @param xGatewayProcessId Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão. (required)
   * @param xGatewayRequestId Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador. (required)
   * @param cpfCnpj CPF ou CNPJ do Pagador do Boleto. (required)
   * @param dataEmissao Data de Emissão do Boleto. (optional)
   * @param numeroApolice Número da Apólice. (optional)
   * @param numeroEndosso Número do Endosso. (optional)
   * @param numeroParcela Número de Parcelas. (optional)
   * @param placa Placa do Automóvel. (optional)
   * @param numeroSucursal Número do Sucursal. (optional)
   * @param origem Origem da Requisição. (optional)
   * @param codigoTipoMensagem Código de Tipo de Mensagem. (optional)
   * @return List<Apolice>
   * @throws ApiException if fails to make API call
   */
  public List<Apolice> apolicesGet(String clientId, String accessToken, String xGatewayProcessId, String xGatewayRequestId, String cpfCnpj, String dataEmissao, String numeroApolice, String numeroEndosso, String numeroParcela, String placa, String numeroSucursal, String origem, String codigoTipoMensagem) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'clientId' is set
    if (clientId == null) {
      throw new ApiException(400, "Missing the required parameter 'clientId' when calling apolicesGet");
    }
    
    // verify the required parameter 'accessToken' is set
    if (accessToken == null) {
      throw new ApiException(400, "Missing the required parameter 'accessToken' when calling apolicesGet");
    }
    
    // verify the required parameter 'xGatewayProcessId' is set
    if (xGatewayProcessId == null) {
      throw new ApiException(400, "Missing the required parameter 'xGatewayProcessId' when calling apolicesGet");
    }
    
    // verify the required parameter 'xGatewayRequestId' is set
    if (xGatewayRequestId == null) {
      throw new ApiException(400, "Missing the required parameter 'xGatewayRequestId' when calling apolicesGet");
    }
    
    // verify the required parameter 'cpfCnpj' is set
    if (cpfCnpj == null) {
      throw new ApiException(400, "Missing the required parameter 'cpfCnpj' when calling apolicesGet");
    }
    
    // create path and map variables
    String localVarPath = "/apolices".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();

    localVarQueryParams.addAll(apiClient.parameterToPairs("", "cpfCnpj", cpfCnpj));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "dataEmissao", dataEmissao));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "numeroApolice", numeroApolice));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "numeroEndosso", numeroEndosso));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "numeroParcela", numeroParcela));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "placa", placa));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "numeroSucursal", numeroSucursal));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "origem", origem));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "codigoTipoMensagem", codigoTipoMensagem));

    if (clientId != null)
      localVarHeaderParams.put("client_id", apiClient.parameterToString(clientId));
if (accessToken != null)
      localVarHeaderParams.put("access_token", apiClient.parameterToString(accessToken));
if (xGatewayProcessId != null)
      localVarHeaderParams.put("x-gateway-process-id", apiClient.parameterToString(xGatewayProcessId));
if (xGatewayRequestId != null)
      localVarHeaderParams.put("x-gateway-request-id", apiClient.parameterToString(xGatewayRequestId));

    
    final String[] localVarAccepts = {
      
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<List<Apolice>> localVarReturnType = new GenericType<List<Apolice>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * 
   * Consulta o historico de Apolices.
   * @param clientId Identificador do Cliente utilizado na autenticação. (required)
   * @param accessToken Token de acesso utilizado na autenticação. (required)
   * @param xGatewayProcessId Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão. (required)
   * @param xGatewayRequestId Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador. (required)
   * @param cpfCnpj CPF ou CNPJ do Pagador do Boleto. (required)
   * @param dataEmissao Data de Emissão do Boleto. (optional)
   * @param numeroApolice Número da Apólice. (optional)
   * @param numeroEndosso Número do Endosso. (optional)
   * @param numeroParcela Número de Parcelas. (optional)
   * @param placa Placa do Automóvel. (optional)
   * @param numeroSucursal Número do Sucursal. (optional)
   * @param origem Origem da Requisição. (optional)
   * @param codigoTipoMensagem Código de Tipo de Mensagem. (optional)
   * @return List<Apolice>
   * @throws ApiException if fails to make API call
   */
  public List<Apolice> apolicesHistoricosGet(String clientId, String accessToken, String xGatewayProcessId, String xGatewayRequestId, String cpfCnpj, String dataEmissao, String numeroApolice, String numeroEndosso, String numeroParcela, String placa, String numeroSucursal, String origem, String codigoTipoMensagem) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'clientId' is set
    if (clientId == null) {
      throw new ApiException(400, "Missing the required parameter 'clientId' when calling apolicesHistoricosGet");
    }
    
    // verify the required parameter 'accessToken' is set
    if (accessToken == null) {
      throw new ApiException(400, "Missing the required parameter 'accessToken' when calling apolicesHistoricosGet");
    }
    
    // verify the required parameter 'xGatewayProcessId' is set
    if (xGatewayProcessId == null) {
      throw new ApiException(400, "Missing the required parameter 'xGatewayProcessId' when calling apolicesHistoricosGet");
    }
    
    // verify the required parameter 'xGatewayRequestId' is set
    if (xGatewayRequestId == null) {
      throw new ApiException(400, "Missing the required parameter 'xGatewayRequestId' when calling apolicesHistoricosGet");
    }
    
    // verify the required parameter 'cpfCnpj' is set
    if (cpfCnpj == null) {
      throw new ApiException(400, "Missing the required parameter 'cpfCnpj' when calling apolicesHistoricosGet");
    }
    
    // create path and map variables
    String localVarPath = "/apolices/historicos".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();

    localVarQueryParams.addAll(apiClient.parameterToPairs("", "cpfCnpj", cpfCnpj));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "dataEmissao", dataEmissao));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "numeroApolice", numeroApolice));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "numeroEndosso", numeroEndosso));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "numeroParcela", numeroParcela));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "placa", placa));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "numeroSucursal", numeroSucursal));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "origem", origem));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "codigoTipoMensagem", codigoTipoMensagem));

    if (clientId != null)
      localVarHeaderParams.put("client_id", apiClient.parameterToString(clientId));
if (accessToken != null)
      localVarHeaderParams.put("access_token", apiClient.parameterToString(accessToken));
if (xGatewayProcessId != null)
      localVarHeaderParams.put("x-gateway-process-id", apiClient.parameterToString(xGatewayProcessId));
if (xGatewayRequestId != null)
      localVarHeaderParams.put("x-gateway-request-id", apiClient.parameterToString(xGatewayRequestId));

    
    final String[] localVarAccepts = {
      
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<List<Apolice>> localVarReturnType = new GenericType<List<Apolice>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
}
