package io.swagger.client.sulamerica.auto.api;

import javax.ws.rs.core.GenericType;

import io.swagger.client.sulamerica.auto.ApiClient;
import io.swagger.client.sulamerica.auto.ApiException;
import io.swagger.client.sulamerica.auto.Configuration;
import io.swagger.client.sulamerica.auto.Pair;
import io.swagger.client.sulamerica.auto.model.Documento;
import io.swagger.client.sulamerica.auto.model.DocumentoBasico;
import io.swagger.client.sulamerica.auto.model.Error;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2017-05-03T15:44:06.294-03:00")
public class DocumentosApi {
  private ApiClient apiClient;

  public DocumentosApi() {
    this(Configuration.getDefaultApiClient());
  }

  public DocumentosApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * 
   * Consulta de Documentos.
   * @param clientId Identificador do Cliente utilizado na autenticação. (required)
   * @param accessToken Token de acesso utilizado na autenticação. (required)
   * @param xGatewayProcessId Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão. (required)
   * @param xGatewayRequestId Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador. (required)
   * @param codigoNac Código Nac. (required)
   * @param numeroDocumento Número do Documento. (required)
   * @return Documento
   * @throws ApiException if fails to make API call
   */
  public Documento documentosCodigoNacNumeroDocumentoGet(String clientId, String accessToken, String xGatewayProcessId, String xGatewayRequestId, String codigoNac, String numeroDocumento) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'clientId' is set
    if (clientId == null) {
      throw new ApiException(400, "Missing the required parameter 'clientId' when calling documentosCodigoNacNumeroDocumentoGet");
    }
    
    // verify the required parameter 'accessToken' is set
    if (accessToken == null) {
      throw new ApiException(400, "Missing the required parameter 'accessToken' when calling documentosCodigoNacNumeroDocumentoGet");
    }
    
    // verify the required parameter 'xGatewayProcessId' is set
    if (xGatewayProcessId == null) {
      throw new ApiException(400, "Missing the required parameter 'xGatewayProcessId' when calling documentosCodigoNacNumeroDocumentoGet");
    }
    
    // verify the required parameter 'xGatewayRequestId' is set
    if (xGatewayRequestId == null) {
      throw new ApiException(400, "Missing the required parameter 'xGatewayRequestId' when calling documentosCodigoNacNumeroDocumentoGet");
    }
    
    // verify the required parameter 'codigoNac' is set
    if (codigoNac == null) {
      throw new ApiException(400, "Missing the required parameter 'codigoNac' when calling documentosCodigoNacNumeroDocumentoGet");
    }
    
    // verify the required parameter 'numeroDocumento' is set
    if (numeroDocumento == null) {
      throw new ApiException(400, "Missing the required parameter 'numeroDocumento' when calling documentosCodigoNacNumeroDocumentoGet");
    }
    
    // create path and map variables
    String localVarPath = "/documentos/{codigoNac}/{numeroDocumento}".replaceAll("\\{format\\}","json")
      .replaceAll("\\{" + "codigoNac" + "\\}", apiClient.escapeString(codigoNac.toString()))
      .replaceAll("\\{" + "numeroDocumento" + "\\}", apiClient.escapeString(numeroDocumento.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    if (clientId != null)
      localVarHeaderParams.put("client_id", apiClient.parameterToString(clientId));
if (accessToken != null)
      localVarHeaderParams.put("access_token", apiClient.parameterToString(accessToken));
if (xGatewayProcessId != null)
      localVarHeaderParams.put("x-gateway-process-id", apiClient.parameterToString(xGatewayProcessId));
if (xGatewayRequestId != null)
      localVarHeaderParams.put("x-gateway-request-id", apiClient.parameterToString(xGatewayRequestId));

    
    final String[] localVarAccepts = {
      
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<Documento> localVarReturnType = new GenericType<Documento>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * 
   * Consulta de Documentos.
   * @param clientId Identificador do Cliente utilizado na autenticação. (required)
   * @param accessToken Token de acesso utilizado na autenticação. (required)
   * @param xGatewayProcessId Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão. (required)
   * @param xGatewayRequestId Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador. (required)
   * @param cpfCnpj CPF ou CNPJ do Pagador do Boleto. (optional)
   * @param tipoPesquisa Tipo de Pesquisa. (optional)
   * @param companhia Número da Companhia. (optional)
   * @param sucursal Número do Surcusal. (optional)
   * @param apolice Número da Apólice. (optional)
   * @param endosso Número do Endosso. (optional)
   * @param proposta Código da Proposta. (optional)
   * @param sinistro Tipo de Sinistro. (optional)
   * @param pedidoEndosso Número de Pedido do Endosso. (optional)
   * @param codigoProdutor Código do Produtor. (optional)
   * @return DocumentoBasico
   * @throws ApiException if fails to make API call
   */
  public DocumentoBasico documentosGet(String clientId, String accessToken, String xGatewayProcessId, String xGatewayRequestId, String cpfCnpj, String tipoPesquisa, String companhia, String sucursal, String apolice, String endosso, String proposta, String sinistro, String pedidoEndosso, String codigoProdutor) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'clientId' is set
    if (clientId == null) {
      throw new ApiException(400, "Missing the required parameter 'clientId' when calling documentosGet");
    }
    
    // verify the required parameter 'accessToken' is set
    if (accessToken == null) {
      throw new ApiException(400, "Missing the required parameter 'accessToken' when calling documentosGet");
    }
    
    // verify the required parameter 'xGatewayProcessId' is set
    if (xGatewayProcessId == null) {
      throw new ApiException(400, "Missing the required parameter 'xGatewayProcessId' when calling documentosGet");
    }
    
    // verify the required parameter 'xGatewayRequestId' is set
    if (xGatewayRequestId == null) {
      throw new ApiException(400, "Missing the required parameter 'xGatewayRequestId' when calling documentosGet");
    }
    
    // create path and map variables
    String localVarPath = "/documentos".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();

    localVarQueryParams.addAll(apiClient.parameterToPairs("", "cpfCnpj", cpfCnpj));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "tipoPesquisa", tipoPesquisa));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "companhia", companhia));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "sucursal", sucursal));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "apolice", apolice));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "endosso", endosso));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "proposta", proposta));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "sinistro", sinistro));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "pedidoEndosso", pedidoEndosso));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "codigoProdutor", codigoProdutor));

    if (clientId != null)
      localVarHeaderParams.put("client_id", apiClient.parameterToString(clientId));
if (accessToken != null)
      localVarHeaderParams.put("access_token", apiClient.parameterToString(accessToken));
if (xGatewayProcessId != null)
      localVarHeaderParams.put("x-gateway-process-id", apiClient.parameterToString(xGatewayProcessId));
if (xGatewayRequestId != null)
      localVarHeaderParams.put("x-gateway-request-id", apiClient.parameterToString(xGatewayRequestId));

    
    final String[] localVarAccepts = {
      
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<DocumentoBasico> localVarReturnType = new GenericType<DocumentoBasico>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
}
