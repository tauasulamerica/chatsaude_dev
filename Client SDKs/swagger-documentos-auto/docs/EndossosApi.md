# EndossosApi

All URIs are relative to *http://apisulamerica.sensedia.com/auto/documento/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**endossosCodigoNacNumeroDocumentoGet**](EndossosApi.md#endossosCodigoNacNumeroDocumentoGet) | **GET** /endossos/{codigoNac}/{numeroDocumento} | 


<a name="endossosCodigoNacNumeroDocumentoGet"></a>
# **endossosCodigoNacNumeroDocumentoGet**
> Documento endossosCodigoNacNumeroDocumentoGet(clientId, accessToken, xGatewayProcessId, xGatewayRequestId, codigoNac, numeroDocumento)



Consulta de Endosso.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.EndossosApi;


EndossosApi apiInstance = new EndossosApi();
String clientId = "clientId_example"; // String | Identificador do Cliente utilizado na autenticação.
String accessToken = "accessToken_example"; // String | Token de acesso utilizado na autenticação.
String xGatewayProcessId = "xGatewayProcessId_example"; // String | Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão.
String xGatewayRequestId = "xGatewayRequestId_example"; // String | Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador.
String codigoNac = "codigoNac_example"; // String | Código Nac.
String numeroDocumento = "numeroDocumento_example"; // String | Número do Documento.
try {
    Documento result = apiInstance.endossosCodigoNacNumeroDocumentoGet(clientId, accessToken, xGatewayProcessId, xGatewayRequestId, codigoNac, numeroDocumento);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EndossosApi#endossosCodigoNacNumeroDocumentoGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**| Identificador do Cliente utilizado na autenticação. |
 **accessToken** | **String**| Token de acesso utilizado na autenticação. |
 **xGatewayProcessId** | **String**| Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão. |
 **xGatewayRequestId** | **String**| Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador. |
 **codigoNac** | **String**| Código Nac. |
 **numeroDocumento** | **String**| Número do Documento. |

### Return type

[**Documento**](Documento.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

