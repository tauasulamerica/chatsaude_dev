
# MotivoRecusa

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**descricaoRecusa** | **String** | Descrição do Motivo de Recusa |  [optional]
**codigoRecusa** | **Integer** | Código de Recusa |  [optional]



