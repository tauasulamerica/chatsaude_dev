
# Eventos

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dataEvento** | **String** | Data e Hora do Evento (DD/MM/AAAA HH:MM:SS) |  [optional]
**descricaoEvento** | **String** | Descrição do Evento |  [optional]
**codigoEvento** | **Integer** | Código do Evento |  [optional]



