
# Apolice

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**numeroEndosso** | **Integer** | Número do Endosso |  [optional]
**numeroSucursal** | **Integer** | Número da Surcusal |  [optional]
**dataEmissao** | **String** | Data de Emissão da Apólice |  [optional]
**numeroApolice** | **Integer** | Número da Apólice |  [optional]
**numeroPlaca** | **String** | Número da Placa do Automóvel |  [optional]
**dataNacimento** | **String** | Data de Nascimento (DD/MM/AAAA) |  [optional]
**carterinha** | **String** | Link para Download da Carterinha |  [optional]
**apolice** | **String** | Link para Downlaod da Apólice |  [optional]



