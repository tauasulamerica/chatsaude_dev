
# DocumentoBasico

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**numeroDocumento** | **Integer** | Número do Documento |  [optional]
**numeroCadastroNegocio** | **String** | Número da Apólice |  [optional]
**siglaCarteiraRamo** | **String** | Sigla da Carteira do Ramo |  [optional]
**codigoProduto** | **Integer** | Código do Produto |  [optional]
**codigoSucursal** | **Integer** | Código da Surcusal |  [optional]
**numeroContrato** | **Integer** | Número do Contrato |  [optional]
**dataEmissao** | **String** | Data de Emissão do Documento |  [optional]
**codigoCompanhia** | **Integer** | Código da Companhia |  [optional]
**siglatipoCobranca** | **String** | Sigla do Tipo de Cobrança |  [optional]
**codigoEstruturaVenda** | **Integer** | Código da Estrutura de Venda |  [optional]
**tipoContrato** | **String** | Tipo de Contrato |  [optional]
**siglaSituacao** | **String** | Sigla da Situação do Documento (RC - Recusado, EM - Emitido, PE - Pendente, CN - Cancelado, CR - Criticado, PP - Pagamento Pendente, AR - A Renovar, RB - Reabilitada) |  [optional]
**nomeSegurado** | **String** | Nome do Segurado |  [optional]
**flagAlteracaoCompanhia** | **String** | Flag de Alteração de Companhia |  [optional]
**dataSituacao** | **String** | Data da Última Mudança de Situação |  [optional]
**nomeConvenioOrigem** | **String** | Nome do Convênio de Origem |  [optional]
**numeroDocumentoOrigem** | **Integer** | Número do Documento de Origem |  [optional]
**dataInicioVigencia** | **String** | Data o Inicio de Vigência |  [optional]
**siglaTipoDocumento** | **String** | Sigla do Tipo de Documento |  [optional]
**dataFimVigencia** | **String** | Data do Fim da Vigência |  [optional]
**valorParcela** | **Double** | Valor da Parcela |  [optional]
**dataVencimento** | **String** | Data de Vencimento do Documento |  [optional]
**dataAtualizacaoInfomacoes** | **String** | Data de Atualização das Informações do Documento |  [optional]
**numeroApoliceVigente** | **Integer** | Número da Apolice Vigente |  [optional]
**nomeEstipulante** | **String** | Nome do Estipulante |  [optional]
**numeroParcelas** | **Integer** | Número de Parcelas |  [optional]
**dataSituacaoDocumento** | **String** | Data da Situação do Documento |  [optional]
**siglaSistema** | **String** | Sigla do Sistema |  [optional]
**descricaoCoberturaSeguro** | **String** | Descrição da Cobertura do Seguro |  [optional]
**numeroEndosso** | **Integer** | Número do Endosso |  [optional]
**numeroApolice** | **Integer** | Número da Apólice |  [optional]
**motivoCancelado** | **String** | Motivo do Cancelamento do Documento |  [optional]
**placaCompleta** | **String** | Placa Completa do Veículo |  [optional]
**valorRestituicao** | **String** | Valor da Restituição |  [optional]
**motivoRecusa** | **String** | Motivo da Recusa |  [optional]



