# BoletosApi

All URIs are relative to *http://apisulamerica.sensedia.com/auto/documento/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**boletosGet**](BoletosApi.md#boletosGet) | **GET** /boletos | 


<a name="boletosGet"></a>
# **boletosGet**
> List&lt;Boleto&gt; boletosGet(clientId, accessToken, cpfCnpj, xGatewayProcessId, xGatewayRequestId, dataEmissao, numeroApolice, numeroEndosso, numeroParcela, placa, numeroSucursal, origem, codigoTipoMensagem)



Consulta de 2° Via de Boletos.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.BoletosApi;


BoletosApi apiInstance = new BoletosApi();
String clientId = "clientId_example"; // String | Identificador do Cliente utilizado na autenticação.
String accessToken = "accessToken_example"; // String | Token de acesso utilizado na autenticação.
String cpfCnpj = "cpfCnpj_example"; // String | CPF ou CNPJ do Pagador do Boleto.
String xGatewayProcessId = "xGatewayProcessId_example"; // String | Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão.
String xGatewayRequestId = "xGatewayRequestId_example"; // String | Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador.
String dataEmissao = "dataEmissao_example"; // String | Data de Emissão do Boleto (DD/MM/AA).
String numeroApolice = "numeroApolice_example"; // String | Número da Apólice.
String numeroEndosso = "numeroEndosso_example"; // String | Número do Endosso.
String numeroParcela = "numeroParcela_example"; // String | Número de Parcelas.
String placa = "placa_example"; // String | Placa do Automóvel.
String numeroSucursal = "numeroSucursal_example"; // String | Número do Sucursal.
String origem = "origem_example"; // String | Origem da Requisição.
String codigoTipoMensagem = "codigoTipoMensagem_example"; // String | Código de Tipo de Mensagem.
try {
    List<Boleto> result = apiInstance.boletosGet(clientId, accessToken, cpfCnpj, xGatewayProcessId, xGatewayRequestId, dataEmissao, numeroApolice, numeroEndosso, numeroParcela, placa, numeroSucursal, origem, codigoTipoMensagem);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BoletosApi#boletosGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**| Identificador do Cliente utilizado na autenticação. |
 **accessToken** | **String**| Token de acesso utilizado na autenticação. |
 **cpfCnpj** | **String**| CPF ou CNPJ do Pagador do Boleto. |
 **xGatewayProcessId** | **String**| Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão. |
 **xGatewayRequestId** | **String**| Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador. |
 **dataEmissao** | **String**| Data de Emissão do Boleto (DD/MM/AA). | [optional]
 **numeroApolice** | **String**| Número da Apólice. | [optional]
 **numeroEndosso** | **String**| Número do Endosso. | [optional]
 **numeroParcela** | **String**| Número de Parcelas. | [optional]
 **placa** | **String**| Placa do Automóvel. | [optional]
 **numeroSucursal** | **String**| Número do Sucursal. | [optional]
 **origem** | **String**| Origem da Requisição. | [optional]
 **codigoTipoMensagem** | **String**| Código de Tipo de Mensagem. | [optional]

### Return type

[**List&lt;Boleto&gt;**](Boleto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

