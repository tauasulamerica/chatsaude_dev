# DocumentosApi

All URIs are relative to *http://apisulamerica.sensedia.com/auto/documento/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**documentosCodigoNacNumeroDocumentoGet**](DocumentosApi.md#documentosCodigoNacNumeroDocumentoGet) | **GET** /documentos/{codigoNac}/{numeroDocumento} | 
[**documentosGet**](DocumentosApi.md#documentosGet) | **GET** /documentos | 


<a name="documentosCodigoNacNumeroDocumentoGet"></a>
# **documentosCodigoNacNumeroDocumentoGet**
> Documento documentosCodigoNacNumeroDocumentoGet(clientId, accessToken, xGatewayProcessId, xGatewayRequestId, codigoNac, numeroDocumento)



Consulta de Documentos.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DocumentosApi;


DocumentosApi apiInstance = new DocumentosApi();
String clientId = "clientId_example"; // String | Identificador do Cliente utilizado na autenticação.
String accessToken = "accessToken_example"; // String | Token de acesso utilizado na autenticação.
String xGatewayProcessId = "xGatewayProcessId_example"; // String | Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão.
String xGatewayRequestId = "xGatewayRequestId_example"; // String | Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador.
String codigoNac = "codigoNac_example"; // String | Código Nac.
String numeroDocumento = "numeroDocumento_example"; // String | Número do Documento.
try {
    Documento result = apiInstance.documentosCodigoNacNumeroDocumentoGet(clientId, accessToken, xGatewayProcessId, xGatewayRequestId, codigoNac, numeroDocumento);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DocumentosApi#documentosCodigoNacNumeroDocumentoGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**| Identificador do Cliente utilizado na autenticação. |
 **accessToken** | **String**| Token de acesso utilizado na autenticação. |
 **xGatewayProcessId** | **String**| Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão. |
 **xGatewayRequestId** | **String**| Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador. |
 **codigoNac** | **String**| Código Nac. |
 **numeroDocumento** | **String**| Número do Documento. |

### Return type

[**Documento**](Documento.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="documentosGet"></a>
# **documentosGet**
> DocumentoBasico documentosGet(clientId, accessToken, xGatewayProcessId, xGatewayRequestId, cpfCnpj, tipoPesquisa, companhia, sucursal, apolice, endosso, proposta, sinistro, pedidoEndosso, codigoProdutor)



Consulta de Documentos.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DocumentosApi;


DocumentosApi apiInstance = new DocumentosApi();
String clientId = "clientId_example"; // String | Identificador do Cliente utilizado na autenticação.
String accessToken = "accessToken_example"; // String | Token de acesso utilizado na autenticação.
String xGatewayProcessId = "xGatewayProcessId_example"; // String | Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão.
String xGatewayRequestId = "xGatewayRequestId_example"; // String | Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador.
String cpfCnpj = "cpfCnpj_example"; // String | CPF ou CNPJ do Pagador do Boleto.
String tipoPesquisa = "tipoPesquisa_example"; // String | Tipo de Pesquisa.
String companhia = "companhia_example"; // String | Número da Companhia.
String sucursal = "sucursal_example"; // String | Número do Surcusal.
String apolice = "apolice_example"; // String | Número da Apólice.
String endosso = "endosso_example"; // String | Número do Endosso.
String proposta = "proposta_example"; // String | Código da Proposta.
String sinistro = "sinistro_example"; // String | Tipo de Sinistro.
String pedidoEndosso = "pedidoEndosso_example"; // String | Número de Pedido do Endosso.
String codigoProdutor = "codigoProdutor_example"; // String | Código do Produtor.
try {
    DocumentoBasico result = apiInstance.documentosGet(clientId, accessToken, xGatewayProcessId, xGatewayRequestId, cpfCnpj, tipoPesquisa, companhia, sucursal, apolice, endosso, proposta, sinistro, pedidoEndosso, codigoProdutor);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DocumentosApi#documentosGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**| Identificador do Cliente utilizado na autenticação. |
 **accessToken** | **String**| Token de acesso utilizado na autenticação. |
 **xGatewayProcessId** | **String**| Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão. |
 **xGatewayRequestId** | **String**| Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador. |
 **cpfCnpj** | **String**| CPF ou CNPJ do Pagador do Boleto. | [optional]
 **tipoPesquisa** | **String**| Tipo de Pesquisa. | [optional]
 **companhia** | **String**| Número da Companhia. | [optional]
 **sucursal** | **String**| Número do Surcusal. | [optional]
 **apolice** | **String**| Número da Apólice. | [optional]
 **endosso** | **String**| Número do Endosso. | [optional]
 **proposta** | **String**| Código da Proposta. | [optional]
 **sinistro** | **String**| Tipo de Sinistro. | [optional]
 **pedidoEndosso** | **String**| Número de Pedido do Endosso. | [optional]
 **codigoProdutor** | **String**| Código do Produtor. | [optional]

### Return type

[**DocumentoBasico**](DocumentoBasico.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

