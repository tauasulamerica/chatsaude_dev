
# Boleto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**numeroEndosso** | **Integer** | Número do Endosso |  [optional]
**numeroSucursal** | **Integer** | Número da Surcusal |  [optional]
**dataVencimento** | **String** | Data de Vencimento do Boleto (DD/MM/AAAA) |  [optional]
**valorParcela** | **Double** | Valor da Parcela do Boleto |  [optional]
**cpfCnpj** | **String** | CPF ou CNPJ do Pagador |  [optional]
**numeroParcela** | **Integer** | Número de Parcelas do Boleto |  [optional]
**numeroApolice** | **Integer** | Número da Apólice |  [optional]
**segundaVia** | **String** | Link para baixar a 2° Via do Boleto |  [optional]



