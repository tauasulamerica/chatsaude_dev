# ApolicesApi

All URIs are relative to *http://apisulamerica.sensedia.com/auto/documento/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apolicesGet**](ApolicesApi.md#apolicesGet) | **GET** /apolices | 
[**apolicesHistoricosGet**](ApolicesApi.md#apolicesHistoricosGet) | **GET** /apolices/historicos | 


<a name="apolicesGet"></a>
# **apolicesGet**
> List&lt;Apolice&gt; apolicesGet(clientId, accessToken, xGatewayProcessId, xGatewayRequestId, cpfCnpj, dataEmissao, numeroApolice, numeroEndosso, numeroParcela, placa, numeroSucursal, origem, codigoTipoMensagem)



Consulta de Apólices.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.ApolicesApi;


ApolicesApi apiInstance = new ApolicesApi();
String clientId = "clientId_example"; // String | Identificador do Cliente utilizado na autenticação.
String accessToken = "accessToken_example"; // String | Token de acesso utilizado na autenticação.
String xGatewayProcessId = "xGatewayProcessId_example"; // String | Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão.
String xGatewayRequestId = "xGatewayRequestId_example"; // String | Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador.
String cpfCnpj = "cpfCnpj_example"; // String | CPF ou CNPJ do Pagador do Boleto.
String dataEmissao = "dataEmissao_example"; // String | Data de Emissão do Boleto.
String numeroApolice = "numeroApolice_example"; // String | Número da Apólice.
String numeroEndosso = "numeroEndosso_example"; // String | Número do Endosso.
String numeroParcela = "numeroParcela_example"; // String | Número de Parcelas.
String placa = "placa_example"; // String | Placa do Automóvel.
String numeroSucursal = "numeroSucursal_example"; // String | Número do Sucursal.
String origem = "origem_example"; // String | Origem da Requisição.
String codigoTipoMensagem = "codigoTipoMensagem_example"; // String | Código de Tipo de Mensagem.
try {
    List<Apolice> result = apiInstance.apolicesGet(clientId, accessToken, xGatewayProcessId, xGatewayRequestId, cpfCnpj, dataEmissao, numeroApolice, numeroEndosso, numeroParcela, placa, numeroSucursal, origem, codigoTipoMensagem);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ApolicesApi#apolicesGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**| Identificador do Cliente utilizado na autenticação. |
 **accessToken** | **String**| Token de acesso utilizado na autenticação. |
 **xGatewayProcessId** | **String**| Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão. |
 **xGatewayRequestId** | **String**| Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador. |
 **cpfCnpj** | **String**| CPF ou CNPJ do Pagador do Boleto. |
 **dataEmissao** | **String**| Data de Emissão do Boleto. | [optional]
 **numeroApolice** | **String**| Número da Apólice. | [optional]
 **numeroEndosso** | **String**| Número do Endosso. | [optional]
 **numeroParcela** | **String**| Número de Parcelas. | [optional]
 **placa** | **String**| Placa do Automóvel. | [optional]
 **numeroSucursal** | **String**| Número do Sucursal. | [optional]
 **origem** | **String**| Origem da Requisição. | [optional]
 **codigoTipoMensagem** | **String**| Código de Tipo de Mensagem. | [optional]

### Return type

[**List&lt;Apolice&gt;**](Apolice.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="apolicesHistoricosGet"></a>
# **apolicesHistoricosGet**
> List&lt;Apolice&gt; apolicesHistoricosGet(clientId, accessToken, xGatewayProcessId, xGatewayRequestId, cpfCnpj, dataEmissao, numeroApolice, numeroEndosso, numeroParcela, placa, numeroSucursal, origem, codigoTipoMensagem)



Consulta o historico de Apolices.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.ApolicesApi;


ApolicesApi apiInstance = new ApolicesApi();
String clientId = "clientId_example"; // String | Identificador do Cliente utilizado na autenticação.
String accessToken = "accessToken_example"; // String | Token de acesso utilizado na autenticação.
String xGatewayProcessId = "xGatewayProcessId_example"; // String | Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão.
String xGatewayRequestId = "xGatewayRequestId_example"; // String | Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador.
String cpfCnpj = "cpfCnpj_example"; // String | CPF ou CNPJ do Pagador do Boleto.
String dataEmissao = "dataEmissao_example"; // String | Data de Emissão do Boleto.
String numeroApolice = "numeroApolice_example"; // String | Número da Apólice.
String numeroEndosso = "numeroEndosso_example"; // String | Número do Endosso.
String numeroParcela = "numeroParcela_example"; // String | Número de Parcelas.
String placa = "placa_example"; // String | Placa do Automóvel.
String numeroSucursal = "numeroSucursal_example"; // String | Número do Sucursal.
String origem = "origem_example"; // String | Origem da Requisição.
String codigoTipoMensagem = "codigoTipoMensagem_example"; // String | Código de Tipo de Mensagem.
try {
    List<Apolice> result = apiInstance.apolicesHistoricosGet(clientId, accessToken, xGatewayProcessId, xGatewayRequestId, cpfCnpj, dataEmissao, numeroApolice, numeroEndosso, numeroParcela, placa, numeroSucursal, origem, codigoTipoMensagem);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ApolicesApi#apolicesHistoricosGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**| Identificador do Cliente utilizado na autenticação. |
 **accessToken** | **String**| Token de acesso utilizado na autenticação. |
 **xGatewayProcessId** | **String**| Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão. |
 **xGatewayRequestId** | **String**| Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador. |
 **cpfCnpj** | **String**| CPF ou CNPJ do Pagador do Boleto. |
 **dataEmissao** | **String**| Data de Emissão do Boleto. | [optional]
 **numeroApolice** | **String**| Número da Apólice. | [optional]
 **numeroEndosso** | **String**| Número do Endosso. | [optional]
 **numeroParcela** | **String**| Número de Parcelas. | [optional]
 **placa** | **String**| Placa do Automóvel. | [optional]
 **numeroSucursal** | **String**| Número do Sucursal. | [optional]
 **origem** | **String**| Origem da Requisição. | [optional]
 **codigoTipoMensagem** | **String**| Código de Tipo de Mensagem. | [optional]

### Return type

[**List&lt;Apolice&gt;**](Apolice.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

