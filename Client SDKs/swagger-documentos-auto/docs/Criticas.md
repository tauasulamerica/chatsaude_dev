
# Criticas

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**descricaoCritica** | **String** | Descrição da Crítica |  [optional]
**codigoCritica** | **String** | Código da Crítica |  [optional]



