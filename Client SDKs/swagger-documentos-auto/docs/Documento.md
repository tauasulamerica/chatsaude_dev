
# Documento

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**numeroApolice** | **Integer** | Número da Apólice |  [optional]
**cobertura** | **String** | Detalhes da Cobertura |  [optional]
**nomeConvenio** | **String** | Nome do Convênio |  [optional]
**numeroPedidoEndosso** | **Integer** | Número de Pedido do Endosso |  [optional]
**origem** | **String** | Origem do Pedido |  [optional]
**dataEmissao** | **String** | Data de Emissão (DD/MM/AAAA) |  [optional]
**eventos** | [**Eventos**](Eventos.md) |  |  [optional]
**numeroContrato** | **Integer** | Número do Contrato |  [optional]
**motivoRecusa** | [**MotivoRecusa**](MotivoRecusa.md) |  |  [optional]
**codigoSucursal** | **Integer** | Código da Surcusal |  [optional]
**dataInicioVigencia** | **String** | Data de Inicio da Vigência (DD/MM/AAAA) |  [optional]
**nomeEstipulante** | **String** | Nome do Estipulante |  [optional]
**numeroItemContrato** | **Integer** | Número de Itens no Contrato |  [optional]
**codigoEstruturaVenda** | **Integer** | Código da Estrutura de Venda |  [optional]
**descricaoMotivoCancelamento** | **String** | Descrição do Motivo de Cancelamento |  [optional]
**sliglaTipoDocumentoCobrado** | **String** | Tipo de Documento Cobrado |  [optional]
**numeroProtocoloBonus** | **Integer** | Número do Protocolo Bônus |  [optional]
**numeroEndosso** | **Integer** | Número do Endosso |  [optional]
**codigoCompanhia** | **Integer** | Código da Companhia |  [optional]
**dataFimVigencia** | **String** | Data do Fim da Vigencia (DD/MM/AAAA) |  [optional]
**flagPodeRecusar** | **Boolean** | Flag de Recusa Permitida |  [optional]
**dataRecusa** | **String** | Data da Recusa (DD/MM/AAAA) |  [optional]
**valorRestituicao** | **Double** | Valor de Restituição |  [optional]
**nomeSegurado** | **String** | Nome do Segurado |  [optional]
**siglaTipoDocumento** | **String** | Sigla do Tipo de Documento |  [optional]
**codigoProduto** | **Integer** | Código do Produto |  [optional]
**codigoConvenio** | **Integer** | Código do Convênio |  [optional]
**siglaCarteiraRamo** | **String** | Sigla da Carteira/Ramo |  [optional]
**dataSituacao** | **String** | Data da Situação (DD/MM/AAAA) |  [optional]
**valorPrimeiraParcela** | **Double** | Valor da Primeira Parcela |  [optional]
**dataPrevPrimeiraParcela** | **String** | Data prevista para a Primeira Parcela (DD/MM/AAAA) |  [optional]
**dataCritica** | **String** | Data Crítica (DD/MM/AAAA HH:MM:SS) |  [optional]
**nomeWorksite** | **String** | Nome do Worksite |  [optional]
**siglaSituacao** | **String** | Sigla da Situação (RC - Recusado, EM - Emitido, PE - Pendente, CN - Cancelado, CR - Criticado, PP - Pagamento Pendente, AR - A Renovar, RB - Reabilitada) |  [optional]
**criticas** | [**List&lt;Criticas&gt;**](Criticas.md) |  |  [optional]
**numeroProposta** | **Integer** | Número da Proposta |  [optional]



