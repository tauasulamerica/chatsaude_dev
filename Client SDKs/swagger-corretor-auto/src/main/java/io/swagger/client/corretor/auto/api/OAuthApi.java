package io.swagger.client.corretor.auto.api;

import javax.ws.rs.core.GenericType;

import io.swagger.client.corretor.auto.ApiClient;
import io.swagger.client.corretor.auto.ApiException;
import io.swagger.client.corretor.auto.Configuration;
import io.swagger.client.corretor.auto.Pair;
import io.swagger.client.corretor.auto.model.AccessToken;
import io.swagger.client.corretor.auto.model.Error;
import io.swagger.client.corretor.auto.model.ErrorAuthorization;
import io.swagger.client.corretor.auto.model.ErrorServer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2017-05-04T12:37:19.854-03:00")
public class OAuthApi {
  private ApiClient apiClient;

  public OAuthApi() {
    this(Configuration.getDefaultApiClient());
  }

  public OAuthApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * 
   * Gerar o access-token.
   * @param authorization Código de Autorização. (required)
   * @return AccessToken
   * @throws ApiException if fails to make API call
   */
  public AccessToken oauthAccessTokenPost(String authorization) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'authorization' is set
    if (authorization == null) {
      throw new ApiException(400, "Missing the required parameter 'authorization' when calling oauthAccessTokenPost");
    }
    
    // create path and map variables
    String localVarPath = "/oauth/access-token".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    if (authorization != null)
      localVarHeaderParams.put("Authorization", apiClient.parameterToString(authorization));

    
    final String[] localVarAccepts = {
      
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<AccessToken> localVarReturnType = new GenericType<AccessToken>() {};
    return apiClient.invokeAPI(localVarPath, "POST", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
}
