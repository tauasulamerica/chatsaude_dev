package io.swagger.client.corretor.auto.api;

import javax.ws.rs.core.GenericType;

import io.swagger.client.corretor.auto.ApiClient;
import io.swagger.client.corretor.auto.ApiException;
import io.swagger.client.corretor.auto.Configuration;
import io.swagger.client.corretor.auto.Pair;
import io.swagger.client.corretor.auto.model.CorrecaoStatus;
import io.swagger.client.corretor.auto.model.Error;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2017-05-04T12:37:19.854-03:00")
public class CorretoresApi {
  private ApiClient apiClient;

  public CorretoresApi() {
    this(Configuration.getDefaultApiClient());
  }

  public CorretoresApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * 
   * Verificação do corretor
   * @param clientId Identificador do Cliente utilizado na autenticação. (required)
   * @param accessToken Token de acesso utilizado na autenticação. (required)
   * @param codigoNac Código NAC (required)
   * @return CorrecaoStatus
   * @throws ApiException if fails to make API call
   */
  public CorrecaoStatus corretoresCodigoNacStatusGet(String clientId, String accessToken, String codigoNac) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'clientId' is set
    if (clientId == null) {
      throw new ApiException(400, "Missing the required parameter 'clientId' when calling corretoresCodigoNacStatusGet");
    }
    
    // verify the required parameter 'accessToken' is set
    if (accessToken == null) {
      throw new ApiException(400, "Missing the required parameter 'accessToken' when calling corretoresCodigoNacStatusGet");
    }
    
    // verify the required parameter 'codigoNac' is set
    if (codigoNac == null) {
      throw new ApiException(400, "Missing the required parameter 'codigoNac' when calling corretoresCodigoNacStatusGet");
    }
    
    // create path and map variables
    String localVarPath = "/corretores/{codigoNac}/status".replaceAll("\\{format\\}","json")
      .replaceAll("\\{" + "codigoNac" + "\\}", apiClient.escapeString(codigoNac.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    if (clientId != null)
      localVarHeaderParams.put("client_id", apiClient.parameterToString(clientId));
if (accessToken != null)
      localVarHeaderParams.put("access_token", apiClient.parameterToString(accessToken));

    
    final String[] localVarAccepts = {
      
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<CorrecaoStatus> localVarReturnType = new GenericType<CorrecaoStatus>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
}
