# swagger-java-client

## Requirements

Building the API client library requires [Maven](https://maven.apache.org/) to be installed.

## Installation

To install the API client library to your local Maven repository, simply execute:

```shell
mvn install
```

To deploy it to a remote Maven repository instead, configure the settings of the repository and execute:

```shell
mvn deploy
```

Refer to the [official documentation](https://maven.apache.org/plugins/maven-deploy-plugin/usage.html) for more information.

### Maven users

Add this dependency to your project's POM:

```xml
<dependency>
    <groupId>io.swagger</groupId>
    <artifactId>swagger-java-client</artifactId>
    <version>1.0.0</version>
    <scope>compile</scope>
</dependency>
```

### Gradle users

Add this dependency to your project's build file:

```groovy
compile "io.swagger:swagger-java-client:1.0.0"
```

### Others

At first generate the JAR by executing:

    mvn package

Then manually install the following JARs:

* target/swagger-java-client-1.0.0.jar
* target/lib/*.jar

## Getting Started

Please follow the [installation](#installation) instruction and execute the following Java code:

```java

import io.swagger.client.*;
import io.swagger.client.auth.*;
import io.swagger.client.model.*;
import io.swagger.client.api.CorretoresApi;

import java.io.File;
import java.util.*;

public class CorretoresApiExample {

    public static void main(String[] args) {
        
        CorretoresApi apiInstance = new CorretoresApi();
        String clientId = "clientId_example"; // String | Identificador do Cliente utilizado na autenticação.
        String accessToken = "accessToken_example"; // String | Token de acesso utilizado na autenticação.
        String codigoNac = "codigoNac_example"; // String | Código NAC
        try {
            CorrecaoStatus result = apiInstance.corretoresCodigoNacStatusGet(clientId, accessToken, codigoNac);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling CorretoresApi#corretoresCodigoNacStatusGet");
            e.printStackTrace();
        }
    }
}

```

## Documentation for API Endpoints

All URIs are relative to *http://apisulamerica.sensedia.com/auto/corretor/api/v1/dev*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*CorretoresApi* | [**corretoresCodigoNacStatusGet**](docs/CorretoresApi.md#corretoresCodigoNacStatusGet) | **GET** /corretores/{codigoNac}/status | 
*OAuthApi* | [**oauthAccessTokenPost**](docs/OAuthApi.md#oauthAccessTokenPost) | **POST** /oauth/access-token | 


## Documentation for Models

 - [AccessToken](docs/AccessToken.md)
 - [CorrecaoStatus](docs/CorrecaoStatus.md)
 - [Error](docs/Error.md)
 - [ErrorAuthorization](docs/ErrorAuthorization.md)
 - [ErrorServer](docs/ErrorServer.md)


## Documentation for Authorization

All endpoints do not require authorization.
Authentication schemes defined for the API:

## Recommendation

It's recommended to create an instance of `ApiClient` per thread in a multithreaded environment to avoid any potential issue.

## Author



