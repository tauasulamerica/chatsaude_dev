# CorretoresApi

All URIs are relative to *http://apisulamerica.sensedia.com/auto/corretor/api/v1/dev*

Method | HTTP request | Description
------------- | ------------- | -------------
[**corretoresCodigoNacStatusGet**](CorretoresApi.md#corretoresCodigoNacStatusGet) | **GET** /corretores/{codigoNac}/status | 


<a name="corretoresCodigoNacStatusGet"></a>
# **corretoresCodigoNacStatusGet**
> CorrecaoStatus corretoresCodigoNacStatusGet(clientId, accessToken, codigoNac)



Verificação do corretor

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.CorretoresApi;


CorretoresApi apiInstance = new CorretoresApi();
String clientId = "clientId_example"; // String | Identificador do Cliente utilizado na autenticação.
String accessToken = "accessToken_example"; // String | Token de acesso utilizado na autenticação.
String codigoNac = "codigoNac_example"; // String | Código NAC
try {
    CorrecaoStatus result = apiInstance.corretoresCodigoNacStatusGet(clientId, accessToken, codigoNac);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CorretoresApi#corretoresCodigoNacStatusGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**| Identificador do Cliente utilizado na autenticação. |
 **accessToken** | **String**| Token de acesso utilizado na autenticação. |
 **codigoNac** | **String**| Código NAC |

### Return type

[**CorrecaoStatus**](CorrecaoStatus.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

