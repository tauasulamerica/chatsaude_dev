# swagger-java-client

## Requirements

Building the API client library requires [Maven](https://maven.apache.org/) to be installed.

## Installation

To install the API client library to your local Maven repository, simply execute:

```shell
mvn install
```

To deploy it to a remote Maven repository instead, configure the settings of the repository and execute:

```shell
mvn deploy
```

Refer to the [official documentation](https://maven.apache.org/plugins/maven-deploy-plugin/usage.html) for more information.

### Maven users

Add this dependency to your project's POM:

```xml
<dependency>
    <groupId>io.swagger</groupId>
    <artifactId>swagger-java-client</artifactId>
    <version>1.0.0</version>
    <scope>compile</scope>
</dependency>
```

### Gradle users

Add this dependency to your project's build file:

```groovy
compile "io.swagger:swagger-java-client:1.0.0"
```

### Others

At first generate the JAR by executing:

    mvn package

Then manually install the following JARs:

* target/swagger-java-client-1.0.0.jar
* target/lib/*.jar

## Getting Started

Please follow the [installation](#installation) instruction and execute the following Java code:

```java

import io.swagger.client.sulamerica.pagamentosssaude.*;
import io.swagger.client.sulamerica.pagamentosssaude.auth.*;
import io.swagger.client.sulamerica.pagamentosssaude.model.*;
import io.swagger.client.sulamerica.pagamentosssaude.api.AutoApi;

import java.io.File;
import java.util.*;

public class AutoApiExample {

    public static void main(String[] args) {
        
        AutoApi apiInstance = new AutoApi();
        String clientId = "clientId_example"; // String | Identificador do Cliente utilizado na autenticação.
        String accessToken = "accessToken_example"; // String | Token de acesso utilizado na autenticação.
        String codigoSucursal = "codigoSucursal_example"; // String | Código da sucursal.
        String origem = "origem_example"; // String | Código do consumidor. Aceita os valores MB(Mobile), EC(Espaço Cliente), PC(Portal do Corretor), CH(Chat), PI(Portal Institucional), CX(Parceira Caixa Seguradora),  CM(CCM Center/Inspire) e UR(Ura).
        String numeroApolice = "numeroApolice_example"; // String | Número da apólice do segurado.<br/><b>É necessário informar este atributo caso não seja informado o Endosso.</b>
        String numeroEndosso = "numeroEndosso_example"; // String | Número do endosso relativo à apólice do segurado.<br/><b>É necessário informar este atributo caso não seja informada a Apólice.</b>
        String cpfCnpj = "cpfCnpj_example"; // String | Número do CPF ou CNPJ do segurado.
        String codigoProdutor = "codigoProdutor_example"; // String | Código do produtor.<br/><b>É necessário informar este atributo em casos de seguro FROTA.</b>
        String numeroContrato = "numeroContrato_example"; // String | Número do contrato.<br/><b>É necessário informar este atributo em casos de seguro FROTA.</b>
        try {
            List<Parcela> result = apiInstance.getParcelas(clientId, accessToken, codigoSucursal, origem, numeroApolice, numeroEndosso, cpfCnpj, codigoProdutor, numeroContrato);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling AutoApi#getParcelas");
            e.printStackTrace();
        }
    }
}

```

## Documentation for API Endpoints

All URIs are relative to *https://apisulamerica.sensedia.com/dev/pagamentos/api/v1*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*AutoApi* | [**getParcelas**](docs/AutoApi.md#getParcelas) | **GET** /parcelas/auto | 
*AutoApi* | [**postBoletosSegundavia**](docs/AutoApi.md#postBoletosSegundavia) | **GET** /boletos/auto/segundaVia | 
*MassificadosApi* | [**getParcelasMassificados**](docs/MassificadosApi.md#getParcelasMassificados) | **GET** /parcelas/massificados | 
*MassificadosApi* | [**postBoletosSegundaviaMassificados**](docs/MassificadosApi.md#postBoletosSegundaviaMassificados) | **GET** /boletos/massificados/segundaVia | 
*OAuthApi* | [**oauthAccessTokenPost**](docs/OAuthApi.md#oauthAccessTokenPost) | **POST** /oauth/access-token | 
*OdontoApi* | [**getBoletoOdonto**](docs/OdontoApi.md#getBoletoOdonto) | **GET** /boletos/odonto/segundaVia | 
*OdontoApi* | [**postCobrancadocumentoOdonto**](docs/OdontoApi.md#postCobrancadocumentoOdonto) | **GET** /documentos/odonto | 
*OdontoApi* | [**postCobrancapagamentoOdonto**](docs/OdontoApi.md#postCobrancapagamentoOdonto) | **GET** /parcelas/odonto | 
*PrevidenciaApi* | [**getBoletosEsporadicoPrevidencia**](docs/PrevidenciaApi.md#getBoletosEsporadicoPrevidencia) | **GET** /boletos/previdencia/esporadico | 
*PrevidenciaApi* | [**getCartaInadinplentesPrevidencia**](docs/PrevidenciaApi.md#getCartaInadinplentesPrevidencia) | **GET** /previdencia/inadimplentes/cartas | 
*PrevidenciaApi* | [**getFaturasPrevidencia**](docs/PrevidenciaApi.md#getFaturasPrevidencia) | **GET** /faturas/previdencia | 
*PrevidenciaApi* | [**getParcelasPrevidencia**](docs/PrevidenciaApi.md#getParcelasPrevidencia) | **GET** /parcelas/previdencia | 
*PrevidenciaApi* | [**postBoletosSegundaviaPrevidencia**](docs/PrevidenciaApi.md#postBoletosSegundaviaPrevidencia) | **GET** /boletos/previdencia/segundaVia | 
*SaudeApi* | [**getFaturasSaude**](docs/SaudeApi.md#getFaturasSaude) | **GET** /faturas/saude | 
*SaudeApi* | [**getParcelasSaude**](docs/SaudeApi.md#getParcelasSaude) | **GET** /parcelas/saude | 
*SaudeApi* | [**postBoletosSegundaviaSaude**](docs/SaudeApi.md#postBoletosSegundaviaSaude) | **GET** /boletos/saude/segundaVia | 
*VidaApi* | [**getCartaInadinplentesVida**](docs/VidaApi.md#getCartaInadinplentesVida) | **GET** /vida/inadimplentes/cartas | 
*VidaApi* | [**getParcelasVida**](docs/VidaApi.md#getParcelasVida) | **GET** /parcelas/vida | 
*VidaApi* | [**postBoletosSegundaviaVida**](docs/VidaApi.md#postBoletosSegundaviaVida) | **GET** /boletos/vida/segundaVia | 


## Documentation for Models

 - [AccessToken](docs/AccessToken.md)
 - [BoletoResponse](docs/BoletoResponse.md)
 - [DocumentoOdonto](docs/DocumentoOdonto.md)
 - [Error](docs/Error.md)
 - [ErrorAuthorization](docs/ErrorAuthorization.md)
 - [ErrorServer](docs/ErrorServer.md)
 - [FaturaPrevidencia](docs/FaturaPrevidencia.md)
 - [Parcela](docs/Parcela.md)
 - [ParcelaFatura](docs/ParcelaFatura.md)
 - [ParcelaIndividualSaude](docs/ParcelaIndividualSaude.md)
 - [ParcelaMassificados](docs/ParcelaMassificados.md)
 - [ParcelaPrevidencia](docs/ParcelaPrevidencia.md)
 - [ParcelaVida](docs/ParcelaVida.md)
 - [ParcelasOdonto](docs/ParcelasOdonto.md)


## Documentation for Authorization

All endpoints do not require authorization.
Authentication schemes defined for the API:

## Recommendation

It's recommended to create an instance of `ApiClient` per thread in a multithreaded environment to avoid any potential issues.

## Author



