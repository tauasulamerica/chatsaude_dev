# MassificadosApi

All URIs are relative to *https://apisulamerica.sensedia.com/dev/pagamentos/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getParcelasMassificados**](MassificadosApi.md#getParcelasMassificados) | **GET** /parcelas/massificados | 
[**postBoletosSegundaviaMassificados**](MassificadosApi.md#postBoletosSegundaviaMassificados) | **GET** /boletos/massificados/segundaVia | 


<a name="getParcelasMassificados"></a>
# **getParcelasMassificados**
> List&lt;ParcelaMassificados&gt; getParcelasMassificados(clientId, accessToken, origem, codigoCompanhia, codigoSucursal, codigoProduto, numeroApolice, numeroEndosso, cpfCnpj, numeroContrato)



Retorna as parcelas referentes a uma apólice ou endosso do segurado.

### Example
```java
// Import classes:
//import io.swagger.client.sulamerica.pagamentosssaude.ApiException;
//import io.swagger.client.sulamerica.pagamentosssaude.api.MassificadosApi;


MassificadosApi apiInstance = new MassificadosApi();
String clientId = "clientId_example"; // String | Identificador do Cliente utilizado na autenticação.
String accessToken = "accessToken_example"; // String | Token de acesso utilizado na autenticação.
String origem = "origem_example"; // String | Código do consumidor. Aceita os valores MB(Mobile), EC(Espaço Cliente), PC(Portal do Corretor), CH(Chat), PI(Portal Institucional), CX(Parceira Caixa Seguradora),  CM(CCM Center/Inspire) e UR(Ura).
String codigoCompanhia = "codigoCompanhia_example"; // String | Código da Companhia
String codigoSucursal = "codigoSucursal_example"; // String | Código da sucursal.
String codigoProduto = "codigoProduto_example"; // String | Código do produto.
String numeroApolice = "numeroApolice_example"; // String | Número da apólice do segurado.<br/><b>É necessário informar este atributo caso não seja informado o Endosso.</b>
String numeroEndosso = "numeroEndosso_example"; // String | Número do endosso relativo à apólice do segurado.<br/><b>É necessário informar este atributo caso não seja informada a Apólice.</b>
String cpfCnpj = "cpfCnpj_example"; // String | Número do CPF ou CNPJ
String numeroContrato = "numeroContrato_example"; // String | Número do Contrato
try {
    List<ParcelaMassificados> result = apiInstance.getParcelasMassificados(clientId, accessToken, origem, codigoCompanhia, codigoSucursal, codigoProduto, numeroApolice, numeroEndosso, cpfCnpj, numeroContrato);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MassificadosApi#getParcelasMassificados");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**| Identificador do Cliente utilizado na autenticação. |
 **accessToken** | **String**| Token de acesso utilizado na autenticação. |
 **origem** | **String**| Código do consumidor. Aceita os valores MB(Mobile), EC(Espaço Cliente), PC(Portal do Corretor), CH(Chat), PI(Portal Institucional), CX(Parceira Caixa Seguradora),  CM(CCM Center/Inspire) e UR(Ura). | [enum: MB, EC, PC, CH, UR, PI, CX, CM]
 **codigoCompanhia** | **String**| Código da Companhia |
 **codigoSucursal** | **String**| Código da sucursal. |
 **codigoProduto** | **String**| Código do produto. |
 **numeroApolice** | **String**| Número da apólice do segurado.&lt;br/&gt;&lt;b&gt;É necessário informar este atributo caso não seja informado o Endosso.&lt;/b&gt; | [optional]
 **numeroEndosso** | **String**| Número do endosso relativo à apólice do segurado.&lt;br/&gt;&lt;b&gt;É necessário informar este atributo caso não seja informada a Apólice.&lt;/b&gt; | [optional]
 **cpfCnpj** | **String**| Número do CPF ou CNPJ | [optional]
 **numeroContrato** | **String**| Número do Contrato | [optional]

### Return type

[**List&lt;ParcelaMassificados&gt;**](ParcelaMassificados.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postBoletosSegundaviaMassificados"></a>
# **postBoletosSegundaviaMassificados**
> BoletoResponse postBoletosSegundaviaMassificados(clientId, accessToken, tipoRetorno, origem, codigoCompanhia, codigoSucursal, contentEncoding, tipoRequisicao, sistemaOrigem, numeroParcela, codigoProduto, numeroApolice, numeroEndosso, dataVencimento, codigoLancamentoFinanceiro, codigoRamo)



Retorna a segunda via de um boleto do canal de Massificados

### Example
```java
// Import classes:
//import io.swagger.client.sulamerica.pagamentosssaude.ApiException;
//import io.swagger.client.sulamerica.pagamentosssaude.api.MassificadosApi;


MassificadosApi apiInstance = new MassificadosApi();
String clientId = "clientId_example"; // String | Identificador do Cliente utilizado na autenticação.
String accessToken = "accessToken_example"; // String | Token de acesso utilizado na autenticação.
String tipoRetorno = "tipoRetorno_example"; // String | Tipo de Retorno. Aceita os valores (1 - Retorna a segunda via de um boleto em PDF(application/pdf); 2 - Retorna a segunda via de um boleto em JPG (image/png); 3 -Retorna a linha digitável da segunda via de um boleto referente a uma parcela do segurado gerada (application/json). Deve estar compativel com o header Accept.
String origem = "origem_example"; // String | Código do consumidor. Aceita os valores MB(Mobile), EC(Espaço Cliente), PC(Portal do Corretor), CH(Chat), PI(Portal Institucional), CX(Parceira Caixa Seguradora),  CM(CCM Center/Inspire) e UR(Ura).
String codigoCompanhia = "codigoCompanhia_example"; // String | Código da companhia.
String codigoSucursal = "codigoSucursal_example"; // String | Código da sucursal.
String contentEncoding = "contentEncoding_example"; // String | Informa o encoding em que se deseja criptografar o response. (Valido para Boletos em PDF tipoRetorno =1) (Valor Valido - BASE64).
String tipoRequisicao = "tipoRequisicao_example"; // String | Tipo da requisição.<br/>Aceita os valores PME, GRP(Grupal) ou IND(Individual). <b> Obrigatório quando o  sistema origem for INS  </b>
String sistemaOrigem = "APM"; // String | Sigla do sistema origem (APM, SIR ou INS). Valor Default: APM
String numeroParcela = "numeroParcela_example"; // String | Número do endosso relativo à apólice do segurado. <b> Obrigatório quando o  sistema origem for APM ou SIR.  </b>
String codigoProduto = "codigoProduto_example"; // String | Código do produto.  <b> Obrigatório quando o  sistema origem for APM ou SIR.  </b>
String numeroApolice = "numeroApolice_example"; // String | Número da apólice do segurado.<br/><b>É necessário informar este atributo caso não seja informado o Endosso.</b>  <b> Obrigatório quando o  sistema origem for INS  </b>
String numeroEndosso = "numeroEndosso_example"; // String | Número do endosso relativo à apólice do segurado.<br/><b>É necessário informar este atributo caso não seja informada a Apólice.</b>
String dataVencimento = "dataVencimento_example"; // String | Data do vencimento da parcela  (DD/MM/AAAA).<br/><b>É necessário informar este atributo em casos de boleto vencido.</b>  <b> Obrigatório quando o  sistema origem for INS </b>
String codigoLancamentoFinanceiro = "codigoLancamentoFinanceiro_example"; // String | Código do Lançamento Financeiro.
String codigoRamo = "codigoRamo_example"; // String | Código do Ramo <b> Obrigatório quando o  sistema origem for INS </b>
try {
    BoletoResponse result = apiInstance.postBoletosSegundaviaMassificados(clientId, accessToken, tipoRetorno, origem, codigoCompanhia, codigoSucursal, contentEncoding, tipoRequisicao, sistemaOrigem, numeroParcela, codigoProduto, numeroApolice, numeroEndosso, dataVencimento, codigoLancamentoFinanceiro, codigoRamo);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MassificadosApi#postBoletosSegundaviaMassificados");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**| Identificador do Cliente utilizado na autenticação. |
 **accessToken** | **String**| Token de acesso utilizado na autenticação. |
 **tipoRetorno** | **String**| Tipo de Retorno. Aceita os valores (1 - Retorna a segunda via de um boleto em PDF(application/pdf); 2 - Retorna a segunda via de um boleto em JPG (image/png); 3 -Retorna a linha digitável da segunda via de um boleto referente a uma parcela do segurado gerada (application/json). Deve estar compativel com o header Accept. |
 **origem** | **String**| Código do consumidor. Aceita os valores MB(Mobile), EC(Espaço Cliente), PC(Portal do Corretor), CH(Chat), PI(Portal Institucional), CX(Parceira Caixa Seguradora),  CM(CCM Center/Inspire) e UR(Ura). | [enum: MB, EC, PC, CH, UR, PI, CX, CM]
 **codigoCompanhia** | **String**| Código da companhia. |
 **codigoSucursal** | **String**| Código da sucursal. |
 **contentEncoding** | **String**| Informa o encoding em que se deseja criptografar o response. (Valido para Boletos em PDF tipoRetorno &#x3D;1) (Valor Valido - BASE64). | [optional] [enum: BASE64]
 **tipoRequisicao** | **String**| Tipo da requisição.&lt;br/&gt;Aceita os valores PME, GRP(Grupal) ou IND(Individual). &lt;b&gt; Obrigatório quando o  sistema origem for INS  &lt;/b&gt; | [optional] [enum: PME, GRP, IND]
 **sistemaOrigem** | **String**| Sigla do sistema origem (APM, SIR ou INS). Valor Default: APM | [optional] [default to APM] [enum: APM, SIR, INS]
 **numeroParcela** | **String**| Número do endosso relativo à apólice do segurado. &lt;b&gt; Obrigatório quando o  sistema origem for APM ou SIR.  &lt;/b&gt; | [optional]
 **codigoProduto** | **String**| Código do produto.  &lt;b&gt; Obrigatório quando o  sistema origem for APM ou SIR.  &lt;/b&gt; | [optional]
 **numeroApolice** | **String**| Número da apólice do segurado.&lt;br/&gt;&lt;b&gt;É necessário informar este atributo caso não seja informado o Endosso.&lt;/b&gt;  &lt;b&gt; Obrigatório quando o  sistema origem for INS  &lt;/b&gt; | [optional]
 **numeroEndosso** | **String**| Número do endosso relativo à apólice do segurado.&lt;br/&gt;&lt;b&gt;É necessário informar este atributo caso não seja informada a Apólice.&lt;/b&gt; | [optional]
 **dataVencimento** | **String**| Data do vencimento da parcela  (DD/MM/AAAA).&lt;br/&gt;&lt;b&gt;É necessário informar este atributo em casos de boleto vencido.&lt;/b&gt;  &lt;b&gt; Obrigatório quando o  sistema origem for INS &lt;/b&gt; | [optional]
 **codigoLancamentoFinanceiro** | **String**| Código do Lançamento Financeiro. | [optional]
 **codigoRamo** | **String**| Código do Ramo &lt;b&gt; Obrigatório quando o  sistema origem for INS &lt;/b&gt; | [optional]

### Return type

[**BoletoResponse**](BoletoResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: image/png, application/json, application/pdf

