# PrevidenciaApi

All URIs are relative to *https://apisulamerica.sensedia.com/dev/pagamentos/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getBoletosEsporadicoPrevidencia**](PrevidenciaApi.md#getBoletosEsporadicoPrevidencia) | **GET** /boletos/previdencia/esporadico | 
[**getCartaInadinplentesPrevidencia**](PrevidenciaApi.md#getCartaInadinplentesPrevidencia) | **GET** /previdencia/inadimplentes/cartas | 
[**getFaturasPrevidencia**](PrevidenciaApi.md#getFaturasPrevidencia) | **GET** /faturas/previdencia | 
[**getParcelasPrevidencia**](PrevidenciaApi.md#getParcelasPrevidencia) | **GET** /parcelas/previdencia | 
[**postBoletosSegundaviaPrevidencia**](PrevidenciaApi.md#postBoletosSegundaviaPrevidencia) | **GET** /boletos/previdencia/segundaVia | 


<a name="getBoletosEsporadicoPrevidencia"></a>
# **getBoletosEsporadicoPrevidencia**
> BoletoResponse getBoletosEsporadicoPrevidencia(clientId, accessToken, tipoRetorno, sistemaOrigem, codigoProduto, dataVencimento, numeroProposta, valorAporte)



Retorna um boleto de aporte esporádico do canal de Previdencia.

### Example
```java
// Import classes:
//import io.swagger.client.sulamerica.pagamentosssaude.ApiException;
//import io.swagger.client.sulamerica.pagamentosssaude.api.PrevidenciaApi;


PrevidenciaApi apiInstance = new PrevidenciaApi();
String clientId = "clientId_example"; // String | Identificador do Cliente utilizado na autenticação.
String accessToken = "accessToken_example"; // String | Token de acesso utilizado na autenticação.
String tipoRetorno = "tipoRetorno_example"; // String | Tipo de Retorno. Aceita os valores (1 - Retorna a segunda via de um boleto em PDF(application/pdf); 2 - Retorna a segunda via de um boleto em JPG (image/jpg); 3 -Retorna a linha digitável da segunda via de um boleto referente a uma parcela do segurado gerada (application/json); 4 - Retorna a segunda via de um boleto em PNG (image/png);
String sistemaOrigem = "sistemaOrigem_example"; // String | Sigla do sistema origem (AVP, CVP)
String codigoProduto = "codigoProduto_example"; // String | Código do Produto
String dataVencimento = "dataVencimento_example"; // String | Data do vencimento do boleto.  (DD/MM/AAAA).
String numeroProposta = "numeroProposta_example"; // String | Número da Proposta
String valorAporte = "valorAporte_example"; // String | Valor do Aporte
try {
    BoletoResponse result = apiInstance.getBoletosEsporadicoPrevidencia(clientId, accessToken, tipoRetorno, sistemaOrigem, codigoProduto, dataVencimento, numeroProposta, valorAporte);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PrevidenciaApi#getBoletosEsporadicoPrevidencia");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**| Identificador do Cliente utilizado na autenticação. |
 **accessToken** | **String**| Token de acesso utilizado na autenticação. |
 **tipoRetorno** | **String**| Tipo de Retorno. Aceita os valores (1 - Retorna a segunda via de um boleto em PDF(application/pdf); 2 - Retorna a segunda via de um boleto em JPG (image/jpg); 3 -Retorna a linha digitável da segunda via de um boleto referente a uma parcela do segurado gerada (application/json); 4 - Retorna a segunda via de um boleto em PNG (image/png); |
 **sistemaOrigem** | **String**| Sigla do sistema origem (AVP, CVP) | [enum: AVP, CVP]
 **codigoProduto** | **String**| Código do Produto |
 **dataVencimento** | **String**| Data do vencimento do boleto.  (DD/MM/AAAA). |
 **numeroProposta** | **String**| Número da Proposta |
 **valorAporte** | **String**| Valor do Aporte |

### Return type

[**BoletoResponse**](BoletoResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/pdf

<a name="getCartaInadinplentesPrevidencia"></a>
# **getCartaInadinplentesPrevidencia**
> getCartaInadinplentesPrevidencia(clientId, accessToken, cpf, numeroProposta, codigoPlano)



Retorna a carta de inadimplência do beneficiario do canal de  previdência.

### Example
```java
// Import classes:
//import io.swagger.client.sulamerica.pagamentosssaude.ApiException;
//import io.swagger.client.sulamerica.pagamentosssaude.api.PrevidenciaApi;


PrevidenciaApi apiInstance = new PrevidenciaApi();
String clientId = "clientId_example"; // String | Identificador do Cliente utilizado na autenticação.
String accessToken = "accessToken_example"; // String | Token de acesso utilizado na autenticação.
String cpf = "cpf_example"; // String | CPF do Beneficiário
String numeroProposta = "numeroProposta_example"; // String | Número da Proposta
String codigoPlano = "codigoPlano_example"; // String | Código do Plano.
try {
    apiInstance.getCartaInadinplentesPrevidencia(clientId, accessToken, cpf, numeroProposta, codigoPlano);
} catch (ApiException e) {
    System.err.println("Exception when calling PrevidenciaApi#getCartaInadinplentesPrevidencia");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**| Identificador do Cliente utilizado na autenticação. |
 **accessToken** | **String**| Token de acesso utilizado na autenticação. |
 **cpf** | **String**| CPF do Beneficiário |
 **numeroProposta** | **String**| Número da Proposta |
 **codigoPlano** | **String**| Código do Plano. |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/pdf

<a name="getFaturasPrevidencia"></a>
# **getFaturasPrevidencia**
> List&lt;FaturaPrevidencia&gt; getFaturasPrevidencia(clientId, accessToken, sistemaOrigem, codigoConvenio, codigoFilial, codigoEstruturaVendas)



Retorna as faturas do canal de Previdência.

### Example
```java
// Import classes:
//import io.swagger.client.sulamerica.pagamentosssaude.ApiException;
//import io.swagger.client.sulamerica.pagamentosssaude.api.PrevidenciaApi;


PrevidenciaApi apiInstance = new PrevidenciaApi();
String clientId = "clientId_example"; // String | Identificador do Cliente utilizado na autenticação.
String accessToken = "accessToken_example"; // String | Token de acesso utilizado na autenticação.
String sistemaOrigem = "sistemaOrigem_example"; // String | Sigla do sistema origem (AVP ou CVP)
String codigoConvenio = "codigoConvenio_example"; // String | Código do Convênio
String codigoFilial = "codigoFilial_example"; // String | Código da Filial
String codigoEstruturaVendas = "codigoEstruturaVendas_example"; // String | Códigos das Estruturas de vendas. Separados por ,. ex: 12,34,44,12
try {
    List<FaturaPrevidencia> result = apiInstance.getFaturasPrevidencia(clientId, accessToken, sistemaOrigem, codigoConvenio, codigoFilial, codigoEstruturaVendas);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PrevidenciaApi#getFaturasPrevidencia");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**| Identificador do Cliente utilizado na autenticação. |
 **accessToken** | **String**| Token de acesso utilizado na autenticação. |
 **sistemaOrigem** | **String**| Sigla do sistema origem (AVP ou CVP) | [enum: AVP, CVP]
 **codigoConvenio** | **String**| Código do Convênio |
 **codigoFilial** | **String**| Código da Filial |
 **codigoEstruturaVendas** | **String**| Códigos das Estruturas de vendas. Separados por ,. ex: 12,34,44,12 | [optional]

### Return type

[**List&lt;FaturaPrevidencia&gt;**](FaturaPrevidencia.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getParcelasPrevidencia"></a>
# **getParcelasPrevidencia**
> List&lt;ParcelaPrevidencia&gt; getParcelasPrevidencia(clientId, accessToken, sistemaOrigem, codigoProduto, numeroProposta, codigoEstruturaVendas)



Retorna as parcelas referentes a uma apólice ou endosso do segurado do canal de Previdência.

### Example
```java
// Import classes:
//import io.swagger.client.sulamerica.pagamentosssaude.ApiException;
//import io.swagger.client.sulamerica.pagamentosssaude.api.PrevidenciaApi;


PrevidenciaApi apiInstance = new PrevidenciaApi();
String clientId = "clientId_example"; // String | Identificador do Cliente utilizado na autenticação.
String accessToken = "accessToken_example"; // String | Token de acesso utilizado na autenticação.
String sistemaOrigem = "sistemaOrigem_example"; // String | Sigla do sistema origem (AVP ou CVP)
String codigoProduto = "codigoProduto_example"; // String | Código do produto.
String numeroProposta = "numeroProposta_example"; // String | Número da proposta
String codigoEstruturaVendas = "codigoEstruturaVendas_example"; // String | Códigos das Estruturas de vendas. Separados por ,. ex: 12,34,44,12
try {
    List<ParcelaPrevidencia> result = apiInstance.getParcelasPrevidencia(clientId, accessToken, sistemaOrigem, codigoProduto, numeroProposta, codigoEstruturaVendas);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PrevidenciaApi#getParcelasPrevidencia");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**| Identificador do Cliente utilizado na autenticação. |
 **accessToken** | **String**| Token de acesso utilizado na autenticação. |
 **sistemaOrigem** | **String**| Sigla do sistema origem (AVP ou CVP) | [enum: AVP, CVP]
 **codigoProduto** | **String**| Código do produto. |
 **numeroProposta** | **String**| Número da proposta |
 **codigoEstruturaVendas** | **String**| Códigos das Estruturas de vendas. Separados por ,. ex: 12,34,44,12 | [optional]

### Return type

[**List&lt;ParcelaPrevidencia&gt;**](ParcelaPrevidencia.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postBoletosSegundaviaPrevidencia"></a>
# **postBoletosSegundaviaPrevidencia**
> BoletoResponse postBoletosSegundaviaPrevidencia(clientId, accessToken, tipoRetorno, sistemaOrigem, contentEncoding, tipoPessoa, numeroParcela, dataVencimento, codigoCompanhia, codigoEstruturaVendas, codigoConvenio, codigoFilial, numeroGrupoFaturamento, numeroSequenciaFatura, numeroProposta, codigoProduto, numeroCertificado, numeroIdentificador)



Retorna a segunda via de um boleto do canal de Previdencia

### Example
```java
// Import classes:
//import io.swagger.client.sulamerica.pagamentosssaude.ApiException;
//import io.swagger.client.sulamerica.pagamentosssaude.api.PrevidenciaApi;


PrevidenciaApi apiInstance = new PrevidenciaApi();
String clientId = "clientId_example"; // String | Identificador do Cliente utilizado na autenticação.
String accessToken = "accessToken_example"; // String | Token de acesso utilizado na autenticação.
String tipoRetorno = "tipoRetorno_example"; // String | Tipo de Retorno. Aceita os valores (1 - Retorna a segunda via de um boleto em PDF(application/pdf); 2 - Retorna a segunda via de um boleto em JPG (image/png); 3 -Retorna a linha digitável da segunda via de um boleto referente a uma parcela do segurado gerada (application/json). Deve estar compativel com o header Accept.
String sistemaOrigem = "sistemaOrigem_example"; // String | Sigla do sistema origem (AVP,  CVP, INS)
String contentEncoding = "contentEncoding_example"; // String | Informa o encoding em que se deseja criptografar o response. (Valido para Boletos em PDF tipoRetorno =1) (Valor Valido - BASE64).
String tipoPessoa = "tipoPessoa_example"; // String | Informa o tipo de Pessoa que está solicitando a segunda via do boleto. PF para pessoa física e PJ para pessoa jurídica <b> Obrigatório quando sistemaOrigem for AVP ou CVP</b>
String numeroParcela = "numeroParcela_example"; // String | Número da Parcela ou da Fatura que deseja gerar o Boleto. <b> Obrigatório quando sistemaOrigem for AVP ou CVP</b>
String dataVencimento = "dataVencimento_example"; // String | Data do vencimento da parcela.  (DD/MM/AAAA) <b> Obrigatório quando sistemaOrigem for AVP ou CVP</b>
String codigoCompanhia = "codigoCompanhia_example"; // String | Código da Companhia
String codigoEstruturaVendas = "codigoEstruturaVendas_example"; // String | Códigos das Estruturas de vendas. Separados por ,. ex: 12,34,44,12
String codigoConvenio = "codigoConvenio_example"; // String | Código do Convênio para <b>pessoa juridica</b>
String codigoFilial = "codigoFilial_example"; // String | Código da Filial para <b>pessoa juridica</b>
String numeroGrupoFaturamento = "numeroGrupoFaturamento_example"; // String | Numero do Grupo Faturamento para <b>pessoa juridica</b>
String numeroSequenciaFatura = "numeroSequenciaFatura_example"; // String | Numero de Sequencia da  <b>pessoa juridica</b>
String numeroProposta = "numeroProposta_example"; // String | Número da Proposta <b>pessoa fisica</b> .  <b> Obrigatório quando sistemaOrigem for INS</b>
String codigoProduto = "codigoProduto_example"; // String | Código do produto <b>pessoa fisica</b>
String numeroCertificado = "numeroCertificado_example"; // String | Numero de Certificado para <b>pessoa fisica</b>
String numeroIdentificador = "numeroIdentificador_example"; // String | Número do Identificador  <b> Obrigatório quando sistemaOrigem for INS</b>
try {
    BoletoResponse result = apiInstance.postBoletosSegundaviaPrevidencia(clientId, accessToken, tipoRetorno, sistemaOrigem, contentEncoding, tipoPessoa, numeroParcela, dataVencimento, codigoCompanhia, codigoEstruturaVendas, codigoConvenio, codigoFilial, numeroGrupoFaturamento, numeroSequenciaFatura, numeroProposta, codigoProduto, numeroCertificado, numeroIdentificador);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PrevidenciaApi#postBoletosSegundaviaPrevidencia");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**| Identificador do Cliente utilizado na autenticação. |
 **accessToken** | **String**| Token de acesso utilizado na autenticação. |
 **tipoRetorno** | **String**| Tipo de Retorno. Aceita os valores (1 - Retorna a segunda via de um boleto em PDF(application/pdf); 2 - Retorna a segunda via de um boleto em JPG (image/png); 3 -Retorna a linha digitável da segunda via de um boleto referente a uma parcela do segurado gerada (application/json). Deve estar compativel com o header Accept. |
 **sistemaOrigem** | **String**| Sigla do sistema origem (AVP,  CVP, INS) | [enum: AVP, CVP, INS]
 **contentEncoding** | **String**| Informa o encoding em que se deseja criptografar o response. (Valido para Boletos em PDF tipoRetorno &#x3D;1) (Valor Valido - BASE64). | [optional] [enum: BASE64]
 **tipoPessoa** | **String**| Informa o tipo de Pessoa que está solicitando a segunda via do boleto. PF para pessoa física e PJ para pessoa jurídica &lt;b&gt; Obrigatório quando sistemaOrigem for AVP ou CVP&lt;/b&gt; | [optional] [enum: PF, PJ]
 **numeroParcela** | **String**| Número da Parcela ou da Fatura que deseja gerar o Boleto. &lt;b&gt; Obrigatório quando sistemaOrigem for AVP ou CVP&lt;/b&gt; | [optional]
 **dataVencimento** | **String**| Data do vencimento da parcela.  (DD/MM/AAAA) &lt;b&gt; Obrigatório quando sistemaOrigem for AVP ou CVP&lt;/b&gt; | [optional]
 **codigoCompanhia** | **String**| Código da Companhia | [optional]
 **codigoEstruturaVendas** | **String**| Códigos das Estruturas de vendas. Separados por ,. ex: 12,34,44,12 | [optional]
 **codigoConvenio** | **String**| Código do Convênio para &lt;b&gt;pessoa juridica&lt;/b&gt; | [optional]
 **codigoFilial** | **String**| Código da Filial para &lt;b&gt;pessoa juridica&lt;/b&gt; | [optional]
 **numeroGrupoFaturamento** | **String**| Numero do Grupo Faturamento para &lt;b&gt;pessoa juridica&lt;/b&gt; | [optional]
 **numeroSequenciaFatura** | **String**| Numero de Sequencia da  &lt;b&gt;pessoa juridica&lt;/b&gt; | [optional]
 **numeroProposta** | **String**| Número da Proposta &lt;b&gt;pessoa fisica&lt;/b&gt; .  &lt;b&gt; Obrigatório quando sistemaOrigem for INS&lt;/b&gt; | [optional]
 **codigoProduto** | **String**| Código do produto &lt;b&gt;pessoa fisica&lt;/b&gt; | [optional]
 **numeroCertificado** | **String**| Numero de Certificado para &lt;b&gt;pessoa fisica&lt;/b&gt; | [optional]
 **numeroIdentificador** | **String**| Número do Identificador  &lt;b&gt; Obrigatório quando sistemaOrigem for INS&lt;/b&gt; | [optional]

### Return type

[**BoletoResponse**](BoletoResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: image/png, application/json, application/pdf

