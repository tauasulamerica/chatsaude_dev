
# BoletoResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ipte** | **String** | Linha digitável do boleto. Retornado quando o tipoRetorno for igual a 3 (Linha Digitável)  |  [optional]



