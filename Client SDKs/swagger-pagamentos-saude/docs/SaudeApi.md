# SaudeApi

All URIs are relative to *https://apisulamerica.sensedia.com/dev/pagamentos/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getFaturasSaude**](SaudeApi.md#getFaturasSaude) | **GET** /faturas/saude | 
[**getParcelasSaude**](SaudeApi.md#getParcelasSaude) | **GET** /parcelas/saude | 
[**postBoletosSegundaviaSaude**](SaudeApi.md#postBoletosSegundaviaSaude) | **GET** /boletos/saude/segundaVia | 


<a name="getFaturasSaude"></a>
# **getFaturasSaude**
> List&lt;ParcelaFatura&gt; getFaturasSaude(clientId, accessToken, origem, codigoEmpresa)



Retorna listagem de faturas de uma empresa saúde PME ou Grupal.&lt;/br&gt;&lt;br&gt;A informação retornada que define se uma parcela está habilitada para geração do boleto será o campo status.

### Example
```java
// Import classes:
//import io.swagger.client.sulamerica.pagamentosssaude.ApiException;
//import io.swagger.client.sulamerica.pagamentosssaude.api.SaudeApi;


SaudeApi apiInstance = new SaudeApi();
String clientId = "clientId_example"; // String | Identificador do Cliente utilizado na autenticação.
String accessToken = "accessToken_example"; // String | Token de acesso utilizado na autenticação.
String origem = "origem_example"; // String | Código do consumidor. Aceita os valores MB(Mobile), EC(Espaço Cliente), PC(Portal do Corretor), CH(Chat), PI(Portal Institucional), CX(Parceira Caixa Seguradora),  CM(CCM Center/Inspire) e UR(Ura).
String codigoEmpresa = "codigoEmpresa_example"; // String | Código de identificação da empresa.
try {
    List<ParcelaFatura> result = apiInstance.getFaturasSaude(clientId, accessToken, origem, codigoEmpresa);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaudeApi#getFaturasSaude");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**| Identificador do Cliente utilizado na autenticação. |
 **accessToken** | **String**| Token de acesso utilizado na autenticação. |
 **origem** | **String**| Código do consumidor. Aceita os valores MB(Mobile), EC(Espaço Cliente), PC(Portal do Corretor), CH(Chat), PI(Portal Institucional), CX(Parceira Caixa Seguradora),  CM(CCM Center/Inspire) e UR(Ura). | [enum: MB, EC, PC, CH, UR, PI, CX, CM]
 **codigoEmpresa** | **String**| Código de identificação da empresa. |

### Return type

[**List&lt;ParcelaFatura&gt;**](ParcelaFatura.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getParcelasSaude"></a>
# **getParcelasSaude**
> List&lt;ParcelaIndividualSaude&gt; getParcelasSaude(clientId, accessToken, origem, codigoBeneficiario)



Retorna a listagem de parcelas de um cliente saúde individual.&lt;/br&gt;&lt;br&gt;Quando valorPago &#x3D; 0.00 and vgbl&#x3D; false, são atendidas as condições para habilitar a geração de um boleto&lt;/br&gt;

### Example
```java
// Import classes:
//import io.swagger.client.sulamerica.pagamentosssaude.ApiException;
//import io.swagger.client.sulamerica.pagamentosssaude.api.SaudeApi;


SaudeApi apiInstance = new SaudeApi();
String clientId = "clientId_example"; // String | Identificador do Cliente utilizado na autenticação.
String accessToken = "accessToken_example"; // String | Token de acesso utilizado na autenticação.
String origem = "origem_example"; // String | Código do consumidor. Aceita os valores MB(Mobile), EC(Espaço Cliente), PC(Portal do Corretor), CH(Chat), PI(Portal Institucional), CX(Parceira Caixa Seguradora),  CM(CCM Center/Inspire) e UR(Ura).
String codigoBeneficiario = "codigoBeneficiario_example"; // String | Código de identificação do beneficiário.
try {
    List<ParcelaIndividualSaude> result = apiInstance.getParcelasSaude(clientId, accessToken, origem, codigoBeneficiario);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaudeApi#getParcelasSaude");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**| Identificador do Cliente utilizado na autenticação. |
 **accessToken** | **String**| Token de acesso utilizado na autenticação. |
 **origem** | **String**| Código do consumidor. Aceita os valores MB(Mobile), EC(Espaço Cliente), PC(Portal do Corretor), CH(Chat), PI(Portal Institucional), CX(Parceira Caixa Seguradora),  CM(CCM Center/Inspire) e UR(Ura). | [enum: MB, EC, PC, CH, UR, PI, CX, CM]
 **codigoBeneficiario** | **String**| Código de identificação do beneficiário. |

### Return type

[**List&lt;ParcelaIndividualSaude&gt;**](ParcelaIndividualSaude.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="postBoletosSegundaviaSaude"></a>
# **postBoletosSegundaviaSaude**
> BoletoResponse postBoletosSegundaviaSaude(clientId, accessToken, tipoRetorno, tipoRequisicao, contentEncoding, status, codigoEmpresa, nossoNumero, numeroParcela, codigoBeneficiario, dataPagamento)



Retorna a segunda via de um boleto do canal saude

### Example
```java
// Import classes:
//import io.swagger.client.sulamerica.pagamentosssaude.ApiException;
//import io.swagger.client.sulamerica.pagamentosssaude.api.SaudeApi;


SaudeApi apiInstance = new SaudeApi();
String clientId = "clientId_example"; // String | Identificador do Cliente utilizado na autenticação.
String accessToken = "accessToken_example"; // String | Token de acesso utilizado na autenticação.
String tipoRetorno = "tipoRetorno_example"; // String | Tipo de Retorno. Aceita os valores (1 - Retorna a segunda via de um boleto em PDF(application/pdf); 2 - Retorna a segunda via de um boleto em JPG (image/png); 3 -Retorna a linha digitável da segunda via de um boleto referente a uma parcela do segurado gerada (application/json). Deve estar compativel com o header Accept.
String tipoRequisicao = "tipoRequisicao_example"; // String | Tipo da requisição.<br/>Aceita os valores PME, GRP(Grupal) ou IND(Individual).
String contentEncoding = "contentEncoding_example"; // String | Informa o encoding em que se deseja criptografar o response. (Valido para Boletos em PDF tipoRetorno =1) (Valor Valido - BASE64).
Integer status = 56; // Integer | Reenvia o status da parcela, que define se será informada nova data para o pagamento. Deverá ser enviado em casos de saúde PME e Grupal.
String codigoEmpresa = "codigoEmpresa_example"; // String | Código de identificação da empresa<br/>Deve ser utilizado em casos de saúde PME e Grupal.
String nossoNumero = "nossoNumero_example"; // String | Nosso número, informação para identificação de uma fatura/boleto<br/>Deve ser utilizado em casos de saúde PME e Grupal.
String numeroParcela = "numeroParcela_example"; // String | Número da Parcela, informação para identificação de um boleto<br/>Deve ser utilizado em casos de saúde Individual
String codigoBeneficiario = "codigoBeneficiario_example"; // String | Código de identificação do beneficiário<br/>Deve ser utilizado em casos de saúde Individual.
String dataPagamento = "dataPagamento_example"; // String | Data de pagamento  (DD/MM/AAAA)<br/>Deve ser utilizada em casos de saúde PME e Grupal, apenas para parcelas que retornem status = 1.<br/>Obs.: A data de geração do boleto deve ser no máximo 60 dias após o vencimento original
try {
    BoletoResponse result = apiInstance.postBoletosSegundaviaSaude(clientId, accessToken, tipoRetorno, tipoRequisicao, contentEncoding, status, codigoEmpresa, nossoNumero, numeroParcela, codigoBeneficiario, dataPagamento);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaudeApi#postBoletosSegundaviaSaude");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**| Identificador do Cliente utilizado na autenticação. |
 **accessToken** | **String**| Token de acesso utilizado na autenticação. |
 **tipoRetorno** | **String**| Tipo de Retorno. Aceita os valores (1 - Retorna a segunda via de um boleto em PDF(application/pdf); 2 - Retorna a segunda via de um boleto em JPG (image/png); 3 -Retorna a linha digitável da segunda via de um boleto referente a uma parcela do segurado gerada (application/json). Deve estar compativel com o header Accept. |
 **tipoRequisicao** | **String**| Tipo da requisição.&lt;br/&gt;Aceita os valores PME, GRP(Grupal) ou IND(Individual). | [enum: PME, GRP, IND]
 **contentEncoding** | **String**| Informa o encoding em que se deseja criptografar o response. (Valido para Boletos em PDF tipoRetorno &#x3D;1) (Valor Valido - BASE64). | [optional] [enum: BASE64]
 **status** | **Integer**| Reenvia o status da parcela, que define se será informada nova data para o pagamento. Deverá ser enviado em casos de saúde PME e Grupal. | [optional]
 **codigoEmpresa** | **String**| Código de identificação da empresa&lt;br/&gt;Deve ser utilizado em casos de saúde PME e Grupal. | [optional]
 **nossoNumero** | **String**| Nosso número, informação para identificação de uma fatura/boleto&lt;br/&gt;Deve ser utilizado em casos de saúde PME e Grupal. | [optional]
 **numeroParcela** | **String**| Número da Parcela, informação para identificação de um boleto&lt;br/&gt;Deve ser utilizado em casos de saúde Individual | [optional]
 **codigoBeneficiario** | **String**| Código de identificação do beneficiário&lt;br/&gt;Deve ser utilizado em casos de saúde Individual. | [optional]
 **dataPagamento** | **String**| Data de pagamento  (DD/MM/AAAA)&lt;br/&gt;Deve ser utilizada em casos de saúde PME e Grupal, apenas para parcelas que retornem status &#x3D; 1.&lt;br/&gt;Obs.: A data de geração do boleto deve ser no máximo 60 dias após o vencimento original | [optional]

### Return type

[**BoletoResponse**](BoletoResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: image/jpeg, application/json, application/pdf

