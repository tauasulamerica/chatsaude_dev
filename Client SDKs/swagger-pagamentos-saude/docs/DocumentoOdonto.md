
# DocumentoOdonto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**nomeRazaoSocial** | **String** | Nome para CPF ou razao social para CNPF |  [optional]
**formaPagamento** | [**FormaPagamentoEnum**](#FormaPagamentoEnum) | Forma de Pagamento |  [optional]
**numeroContrato** | **Long** | Numero do Contrato |  [optional]
**numeroContratoComplementar** | **Long** | Número do Contrato Complementar |  [optional]


<a name="FormaPagamentoEnum"></a>
## Enum: FormaPagamentoEnum
Name | Value
---- | -----
BOLETO | &quot;BOLETO&quot;
DCC | &quot;DCC&quot;



