
# ParcelaIndividualSaude

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**numeroParcela** | **Integer** | Número da parcela |  [optional]
**dataVencimento** | **String** | Data de Vencimento da Parcela. (DD/MM/AAAA) |  [optional]
**dataPagamento** | **String** | Data de Pagamento da Parcela. (DD/MM/AAAA) |  [optional]
**dataBaixa** | **String** | Data de Baixa da Parcela. (DD/MM/AAAA) |  [optional]
**dataCancelamento** | **String** | Data de Cancelamento. (DD/MM/AAAA) |  [optional]
**valorCobrancaAdicional** | **Double** | Valor da Cobrança adicional |  [optional]
**valorEmitido** | **Double** | Valor Emitido |  [optional]
**valorPago** | **Double** | Valor Pago |  [optional]
**situacao** | **String** | Exibe a situacao atual da parcela. Ex.: PAGO, PENDENTE |  [optional]
**numeroProtocolo** | **String** | Número do Protocolo |  [optional]
**vgbl** | **Boolean** | Relacionado a poder ou não habilitar o boleto, refere-se se a uma informação de negócio do plano, que pode ser comum ou vgbl (espécie de previdência de plano de saúde - a ser utilizada na aposentadoria). (true: não gera boleto; false: gera boleto) |  [optional]



