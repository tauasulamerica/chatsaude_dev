# OdontoApi

All URIs are relative to *https://apisulamerica.sensedia.com/dev/pagamentos/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getBoletoOdonto**](OdontoApi.md#getBoletoOdonto) | **GET** /boletos/odonto/segundaVia | 
[**postCobrancadocumentoOdonto**](OdontoApi.md#postCobrancadocumentoOdonto) | **GET** /documentos/odonto | 
[**postCobrancapagamentoOdonto**](OdontoApi.md#postCobrancapagamentoOdonto) | **GET** /parcelas/odonto | 


<a name="getBoletoOdonto"></a>
# **getBoletoOdonto**
> getBoletoOdonto(clientId, accessToken, tipoRetorno, modalidade, codigoLancamentoFinanceiro)



Retorna a segunda via do boleto de odonto.

### Example
```java
// Import classes:
//import io.swagger.client.sulamerica.pagamentosssaude.ApiException;
//import io.swagger.client.sulamerica.pagamentosssaude.api.OdontoApi;


OdontoApi apiInstance = new OdontoApi();
String clientId = "clientId_example"; // String | Identificador do Cliente utilizado na autenticação.
String accessToken = "accessToken_example"; // String | Token de acesso utilizado na autenticação.
String tipoRetorno = "tipoRetorno_example"; // String | Tipo de Retorno. Aceita os valores (1 - Retorna a segunda via de um boleto em PDF(application/pdf); 
String modalidade = "modalidade_example"; // String | Modalidade que será gerado o boleto.
String codigoLancamentoFinanceiro = "codigoLancamentoFinanceiro_example"; // String | Código do Lançamento Financeiro.
try {
    apiInstance.getBoletoOdonto(clientId, accessToken, tipoRetorno, modalidade, codigoLancamentoFinanceiro);
} catch (ApiException e) {
    System.err.println("Exception when calling OdontoApi#getBoletoOdonto");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**| Identificador do Cliente utilizado na autenticação. |
 **accessToken** | **String**| Token de acesso utilizado na autenticação. |
 **tipoRetorno** | **String**| Tipo de Retorno. Aceita os valores (1 - Retorna a segunda via de um boleto em PDF(application/pdf);  |
 **modalidade** | **String**| Modalidade que será gerado o boleto. | [enum: PME]
 **codigoLancamentoFinanceiro** | **String**| Código do Lançamento Financeiro. |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/pdf

<a name="postCobrancadocumentoOdonto"></a>
# **postCobrancadocumentoOdonto**
> List&lt;DocumentoOdonto&gt; postCobrancadocumentoOdonto(clientId, accessToken, origem, situacaoParcela, cpnj, cpf, numeroContrato, codigoBeneficiario, codigoEstruturaVendas)



Retorna lista de contratos de Odonto

### Example
```java
// Import classes:
//import io.swagger.client.sulamerica.pagamentosssaude.ApiException;
//import io.swagger.client.sulamerica.pagamentosssaude.api.OdontoApi;


OdontoApi apiInstance = new OdontoApi();
String clientId = "clientId_example"; // String | Identificador do Cliente utilizado na autenticação.
String accessToken = "accessToken_example"; // String | Token de acesso utilizado na autenticação.
String origem = "origem_example"; // String | Origem da chamada
String situacaoParcela = "situacaoParcela_example"; // String | Situação das parcelas dos documentos que deseja listar.
String cpnj = "cpnj_example"; // String | CNPJ do cliente - Não deve ser informado em conjunto com CPF
String cpf = "cpf_example"; // String | CPF do cliente - Não deve ser informado em conjunto com CNPJ
String numeroContrato = "numeroContrato_example"; // String | Numero do contrato
String codigoBeneficiario = "codigoBeneficiario_example"; // String | Codigo do beneficiario / numero da carteirinha
String codigoEstruturaVendas = "codigoEstruturaVendas_example"; // String | Códigos das Estruturas de vendas. Separados por ,. ex: 12,34,44,12
try {
    List<DocumentoOdonto> result = apiInstance.postCobrancadocumentoOdonto(clientId, accessToken, origem, situacaoParcela, cpnj, cpf, numeroContrato, codigoBeneficiario, codigoEstruturaVendas);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OdontoApi#postCobrancadocumentoOdonto");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**| Identificador do Cliente utilizado na autenticação. |
 **accessToken** | **String**| Token de acesso utilizado na autenticação. |
 **origem** | **String**| Origem da chamada | [enum: PORTAL_CORRETOR, PORTAL_INSTITUCIONAL, API_CLIENTE, MOBILE]
 **situacaoParcela** | **String**| Situação das parcelas dos documentos que deseja listar. | [enum: PAGO, PENDENTE]
 **cpnj** | **String**| CNPJ do cliente - Não deve ser informado em conjunto com CPF | [optional]
 **cpf** | **String**| CPF do cliente - Não deve ser informado em conjunto com CNPJ | [optional]
 **numeroContrato** | **String**| Numero do contrato | [optional]
 **codigoBeneficiario** | **String**| Codigo do beneficiario / numero da carteirinha | [optional]
 **codigoEstruturaVendas** | **String**| Códigos das Estruturas de vendas. Separados por ,. ex: 12,34,44,12 | [optional]

### Return type

[**List&lt;DocumentoOdonto&gt;**](DocumentoOdonto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="postCobrancapagamentoOdonto"></a>
# **postCobrancapagamentoOdonto**
> List&lt;ParcelasOdonto&gt; postCobrancapagamentoOdonto(clientId, accessToken, origem, numeroContrato, numeroContratoComplemento, ano, mes, codigoEstruturaVendas)



Retorna lista de Parcelas de Odonto

### Example
```java
// Import classes:
//import io.swagger.client.sulamerica.pagamentosssaude.ApiException;
//import io.swagger.client.sulamerica.pagamentosssaude.api.OdontoApi;


OdontoApi apiInstance = new OdontoApi();
String clientId = "clientId_example"; // String | Identificador do Cliente utilizado na autenticação.
String accessToken = "accessToken_example"; // String | Token de acesso utilizado na autenticação.
String origem = "origem_example"; // String | Origem da Requisição
String numeroContrato = "numeroContrato_example"; // String | Número do Contrato
String numeroContratoComplemento = "numeroContratoComplemento_example"; // String | Número do Contrato Complemento
String ano = "ano_example"; // String | Ano no formato yyyy - Exemplo 2017 - se preenchido com 0(zero) retorna todos os anos
String mes = "mes_example"; // String | Ano no formato mm - Exemplo 09 - se preenchido com 0(zero) retorna todos os meses
String codigoEstruturaVendas = "codigoEstruturaVendas_example"; // String | Códigos das Estruturas de vendas. Separados por ,. ex: 12,34,44,12
try {
    List<ParcelasOdonto> result = apiInstance.postCobrancapagamentoOdonto(clientId, accessToken, origem, numeroContrato, numeroContratoComplemento, ano, mes, codigoEstruturaVendas);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OdontoApi#postCobrancapagamentoOdonto");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**| Identificador do Cliente utilizado na autenticação. |
 **accessToken** | **String**| Token de acesso utilizado na autenticação. |
 **origem** | **String**| Origem da Requisição | [enum: PORTAL_CORRETOR, PORTAL_INSTITUCIONAL, API_CLIENTE, MOBILE]
 **numeroContrato** | **String**| Número do Contrato |
 **numeroContratoComplemento** | **String**| Número do Contrato Complemento |
 **ano** | **String**| Ano no formato yyyy - Exemplo 2017 - se preenchido com 0(zero) retorna todos os anos |
 **mes** | **String**| Ano no formato mm - Exemplo 09 - se preenchido com 0(zero) retorna todos os meses |
 **codigoEstruturaVendas** | **String**| Códigos das Estruturas de vendas. Separados por ,. ex: 12,34,44,12 | [optional]

### Return type

[**List&lt;ParcelasOdonto&gt;**](ParcelasOdonto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

