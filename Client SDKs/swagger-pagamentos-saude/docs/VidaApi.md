# VidaApi

All URIs are relative to *https://apisulamerica.sensedia.com/dev/pagamentos/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getCartaInadinplentesVida**](VidaApi.md#getCartaInadinplentesVida) | **GET** /vida/inadimplentes/cartas | 
[**getParcelasVida**](VidaApi.md#getParcelasVida) | **GET** /parcelas/vida | 
[**postBoletosSegundaviaVida**](VidaApi.md#postBoletosSegundaviaVida) | **GET** /boletos/vida/segundaVia | 


<a name="getCartaInadinplentesVida"></a>
# **getCartaInadinplentesVida**
> getCartaInadinplentesVida(clientId, accessToken, cpf, numeroProposta, codigoPlano)



Retorna a carta de inadimplência do beneficiario do canal de  Vida.

### Example
```java
// Import classes:
//import io.swagger.client.sulamerica.pagamentosssaude.ApiException;
//import io.swagger.client.sulamerica.pagamentosssaude.api.VidaApi;


VidaApi apiInstance = new VidaApi();
String clientId = "clientId_example"; // String | Identificador do Cliente utilizado na autenticação.
String accessToken = "accessToken_example"; // String | Token de acesso utilizado na autenticação.
String cpf = "cpf_example"; // String | CPF do Beneficiário
String numeroProposta = "numeroProposta_example"; // String | Número da Proposta
String codigoPlano = "codigoPlano_example"; // String | Código do Plano.
try {
    apiInstance.getCartaInadinplentesVida(clientId, accessToken, cpf, numeroProposta, codigoPlano);
} catch (ApiException e) {
    System.err.println("Exception when calling VidaApi#getCartaInadinplentesVida");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**| Identificador do Cliente utilizado na autenticação. |
 **accessToken** | **String**| Token de acesso utilizado na autenticação. |
 **cpf** | **String**| CPF do Beneficiário |
 **numeroProposta** | **String**| Número da Proposta |
 **codigoPlano** | **String**| Código do Plano. |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/pdf

<a name="getParcelasVida"></a>
# **getParcelasVida**
> List&lt;ParcelaVida&gt; getParcelasVida(clientId, accessToken, sistemaOrigem, origem, codigoCompanhia, codigoSucursal, codigoRamo, codigoProdutor, numeroApolice, numeroEndosso, numeroCertificado, numeroGrupoFaturamento, codigoEstruturaVendas, anoParcelas)



Retorna as parcelas referentes a uma apólice ou endosso do segurado do canal Vida.

### Example
```java
// Import classes:
//import io.swagger.client.sulamerica.pagamentosssaude.ApiException;
//import io.swagger.client.sulamerica.pagamentosssaude.api.VidaApi;


VidaApi apiInstance = new VidaApi();
String clientId = "clientId_example"; // String | Identificador do Cliente utilizado na autenticação.
String accessToken = "accessToken_example"; // String | Token de acesso utilizado na autenticação.
String sistemaOrigem = "sistemaOrigem_example"; // String | Sigla do sistema origem (SUV ou CVP)
String origem = "origem_example"; // String | Código do consumidor. Aceita os valores MB(Mobile), EC(Espaço Cliente), PC(Portal do Corretor), CH(Chat), PI(Portal Institucional), CX(Parceira Caixa Seguradora),  CM(CCM Center/Inspire) e UR(Ura).
String codigoCompanhia = "codigoCompanhia_example"; // String | Código da Companhia
String codigoSucursal = "codigoSucursal_example"; // String | Código da sucursal.
String codigoRamo = "codigoRamo_example"; // String | Código do Ramo.
String codigoProdutor = "codigoProdutor_example"; // String | Código do Produtor
String numeroApolice = "numeroApolice_example"; // String | Número da apólice do segurado.<br/><b>É necessário informar este atributo caso não seja informado o Endosso.</b>
String numeroEndosso = "numeroEndosso_example"; // String | Número do endosso relativo à apólice do segurado.<br/><b>É necessário informar este atributo caso não seja informada a Apólice.</b>
String numeroCertificado = "numeroCertificado_example"; // String | Número do Certifcado. <b> Obrigatório para Pessoa Física</b>
String numeroGrupoFaturamento = "numeroGrupoFaturamento_example"; // String | Número do Grupo de Faturamento. <b> Obrigatório para Pessoa Juridica</b>
String codigoEstruturaVendas = "codigoEstruturaVendas_example"; // String | Códigos das Estruturas de vendas. Separados por ,. ex: 12,34,44,12
String anoParcelas = "anoParcelas_example"; // String | Ano das parcelas que serão listadas. (Formato: AAAA)
try {
    List<ParcelaVida> result = apiInstance.getParcelasVida(clientId, accessToken, sistemaOrigem, origem, codigoCompanhia, codigoSucursal, codigoRamo, codigoProdutor, numeroApolice, numeroEndosso, numeroCertificado, numeroGrupoFaturamento, codigoEstruturaVendas, anoParcelas);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VidaApi#getParcelasVida");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**| Identificador do Cliente utilizado na autenticação. |
 **accessToken** | **String**| Token de acesso utilizado na autenticação. |
 **sistemaOrigem** | **String**| Sigla do sistema origem (SUV ou CVP) | [enum: SUV, CVP]
 **origem** | **String**| Código do consumidor. Aceita os valores MB(Mobile), EC(Espaço Cliente), PC(Portal do Corretor), CH(Chat), PI(Portal Institucional), CX(Parceira Caixa Seguradora),  CM(CCM Center/Inspire) e UR(Ura). | [enum: MB, EC, PC, CH, UR, PI, CX, CM]
 **codigoCompanhia** | **String**| Código da Companhia |
 **codigoSucursal** | **String**| Código da sucursal. |
 **codigoRamo** | **String**| Código do Ramo. |
 **codigoProdutor** | **String**| Código do Produtor | [optional]
 **numeroApolice** | **String**| Número da apólice do segurado.&lt;br/&gt;&lt;b&gt;É necessário informar este atributo caso não seja informado o Endosso.&lt;/b&gt; | [optional]
 **numeroEndosso** | **String**| Número do endosso relativo à apólice do segurado.&lt;br/&gt;&lt;b&gt;É necessário informar este atributo caso não seja informada a Apólice.&lt;/b&gt; | [optional]
 **numeroCertificado** | **String**| Número do Certifcado. &lt;b&gt; Obrigatório para Pessoa Física&lt;/b&gt; | [optional]
 **numeroGrupoFaturamento** | **String**| Número do Grupo de Faturamento. &lt;b&gt; Obrigatório para Pessoa Juridica&lt;/b&gt; | [optional]
 **codigoEstruturaVendas** | **String**| Códigos das Estruturas de vendas. Separados por ,. ex: 12,34,44,12 | [optional]
 **anoParcelas** | **String**| Ano das parcelas que serão listadas. (Formato: AAAA) | [optional]

### Return type

[**List&lt;ParcelaVida&gt;**](ParcelaVida.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postBoletosSegundaviaVida"></a>
# **postBoletosSegundaviaVida**
> BoletoResponse postBoletosSegundaviaVida(clientId, accessToken, tipoRetorno, sistemaOrigem, contentEncoding, tipoPessoa, numeroParcela, codigoCompanhia, codigoSucursal, codigoRamo, numeroApolice, numeroEndosso, dataVencimento, codigoEstruturaVendas, numeroGrupoFaturamento, numeroSequenciaFatura, numeroCertificado, numeroCVP, nossoNumero)



Retorna a segunda via de um boleto do canal de Vida

### Example
```java
// Import classes:
//import io.swagger.client.sulamerica.pagamentosssaude.ApiException;
//import io.swagger.client.sulamerica.pagamentosssaude.api.VidaApi;


VidaApi apiInstance = new VidaApi();
String clientId = "clientId_example"; // String | Identificador do Cliente utilizado na autenticação.
String accessToken = "accessToken_example"; // String | Token de acesso utilizado na autenticação.
String tipoRetorno = "tipoRetorno_example"; // String | Tipo de Retorno. Aceita os valores (1 - Retorna a segunda via de um boleto em PDF(application/pdf); 2 - Retorna a segunda via de um boleto em JPG (image/png); 3 -Retorna a linha digitável da segunda via de um boleto referente a uma parcela do segurado gerada (application/json). Deve estar compativel com o header Accept.
String sistemaOrigem = "sistemaOrigem_example"; // String | Sigla do sistema origem (INS, SUV, CVP ou CSA)
String contentEncoding = "contentEncoding_example"; // String | Informa o encoding em que se deseja criptografar o response. (Valido para Boletos em PDF tipoRetorno =1) (Valor Valido - BASE64).
String tipoPessoa = "tipoPessoa_example"; // String | Informa o tipo de Pessoa que está solicitando a segunda via do boleto. PF para pessoa física e PJ para pessoa jurídica.  <b> Obrigatório quando sistemaOrigem for SUV, CVP ou CSA</b>
String numeroParcela = "numeroParcela_example"; // String | Número do endosso relativo à apólice do segurado. <b> Obrigatório quando sistemaOrigem for SUV, CVP ou CSA</b>
String codigoCompanhia = "codigoCompanhia_example"; // String | Código da companhia.  <b> Obrigatório quando sistemaOrigem for SUV, CVP ou CSA</b>
String codigoSucursal = "codigoSucursal_example"; // String | Código da sucursal.  <b> Obrigatório quando sistemaOrigem for SUV, CVP ou CSA</b>
String codigoRamo = "codigoRamo_example"; // String | Código do Ramo  <b> Obrigatório quando sistemaOrigem for SUV, CVP ou CSA</b>
String numeroApolice = "numeroApolice_example"; // String | Número da apólice do segurado.  <b> Obrigatório quando sistemaOrigem for SUV, CVP ou CSA</b>
String numeroEndosso = "numeroEndosso_example"; // String | Número do endosso relativo à apólice do segurado.
String dataVencimento = "dataVencimento_example"; // String | Data do vencimento da parcela.  (DD/MM/AAAA)
String codigoEstruturaVendas = "codigoEstruturaVendas_example"; // String | Códigos das Estruturas de vendas. Separados por ,. ex: 12,34,44,12
String numeroGrupoFaturamento = "numeroGrupoFaturamento_example"; // String | Numero do Grupo Faturamento para <b>pessoa juridica</b>
String numeroSequenciaFatura = "numeroSequenciaFatura_example"; // String | Numero de Sequencia da  <b>pessoa juridica</b>
String numeroCertificado = "numeroCertificado_example"; // String | Numero de Certificado para <b>pessoa fisica</b>
String numeroCVP = "numeroCVP_example"; // String | Número CVP do Boleto  <b> Obrigatório quando sistemaOrigem for INS</b>
String nossoNumero = "nossoNumero_example"; // String | Nosso Número  <b> Obrigatório quando sistemaOrigem for INS</b>
try {
    BoletoResponse result = apiInstance.postBoletosSegundaviaVida(clientId, accessToken, tipoRetorno, sistemaOrigem, contentEncoding, tipoPessoa, numeroParcela, codigoCompanhia, codigoSucursal, codigoRamo, numeroApolice, numeroEndosso, dataVencimento, codigoEstruturaVendas, numeroGrupoFaturamento, numeroSequenciaFatura, numeroCertificado, numeroCVP, nossoNumero);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VidaApi#postBoletosSegundaviaVida");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**| Identificador do Cliente utilizado na autenticação. |
 **accessToken** | **String**| Token de acesso utilizado na autenticação. |
 **tipoRetorno** | **String**| Tipo de Retorno. Aceita os valores (1 - Retorna a segunda via de um boleto em PDF(application/pdf); 2 - Retorna a segunda via de um boleto em JPG (image/png); 3 -Retorna a linha digitável da segunda via de um boleto referente a uma parcela do segurado gerada (application/json). Deve estar compativel com o header Accept. |
 **sistemaOrigem** | **String**| Sigla do sistema origem (INS, SUV, CVP ou CSA) | [enum: SUV, CVP, CSA, INS]
 **contentEncoding** | **String**| Informa o encoding em que se deseja criptografar o response. (Valido para Boletos em PDF tipoRetorno &#x3D;1) (Valor Valido - BASE64). | [optional] [enum: BASE64]
 **tipoPessoa** | **String**| Informa o tipo de Pessoa que está solicitando a segunda via do boleto. PF para pessoa física e PJ para pessoa jurídica.  &lt;b&gt; Obrigatório quando sistemaOrigem for SUV, CVP ou CSA&lt;/b&gt; | [optional] [enum: PF, PJ]
 **numeroParcela** | **String**| Número do endosso relativo à apólice do segurado. &lt;b&gt; Obrigatório quando sistemaOrigem for SUV, CVP ou CSA&lt;/b&gt; | [optional]
 **codigoCompanhia** | **String**| Código da companhia.  &lt;b&gt; Obrigatório quando sistemaOrigem for SUV, CVP ou CSA&lt;/b&gt; | [optional]
 **codigoSucursal** | **String**| Código da sucursal.  &lt;b&gt; Obrigatório quando sistemaOrigem for SUV, CVP ou CSA&lt;/b&gt; | [optional]
 **codigoRamo** | **String**| Código do Ramo  &lt;b&gt; Obrigatório quando sistemaOrigem for SUV, CVP ou CSA&lt;/b&gt; | [optional]
 **numeroApolice** | **String**| Número da apólice do segurado.  &lt;b&gt; Obrigatório quando sistemaOrigem for SUV, CVP ou CSA&lt;/b&gt; | [optional]
 **numeroEndosso** | **String**| Número do endosso relativo à apólice do segurado. | [optional]
 **dataVencimento** | **String**| Data do vencimento da parcela.  (DD/MM/AAAA) | [optional]
 **codigoEstruturaVendas** | **String**| Códigos das Estruturas de vendas. Separados por ,. ex: 12,34,44,12 | [optional]
 **numeroGrupoFaturamento** | **String**| Numero do Grupo Faturamento para &lt;b&gt;pessoa juridica&lt;/b&gt; | [optional]
 **numeroSequenciaFatura** | **String**| Numero de Sequencia da  &lt;b&gt;pessoa juridica&lt;/b&gt; | [optional]
 **numeroCertificado** | **String**| Numero de Certificado para &lt;b&gt;pessoa fisica&lt;/b&gt; | [optional]
 **numeroCVP** | **String**| Número CVP do Boleto  &lt;b&gt; Obrigatório quando sistemaOrigem for INS&lt;/b&gt; | [optional]
 **nossoNumero** | **String**| Nosso Número  &lt;b&gt; Obrigatório quando sistemaOrigem for INS&lt;/b&gt; | [optional]

### Return type

[**BoletoResponse**](BoletoResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: image/png, application/json, application/pdf

