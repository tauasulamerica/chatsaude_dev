
# ParcelaVida

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dataVencimento** | **String** | Data de Vencimento (DD/MM/AAAA) |  [optional]
**novaDataVencimento** | **String** | Nova Data de Vencimento (DD/MM/AAAA) |  [optional]
**dataPagamento** | **String** | Data de Pagamento (DD/MM/AAAA) |  [optional]
**valorBoleto** | **Double** | Valor do boleto |  [optional]
**valorPago** | **Double** | Valor do Pagamento |  [optional]
**situacao** | **String** | Situação da Parcela. Ex.: PAGO, PENDENTE, CANCELADO |  [optional]
**competencia** | **String** | Informa a qual mês a  fatura se refere (MM/AAAA) |  [optional]
**numeroParcela** | **String** | Número da Parcela |  [optional]
**emitirParcela** | **Boolean** | Informa se será possivel emitir uma nova Parcela.  |  [optional]
**mensagem** | **String** | Mensagem de alerta |  [optional]
**anosParcelas** | **List&lt;String&gt;** | Lista de anos das parcelas. |  [optional]
**numeroFatura** | **String** | Número da Fatura |  [optional]
**numeroSequenciaFatura** | **String** | Número de Sequência da Fatura |  [optional]



