
# ParcelasOdonto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**competencia** | **String** |  |  [optional]
**numeroParcela** | **Integer** |  |  [optional]
**vencimento** | **String** | data no formato dd/MM/yyyy - Exemplo 29/09/2017 |  [optional]
**situacaoParcela** | **String** | Situação da Parcela. Ex.: PAGO, PENDENTE, CANCELADO |  [optional]
**valorPagar** | [**BigDecimal**](BigDecimal.md) | formato 0,00 |  [optional]
**tipoProduto** | **String** |  |  [optional]
**dataPagamento** | **String** | data no formato dd/MM/yyyy - Exemplo 29/09/2017 |  [optional]
**valorPago** | [**BigDecimal**](BigDecimal.md) | formato 0,00 |  [optional]
**linkBoleto** | **String** | url do para apresentar o boleto no navegador |  [optional]
**mensagemBoleto** | **String** | Caso situacao &#x3D; pago - Boleto já Pago! ou caso da de vencimento maior que 60 dias - Não é possivel gerar boleto para parcela vencida a mais de 60 dias! |  [optional]



