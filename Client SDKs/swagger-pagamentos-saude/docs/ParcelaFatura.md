
# ParcelaFatura

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dataVencimento** | **String** | Data de Vencimento (DD/MM/AAAA) |  [optional]
**dataPagamento** | **String** | Data de Pagamento (DD/MM/AAAA) |  [optional]
**valorFaturado** | **Double** | Valor Faturado |  [optional]
**valorPago** | **Double** | Valor do Pagamento |  [optional]
**situacao** | **String** | Situação da Parcela. Ex.: PAGO, PENDENTE, CANCELADO |  [optional]
**competencia** | **String** | Informa a qual mês a  fatura se refere (DD/MM/AAAA) |  [optional]
**inicioValidade** | **String** | data de Inicio da Validade (DD/MM/AAAA) |  [optional]
**fimValidade** | **String** | data de final da Validade (DD/MM/AAAA) |  [optional]
**nossoNumero** | **String** | Nosso Número |  [optional]
**produto** | **String** | Tipo de produto, atualmente retorna PME ou GRUPAL |  [optional]
**status** | [**BigDecimal**](BigDecimal.md) | Informa se o link de geração de boleto deverá ser habilitado. (0 -  permite a geração do boleto sem nova data; 1 - abre para digitação de nova data, caso produto seja PME; 2 - exibe alerta com informação retornada no campo mensagem |  [optional]
**mensagem** | **String** | Mensagem de alerta |  [optional]



