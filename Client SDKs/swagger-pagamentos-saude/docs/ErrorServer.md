
# ErrorServer

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | **String** | Resultado do Erro. |  [optional]
**errors** | **List&lt;String&gt;** | Lista de erros |  [optional]
**status** | **String** | Status de retorno. |  [optional]



