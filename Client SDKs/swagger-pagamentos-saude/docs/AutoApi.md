# AutoApi

All URIs are relative to *https://apisulamerica.sensedia.com/dev/pagamentos/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getParcelas**](AutoApi.md#getParcelas) | **GET** /parcelas/auto | 
[**postBoletosSegundavia**](AutoApi.md#postBoletosSegundavia) | **GET** /boletos/auto/segundaVia | 


<a name="getParcelas"></a>
# **getParcelas**
> List&lt;Parcela&gt; getParcelas(clientId, accessToken, codigoSucursal, origem, numeroApolice, numeroEndosso, cpfCnpj, codigoProdutor, numeroContrato)



Retorna as parcelas referentes a uma apólice ou endosso do segurado.

### Example
```java
// Import classes:
//import io.swagger.client.sulamerica.pagamentosssaude.ApiException;
//import io.swagger.client.sulamerica.pagamentosssaude.api.AutoApi;


AutoApi apiInstance = new AutoApi();
String clientId = "clientId_example"; // String | Identificador do Cliente utilizado na autenticação.
String accessToken = "accessToken_example"; // String | Token de acesso utilizado na autenticação.
String codigoSucursal = "codigoSucursal_example"; // String | Código da sucursal.
String origem = "origem_example"; // String | Código do consumidor. Aceita os valores MB(Mobile), EC(Espaço Cliente), PC(Portal do Corretor), CH(Chat), PI(Portal Institucional), CX(Parceira Caixa Seguradora),  CM(CCM Center/Inspire) e UR(Ura).
String numeroApolice = "numeroApolice_example"; // String | Número da apólice do segurado.<br/><b>É necessário informar este atributo caso não seja informado o Endosso.</b>
String numeroEndosso = "numeroEndosso_example"; // String | Número do endosso relativo à apólice do segurado.<br/><b>É necessário informar este atributo caso não seja informada a Apólice.</b>
String cpfCnpj = "cpfCnpj_example"; // String | Número do CPF ou CNPJ do segurado.
String codigoProdutor = "codigoProdutor_example"; // String | Código do produtor.<br/><b>É necessário informar este atributo em casos de seguro FROTA.</b>
String numeroContrato = "numeroContrato_example"; // String | Número do contrato.<br/><b>É necessário informar este atributo em casos de seguro FROTA.</b>
try {
    List<Parcela> result = apiInstance.getParcelas(clientId, accessToken, codigoSucursal, origem, numeroApolice, numeroEndosso, cpfCnpj, codigoProdutor, numeroContrato);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AutoApi#getParcelas");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**| Identificador do Cliente utilizado na autenticação. |
 **accessToken** | **String**| Token de acesso utilizado na autenticação. |
 **codigoSucursal** | **String**| Código da sucursal. |
 **origem** | **String**| Código do consumidor. Aceita os valores MB(Mobile), EC(Espaço Cliente), PC(Portal do Corretor), CH(Chat), PI(Portal Institucional), CX(Parceira Caixa Seguradora),  CM(CCM Center/Inspire) e UR(Ura). | [enum: MB, EC, PC, CH, UR, PI, CX, CM]
 **numeroApolice** | **String**| Número da apólice do segurado.&lt;br/&gt;&lt;b&gt;É necessário informar este atributo caso não seja informado o Endosso.&lt;/b&gt; | [optional]
 **numeroEndosso** | **String**| Número do endosso relativo à apólice do segurado.&lt;br/&gt;&lt;b&gt;É necessário informar este atributo caso não seja informada a Apólice.&lt;/b&gt; | [optional]
 **cpfCnpj** | **String**| Número do CPF ou CNPJ do segurado. | [optional]
 **codigoProdutor** | **String**| Código do produtor.&lt;br/&gt;&lt;b&gt;É necessário informar este atributo em casos de seguro FROTA.&lt;/b&gt; | [optional]
 **numeroContrato** | **String**| Número do contrato.&lt;br/&gt;&lt;b&gt;É necessário informar este atributo em casos de seguro FROTA.&lt;/b&gt; | [optional]

### Return type

[**List&lt;Parcela&gt;**](Parcela.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postBoletosSegundavia"></a>
# **postBoletosSegundavia**
> BoletoResponse postBoletosSegundavia(clientId, accessToken, accept, tipoRetorno, origem, vencido, numeroParcela, codigoSucursal, numeroApolice, numeroEndosso, cpfCnpj, dataVencimento)



Retorna a segunda via de um boleto do canal de Auto 

### Example
```java
// Import classes:
//import io.swagger.client.sulamerica.pagamentosssaude.ApiException;
//import io.swagger.client.sulamerica.pagamentosssaude.api.AutoApi;


AutoApi apiInstance = new AutoApi();
String clientId = "clientId_example"; // String | Identificador do Cliente utilizado na autenticação.
String accessToken = "accessToken_example"; // String | Token de acesso utilizado na autenticação.
String accept = "accept_example"; // String | Tipo de mídia que o cliente aceita de retorno. Exemplo, caso o cliente esteja aguardando um PDF, enviar application/pdf 
String tipoRetorno = "tipoRetorno_example"; // String | Tipo de Retorno. Aceita os valores (1 - Retorna a segunda via de um boleto em PDF(application/pdf); 2 - Retorna a segunda via de um boleto em JPG (image/png); 3 -Retorna a linha digitável da segunda via de um boleto referente a uma parcela do segurado gerada (application/json). Deve estar compativel com o header Accept.
String origem = "origem_example"; // String | Código do consumidor. Aceita os valores MB(Mobile), EC(Espaço Cliente), PC(Portal do Corretor), CH(Chat), PI(Portal Institucional), CX(Parceira Caixa Seguradora),  CM(CCM Center/Inspire) e UR(Ura).
Boolean vencido = true; // Boolean | Define se o boleto é vencido.
String numeroParcela = "numeroParcela_example"; // String | Número do endosso relativo à apólice do segurado.
String codigoSucursal = "codigoSucursal_example"; // String | Código da sucursal.
String numeroApolice = "numeroApolice_example"; // String | Número da apólice do segurado.<br/><b>É necessário informar este atributo caso não seja informado o Endosso.</b>
String numeroEndosso = "numeroEndosso_example"; // String | Número do endosso relativo à apólice do segurado.<br/><b>É necessário informar este atributo caso não seja informada a Apólice.</b>
String cpfCnpj = "cpfCnpj_example"; // String | Número do CPF ou CNPJ do segurado.
String dataVencimento = "dataVencimento_example"; // String | Data do vencimento da parcela. (DD/MM/AAAA)<br/><b>É necessário informar este atributo em casos de boleto vencido.</b>
try {
    BoletoResponse result = apiInstance.postBoletosSegundavia(clientId, accessToken, accept, tipoRetorno, origem, vencido, numeroParcela, codigoSucursal, numeroApolice, numeroEndosso, cpfCnpj, dataVencimento);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AutoApi#postBoletosSegundavia");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**| Identificador do Cliente utilizado na autenticação. |
 **accessToken** | **String**| Token de acesso utilizado na autenticação. |
 **accept** | **String**| Tipo de mídia que o cliente aceita de retorno. Exemplo, caso o cliente esteja aguardando um PDF, enviar application/pdf  |
 **tipoRetorno** | **String**| Tipo de Retorno. Aceita os valores (1 - Retorna a segunda via de um boleto em PDF(application/pdf); 2 - Retorna a segunda via de um boleto em JPG (image/png); 3 -Retorna a linha digitável da segunda via de um boleto referente a uma parcela do segurado gerada (application/json). Deve estar compativel com o header Accept. |
 **origem** | **String**| Código do consumidor. Aceita os valores MB(Mobile), EC(Espaço Cliente), PC(Portal do Corretor), CH(Chat), PI(Portal Institucional), CX(Parceira Caixa Seguradora),  CM(CCM Center/Inspire) e UR(Ura). | [enum: MB, EC, PC, CH, UR, PI, CX, CM]
 **vencido** | **Boolean**| Define se o boleto é vencido. |
 **numeroParcela** | **String**| Número do endosso relativo à apólice do segurado. |
 **codigoSucursal** | **String**| Código da sucursal. |
 **numeroApolice** | **String**| Número da apólice do segurado.&lt;br/&gt;&lt;b&gt;É necessário informar este atributo caso não seja informado o Endosso.&lt;/b&gt; | [optional]
 **numeroEndosso** | **String**| Número do endosso relativo à apólice do segurado.&lt;br/&gt;&lt;b&gt;É necessário informar este atributo caso não seja informada a Apólice.&lt;/b&gt; | [optional]
 **cpfCnpj** | **String**| Número do CPF ou CNPJ do segurado. | [optional]
 **dataVencimento** | **String**| Data do vencimento da parcela. (DD/MM/AAAA)&lt;br/&gt;&lt;b&gt;É necessário informar este atributo em casos de boleto vencido.&lt;/b&gt; | [optional]

### Return type

[**BoletoResponse**](BoletoResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: image/png, application/json, application/pdf

