
# ParcelaMassificados

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**numeroParcela** | **Integer** | Número da Parcela |  [optional]
**dataVencimento** | **String** | Data de Vencimento |  [optional]
**dataPagamento** | **String** | Data de Pagamento |  [optional]
**valorParcela** | **Double** | Valor do Pagamento |  [optional]
**status** | **String** | Status da Parcela |  [optional]
**tipoPagamento** | **String** | Tipo de Pagamento |  [optional]
**codigoLancamentoFinanceiro** | **String** | Código do Lançamento Financeiro |  [optional]
**nomeSegurado** | **String** | Nome do Segurado |  [optional]
**siglaSistema** | **String** | Sigla do Sistema |  [optional]



