package io.swagger.client.sulamerica.pagamentosssaude.api;

import io.swagger.client.sulamerica.pagamentosssaude.ApiException;
import io.swagger.client.sulamerica.pagamentosssaude.ApiClient;
import io.swagger.client.sulamerica.pagamentosssaude.Configuration;
import io.swagger.client.sulamerica.pagamentosssaude.Pair;

import javax.ws.rs.core.GenericType;

import io.swagger.client.sulamerica.pagamentosssaude.model.BoletoResponse;
import io.swagger.client.sulamerica.pagamentosssaude.model.Error;
import io.swagger.client.sulamerica.pagamentosssaude.model.FaturaPrevidencia;
import io.swagger.client.sulamerica.pagamentosssaude.model.ParcelaPrevidencia;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-03-20T19:25:42.811-03:00")
public class PrevidenciaApi {
  private ApiClient apiClient;

  public PrevidenciaApi() {
    this(Configuration.getDefaultApiClient());
  }

  public PrevidenciaApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * 
   * Retorna um boleto de aporte esporádico do canal de Previdencia.
   * @param clientId Identificador do Cliente utilizado na autenticação. (required)
   * @param accessToken Token de acesso utilizado na autenticação. (required)
   * @param tipoRetorno Tipo de Retorno. Aceita os valores (1 - Retorna a segunda via de um boleto em PDF(application/pdf); 2 - Retorna a segunda via de um boleto em JPG (image/jpg); 3 -Retorna a linha digitável da segunda via de um boleto referente a uma parcela do segurado gerada (application/json); 4 - Retorna a segunda via de um boleto em PNG (image/png); (required)
   * @param sistemaOrigem Sigla do sistema origem (AVP, CVP) (required)
   * @param codigoProduto Código do Produto (required)
   * @param dataVencimento Data do vencimento do boleto.  (DD/MM/AAAA). (required)
   * @param numeroProposta Número da Proposta (required)
   * @param valorAporte Valor do Aporte (required)
   * @return BoletoResponse
   * @throws ApiException if fails to make API call
   */
  public BoletoResponse getBoletosEsporadicoPrevidencia(String clientId, String accessToken, String tipoRetorno, String sistemaOrigem, String codigoProduto, String dataVencimento, String numeroProposta, String valorAporte) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'clientId' is set
    if (clientId == null) {
      throw new ApiException(400, "Missing the required parameter 'clientId' when calling getBoletosEsporadicoPrevidencia");
    }
    
    // verify the required parameter 'accessToken' is set
    if (accessToken == null) {
      throw new ApiException(400, "Missing the required parameter 'accessToken' when calling getBoletosEsporadicoPrevidencia");
    }
    
    // verify the required parameter 'tipoRetorno' is set
    if (tipoRetorno == null) {
      throw new ApiException(400, "Missing the required parameter 'tipoRetorno' when calling getBoletosEsporadicoPrevidencia");
    }
    
    // verify the required parameter 'sistemaOrigem' is set
    if (sistemaOrigem == null) {
      throw new ApiException(400, "Missing the required parameter 'sistemaOrigem' when calling getBoletosEsporadicoPrevidencia");
    }
    
    // verify the required parameter 'codigoProduto' is set
    if (codigoProduto == null) {
      throw new ApiException(400, "Missing the required parameter 'codigoProduto' when calling getBoletosEsporadicoPrevidencia");
    }
    
    // verify the required parameter 'dataVencimento' is set
    if (dataVencimento == null) {
      throw new ApiException(400, "Missing the required parameter 'dataVencimento' when calling getBoletosEsporadicoPrevidencia");
    }
    
    // verify the required parameter 'numeroProposta' is set
    if (numeroProposta == null) {
      throw new ApiException(400, "Missing the required parameter 'numeroProposta' when calling getBoletosEsporadicoPrevidencia");
    }
    
    // verify the required parameter 'valorAporte' is set
    if (valorAporte == null) {
      throw new ApiException(400, "Missing the required parameter 'valorAporte' when calling getBoletosEsporadicoPrevidencia");
    }
    
    // create path and map variables
    String localVarPath = "/boletos/previdencia/esporadico";

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();

    localVarQueryParams.addAll(apiClient.parameterToPairs("", "tipoRetorno", tipoRetorno));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "sistemaOrigem", sistemaOrigem));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "codigoProduto", codigoProduto));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "dataVencimento", dataVencimento));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "numeroProposta", numeroProposta));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "valorAporte", valorAporte));

    if (clientId != null)
      localVarHeaderParams.put("client_id", apiClient.parameterToString(clientId));
if (accessToken != null)
      localVarHeaderParams.put("access_token", apiClient.parameterToString(accessToken));

    
    final String[] localVarAccepts = {
      "application/pdf"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      "application/json"
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<BoletoResponse> localVarReturnType = new GenericType<BoletoResponse>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * 
   * Retorna a carta de inadimplência do beneficiario do canal de  previdência.
   * @param clientId Identificador do Cliente utilizado na autenticação. (required)
   * @param accessToken Token de acesso utilizado na autenticação. (required)
   * @param cpf CPF do Beneficiário (required)
   * @param numeroProposta Número da Proposta (required)
   * @param codigoPlano Código do Plano. (required)
   * @throws ApiException if fails to make API call
   */
  public void getCartaInadinplentesPrevidencia(String clientId, String accessToken, String cpf, String numeroProposta, String codigoPlano) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'clientId' is set
    if (clientId == null) {
      throw new ApiException(400, "Missing the required parameter 'clientId' when calling getCartaInadinplentesPrevidencia");
    }
    
    // verify the required parameter 'accessToken' is set
    if (accessToken == null) {
      throw new ApiException(400, "Missing the required parameter 'accessToken' when calling getCartaInadinplentesPrevidencia");
    }
    
    // verify the required parameter 'cpf' is set
    if (cpf == null) {
      throw new ApiException(400, "Missing the required parameter 'cpf' when calling getCartaInadinplentesPrevidencia");
    }
    
    // verify the required parameter 'numeroProposta' is set
    if (numeroProposta == null) {
      throw new ApiException(400, "Missing the required parameter 'numeroProposta' when calling getCartaInadinplentesPrevidencia");
    }
    
    // verify the required parameter 'codigoPlano' is set
    if (codigoPlano == null) {
      throw new ApiException(400, "Missing the required parameter 'codigoPlano' when calling getCartaInadinplentesPrevidencia");
    }
    
    // create path and map variables
    String localVarPath = "/previdencia/inadimplentes/cartas";

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();

    localVarQueryParams.addAll(apiClient.parameterToPairs("", "cpf", cpf));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "numeroProposta", numeroProposta));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "codigoPlano", codigoPlano));

    if (clientId != null)
      localVarHeaderParams.put("client_id", apiClient.parameterToString(clientId));
if (accessToken != null)
      localVarHeaderParams.put("access_token", apiClient.parameterToString(accessToken));

    
    final String[] localVarAccepts = {
      "application/pdf"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      "application/json"
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };


    apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, null);
  }
  /**
   * 
   * Retorna as faturas do canal de Previdência.
   * @param clientId Identificador do Cliente utilizado na autenticação. (required)
   * @param accessToken Token de acesso utilizado na autenticação. (required)
   * @param sistemaOrigem Sigla do sistema origem (AVP ou CVP) (required)
   * @param codigoConvenio Código do Convênio (required)
   * @param codigoFilial Código da Filial (required)
   * @param codigoEstruturaVendas Códigos das Estruturas de vendas. Separados por ,. ex: 12,34,44,12 (optional)
   * @return List&lt;FaturaPrevidencia&gt;
   * @throws ApiException if fails to make API call
   */
  public List<FaturaPrevidencia> getFaturasPrevidencia(String clientId, String accessToken, String sistemaOrigem, String codigoConvenio, String codigoFilial, String codigoEstruturaVendas) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'clientId' is set
    if (clientId == null) {
      throw new ApiException(400, "Missing the required parameter 'clientId' when calling getFaturasPrevidencia");
    }
    
    // verify the required parameter 'accessToken' is set
    if (accessToken == null) {
      throw new ApiException(400, "Missing the required parameter 'accessToken' when calling getFaturasPrevidencia");
    }
    
    // verify the required parameter 'sistemaOrigem' is set
    if (sistemaOrigem == null) {
      throw new ApiException(400, "Missing the required parameter 'sistemaOrigem' when calling getFaturasPrevidencia");
    }
    
    // verify the required parameter 'codigoConvenio' is set
    if (codigoConvenio == null) {
      throw new ApiException(400, "Missing the required parameter 'codigoConvenio' when calling getFaturasPrevidencia");
    }
    
    // verify the required parameter 'codigoFilial' is set
    if (codigoFilial == null) {
      throw new ApiException(400, "Missing the required parameter 'codigoFilial' when calling getFaturasPrevidencia");
    }
    
    // create path and map variables
    String localVarPath = "/faturas/previdencia";

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();

    localVarQueryParams.addAll(apiClient.parameterToPairs("", "sistemaOrigem", sistemaOrigem));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "codigoConvenio", codigoConvenio));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "codigoFilial", codigoFilial));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "codigoEstruturaVendas", codigoEstruturaVendas));

    if (clientId != null)
      localVarHeaderParams.put("client_id", apiClient.parameterToString(clientId));
if (accessToken != null)
      localVarHeaderParams.put("access_token", apiClient.parameterToString(accessToken));

    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      "application/json"
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<List<FaturaPrevidencia>> localVarReturnType = new GenericType<List<FaturaPrevidencia>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * 
   * Retorna as parcelas referentes a uma apólice ou endosso do segurado do canal de Previdência.
   * @param clientId Identificador do Cliente utilizado na autenticação. (required)
   * @param accessToken Token de acesso utilizado na autenticação. (required)
   * @param sistemaOrigem Sigla do sistema origem (AVP ou CVP) (required)
   * @param codigoProduto Código do produto. (required)
   * @param numeroProposta Número da proposta (required)
   * @param codigoEstruturaVendas Códigos das Estruturas de vendas. Separados por ,. ex: 12,34,44,12 (optional)
   * @return List&lt;ParcelaPrevidencia&gt;
   * @throws ApiException if fails to make API call
   */
  public List<ParcelaPrevidencia> getParcelasPrevidencia(String clientId, String accessToken, String sistemaOrigem, String codigoProduto, String numeroProposta, String codigoEstruturaVendas) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'clientId' is set
    if (clientId == null) {
      throw new ApiException(400, "Missing the required parameter 'clientId' when calling getParcelasPrevidencia");
    }
    
    // verify the required parameter 'accessToken' is set
    if (accessToken == null) {
      throw new ApiException(400, "Missing the required parameter 'accessToken' when calling getParcelasPrevidencia");
    }
    
    // verify the required parameter 'sistemaOrigem' is set
    if (sistemaOrigem == null) {
      throw new ApiException(400, "Missing the required parameter 'sistemaOrigem' when calling getParcelasPrevidencia");
    }
    
    // verify the required parameter 'codigoProduto' is set
    if (codigoProduto == null) {
      throw new ApiException(400, "Missing the required parameter 'codigoProduto' when calling getParcelasPrevidencia");
    }
    
    // verify the required parameter 'numeroProposta' is set
    if (numeroProposta == null) {
      throw new ApiException(400, "Missing the required parameter 'numeroProposta' when calling getParcelasPrevidencia");
    }
    
    // create path and map variables
    String localVarPath = "/parcelas/previdencia";

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();

    localVarQueryParams.addAll(apiClient.parameterToPairs("", "sistemaOrigem", sistemaOrigem));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "codigoProduto", codigoProduto));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "numeroProposta", numeroProposta));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "codigoEstruturaVendas", codigoEstruturaVendas));

    if (clientId != null)
      localVarHeaderParams.put("client_id", apiClient.parameterToString(clientId));
if (accessToken != null)
      localVarHeaderParams.put("access_token", apiClient.parameterToString(accessToken));

    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      "application/json"
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<List<ParcelaPrevidencia>> localVarReturnType = new GenericType<List<ParcelaPrevidencia>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * 
   * Retorna a segunda via de um boleto do canal de Previdencia
   * @param clientId Identificador do Cliente utilizado na autenticação. (required)
   * @param accessToken Token de acesso utilizado na autenticação. (required)
   * @param tipoRetorno Tipo de Retorno. Aceita os valores (1 - Retorna a segunda via de um boleto em PDF(application/pdf); 2 - Retorna a segunda via de um boleto em JPG (image/png); 3 -Retorna a linha digitável da segunda via de um boleto referente a uma parcela do segurado gerada (application/json). Deve estar compativel com o header Accept. (required)
   * @param sistemaOrigem Sigla do sistema origem (AVP,  CVP, INS) (required)
   * @param contentEncoding Informa o encoding em que se deseja criptografar o response. (Valido para Boletos em PDF tipoRetorno &#x3D;1) (Valor Valido - BASE64). (optional)
   * @param tipoPessoa Informa o tipo de Pessoa que está solicitando a segunda via do boleto. PF para pessoa física e PJ para pessoa jurídica &lt;b&gt; Obrigatório quando sistemaOrigem for AVP ou CVP&lt;/b&gt; (optional)
   * @param numeroParcela Número da Parcela ou da Fatura que deseja gerar o Boleto. &lt;b&gt; Obrigatório quando sistemaOrigem for AVP ou CVP&lt;/b&gt; (optional)
   * @param dataVencimento Data do vencimento da parcela.  (DD/MM/AAAA) &lt;b&gt; Obrigatório quando sistemaOrigem for AVP ou CVP&lt;/b&gt; (optional)
   * @param codigoCompanhia Código da Companhia (optional)
   * @param codigoEstruturaVendas Códigos das Estruturas de vendas. Separados por ,. ex: 12,34,44,12 (optional)
   * @param codigoConvenio Código do Convênio para &lt;b&gt;pessoa juridica&lt;/b&gt; (optional)
   * @param codigoFilial Código da Filial para &lt;b&gt;pessoa juridica&lt;/b&gt; (optional)
   * @param numeroGrupoFaturamento Numero do Grupo Faturamento para &lt;b&gt;pessoa juridica&lt;/b&gt; (optional)
   * @param numeroSequenciaFatura Numero de Sequencia da  &lt;b&gt;pessoa juridica&lt;/b&gt; (optional)
   * @param numeroProposta Número da Proposta &lt;b&gt;pessoa fisica&lt;/b&gt; .  &lt;b&gt; Obrigatório quando sistemaOrigem for INS&lt;/b&gt; (optional)
   * @param codigoProduto Código do produto &lt;b&gt;pessoa fisica&lt;/b&gt; (optional)
   * @param numeroCertificado Numero de Certificado para &lt;b&gt;pessoa fisica&lt;/b&gt; (optional)
   * @param numeroIdentificador Número do Identificador  &lt;b&gt; Obrigatório quando sistemaOrigem for INS&lt;/b&gt; (optional)
   * @return BoletoResponse
   * @throws ApiException if fails to make API call
   */
  public BoletoResponse postBoletosSegundaviaPrevidencia(String clientId, String accessToken, String tipoRetorno, String sistemaOrigem, String contentEncoding, String tipoPessoa, String numeroParcela, String dataVencimento, String codigoCompanhia, String codigoEstruturaVendas, String codigoConvenio, String codigoFilial, String numeroGrupoFaturamento, String numeroSequenciaFatura, String numeroProposta, String codigoProduto, String numeroCertificado, String numeroIdentificador) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'clientId' is set
    if (clientId == null) {
      throw new ApiException(400, "Missing the required parameter 'clientId' when calling postBoletosSegundaviaPrevidencia");
    }
    
    // verify the required parameter 'accessToken' is set
    if (accessToken == null) {
      throw new ApiException(400, "Missing the required parameter 'accessToken' when calling postBoletosSegundaviaPrevidencia");
    }
    
    // verify the required parameter 'tipoRetorno' is set
    if (tipoRetorno == null) {
      throw new ApiException(400, "Missing the required parameter 'tipoRetorno' when calling postBoletosSegundaviaPrevidencia");
    }
    
    // verify the required parameter 'sistemaOrigem' is set
    if (sistemaOrigem == null) {
      throw new ApiException(400, "Missing the required parameter 'sistemaOrigem' when calling postBoletosSegundaviaPrevidencia");
    }
    
    // create path and map variables
    String localVarPath = "/boletos/previdencia/segundaVia";

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();

    localVarQueryParams.addAll(apiClient.parameterToPairs("", "tipoRetorno", tipoRetorno));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "sistemaOrigem", sistemaOrigem));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "tipoPessoa", tipoPessoa));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "numeroParcela", numeroParcela));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "dataVencimento", dataVencimento));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "codigoCompanhia", codigoCompanhia));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "codigoEstruturaVendas", codigoEstruturaVendas));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "codigoConvenio", codigoConvenio));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "codigoFilial", codigoFilial));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "numeroGrupoFaturamento", numeroGrupoFaturamento));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "numeroSequenciaFatura", numeroSequenciaFatura));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "numeroProposta", numeroProposta));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "codigoProduto", codigoProduto));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "numeroCertificado", numeroCertificado));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "numeroIdentificador", numeroIdentificador));

    if (clientId != null)
      localVarHeaderParams.put("client_id", apiClient.parameterToString(clientId));
if (accessToken != null)
      localVarHeaderParams.put("access_token", apiClient.parameterToString(accessToken));
if (contentEncoding != null)
      localVarHeaderParams.put("content-encoding", apiClient.parameterToString(contentEncoding));

    
    final String[] localVarAccepts = {
      "image/png", "application/json", "application/pdf"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      "application/json"
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<BoletoResponse> localVarReturnType = new GenericType<BoletoResponse>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
}
