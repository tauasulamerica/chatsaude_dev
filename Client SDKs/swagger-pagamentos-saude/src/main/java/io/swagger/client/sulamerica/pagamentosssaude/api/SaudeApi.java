package io.swagger.client.sulamerica.pagamentosssaude.api;

import io.swagger.client.sulamerica.pagamentosssaude.ApiException;
import io.swagger.client.sulamerica.pagamentosssaude.ApiClient;
import io.swagger.client.sulamerica.pagamentosssaude.Configuration;
import io.swagger.client.sulamerica.pagamentosssaude.Pair;

import javax.ws.rs.core.GenericType;

import io.swagger.client.sulamerica.pagamentosssaude.model.BoletoResponse;
import io.swagger.client.sulamerica.pagamentosssaude.model.Error;
import io.swagger.client.sulamerica.pagamentosssaude.model.ParcelaFatura;
import io.swagger.client.sulamerica.pagamentosssaude.model.ParcelaIndividualSaude;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-03-20T19:25:42.811-03:00")
public class SaudeApi {
  private ApiClient apiClient;

  public SaudeApi() {
    this(Configuration.getDefaultApiClient());
  }

  public SaudeApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * 
   * Retorna listagem de faturas de uma empresa saúde PME ou Grupal.&lt;/br&gt;&lt;br&gt;A informação retornada que define se uma parcela está habilitada para geração do boleto será o campo status.
   * @param clientId Identificador do Cliente utilizado na autenticação. (required)
   * @param accessToken Token de acesso utilizado na autenticação. (required)
   * @param origem Código do consumidor. Aceita os valores MB(Mobile), EC(Espaço Cliente), PC(Portal do Corretor), CH(Chat), PI(Portal Institucional), CX(Parceira Caixa Seguradora),  CM(CCM Center/Inspire) e UR(Ura). (required)
   * @param codigoEmpresa Código de identificação da empresa. (required)
   * @return List&lt;ParcelaFatura&gt;
   * @throws ApiException if fails to make API call
   */
  public List<ParcelaFatura> getFaturasSaude(String clientId, String accessToken, String origem, String codigoEmpresa) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'clientId' is set
    if (clientId == null) {
      throw new ApiException(400, "Missing the required parameter 'clientId' when calling getFaturasSaude");
    }
    
    // verify the required parameter 'accessToken' is set
    if (accessToken == null) {
      throw new ApiException(400, "Missing the required parameter 'accessToken' when calling getFaturasSaude");
    }
    
    // verify the required parameter 'origem' is set
    if (origem == null) {
      throw new ApiException(400, "Missing the required parameter 'origem' when calling getFaturasSaude");
    }
    
    // verify the required parameter 'codigoEmpresa' is set
    if (codigoEmpresa == null) {
      throw new ApiException(400, "Missing the required parameter 'codigoEmpresa' when calling getFaturasSaude");
    }
    
    // create path and map variables
    String localVarPath = "/faturas/saude";

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();

    localVarQueryParams.addAll(apiClient.parameterToPairs("", "origem", origem));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "codigoEmpresa", codigoEmpresa));

    if (clientId != null)
      localVarHeaderParams.put("client_id", apiClient.parameterToString(clientId));
if (accessToken != null)
      localVarHeaderParams.put("access_token", apiClient.parameterToString(accessToken));

    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<List<ParcelaFatura>> localVarReturnType = new GenericType<List<ParcelaFatura>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * 
   * Retorna a listagem de parcelas de um cliente saúde individual.&lt;/br&gt;&lt;br&gt;Quando valorPago &#x3D; 0.00 and vgbl&#x3D; false, são atendidas as condições para habilitar a geração de um boleto&lt;/br&gt;
   * @param clientId Identificador do Cliente utilizado na autenticação. (required)
   * @param accessToken Token de acesso utilizado na autenticação. (required)
   * @param origem Código do consumidor. Aceita os valores MB(Mobile), EC(Espaço Cliente), PC(Portal do Corretor), CH(Chat), PI(Portal Institucional), CX(Parceira Caixa Seguradora),  CM(CCM Center/Inspire) e UR(Ura). (required)
   * @param codigoBeneficiario Código de identificação do beneficiário. (required)
   * @return List&lt;ParcelaIndividualSaude&gt;
   * @throws ApiException if fails to make API call
   */
  public List<ParcelaIndividualSaude> getParcelasSaude(String clientId, String accessToken, String origem, String codigoBeneficiario) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'clientId' is set
    if (clientId == null) {
      throw new ApiException(400, "Missing the required parameter 'clientId' when calling getParcelasSaude");
    }
    
    // verify the required parameter 'accessToken' is set
    if (accessToken == null) {
      throw new ApiException(400, "Missing the required parameter 'accessToken' when calling getParcelasSaude");
    }
    
    // verify the required parameter 'origem' is set
    if (origem == null) {
      throw new ApiException(400, "Missing the required parameter 'origem' when calling getParcelasSaude");
    }
    
    // verify the required parameter 'codigoBeneficiario' is set
    if (codigoBeneficiario == null) {
      throw new ApiException(400, "Missing the required parameter 'codigoBeneficiario' when calling getParcelasSaude");
    }
    
    // create path and map variables
    String localVarPath = "/parcelas/saude";

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();

    localVarQueryParams.addAll(apiClient.parameterToPairs("", "origem", origem));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "codigoBeneficiario", codigoBeneficiario));

    if (clientId != null)
      localVarHeaderParams.put("client_id", apiClient.parameterToString(clientId));
if (accessToken != null)
      localVarHeaderParams.put("access_token", apiClient.parameterToString(accessToken));

    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<List<ParcelaIndividualSaude>> localVarReturnType = new GenericType<List<ParcelaIndividualSaude>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * 
   * Retorna a segunda via de um boleto do canal saude
   * @param clientId Identificador do Cliente utilizado na autenticação. (required)
   * @param accessToken Token de acesso utilizado na autenticação. (required)
   * @param tipoRetorno Tipo de Retorno. Aceita os valores (1 - Retorna a segunda via de um boleto em PDF(application/pdf); 2 - Retorna a segunda via de um boleto em JPG (image/png); 3 -Retorna a linha digitável da segunda via de um boleto referente a uma parcela do segurado gerada (application/json). Deve estar compativel com o header Accept. (required)
   * @param tipoRequisicao Tipo da requisição.&lt;br/&gt;Aceita os valores PME, GRP(Grupal) ou IND(Individual). (required)
   * @param contentEncoding Informa o encoding em que se deseja criptografar o response. (Valido para Boletos em PDF tipoRetorno &#x3D;1) (Valor Valido - BASE64). (optional)
   * @param status Reenvia o status da parcela, que define se será informada nova data para o pagamento. Deverá ser enviado em casos de saúde PME e Grupal. (optional)
   * @param codigoEmpresa Código de identificação da empresa&lt;br/&gt;Deve ser utilizado em casos de saúde PME e Grupal. (optional)
   * @param nossoNumero Nosso número, informação para identificação de uma fatura/boleto&lt;br/&gt;Deve ser utilizado em casos de saúde PME e Grupal. (optional)
   * @param numeroParcela Número da Parcela, informação para identificação de um boleto&lt;br/&gt;Deve ser utilizado em casos de saúde Individual (optional)
   * @param codigoBeneficiario Código de identificação do beneficiário&lt;br/&gt;Deve ser utilizado em casos de saúde Individual. (optional)
   * @param dataPagamento Data de pagamento  (DD/MM/AAAA)&lt;br/&gt;Deve ser utilizada em casos de saúde PME e Grupal, apenas para parcelas que retornem status &#x3D; 1.&lt;br/&gt;Obs.: A data de geração do boleto deve ser no máximo 60 dias após o vencimento original (optional)
   * @return BoletoResponse
   * @throws ApiException if fails to make API call
   */
  public BoletoResponse postBoletosSegundaviaSaude(String clientId, String accessToken, String tipoRetorno, String tipoRequisicao, String contentEncoding, Integer status, String codigoEmpresa, String nossoNumero, String numeroParcela, String codigoBeneficiario, String dataPagamento) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'clientId' is set
    if (clientId == null) {
      throw new ApiException(400, "Missing the required parameter 'clientId' when calling postBoletosSegundaviaSaude");
    }
    
    // verify the required parameter 'accessToken' is set
    if (accessToken == null) {
      throw new ApiException(400, "Missing the required parameter 'accessToken' when calling postBoletosSegundaviaSaude");
    }
    
    // verify the required parameter 'tipoRetorno' is set
    if (tipoRetorno == null) {
      throw new ApiException(400, "Missing the required parameter 'tipoRetorno' when calling postBoletosSegundaviaSaude");
    }
    
    // verify the required parameter 'tipoRequisicao' is set
    if (tipoRequisicao == null) {
      throw new ApiException(400, "Missing the required parameter 'tipoRequisicao' when calling postBoletosSegundaviaSaude");
    }
    
    // create path and map variables
    String localVarPath = "/boletos/saude/segundaVia";

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();

    localVarQueryParams.addAll(apiClient.parameterToPairs("", "tipoRetorno", tipoRetorno));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "tipoRequisicao", tipoRequisicao));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "status", status));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "codigoEmpresa", codigoEmpresa));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "nossoNumero", nossoNumero));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "numeroParcela", numeroParcela));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "codigoBeneficiario", codigoBeneficiario));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "dataPagamento", dataPagamento));

    if (clientId != null)
      localVarHeaderParams.put("client_id", apiClient.parameterToString(clientId));
if (accessToken != null)
      localVarHeaderParams.put("access_token", apiClient.parameterToString(accessToken));
if (contentEncoding != null)
      localVarHeaderParams.put("content-encoding", apiClient.parameterToString(contentEncoding));

    
    final String[] localVarAccepts = {
      "image/jpeg", "application/json", "application/pdf"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      "application/json"
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<BoletoResponse> localVarReturnType = new GenericType<BoletoResponse>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
}
