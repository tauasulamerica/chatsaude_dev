package io.swagger.client.sulamerica.pagamentosssaude.api;

import io.swagger.client.sulamerica.pagamentosssaude.ApiException;
import io.swagger.client.sulamerica.pagamentosssaude.ApiClient;
import io.swagger.client.sulamerica.pagamentosssaude.Configuration;
import io.swagger.client.sulamerica.pagamentosssaude.Pair;

import javax.ws.rs.core.GenericType;

import io.swagger.client.sulamerica.pagamentosssaude.model.BoletoResponse;
import io.swagger.client.sulamerica.pagamentosssaude.model.Error;
import io.swagger.client.sulamerica.pagamentosssaude.model.ParcelaMassificados;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-03-20T19:25:42.811-03:00")
public class MassificadosApi {
  private ApiClient apiClient;

  public MassificadosApi() {
    this(Configuration.getDefaultApiClient());
  }

  public MassificadosApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * 
   * Retorna as parcelas referentes a uma apólice ou endosso do segurado.
   * @param clientId Identificador do Cliente utilizado na autenticação. (required)
   * @param accessToken Token de acesso utilizado na autenticação. (required)
   * @param origem Código do consumidor. Aceita os valores MB(Mobile), EC(Espaço Cliente), PC(Portal do Corretor), CH(Chat), PI(Portal Institucional), CX(Parceira Caixa Seguradora),  CM(CCM Center/Inspire) e UR(Ura). (required)
   * @param codigoCompanhia Código da Companhia (required)
   * @param codigoSucursal Código da sucursal. (required)
   * @param codigoProduto Código do produto. (required)
   * @param numeroApolice Número da apólice do segurado.&lt;br/&gt;&lt;b&gt;É necessário informar este atributo caso não seja informado o Endosso.&lt;/b&gt; (optional)
   * @param numeroEndosso Número do endosso relativo à apólice do segurado.&lt;br/&gt;&lt;b&gt;É necessário informar este atributo caso não seja informada a Apólice.&lt;/b&gt; (optional)
   * @param cpfCnpj Número do CPF ou CNPJ (optional)
   * @param numeroContrato Número do Contrato (optional)
   * @return List&lt;ParcelaMassificados&gt;
   * @throws ApiException if fails to make API call
   */
  public List<ParcelaMassificados> getParcelasMassificados(String clientId, String accessToken, String origem, String codigoCompanhia, String codigoSucursal, String codigoProduto, String numeroApolice, String numeroEndosso, String cpfCnpj, String numeroContrato) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'clientId' is set
    if (clientId == null) {
      throw new ApiException(400, "Missing the required parameter 'clientId' when calling getParcelasMassificados");
    }
    
    // verify the required parameter 'accessToken' is set
    if (accessToken == null) {
      throw new ApiException(400, "Missing the required parameter 'accessToken' when calling getParcelasMassificados");
    }
    
    // verify the required parameter 'origem' is set
    if (origem == null) {
      throw new ApiException(400, "Missing the required parameter 'origem' when calling getParcelasMassificados");
    }
    
    // verify the required parameter 'codigoCompanhia' is set
    if (codigoCompanhia == null) {
      throw new ApiException(400, "Missing the required parameter 'codigoCompanhia' when calling getParcelasMassificados");
    }
    
    // verify the required parameter 'codigoSucursal' is set
    if (codigoSucursal == null) {
      throw new ApiException(400, "Missing the required parameter 'codigoSucursal' when calling getParcelasMassificados");
    }
    
    // verify the required parameter 'codigoProduto' is set
    if (codigoProduto == null) {
      throw new ApiException(400, "Missing the required parameter 'codigoProduto' when calling getParcelasMassificados");
    }
    
    // create path and map variables
    String localVarPath = "/parcelas/massificados";

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();

    localVarQueryParams.addAll(apiClient.parameterToPairs("", "origem", origem));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "codigoCompanhia", codigoCompanhia));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "codigoSucursal", codigoSucursal));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "codigoProduto", codigoProduto));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "numeroApolice", numeroApolice));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "numeroEndosso", numeroEndosso));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "cpfCnpj", cpfCnpj));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "numeroContrato", numeroContrato));

    if (clientId != null)
      localVarHeaderParams.put("client_id", apiClient.parameterToString(clientId));
if (accessToken != null)
      localVarHeaderParams.put("access_token", apiClient.parameterToString(accessToken));

    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      "application/json"
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<List<ParcelaMassificados>> localVarReturnType = new GenericType<List<ParcelaMassificados>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * 
   * Retorna a segunda via de um boleto do canal de Massificados
   * @param clientId Identificador do Cliente utilizado na autenticação. (required)
   * @param accessToken Token de acesso utilizado na autenticação. (required)
   * @param tipoRetorno Tipo de Retorno. Aceita os valores (1 - Retorna a segunda via de um boleto em PDF(application/pdf); 2 - Retorna a segunda via de um boleto em JPG (image/png); 3 -Retorna a linha digitável da segunda via de um boleto referente a uma parcela do segurado gerada (application/json). Deve estar compativel com o header Accept. (required)
   * @param origem Código do consumidor. Aceita os valores MB(Mobile), EC(Espaço Cliente), PC(Portal do Corretor), CH(Chat), PI(Portal Institucional), CX(Parceira Caixa Seguradora),  CM(CCM Center/Inspire) e UR(Ura). (required)
   * @param codigoCompanhia Código da companhia. (required)
   * @param codigoSucursal Código da sucursal. (required)
   * @param contentEncoding Informa o encoding em que se deseja criptografar o response. (Valido para Boletos em PDF tipoRetorno &#x3D;1) (Valor Valido - BASE64). (optional)
   * @param tipoRequisicao Tipo da requisição.&lt;br/&gt;Aceita os valores PME, GRP(Grupal) ou IND(Individual). &lt;b&gt; Obrigatório quando o  sistema origem for INS  &lt;/b&gt; (optional)
   * @param sistemaOrigem Sigla do sistema origem (APM, SIR ou INS). Valor Default: APM (optional, default to APM)
   * @param numeroParcela Número do endosso relativo à apólice do segurado. &lt;b&gt; Obrigatório quando o  sistema origem for APM ou SIR.  &lt;/b&gt; (optional)
   * @param codigoProduto Código do produto.  &lt;b&gt; Obrigatório quando o  sistema origem for APM ou SIR.  &lt;/b&gt; (optional)
   * @param numeroApolice Número da apólice do segurado.&lt;br/&gt;&lt;b&gt;É necessário informar este atributo caso não seja informado o Endosso.&lt;/b&gt;  &lt;b&gt; Obrigatório quando o  sistema origem for INS  &lt;/b&gt; (optional)
   * @param numeroEndosso Número do endosso relativo à apólice do segurado.&lt;br/&gt;&lt;b&gt;É necessário informar este atributo caso não seja informada a Apólice.&lt;/b&gt; (optional)
   * @param dataVencimento Data do vencimento da parcela  (DD/MM/AAAA).&lt;br/&gt;&lt;b&gt;É necessário informar este atributo em casos de boleto vencido.&lt;/b&gt;  &lt;b&gt; Obrigatório quando o  sistema origem for INS &lt;/b&gt; (optional)
   * @param codigoLancamentoFinanceiro Código do Lançamento Financeiro. (optional)
   * @param codigoRamo Código do Ramo &lt;b&gt; Obrigatório quando o  sistema origem for INS &lt;/b&gt; (optional)
   * @return BoletoResponse
   * @throws ApiException if fails to make API call
   */
  public BoletoResponse postBoletosSegundaviaMassificados(String clientId, String accessToken, String tipoRetorno, String origem, String codigoCompanhia, String codigoSucursal, String contentEncoding, String tipoRequisicao, String sistemaOrigem, String numeroParcela, String codigoProduto, String numeroApolice, String numeroEndosso, String dataVencimento, String codigoLancamentoFinanceiro, String codigoRamo) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'clientId' is set
    if (clientId == null) {
      throw new ApiException(400, "Missing the required parameter 'clientId' when calling postBoletosSegundaviaMassificados");
    }
    
    // verify the required parameter 'accessToken' is set
    if (accessToken == null) {
      throw new ApiException(400, "Missing the required parameter 'accessToken' when calling postBoletosSegundaviaMassificados");
    }
    
    // verify the required parameter 'tipoRetorno' is set
    if (tipoRetorno == null) {
      throw new ApiException(400, "Missing the required parameter 'tipoRetorno' when calling postBoletosSegundaviaMassificados");
    }
    
    // verify the required parameter 'origem' is set
    if (origem == null) {
      throw new ApiException(400, "Missing the required parameter 'origem' when calling postBoletosSegundaviaMassificados");
    }
    
    // verify the required parameter 'codigoCompanhia' is set
    if (codigoCompanhia == null) {
      throw new ApiException(400, "Missing the required parameter 'codigoCompanhia' when calling postBoletosSegundaviaMassificados");
    }
    
    // verify the required parameter 'codigoSucursal' is set
    if (codigoSucursal == null) {
      throw new ApiException(400, "Missing the required parameter 'codigoSucursal' when calling postBoletosSegundaviaMassificados");
    }
    
    // create path and map variables
    String localVarPath = "/boletos/massificados/segundaVia";

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();

    localVarQueryParams.addAll(apiClient.parameterToPairs("", "tipoRetorno", tipoRetorno));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "origem", origem));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "tipoRequisicao", tipoRequisicao));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "sistemaOrigem", sistemaOrigem));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "numeroParcela", numeroParcela));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "codigoCompanhia", codigoCompanhia));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "codigoSucursal", codigoSucursal));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "codigoProduto", codigoProduto));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "numeroApolice", numeroApolice));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "numeroEndosso", numeroEndosso));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "dataVencimento", dataVencimento));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "codigoLancamentoFinanceiro", codigoLancamentoFinanceiro));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "codigoRamo", codigoRamo));

    if (clientId != null)
      localVarHeaderParams.put("client_id", apiClient.parameterToString(clientId));
if (accessToken != null)
      localVarHeaderParams.put("access_token", apiClient.parameterToString(accessToken));
if (contentEncoding != null)
      localVarHeaderParams.put("content-encoding", apiClient.parameterToString(contentEncoding));

    
    final String[] localVarAccepts = {
      "image/png", "application/json", "application/pdf"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      "application/json"
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<BoletoResponse> localVarReturnType = new GenericType<BoletoResponse>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
}
