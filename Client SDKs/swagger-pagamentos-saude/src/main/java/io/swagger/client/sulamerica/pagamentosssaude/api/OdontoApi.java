package io.swagger.client.sulamerica.pagamentosssaude.api;

import io.swagger.client.sulamerica.pagamentosssaude.ApiException;
import io.swagger.client.sulamerica.pagamentosssaude.ApiClient;
import io.swagger.client.sulamerica.pagamentosssaude.Configuration;
import io.swagger.client.sulamerica.pagamentosssaude.Pair;

import javax.ws.rs.core.GenericType;

import io.swagger.client.sulamerica.pagamentosssaude.model.DocumentoOdonto;
import io.swagger.client.sulamerica.pagamentosssaude.model.Error;
import io.swagger.client.sulamerica.pagamentosssaude.model.ParcelasOdonto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-03-20T19:25:42.811-03:00")
public class OdontoApi {
  private ApiClient apiClient;

  public OdontoApi() {
    this(Configuration.getDefaultApiClient());
  }

  public OdontoApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * 
   * Retorna a segunda via do boleto de odonto.
   * @param clientId Identificador do Cliente utilizado na autenticação. (required)
   * @param accessToken Token de acesso utilizado na autenticação. (required)
   * @param tipoRetorno Tipo de Retorno. Aceita os valores (1 - Retorna a segunda via de um boleto em PDF(application/pdf);  (required)
   * @param modalidade Modalidade que será gerado o boleto. (required)
   * @param codigoLancamentoFinanceiro Código do Lançamento Financeiro. (required)
   * @throws ApiException if fails to make API call
   */
  public void getBoletoOdonto(String clientId, String accessToken, String tipoRetorno, String modalidade, String codigoLancamentoFinanceiro) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'clientId' is set
    if (clientId == null) {
      throw new ApiException(400, "Missing the required parameter 'clientId' when calling getBoletoOdonto");
    }
    
    // verify the required parameter 'accessToken' is set
    if (accessToken == null) {
      throw new ApiException(400, "Missing the required parameter 'accessToken' when calling getBoletoOdonto");
    }
    
    // verify the required parameter 'tipoRetorno' is set
    if (tipoRetorno == null) {
      throw new ApiException(400, "Missing the required parameter 'tipoRetorno' when calling getBoletoOdonto");
    }
    
    // verify the required parameter 'modalidade' is set
    if (modalidade == null) {
      throw new ApiException(400, "Missing the required parameter 'modalidade' when calling getBoletoOdonto");
    }
    
    // verify the required parameter 'codigoLancamentoFinanceiro' is set
    if (codigoLancamentoFinanceiro == null) {
      throw new ApiException(400, "Missing the required parameter 'codigoLancamentoFinanceiro' when calling getBoletoOdonto");
    }
    
    // create path and map variables
    String localVarPath = "/boletos/odonto/segundaVia";

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();

    localVarQueryParams.addAll(apiClient.parameterToPairs("", "tipoRetorno", tipoRetorno));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "modalidade", modalidade));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "codigoLancamentoFinanceiro", codigoLancamentoFinanceiro));

    if (clientId != null)
      localVarHeaderParams.put("client_id", apiClient.parameterToString(clientId));
if (accessToken != null)
      localVarHeaderParams.put("access_token", apiClient.parameterToString(accessToken));

    
    final String[] localVarAccepts = {
      "application/pdf"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };


    apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, null);
  }
  /**
   * 
   * Retorna lista de contratos de Odonto
   * @param clientId Identificador do Cliente utilizado na autenticação. (required)
   * @param accessToken Token de acesso utilizado na autenticação. (required)
   * @param origem Origem da chamada (required)
   * @param situacaoParcela Situação das parcelas dos documentos que deseja listar. (required)
   * @param cpnj CNPJ do cliente - Não deve ser informado em conjunto com CPF (optional)
   * @param cpf CPF do cliente - Não deve ser informado em conjunto com CNPJ (optional)
   * @param numeroContrato Numero do contrato (optional)
   * @param codigoBeneficiario Codigo do beneficiario / numero da carteirinha (optional)
   * @param codigoEstruturaVendas Códigos das Estruturas de vendas. Separados por ,. ex: 12,34,44,12 (optional)
   * @return List&lt;DocumentoOdonto&gt;
   * @throws ApiException if fails to make API call
   */
  public List<DocumentoOdonto> postCobrancadocumentoOdonto(String clientId, String accessToken, String origem, String situacaoParcela, String cpnj, String cpf, String numeroContrato, String codigoBeneficiario, String codigoEstruturaVendas) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'clientId' is set
    if (clientId == null) {
      throw new ApiException(400, "Missing the required parameter 'clientId' when calling postCobrancadocumentoOdonto");
    }
    
    // verify the required parameter 'accessToken' is set
    if (accessToken == null) {
      throw new ApiException(400, "Missing the required parameter 'accessToken' when calling postCobrancadocumentoOdonto");
    }
    
    // verify the required parameter 'origem' is set
    if (origem == null) {
      throw new ApiException(400, "Missing the required parameter 'origem' when calling postCobrancadocumentoOdonto");
    }
    
    // verify the required parameter 'situacaoParcela' is set
    if (situacaoParcela == null) {
      throw new ApiException(400, "Missing the required parameter 'situacaoParcela' when calling postCobrancadocumentoOdonto");
    }
    
    // create path and map variables
    String localVarPath = "/documentos/odonto";

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();

    localVarQueryParams.addAll(apiClient.parameterToPairs("", "origem", origem));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "cpnj", cpnj));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "cpf", cpf));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "numeroContrato", numeroContrato));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "codigoBeneficiario", codigoBeneficiario));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "situacaoParcela", situacaoParcela));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "codigoEstruturaVendas", codigoEstruturaVendas));

    if (clientId != null)
      localVarHeaderParams.put("client_id", apiClient.parameterToString(clientId));
if (accessToken != null)
      localVarHeaderParams.put("access_token", apiClient.parameterToString(accessToken));

    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<List<DocumentoOdonto>> localVarReturnType = new GenericType<List<DocumentoOdonto>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * 
   * Retorna lista de Parcelas de Odonto
   * @param clientId Identificador do Cliente utilizado na autenticação. (required)
   * @param accessToken Token de acesso utilizado na autenticação. (required)
   * @param origem Origem da Requisição (required)
   * @param numeroContrato Número do Contrato (required)
   * @param numeroContratoComplemento Número do Contrato Complemento (required)
   * @param ano Ano no formato yyyy - Exemplo 2017 - se preenchido com 0(zero) retorna todos os anos (required)
   * @param mes Ano no formato mm - Exemplo 09 - se preenchido com 0(zero) retorna todos os meses (required)
   * @param codigoEstruturaVendas Códigos das Estruturas de vendas. Separados por ,. ex: 12,34,44,12 (optional)
   * @return List&lt;ParcelasOdonto&gt;
   * @throws ApiException if fails to make API call
   */
  public List<ParcelasOdonto> postCobrancapagamentoOdonto(String clientId, String accessToken, String origem, String numeroContrato, String numeroContratoComplemento, String ano, String mes, String codigoEstruturaVendas) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'clientId' is set
    if (clientId == null) {
      throw new ApiException(400, "Missing the required parameter 'clientId' when calling postCobrancapagamentoOdonto");
    }
    
    // verify the required parameter 'accessToken' is set
    if (accessToken == null) {
      throw new ApiException(400, "Missing the required parameter 'accessToken' when calling postCobrancapagamentoOdonto");
    }
    
    // verify the required parameter 'origem' is set
    if (origem == null) {
      throw new ApiException(400, "Missing the required parameter 'origem' when calling postCobrancapagamentoOdonto");
    }
    
    // verify the required parameter 'numeroContrato' is set
    if (numeroContrato == null) {
      throw new ApiException(400, "Missing the required parameter 'numeroContrato' when calling postCobrancapagamentoOdonto");
    }
    
    // verify the required parameter 'numeroContratoComplemento' is set
    if (numeroContratoComplemento == null) {
      throw new ApiException(400, "Missing the required parameter 'numeroContratoComplemento' when calling postCobrancapagamentoOdonto");
    }
    
    // verify the required parameter 'ano' is set
    if (ano == null) {
      throw new ApiException(400, "Missing the required parameter 'ano' when calling postCobrancapagamentoOdonto");
    }
    
    // verify the required parameter 'mes' is set
    if (mes == null) {
      throw new ApiException(400, "Missing the required parameter 'mes' when calling postCobrancapagamentoOdonto");
    }
    
    // create path and map variables
    String localVarPath = "/parcelas/odonto";

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();

    localVarQueryParams.addAll(apiClient.parameterToPairs("", "origem", origem));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "numeroContrato", numeroContrato));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "numeroContratoComplemento", numeroContratoComplemento));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "ano", ano));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "mes", mes));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "codigoEstruturaVendas", codigoEstruturaVendas));

    if (clientId != null)
      localVarHeaderParams.put("client_id", apiClient.parameterToString(clientId));
if (accessToken != null)
      localVarHeaderParams.put("access_token", apiClient.parameterToString(accessToken));

    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<List<ParcelasOdonto>> localVarReturnType = new GenericType<List<ParcelasOdonto>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
}
