package io.swagger.client.sulamerica.pagamentosssaude.api;

import io.swagger.client.sulamerica.pagamentosssaude.ApiException;
import io.swagger.client.sulamerica.pagamentosssaude.ApiClient;
import io.swagger.client.sulamerica.pagamentosssaude.Configuration;
import io.swagger.client.sulamerica.pagamentosssaude.Pair;

import javax.ws.rs.core.GenericType;

import io.swagger.client.sulamerica.pagamentosssaude.model.BoletoResponse;
import io.swagger.client.sulamerica.pagamentosssaude.model.Error;
import io.swagger.client.sulamerica.pagamentosssaude.model.ParcelaVida;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-03-20T19:25:42.811-03:00")
public class VidaApi {
  private ApiClient apiClient;

  public VidaApi() {
    this(Configuration.getDefaultApiClient());
  }

  public VidaApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * 
   * Retorna a carta de inadimplência do beneficiario do canal de  Vida.
   * @param clientId Identificador do Cliente utilizado na autenticação. (required)
   * @param accessToken Token de acesso utilizado na autenticação. (required)
   * @param cpf CPF do Beneficiário (required)
   * @param numeroProposta Número da Proposta (required)
   * @param codigoPlano Código do Plano. (required)
   * @throws ApiException if fails to make API call
   */
  public void getCartaInadinplentesVida(String clientId, String accessToken, String cpf, String numeroProposta, String codigoPlano) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'clientId' is set
    if (clientId == null) {
      throw new ApiException(400, "Missing the required parameter 'clientId' when calling getCartaInadinplentesVida");
    }
    
    // verify the required parameter 'accessToken' is set
    if (accessToken == null) {
      throw new ApiException(400, "Missing the required parameter 'accessToken' when calling getCartaInadinplentesVida");
    }
    
    // verify the required parameter 'cpf' is set
    if (cpf == null) {
      throw new ApiException(400, "Missing the required parameter 'cpf' when calling getCartaInadinplentesVida");
    }
    
    // verify the required parameter 'numeroProposta' is set
    if (numeroProposta == null) {
      throw new ApiException(400, "Missing the required parameter 'numeroProposta' when calling getCartaInadinplentesVida");
    }
    
    // verify the required parameter 'codigoPlano' is set
    if (codigoPlano == null) {
      throw new ApiException(400, "Missing the required parameter 'codigoPlano' when calling getCartaInadinplentesVida");
    }
    
    // create path and map variables
    String localVarPath = "/vida/inadimplentes/cartas";

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();

    localVarQueryParams.addAll(apiClient.parameterToPairs("", "cpf", cpf));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "numeroProposta", numeroProposta));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "codigoPlano", codigoPlano));

    if (clientId != null)
      localVarHeaderParams.put("client_id", apiClient.parameterToString(clientId));
if (accessToken != null)
      localVarHeaderParams.put("access_token", apiClient.parameterToString(accessToken));

    
    final String[] localVarAccepts = {
      "application/pdf"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      "application/json"
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };


    apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, null);
  }
  /**
   * 
   * Retorna as parcelas referentes a uma apólice ou endosso do segurado do canal Vida.
   * @param clientId Identificador do Cliente utilizado na autenticação. (required)
   * @param accessToken Token de acesso utilizado na autenticação. (required)
   * @param sistemaOrigem Sigla do sistema origem (SUV ou CVP) (required)
   * @param origem Código do consumidor. Aceita os valores MB(Mobile), EC(Espaço Cliente), PC(Portal do Corretor), CH(Chat), PI(Portal Institucional), CX(Parceira Caixa Seguradora),  CM(CCM Center/Inspire) e UR(Ura). (required)
   * @param codigoCompanhia Código da Companhia (required)
   * @param codigoSucursal Código da sucursal. (required)
   * @param codigoRamo Código do Ramo. (required)
   * @param codigoProdutor Código do Produtor (optional)
   * @param numeroApolice Número da apólice do segurado.&lt;br/&gt;&lt;b&gt;É necessário informar este atributo caso não seja informado o Endosso.&lt;/b&gt; (optional)
   * @param numeroEndosso Número do endosso relativo à apólice do segurado.&lt;br/&gt;&lt;b&gt;É necessário informar este atributo caso não seja informada a Apólice.&lt;/b&gt; (optional)
   * @param numeroCertificado Número do Certifcado. &lt;b&gt; Obrigatório para Pessoa Física&lt;/b&gt; (optional)
   * @param numeroGrupoFaturamento Número do Grupo de Faturamento. &lt;b&gt; Obrigatório para Pessoa Juridica&lt;/b&gt; (optional)
   * @param codigoEstruturaVendas Códigos das Estruturas de vendas. Separados por ,. ex: 12,34,44,12 (optional)
   * @param anoParcelas Ano das parcelas que serão listadas. (Formato: AAAA) (optional)
   * @return List&lt;ParcelaVida&gt;
   * @throws ApiException if fails to make API call
   */
  public List<ParcelaVida> getParcelasVida(String clientId, String accessToken, String sistemaOrigem, String origem, String codigoCompanhia, String codigoSucursal, String codigoRamo, String codigoProdutor, String numeroApolice, String numeroEndosso, String numeroCertificado, String numeroGrupoFaturamento, String codigoEstruturaVendas, String anoParcelas) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'clientId' is set
    if (clientId == null) {
      throw new ApiException(400, "Missing the required parameter 'clientId' when calling getParcelasVida");
    }
    
    // verify the required parameter 'accessToken' is set
    if (accessToken == null) {
      throw new ApiException(400, "Missing the required parameter 'accessToken' when calling getParcelasVida");
    }
    
    // verify the required parameter 'sistemaOrigem' is set
    if (sistemaOrigem == null) {
      throw new ApiException(400, "Missing the required parameter 'sistemaOrigem' when calling getParcelasVida");
    }
    
    // verify the required parameter 'origem' is set
    if (origem == null) {
      throw new ApiException(400, "Missing the required parameter 'origem' when calling getParcelasVida");
    }
    
    // verify the required parameter 'codigoCompanhia' is set
    if (codigoCompanhia == null) {
      throw new ApiException(400, "Missing the required parameter 'codigoCompanhia' when calling getParcelasVida");
    }
    
    // verify the required parameter 'codigoSucursal' is set
    if (codigoSucursal == null) {
      throw new ApiException(400, "Missing the required parameter 'codigoSucursal' when calling getParcelasVida");
    }
    
    // verify the required parameter 'codigoRamo' is set
    if (codigoRamo == null) {
      throw new ApiException(400, "Missing the required parameter 'codigoRamo' when calling getParcelasVida");
    }
    
    // create path and map variables
    String localVarPath = "/parcelas/vida";

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();

    localVarQueryParams.addAll(apiClient.parameterToPairs("", "sistemaOrigem", sistemaOrigem));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "origem", origem));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "codigoCompanhia", codigoCompanhia));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "codigoSucursal", codigoSucursal));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "codigoRamo", codigoRamo));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "codigoProdutor", codigoProdutor));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "numeroApolice", numeroApolice));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "numeroEndosso", numeroEndosso));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "numeroCertificado", numeroCertificado));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "numeroGrupoFaturamento", numeroGrupoFaturamento));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "codigoEstruturaVendas", codigoEstruturaVendas));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "anoParcelas", anoParcelas));

    if (clientId != null)
      localVarHeaderParams.put("client_id", apiClient.parameterToString(clientId));
if (accessToken != null)
      localVarHeaderParams.put("access_token", apiClient.parameterToString(accessToken));

    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      "application/json"
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<List<ParcelaVida>> localVarReturnType = new GenericType<List<ParcelaVida>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * 
   * Retorna a segunda via de um boleto do canal de Vida
   * @param clientId Identificador do Cliente utilizado na autenticação. (required)
   * @param accessToken Token de acesso utilizado na autenticação. (required)
   * @param tipoRetorno Tipo de Retorno. Aceita os valores (1 - Retorna a segunda via de um boleto em PDF(application/pdf); 2 - Retorna a segunda via de um boleto em JPG (image/png); 3 -Retorna a linha digitável da segunda via de um boleto referente a uma parcela do segurado gerada (application/json). Deve estar compativel com o header Accept. (required)
   * @param sistemaOrigem Sigla do sistema origem (INS, SUV, CVP ou CSA) (required)
   * @param contentEncoding Informa o encoding em que se deseja criptografar o response. (Valido para Boletos em PDF tipoRetorno &#x3D;1) (Valor Valido - BASE64). (optional)
   * @param tipoPessoa Informa o tipo de Pessoa que está solicitando a segunda via do boleto. PF para pessoa física e PJ para pessoa jurídica.  &lt;b&gt; Obrigatório quando sistemaOrigem for SUV, CVP ou CSA&lt;/b&gt; (optional)
   * @param numeroParcela Número do endosso relativo à apólice do segurado. &lt;b&gt; Obrigatório quando sistemaOrigem for SUV, CVP ou CSA&lt;/b&gt; (optional)
   * @param codigoCompanhia Código da companhia.  &lt;b&gt; Obrigatório quando sistemaOrigem for SUV, CVP ou CSA&lt;/b&gt; (optional)
   * @param codigoSucursal Código da sucursal.  &lt;b&gt; Obrigatório quando sistemaOrigem for SUV, CVP ou CSA&lt;/b&gt; (optional)
   * @param codigoRamo Código do Ramo  &lt;b&gt; Obrigatório quando sistemaOrigem for SUV, CVP ou CSA&lt;/b&gt; (optional)
   * @param numeroApolice Número da apólice do segurado.  &lt;b&gt; Obrigatório quando sistemaOrigem for SUV, CVP ou CSA&lt;/b&gt; (optional)
   * @param numeroEndosso Número do endosso relativo à apólice do segurado. (optional)
   * @param dataVencimento Data do vencimento da parcela.  (DD/MM/AAAA) (optional)
   * @param codigoEstruturaVendas Códigos das Estruturas de vendas. Separados por ,. ex: 12,34,44,12 (optional)
   * @param numeroGrupoFaturamento Numero do Grupo Faturamento para &lt;b&gt;pessoa juridica&lt;/b&gt; (optional)
   * @param numeroSequenciaFatura Numero de Sequencia da  &lt;b&gt;pessoa juridica&lt;/b&gt; (optional)
   * @param numeroCertificado Numero de Certificado para &lt;b&gt;pessoa fisica&lt;/b&gt; (optional)
   * @param numeroCVP Número CVP do Boleto  &lt;b&gt; Obrigatório quando sistemaOrigem for INS&lt;/b&gt; (optional)
   * @param nossoNumero Nosso Número  &lt;b&gt; Obrigatório quando sistemaOrigem for INS&lt;/b&gt; (optional)
   * @return BoletoResponse
   * @throws ApiException if fails to make API call
   */
  public BoletoResponse postBoletosSegundaviaVida(String clientId, String accessToken, String tipoRetorno, String sistemaOrigem, String contentEncoding, String tipoPessoa, String numeroParcela, String codigoCompanhia, String codigoSucursal, String codigoRamo, String numeroApolice, String numeroEndosso, String dataVencimento, String codigoEstruturaVendas, String numeroGrupoFaturamento, String numeroSequenciaFatura, String numeroCertificado, String numeroCVP, String nossoNumero) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'clientId' is set
    if (clientId == null) {
      throw new ApiException(400, "Missing the required parameter 'clientId' when calling postBoletosSegundaviaVida");
    }
    
    // verify the required parameter 'accessToken' is set
    if (accessToken == null) {
      throw new ApiException(400, "Missing the required parameter 'accessToken' when calling postBoletosSegundaviaVida");
    }
    
    // verify the required parameter 'tipoRetorno' is set
    if (tipoRetorno == null) {
      throw new ApiException(400, "Missing the required parameter 'tipoRetorno' when calling postBoletosSegundaviaVida");
    }
    
    // verify the required parameter 'sistemaOrigem' is set
    if (sistemaOrigem == null) {
      throw new ApiException(400, "Missing the required parameter 'sistemaOrigem' when calling postBoletosSegundaviaVida");
    }
    
    // create path and map variables
    String localVarPath = "/boletos/vida/segundaVia";

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();

    localVarQueryParams.addAll(apiClient.parameterToPairs("", "tipoRetorno", tipoRetorno));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "tipoPessoa", tipoPessoa));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "sistemaOrigem", sistemaOrigem));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "numeroParcela", numeroParcela));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "codigoCompanhia", codigoCompanhia));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "codigoSucursal", codigoSucursal));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "codigoRamo", codigoRamo));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "numeroApolice", numeroApolice));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "numeroEndosso", numeroEndosso));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "dataVencimento", dataVencimento));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "codigoEstruturaVendas", codigoEstruturaVendas));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "numeroGrupoFaturamento", numeroGrupoFaturamento));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "numeroSequenciaFatura", numeroSequenciaFatura));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "numeroCertificado", numeroCertificado));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "numeroCVP", numeroCVP));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "nossoNumero", nossoNumero));

    if (clientId != null)
      localVarHeaderParams.put("client_id", apiClient.parameterToString(clientId));
if (accessToken != null)
      localVarHeaderParams.put("access_token", apiClient.parameterToString(accessToken));
if (contentEncoding != null)
      localVarHeaderParams.put("content-encoding", apiClient.parameterToString(contentEncoding));

    
    final String[] localVarAccepts = {
      "image/png", "application/json", "application/pdf"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      "application/json"
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<BoletoResponse> localVarReturnType = new GenericType<BoletoResponse>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
}
