package io.swagger.client.sulamerica.pagamentosssaude.api;

import io.swagger.client.sulamerica.pagamentosssaude.ApiException;
import io.swagger.client.sulamerica.pagamentosssaude.ApiClient;
import io.swagger.client.sulamerica.pagamentosssaude.Configuration;
import io.swagger.client.sulamerica.pagamentosssaude.Pair;

import javax.ws.rs.core.GenericType;

import io.swagger.client.sulamerica.pagamentosssaude.model.BoletoResponse;
import io.swagger.client.sulamerica.pagamentosssaude.model.Error;
import io.swagger.client.sulamerica.pagamentosssaude.model.Parcela;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-03-20T19:25:42.811-03:00")
public class AutoApi {
  private ApiClient apiClient;

  public AutoApi() {
    this(Configuration.getDefaultApiClient());
  }

  public AutoApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * 
   * Retorna as parcelas referentes a uma apólice ou endosso do segurado.
   * @param clientId Identificador do Cliente utilizado na autenticação. (required)
   * @param accessToken Token de acesso utilizado na autenticação. (required)
   * @param codigoSucursal Código da sucursal. (required)
   * @param origem Código do consumidor. Aceita os valores MB(Mobile), EC(Espaço Cliente), PC(Portal do Corretor), CH(Chat), PI(Portal Institucional), CX(Parceira Caixa Seguradora),  CM(CCM Center/Inspire) e UR(Ura). (required)
   * @param numeroApolice Número da apólice do segurado.&lt;br/&gt;&lt;b&gt;É necessário informar este atributo caso não seja informado o Endosso.&lt;/b&gt; (optional)
   * @param numeroEndosso Número do endosso relativo à apólice do segurado.&lt;br/&gt;&lt;b&gt;É necessário informar este atributo caso não seja informada a Apólice.&lt;/b&gt; (optional)
   * @param cpfCnpj Número do CPF ou CNPJ do segurado. (optional)
   * @param codigoProdutor Código do produtor.&lt;br/&gt;&lt;b&gt;É necessário informar este atributo em casos de seguro FROTA.&lt;/b&gt; (optional)
   * @param numeroContrato Número do contrato.&lt;br/&gt;&lt;b&gt;É necessário informar este atributo em casos de seguro FROTA.&lt;/b&gt; (optional)
   * @return List&lt;Parcela&gt;
   * @throws ApiException if fails to make API call
   */
  public List<Parcela> getParcelas(String clientId, String accessToken, String codigoSucursal, String origem, String numeroApolice, String numeroEndosso, String cpfCnpj, String codigoProdutor, String numeroContrato) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'clientId' is set
    if (clientId == null) {
      throw new ApiException(400, "Missing the required parameter 'clientId' when calling getParcelas");
    }
    
    // verify the required parameter 'accessToken' is set
    if (accessToken == null) {
      throw new ApiException(400, "Missing the required parameter 'accessToken' when calling getParcelas");
    }
    
    // verify the required parameter 'codigoSucursal' is set
    if (codigoSucursal == null) {
      throw new ApiException(400, "Missing the required parameter 'codigoSucursal' when calling getParcelas");
    }
    
    // verify the required parameter 'origem' is set
    if (origem == null) {
      throw new ApiException(400, "Missing the required parameter 'origem' when calling getParcelas");
    }
    
    // create path and map variables
    String localVarPath = "/parcelas/auto";

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();

    localVarQueryParams.addAll(apiClient.parameterToPairs("", "codigoSucursal", codigoSucursal));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "numeroApolice", numeroApolice));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "numeroEndosso", numeroEndosso));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "cpfCnpj", cpfCnpj));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "codigoProdutor", codigoProdutor));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "numeroContrato", numeroContrato));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "origem", origem));

    if (clientId != null)
      localVarHeaderParams.put("client_id", apiClient.parameterToString(clientId));
if (accessToken != null)
      localVarHeaderParams.put("access_token", apiClient.parameterToString(accessToken));

    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      "application/json"
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<List<Parcela>> localVarReturnType = new GenericType<List<Parcela>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * 
   * Retorna a segunda via de um boleto do canal de Auto 
   * @param clientId Identificador do Cliente utilizado na autenticação. (required)
   * @param accessToken Token de acesso utilizado na autenticação. (required)
   * @param accept Tipo de mídia que o cliente aceita de retorno. Exemplo, caso o cliente esteja aguardando um PDF, enviar application/pdf  (required)
   * @param tipoRetorno Tipo de Retorno. Aceita os valores (1 - Retorna a segunda via de um boleto em PDF(application/pdf); 2 - Retorna a segunda via de um boleto em JPG (image/png); 3 -Retorna a linha digitável da segunda via de um boleto referente a uma parcela do segurado gerada (application/json). Deve estar compativel com o header Accept. (required)
   * @param origem Código do consumidor. Aceita os valores MB(Mobile), EC(Espaço Cliente), PC(Portal do Corretor), CH(Chat), PI(Portal Institucional), CX(Parceira Caixa Seguradora),  CM(CCM Center/Inspire) e UR(Ura). (required)
   * @param vencido Define se o boleto é vencido. (required)
   * @param numeroParcela Número do endosso relativo à apólice do segurado. (required)
   * @param codigoSucursal Código da sucursal. (required)
   * @param numeroApolice Número da apólice do segurado.&lt;br/&gt;&lt;b&gt;É necessário informar este atributo caso não seja informado o Endosso.&lt;/b&gt; (optional)
   * @param numeroEndosso Número do endosso relativo à apólice do segurado.&lt;br/&gt;&lt;b&gt;É necessário informar este atributo caso não seja informada a Apólice.&lt;/b&gt; (optional)
   * @param cpfCnpj Número do CPF ou CNPJ do segurado. (optional)
   * @param dataVencimento Data do vencimento da parcela. (DD/MM/AAAA)&lt;br/&gt;&lt;b&gt;É necessário informar este atributo em casos de boleto vencido.&lt;/b&gt; (optional)
   * @return BoletoResponse
   * @throws ApiException if fails to make API call
   */
  public BoletoResponse postBoletosSegundavia(String clientId, String accessToken, String accept, String tipoRetorno, String origem, Boolean vencido, String numeroParcela, String codigoSucursal, String numeroApolice, String numeroEndosso, String cpfCnpj, String dataVencimento) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'clientId' is set
    if (clientId == null) {
      throw new ApiException(400, "Missing the required parameter 'clientId' when calling postBoletosSegundavia");
    }
    
    // verify the required parameter 'accessToken' is set
    if (accessToken == null) {
      throw new ApiException(400, "Missing the required parameter 'accessToken' when calling postBoletosSegundavia");
    }
    
    // verify the required parameter 'accept' is set
    if (accept == null) {
      throw new ApiException(400, "Missing the required parameter 'accept' when calling postBoletosSegundavia");
    }
    
    // verify the required parameter 'tipoRetorno' is set
    if (tipoRetorno == null) {
      throw new ApiException(400, "Missing the required parameter 'tipoRetorno' when calling postBoletosSegundavia");
    }
    
    // verify the required parameter 'origem' is set
    if (origem == null) {
      throw new ApiException(400, "Missing the required parameter 'origem' when calling postBoletosSegundavia");
    }
    
    // verify the required parameter 'vencido' is set
    if (vencido == null) {
      throw new ApiException(400, "Missing the required parameter 'vencido' when calling postBoletosSegundavia");
    }
    
    // verify the required parameter 'numeroParcela' is set
    if (numeroParcela == null) {
      throw new ApiException(400, "Missing the required parameter 'numeroParcela' when calling postBoletosSegundavia");
    }
    
    // verify the required parameter 'codigoSucursal' is set
    if (codigoSucursal == null) {
      throw new ApiException(400, "Missing the required parameter 'codigoSucursal' when calling postBoletosSegundavia");
    }
    
    // create path and map variables
    String localVarPath = "/boletos/auto/segundaVia";

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();

    localVarQueryParams.addAll(apiClient.parameterToPairs("", "tipoRetorno", tipoRetorno));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "origem", origem));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "vencido", vencido));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "numeroParcela", numeroParcela));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "codigoSucursal", codigoSucursal));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "numeroApolice", numeroApolice));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "numeroEndosso", numeroEndosso));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "cpfCnpj", cpfCnpj));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "dataVencimento", dataVencimento));

    if (clientId != null)
      localVarHeaderParams.put("client_id", apiClient.parameterToString(clientId));
if (accessToken != null)
      localVarHeaderParams.put("access_token", apiClient.parameterToString(accessToken));
if (accept != null)
      localVarHeaderParams.put("Accept", apiClient.parameterToString(accept));

    
    final String[] localVarAccepts = {
      "image/png", "application/json", "application/pdf"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      "application/json"
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<BoletoResponse> localVarReturnType = new GenericType<BoletoResponse>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
}
