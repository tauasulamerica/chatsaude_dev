package io.swagger.client.sensedia.watson.api;

import javax.ws.rs.core.GenericType;

import io.swagger.client.sensedia.watson.ApiClient;
import io.swagger.client.sensedia.watson.ApiException;
import io.swagger.client.sensedia.watson.Configuration;
import io.swagger.client.sensedia.watson.Pair;
import io.swagger.client.sensedia.watson.model.AccessToken;
import io.swagger.client.sensedia.watson.model.Error;
import io.swagger.client.sensedia.watson.model.ErrorAuthorization;
import io.swagger.client.sensedia.watson.model.ErrorServer;
import io.swagger.client.sensedia.watson.model.RequestAccessToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2017-05-03T14:42:59.834-03:00")
public class AccessTokenApi {
  private ApiClient apiClient;

  public AccessTokenApi() {
    this(Configuration.getDefaultApiClient());
  }

  public AccessTokenApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * 
   * Gerar o access-token.
   * @param authorization Código de Autorização. (required)
   * @param input Body de autenticação (required)
   * @return AccessToken
   * @throws ApiException if fails to make API call
   */
  public AccessToken oauthAccessTokenPost(String authorization, RequestAccessToken input) throws ApiException {
    Object localVarPostBody = input;
    
    // verify the required parameter 'authorization' is set
    if (authorization == null) {
      throw new ApiException(400, "Missing the required parameter 'authorization' when calling oauthAccessTokenPost");
    }
    
    // verify the required parameter 'input' is set
    if (input == null) {
      throw new ApiException(400, "Missing the required parameter 'input' when calling oauthAccessTokenPost");
    }
    
    // create path and map variables
    String localVarPath = "/oauth/access-token".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    if (authorization != null)
      localVarHeaderParams.put("Authorization", apiClient.parameterToString(authorization));

    
    final String[] localVarAccepts = {
      
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<AccessToken> localVarReturnType = new GenericType<AccessToken>() {};
    return apiClient.invokeAPI(localVarPath, "POST", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
}
