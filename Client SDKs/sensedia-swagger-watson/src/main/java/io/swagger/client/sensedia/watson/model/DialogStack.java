/**
 * API Chat do Corretor Auto - Watson Conversation
 * Esta API tem como objetivo disponibilizar e gerenciar a API de Conversation do IBM Watson.
 *
 * OpenAPI spec version: 1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package io.swagger.client.sensedia.watson.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.api.server.spi.config.ApiResourceProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


/**
 * 
 */
@ApiModel(description = "")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2017-05-03T14:42:59.834-03:00")
public class DialogStack   {
  @JsonProperty("dialog_node")
  @ApiResourceProperty(name = "dialog_node")
  private String dialogNode = null;

  public DialogStack dialogNode(String dialogNode) {
    this.dialogNode = dialogNode;
    return this;
  }

   /**
   * Identificador do nó de diálogo
   * @return dialogNode
  **/
  @ApiModelProperty(example = "null", value = "Identificador do nó de diálogo")
  public String getDialogNode() {
    return dialogNode;
  }

  public void setDialogNode(String dialogNode) {
    this.dialogNode = dialogNode;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DialogStack dialogStack = (DialogStack) o;
    return Objects.equals(this.dialogNode, dialogStack.dialogNode);
  }

  @Override
  public int hashCode() {
    return Objects.hash(dialogNode);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DialogStack {\n");
    
    sb.append("    dialogNode: ").append(toIndentedString(dialogNode)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

