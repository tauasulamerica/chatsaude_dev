package io.swagger.client.sensedia.watson.api;

import javax.ws.rs.core.GenericType;

import io.swagger.client.sensedia.watson.ApiClient;
import io.swagger.client.sensedia.watson.ApiException;
import io.swagger.client.sensedia.watson.Configuration;
import io.swagger.client.sensedia.watson.Pair;
import io.swagger.client.sensedia.watson.model.Error;
import io.swagger.client.sensedia.watson.model.MessageRequest;
import io.swagger.client.sensedia.watson.model.MessageResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2017-05-03T14:42:59.834-03:00")
public class ConversaApi {
  private ApiClient apiClient;

  public ConversaApi() {
    this(Configuration.getDefaultApiClient());
  }

  public ConversaApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * Realiza processo de conversação com Watson
   * Realiza processo de conversação com Watson
   * @param clientId Identificador do Cliente utilizado na autenticação. (required)
   * @param accessToken Token de acesso utilizado na autenticação. (required)
   * @param xGatewayProcessId Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão. (optional)
   * @param xGatewayRequestId Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador.   (optional)
   * @param body Dados de entrada do usuário, com intenções opcionais, entidades e outras propriedades da resposta. No inicio da iteração com o Watson o único objeto necessário é o input.  Para dar Continuidade a conversa, é necessário sempre informar o objeto context retornando no response anterior. (optional)
   * @return MessageResponse
   * @throws ApiException if fails to make API call
   */
  public MessageResponse conversationPost(String clientId, String accessToken, String xGatewayProcessId, String xGatewayRequestId, MessageRequest body) throws ApiException {
    Object localVarPostBody = body;
    
    // verify the required parameter 'clientId' is set
    if (clientId == null) {
      throw new ApiException(400, "Missing the required parameter 'clientId' when calling conversationPost");
    }
    
    // verify the required parameter 'accessToken' is set
    if (accessToken == null) {
      throw new ApiException(400, "Missing the required parameter 'accessToken' when calling conversationPost");
    }
    
    // create path and map variables
    String localVarPath = "/conversation".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    if (clientId != null)
      localVarHeaderParams.put("client_id", apiClient.parameterToString(clientId));
if (accessToken != null)
      localVarHeaderParams.put("access_token", apiClient.parameterToString(accessToken));
if (xGatewayProcessId != null)
      localVarHeaderParams.put("x-gateway-process-id", apiClient.parameterToString(xGatewayProcessId));
if (xGatewayRequestId != null)
      localVarHeaderParams.put("x-gateway-request-id", apiClient.parameterToString(xGatewayRequestId));

    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      "application/json"
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<MessageResponse> localVarReturnType = new GenericType<MessageResponse>() {};
    return apiClient.invokeAPI(localVarPath, "POST", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
}
