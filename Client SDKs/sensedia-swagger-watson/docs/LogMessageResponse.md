
# LogMessageResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**level** | [**LevelEnum**](#LevelEnum) | Gravidade da mensagem |  [optional]
**msg** | **String** | Mensagem |  [optional]


<a name="LevelEnum"></a>
## Enum: LevelEnum
Name | Value
---- | -----
INFO | &quot;info&quot;
ERROR | &quot;error&quot;
WARN | &quot;warn&quot;



