# AccessTokenApi

All URIs are relative to *https://apisulamerica.sensedia.com/auto/corretor/chat/bot/watson/api/v1/dev*

Method | HTTP request | Description
------------- | ------------- | -------------
[**oauthAccessTokenPost**](AccessTokenApi.md#oauthAccessTokenPost) | **POST** /oauth/access-token | 


<a name="oauthAccessTokenPost"></a>
# **oauthAccessTokenPost**
> AccessToken oauthAccessTokenPost(authorization, input)



Gerar o access-token.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.AccessTokenApi;


AccessTokenApi apiInstance = new AccessTokenApi();
String authorization = "authorization_example"; // String | Código de Autorização.
RequestAccessToken input = new RequestAccessToken(); // RequestAccessToken | Body de autenticação
try {
    AccessToken result = apiInstance.oauthAccessTokenPost(authorization, input);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AccessTokenApi#oauthAccessTokenPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Código de Autorização. |
 **input** | [**RequestAccessToken**](RequestAccessToken.md)| Body de autenticação |

### Return type

[**AccessToken**](AccessToken.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

