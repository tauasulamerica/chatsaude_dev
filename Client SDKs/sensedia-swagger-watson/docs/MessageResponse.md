
# MessageResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**input** | [**InputData**](InputData.md) |  | 
**context** | [**Context**](Context.md) |  | 
**entities** | [**List&lt;EntityResponse&gt;**](EntityResponse.md) | Termos da solicitação que são identificados como entidades. | 
**intents** | [**List&lt;Intent&gt;**](Intent.md) | Termos da solicitação que são identificados como intenções. | 
**output** | [**OutputData**](OutputData.md) |  | 



