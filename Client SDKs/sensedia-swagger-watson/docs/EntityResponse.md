
# EntityResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**entity** | **String** | Entidade reconhecida de um termo na entrada. |  [optional]
**location** | **List&lt;Integer&gt;** | Deslocamentos de caracteres baseados em zero que indicam onde o valor da entidade começa e termina no texto de entrada. |  [optional]
**value** | **String** | O termo no texto de entrada que foi reconhecido. |  [optional]



