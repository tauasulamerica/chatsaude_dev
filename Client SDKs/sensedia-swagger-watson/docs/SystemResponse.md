
# SystemResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dialogStack** | [**List&lt;DialogStack&gt;**](DialogStack.md) | Lista de Identificadores de diálogo que estão em foco na conversação. | 
**dialogTurnCounter** | **Integer** | Número de ciclos de entradas e respostas do usuário nesta conversa. | 
**dialogRequestCounter** | **Integer** | O número de entradas nesta conversa. Esse contador pode ser maior que o contador &lt;tt&gt; dialog_turn_counter &lt;/ tt&gt; quando várias entradas são necessárias antes que uma resposta possa ser retornada. | 



