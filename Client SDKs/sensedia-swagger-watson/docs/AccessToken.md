
# AccessToken

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accessToken** | **String** | Código acces_token. |  [optional]
**tokenType** | **String** | Tipo do Token. |  [optional]
**expiresIn** | [**BigDecimal**](BigDecimal.md) | Tempo de expiração do Token, em s. |  [optional]



