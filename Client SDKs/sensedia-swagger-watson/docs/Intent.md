
# Intent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**intent** | **String** | Nome da intenção reconhecida. |  [optional]
**confidence** | **Double** | A porcentagem  que representa a confiança que o Watson tem nessa intenção. |  [optional]



