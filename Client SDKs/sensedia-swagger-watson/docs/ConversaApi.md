# ConversaApi

All URIs are relative to *https://apisulamerica.sensedia.com/auto/corretor/chat/bot/watson/api/v1/dev*

Method | HTTP request | Description
------------- | ------------- | -------------
[**conversationPost**](ConversaApi.md#conversationPost) | **POST** /conversation | Realiza processo de conversação com Watson


<a name="conversationPost"></a>
# **conversationPost**
> MessageResponse conversationPost(clientId, accessToken, xGatewayProcessId, xGatewayRequestId, body)

Realiza processo de conversação com Watson

Realiza processo de conversação com Watson

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.ConversaApi;


ConversaApi apiInstance = new ConversaApi();
String clientId = "clientId_example"; // String | Identificador do Cliente utilizado na autenticação.
String accessToken = "accessToken_example"; // String | Token de acesso utilizado na autenticação.
String xGatewayProcessId = "xGatewayProcessId_example"; // String | Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão.
String xGatewayRequestId = "xGatewayRequestId_example"; // String | Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador.  
MessageRequest body = new MessageRequest(); // MessageRequest | Dados de entrada do usuário, com intenções opcionais, entidades e outras propriedades da resposta. No inicio da iteração com o Watson o único objeto necessário é o input.  Para dar Continuidade a conversa, é necessário sempre informar o objeto context retornando no response anterior.
try {
    MessageResponse result = apiInstance.conversationPost(clientId, accessToken, xGatewayProcessId, xGatewayRequestId, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ConversaApi#conversationPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**| Identificador do Cliente utilizado na autenticação. |
 **accessToken** | **String**| Token de acesso utilizado na autenticação. |
 **xGatewayProcessId** | **String**| Código único que identifica uma sessão de chat. Deve ser gerado pelo componente orquestrador na primeira iteração do usuário e deve ser repassado e retornado para todas as chamadas de APIs dentro dessa sessão. | [optional]
 **xGatewayRequestId** | **String**| Código único que identifica uma requisição de chat, deve ser gerado antes da chamada do orquestrador pelo cliente e repassado para todas as requisições realizadas pelo orquestrador.   | [optional]
 **body** | [**MessageRequest**](MessageRequest.md)| Dados de entrada do usuário, com intenções opcionais, entidades e outras propriedades da resposta. No inicio da iteração com o Watson o único objeto necessário é o input.  Para dar Continuidade a conversa, é necessário sempre informar o objeto context retornando no response anterior. | [optional]

### Return type

[**MessageResponse**](MessageResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

