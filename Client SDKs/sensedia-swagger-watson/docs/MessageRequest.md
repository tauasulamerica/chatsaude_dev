
# MessageRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**input** | [**InputData**](InputData.md) |  | 
**alternateIntents** | **Boolean** | Informa se deve retornar mais de uma intenção. (true - para retornar todas as intenções correspondentes) |  [optional]
**context** | [**Context**](Context.md) |  |  [optional]
**entities** | [**List&lt;EntityResponse&gt;**](EntityResponse.md) | Entidades da resposta anterior quando elas não precisam mudar. (Para evitar que Watson tente identificá-las). |  [optional]
**intents** | [**List&lt;Intent&gt;**](Intent.md) | Intenções  da resposta anterior quando elas não precisam mudar. (Para evitar que Watson tente identificá-las). |  [optional]
**output** | [**OutputData**](OutputData.md) |  |  [optional]



