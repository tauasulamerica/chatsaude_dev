
# Context

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**conversationId** | **String** | Identificador da Conversa. |  [optional]
**system** | [**SystemResponse**](SystemResponse.md) |  |  [optional]



