
# OutputData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**logMessages** | [**List&lt;LogMessageResponse&gt;**](LogMessageResponse.md) | Mensagens registradas com a solicitação. | 
**text** | **List&lt;String&gt;** | Lista de respostas enviadas ao usuário. | 
**nodesVisited** | **List&lt;String&gt;** | Lista de nós que foram executados para gerar a resposta. |  [optional]



